
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie-edge">
</head>
<link rel="stylesheet" type="text/css" href="styles.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body style="background-image: url(upload/backfoto.jpg);">
    <ul class="setting">
        <li ><a  href="Homepage.php?tid=". 0> <i class="fa fa-home"> Back Home</i></a></li>
    </ul>
    <?php
require_once('Connection.php');
include ('content_function.php');
    session_start();

    if(!isset($_SESSION['User']))
    {
        
        header("location:index.php");
    }            

?>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 m-auto">
                <div class="card bg-dark mt-5">
                    <div class="card-title bg-primary text-white mt-5">
                        <h3 class="text-center py-3">Change User Name!</h3>
                    </div>
                    <?php

                 
                        if(@$_GET['Empty']==true)
                        {
                    ?>
                        <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Empty'] ?></div>                                
                    <?php
                        }
                    ?>
 
 
                    <?php 
                        if(@$_GET['Invalid']==true)
                        {
                    ?>
                        <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Invalid'] ?></div>                                
                    <?php
                        }
                    ?>
                    
                    <div class="card-body ">
                        <form action="<?php echo test_input ("Name_processing.php")?>" method="post">
                        
                            <input type="text" name="New_Name" placeholder=" Enter New Name" maxlength = '35' class="form-control mb-3">
                            <i style="position: absolute; left:0; padding: 15px 30px; " class="fa fa-lock fa-lg f-fw" aria-hidden="true" ></i>
                            <input type="password" name="Password" maxlength = '35' placeholder="  Enter your Password" class="form-control mb-3">
                            <button class="button1" class="btn btn-success mt-3" name="change">Save Change</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
                
</body>
</html>