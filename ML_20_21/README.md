# Bachelor Project Machine Learning

This project serves as a template for every group's git-repository. It will be updated during the semester to include new tasks and sample solutions for existing tasks.

**Important:** Please read the information in this README file carefully before starting with the project.

## Getting Started
Please read the documentation on how to [get started](doc/getting_started.md) before you attempt to solve any of the exercises.

## How to Submit Your Solutions
Make sure to read the documentation on [how to submit](doc/submission.md) for detailed instructions on how you should submit your work.

## Passing the Project
Please see the [FAQ](doc/faq.md) section for information on what we expect from you to pass this project.

## Contact
You can always contact us via the [Mattermost Chat](https://ml-chat.cs.uni-kl.de) if you have any questions, remarks or problems. If your question is related to the content of the exercises or formalities, please consider posting it to the `Project ML (BSc)` channel so that we can answer it for everyone. In case you have a question that you don't want everyone else to read, you can also DM us.

### Whom to contact
Questions regarding the exercise feedback:
* Till Werner: [t_werner17@cs.uni-kl.de](mailto:t_werner17@cs.uni-kl.de), `@till_werner` on Mattermost

Everything else:
* Tobias Michels: [t_michels15@cs.uni-kl.de](mailto:t_michels15@cs.uni-kl.de), `@tmichels` on Mattermost

# Prerequisites for task 2
To execute the files in src/task2 the following libraries are needed
### pandas
### numpy
### sklearn
### matplotlib
### nltk
### seaborn
