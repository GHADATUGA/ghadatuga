\documentclass[12pt]{article}
\input{code/header}
\addbibresource{references.bib}
\setlength{\abovecaptionskip}{0pt}
\newcommand{\tab}{\hspace{1cm}}
\title{Machine Learning (Project)\\Task 2 Report}
\author{Mohamed Ghazal \\ m\_ghazal19@cs.uni-kl.de\and
	Nico Müller\\ muelleni@rhrk.uni-kl.de\and
	Ghad Niyomwungeri\\
	niyomwun@rhrk.uni-kl.de}
\date{\today}

\begin{document}
\maketitle
\section{Introduction}
In contrast to the previous task, we now consider the case where our label space Y is
no longer finite. Instead we assume that $\mathcal{Y} = \mathbb{R}$ and we call such a problem regression instead of classification. Other than that we still want to find a function $\hat{f}$ such that $\hat{f}(x) \approx y.$\\
Perhaps a bit surprisingly, problems of this kind are often easier to solve. Assuming that
the function is to be linear, i.e., $\mathcal{X}=\mathbb{R}$ and $\hat{f}(x)=\mathbf{w}^{T}\mathbf{x}$ for some $w\in\mathbb{R}$.\\
If, however, $n > d$, this direct approach won’t yield a solution and therefore we need to change our approach. For example, we could try to find a w that minimises the squared difference between the true labels and our function output while at the same time having a small norm:
\begin{equation}\label{eq:1}
	\mathbf{w}^{*} = \underset{\mathbf{w}\in\mathbb{R}^{d}}{\text{arg min}} \parallel \mathbf{w}\parallel^{2} + C\sum_{i=1}^{n}(\mathbf{w}^{T} \mathbf{x}_{i} - y_{i})^{2},
\end{equation}
where $C > 0$ is called \textit{regularization constant} and controls the amount of regularisation that is applied to $\mathbf{w}$. This method is called ridge regression and its solution can be found by solving another linear system $(\mathbf{X}^{T} \mathbf{X} + \cfrac{1}{C}\ I_{d}) = \mathbf{X}^{T}\mathbf{y}$ instead, which is guaranteed to have a unique solution.
\section{Data Preprocessing}
\paragraph{}
In theory, you can now throw ridge regression at a problem and solve it, but, as usual, it is not quite that easy in practice. Note that we assumed $\mathcal{X} = \mathbb{R}^{d}$, i.e., inputs are available as real-valued vectors, but often this is not the case. So essentially, we are looking for a function $t: \mathcal{X} \rightarrow \mathbb{R}^{d}$ that \textit{transforms} other types of data into a format usable by ML methods such as ridge regression.\\
For example, in the last sheet, we had images as our input for the classification task. Fortunately, it was easy to convert an image into a real-valued vector: given the image as an $3\times h\times w$ array of pixel values from $\{0,1,...,255\}$, we simply divide each value by 255 and reshape the array into a 3hw-dimensional vector. In fact, this transformation appears to be natural enough for us that we don’t really put any further thought into it. But at the same time, you would probably agree that plugging the binary representation of a jpeg-encoded image directly into an ML algorithm would not make much sense.\\
\paragraph{}
First after taking a close look over the provided data, we found out that a lot of infos about some wineries were missing and NaN. This prompted us to look deeper into the matter to understand how best we can prepare this set of data to be used in the training of a new model.\\
In the project sheet we first had to create a new column year, where we stored the vintage of each wine. These values had to be extracted from the title column, but with some tight logic and further understanding of the provided data. We had to take into account that the year must be a four digit number and we had to exclude senseless values that are greater than 2020 and less than 1900.\\
\tab To remedy the situation of missing values we tried to understand the problem at hand and how we can replace NaN values with the most suitable values possible...\\
In order to work efficiently with the provided \texttt{.csv} file, we used the library \textit{pandas}, which provides a swath of methods and classes to work with \texttt{.csv} files. This includes creating a \texttt{DataFrame} object that contains the data of the \texttt{.csv} file and simplified the process of adding columns and accessing them for example.  In the following code snippet is an example of such operation, where we add a new column to a created \texttt{DataFrame} object through manipulation of another column.\\
 \begin{lstlisting}[language=IPython]
df = dt.assign(year = lambda x: extract_years(x['title']))
 \end{lstlisting}
 \paragraph{}
As mentioned above a further exploration of the data was necessary to understand how we could best prepare the data for the training of a model. To achieve this goal we had to find ways to plot the data and make histograms of the distribution of vales in each column of the \texttt{DataFrame}. For this we used the library \texttt{seaborn}, which is statistical data visualisation tool best suited for our purposes. In the code snippet bellow a basic usage of the library to plot the count of each value in the column \texttt{'taster\_name'} with \texttt{seaborn} imported as \texttt{sns}. 
 \begin{lstlisting}[language=IPython]
 sns.countplot(y='taster\_name', data=data, order=data['taster\_name'].value_counts().index)
 \end{lstlisting}
 In the next figure we see the result of the execution of the previous code snippet.
 \begin{figure}[htbp]
\begin{center}
	\includegraphics[width=\textwidth]{figures/count_taster_name.png}
	    \caption{}
\label{fig:1}
\end{center}
\end{figure}
This was a direct usage of the \texttt{seaborn} library.
\paragraph{}
Now let's plot the distribution of each value in the dataset.
\begin{enumerate}
\item The plot of the price distribution.
 \begin{figure}[H]
\begin{center}
	\includegraphics[width=\textwidth]{figures/desity_price.png}
	    \caption{}
\label{fig:1}
\end{center}
\end{figure}
To fill up missing data in the column \texttt{price} we filled NA values with the mean of all known prices so we don't lose wines that have other infos. This is achieved by executing the following command.
 \begin{lstlisting}[language=IPython]
df['price'] = df['price'].fillna(df['price'].mean())
 \end{lstlisting}
\item Plotting the count of points in the dataset
\begin{figure}[htbp]
\begin{center}
	\includegraphics[width=\textwidth]{figures/count_points.png}
	    \caption{}
\label{fig:2}
\end{center}
\end{figure}
As we can see here the most frequently given points are 88, 87 and 90.

\newpage
\item Plotting the count of years, that were extracted from the title of the wines
\begin{figure}[htbp]
\begin{center}
	\includegraphics[width=\textwidth]{figures/hist_years.png}
	    \caption{}
\label{fig:3}
\end{center}
\end{figure}
\end{enumerate}

\paragraph{}
\textbf{Missing Values} \newline \newline
As you can see in the image below, some columns had missing values. Columns like '\texttt{region\_2}' and 'designation' had the most missing values. It is important not to throw away these data points, because they make up too much of the data.
The missing values in the 'price' column were replaced with the mean, while the other categorical columns are imputed by the most frequently occurring values.
The columns 'country' and 'province' contain only a small amount of missing values, so you can drop the missing rows here
\begin{figure}
\begin{center}
	\includegraphics[width=\textwidth]{figures/distributionMissingVal.png}
\end{center}
	    \caption{}
\label{fig:2}
\end{figure}

\paragraph{}
\textbf{Data cleaning} \newline \newline
After that, we want to make sure that every description is unique, so we drop
the duplicates. Additionally we set all words to lowercase and ensure that the variety name doesn't occur in the description, so we add variety name to our list of stop words to remove it from the description. In the next step we used the '\texttt{ntlk.corpus}' library '\texttt{stopwords}' and our list to remove the stop words from 'description'.

 \begin{lstlisting}[language=IPython]
# Remove stop words in description
extras = ['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', 'cab',"%", "that's"]
stopwords = set(stopwords.words('english'))
stopwords.update(variety_list)
stopwords.update(extras)
df["description"] = df["description"].apply(lambda x: x.lower())    
df["description"] = df["description"].apply(lambda x: ' '.join([word for word in x.split() if word not in (stopwords)]))
 \end{lstlisting}
 
\paragraph{}
\textbf{Modelling and preparation of data for Regression} \newline \newline
The idea behind this task is to predict the rating of a wine, i.e., the points column, from the rest of each data point. So we need to consider, which column is important for the prediction. Here we have chosen the columns 'description', 'designation', 'price', 'province', '\texttt{region\_1}', '\texttt{taster\_name}', 'variety', 'winery' and 'year'. \newline
But first we do our imports for this task:
\begin{lstlisting}[language=IPython]
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.decomposition import PCA
\end{lstlisting}
We use \texttt{TfidfVectorizor} to process the wine descriptions to get \texttt{Tfidf} features.
Our only numerical values are from the price column, the rest are one hot vectors, and the \texttt{Tfidf} features as mentioned for the wine descriptions. \newline
After the transformation of our selected columns, we stack the matrices we want to add to Ridge Regression into an \texttt{hstack}:
\begin{lstlisting}[language=IPython]
X = np.hstack((descrip, provinces, varieties, price[:,None]))
\end{lstlisting}
In this example the \texttt{Tfidf} features of 'descripction', 'province', 'variety' and price.
After the cleaning of the dataset we habe made a statistic of numerical values, and the result is in the figure \ref{fig:numstat}
\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{figures/numerical_stats.png}
\caption{A statistic of numerical values}
\label{fig:numstat}
\end{center}
\end{figure}

\paragraph{When we have to predict a new data point, we do the following}
In order to handle a new datapoint, we can follow the same process done in previous data frame.  That means, we extract year (Vintage) from the title column, make a scale of column with numerical values, extra features according to important words taken from description before we delete description.  In the transformation process we have deleted the records with NaN values.  So, the new entered data point should not contain NaN values otherwise will be delete or we impute the missing values with the mean or frequently occurred values of already recorded wine.  Also, we create dummies for column with categorical data before we delete them.  Then the result will be attached to our current data frame. \\
Once we think of a better solution, we would use  machine learning pipeline, which automate the workflow by enabling data to be transformed and correlated into a model that can then be analyzed to achieve output. Through using this ML pipeline, the process of inputting data into the model will be fully automated.

\paragraph{Problems involving the transformation we used on nominal data}
However, the transformation involves a problem. When we are supposed to predict a new datapoint, we 
need to re-train the whole model because of TfIdf. This is very bad. A consideration to solve the problem 
would be to perform TfIdf on the training set seperately and then use the Idf-vector from the training transformation
to calculate the TfIdf vectors of the test set.

\section{Regression}
Since we now have a function that transforms a data point to a real-valued vector, we can finally attempt to apply ridge regression on the wine reviews dataset. In fact, our ultimate goal will be to predict the rating of a wine, i.e., the points column, from the rest of each data point. 
Now we want to investigate the relationship between columns in our dataset. More specifically, we are interested in the “points” column. Therefore, we executed the following steps for each of the other columns after it has been transformed into a vector of real numbers:
\begin{enumerate}
    \item  If the column is represented by a d-dimensional vector with $d > 1$, you’ll need to come up with a projection $p: \mathbb{R}^{d} \rightarrow \mathbb{R}$ first. To achieve this we used PCA as the  sophisticated dimensionality reduction method, which we imported from the library \texttt{sklearn}.
    \item We drew a scatter plot that shows the projected value of the column, i.e., $p(v)$, on the x-axis and the corresponding value of the points column on the y-axis.
    \item We used the scatter plot from 2. and plotted the the produced linear function on it as shown in the fig. \cref{fig:5}
    \begin{figure}[H]
        \centering
        \includegraphics[width=\textwidth]{figures/plot_point.png}
        \caption{}
        \label{fig:5}
    \end{figure}
\end{enumerate}
The same process was done to all columns of the input data. And these are the produced plots for each one of them:
\begin{figure}[H]
\centering
\begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/plot_varieties.png}
            \caption{}
        \label{subfig:1}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/plot_taster_name.png}
        \caption{}
    \label{subfig:2}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/plot_prices.png}
    \caption{}
    \label{subfig:3}
\end{subfigure}
 \caption{}
    \label{fig:6}
\end{figure}
\nocite{*}
The current version of our class \texttt{RidgeRegression} will always produce a line or, more generally, a hyperplane that contains the point $0\in\mathbb{R}^{d+1}$, since the point $x=0\in\mathbb{R}^d$ is always mapped to $y=0$. To allow the model the learning of lines and hyperplanes the doesn't necessarily contain the point $0\in\mathbb{R}^{d+1}$, a learneable bias is included to the output to which 0 will be mapped. Based on that, we can formulate a new version of ridge regression that includes the bias term:
\begin{equation*}
\mathbf{w}^{*}, b^{*} = \underset{b\in\mathbb{R},\mathbf{w}\in\mathbb{R}^{d}}{\text{arg min}} \parallel \mathbf{w}\parallel^{2} +\ b^{2}+ C\sum_{i=1}^{n}(\mathbf{w}^{T} \mathbf{x}_{i} + b - y_{i})^{2},
\end{equation*}
which solution can be found by solving another linear system $(\mathbf{X}^{T} \mathbf{X} + \cfrac{1}{C}\ I_{d}) = \mathbf{X}^{T}\mathbf{y} - \mathbf{X}^{T}b$.\\
Now the valuation of the produced model will be needed to check its performance. To do this we used MSE to estimate the error rate of the produced model. The implementation of the MSE error calculated is as follows:
 \begin{lstlisting}[language=IPython]
np.square(np.subtract(y_test, predictions)).mean()
 \end{lstlisting}
 In our case the MSE of our model was $5.41$ as seen the output of the the excecutin of \texttt{regression.py}.
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/MSE_model.png}
        \caption{}
    \label{fig:9}
\end{figure}
In addition to calculating the MSE error Cross-validation is also a method to evaluate a machine learning model, but in contrast to the MSE the learning method itself is evaluated. This is achieved by multisampling the input data and partitioning it in several partitions and each time using one as a test data and the rest as training data.\\
We will be using 5-fold cross validation, which partitioned the dataset into 5 partitions.
This can be achieved with the following procedure:
\begin{enumerate}
    \item Shuffle the dataset, which can be done using the \textit{numpy} library with the command:
\begin{lstlisting}[language=IPython]
np.random.shuffle(X)
np.random.shuffle(y)
\end{lstlisting}
    \item Split the dataset into k groups, which can be done with 
\begin{lstlisting}[language=IPython]
X_{}groups = np.vsplit(X, n)
y_{}groups = np.vsplit(y, n)
\end{lstlisting}
    \item For each unique group:\\
Take the group as a hold out or test data set
    \item Take the remaining groups as a training data set.
    \item Fit a model on the training set and evaluate it on the test set, then retain the evaluation score and discard the model
\begin{lstlisting}[language=IPython]
X_test = X_groups[i]
y_test = y_groups[i]
X_training = np.vstack([group for j, group in enumerate(X_groups) if j != i])
y_training = np.vstack([group for j, group in enumerate(y_groups) if j != i])
model = RidgeRegressionBias() if with_bias else RidgeRegression()
model.fit(X_training, y_training)
predictions = model.predict(X_test)
result = np.square(y_test - predictions).mean()
\end{lstlisting}
    \item Summarize the skill of the model using the calculated MSE error used by the model
\end{enumerate}
Lastly we have noticed that each component of W is forced to be small in magnitude by the regularisation term. If the corresponding input feature varies only marginally compared to the other features, it can never contribute much to the final prediction, even if it would be important for the problem at hand. On the other hand, if the corresponding feature takes values in a rather big interval, its effect on the final prediction will be disproportionately large. \\
This Effect can be mitigated by using a cost function that can adapt to how much each component of the W varies by using a penalty term, that regularises the coefficients such that if the coefficients take large values the optimisation function is penalised. This can be implemented as
\begin{lstlisting}[language=IPython]
for i in range(X.shape[1]):
	variance = np.var(X[:, i])
	C_I[i, i] = 1 / variance if variance else 10
\end{lstlisting}
\newpage
\nocite{*}
\printbibliography
\end{document}