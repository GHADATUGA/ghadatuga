import math
import random

import gym
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.autograd import Variable


def interactive_cart_pole():
    """
    Allows you to control the cart with the arrow keys.
    Press Space to reset the cart-pole and press Escape to close the window.
    """
    discount_factor = 0.98
    num_episodes = 10000
    min_learning_rate = 0.1
    min_epsilon = 0.1
    decay = 25
    bins = (1, 1, 2, 20)  # number of bins --buckets
    env = gym.make('CartPole-v1')

    # [position, velocity, angle, angular velocity]
    upper_bounds = [env.observation_space.high[0], 0.5, env.observation_space.high[2], math.radians(20) / 1.]
    lower_bounds = [env.observation_space.low[0], -0.5, env.observation_space.low[2], -math.radians(20) / 1.]
    sarsa_table = np.zeros(bins + (env.action_space.n,))

    # get max epsilon
    def get_eps(t):
        return max(min_epsilon, min(1., 1. - math.log10((t + 1) / decay)))

    # select  action

    def performPolicy(state):
        rand_value = np.random.random()
        if rand_value < epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(sarsa_table[state])
        return action

    def update_sarsa(state, action, reward, new_state, new_action):
        sarsa_table[state][action] += learning_rate * (reward + discount_factor * \
                                                       (sarsa_table[new_state][new_action]) - sarsa_table[state][
                                                           action])

    def get_learning_rate(t):
        return max(min(1., 1. - math.log10((t + 1) / decay)), min_learning_rate)

    def state_discretizion(observations):
        discretized_list = list()
        for i in range(len(observations)):
            scaling = (observations[i] + abs(lower_bounds[i])) / (upper_bounds[i] - lower_bounds[i])
            new_observation = int(round((bins[i] - 1) * scaling))
            new_observation = min(bins[i] - 1, max(0, new_observation))
            discretized_list.append(new_observation)
        return tuple(discretized_list)
    
     def plot_res(values, title='Discretization Sarsa'):
        ''' Plot the reward curve and histogram of results over time.'''

        # Define the figure
        f, ax = plt.subplots(figsize=(12, 5))
        f.suptitle(title)
        ax.plot(values, label='score per run')
        ax.axhline(195, c='red', ls='--', label='goal')
        ax.set_xlabel('Episodes')
        ax.set_ylabel('Reward')
        ax.legend()
        plt.show()
        

    # def train_and_run(self):
    episode_rewards = []
    for ep in range(num_episodes):
        current_state = state_discretizion(env.reset())

        learning_rate = get_learning_rate(ep)
        epsilon = get_eps(ep)
        done = False
        rewards = []
        while not done:

            action = performPolicy(current_state)
            obs, reward, done, _ = env.step(action)
            rewards.append(reward)
            new_state = state_discretizion(obs)
            new_action = performPolicy(new_state)
            update_sarsa(current_state, action, reward, new_state, new_action)
            current_state = new_state
            env.render()

            # Exploration / Explotation Tradeoff
            # as we learn more about the game, become more certain in making decision -- so
            # if ep == 100: min_epsilon /= 2

            episode_rewards.append(np.sum(rewards))
            mean_reward = np.mean(episode_rewards[max(ep - 100, 0):ep + 1])
            #  did we solve the task already? --check.          
            if mean_reward >= 195.0 and ep >= 100:
                print("Episodes before solve {}".format(ep - 100 + 1))
                break
            if ((ep % 100) == 0) and ep > 0:
                print("Episode {}/{} finished. Mean reward over last 100 episodes: {:.2f}" \
                      .format(ep, num_episodes, (mean_reward)))

    env.close()
    plot_res(episode_rewards, 'Discretization Sarsa')


class Deep_Sarsa():
    """ Class for Deep Sarsa """

    def __init__(self, input_dim, output_dim, hidden_dim, lr):
        # Setting up criterion that measures the mean squared error
        self.criterion = torch.nn.MSELoss()
        # Setting up Layers for my network (2 hidden layers)
        self.model = torch.nn.Sequential(
            torch.nn.Linear(input_dim, hidden_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(hidden_dim, hidden_dim * 2),
            torch.nn.LeakyReLU(),
            torch.nn.Linear(hidden_dim * 2, output_dim)
        )
        # Use Adam for optimization
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr)

    def update(self, state, sample):
        """Update the weights of the network given a training sample. """
        sample_pred = self.model(torch.Tensor(state))
        loss = self.criterion(sample_pred, Variable(torch.Tensor(sample)))
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        # Backpropagation and parameter-update

    def predict(self, state):
        """ Compute Q values for all actions using the DQL. """
        with torch.no_grad():
            return self.model(torch.Tensor(state))

    def replay(self, memory, size, gamma=0.9, epsilon=0.1):
        """New replay function"""
        # Try to improve replay speed
        if len(memory) >= size:
            batch = random.sample(memory, size)
            batch_t = list(map(list, zip(*batch)))  # Transpose batch list
            states = batch_t[0]
            actions = batch_t[1]
            next_states = batch_t[2]
            rewards = batch_t[3]
            is_dones = batch_t[4]

            states = torch.Tensor(states)
            actions_tensor = torch.Tensor(actions)
            next_states = torch.Tensor(next_states)
            rewards = torch.Tensor(rewards)
            is_dones_tensor = torch.Tensor(is_dones)

            is_dones_indices = torch.where(is_dones_tensor == True)[0]

            all_q_values = self.model(states)  # predicted q_values of all states
            all_q_values_next = self.model(next_states)

            act = np.array([0, 1])
            rand_action = np.random.choice(act)
            # Update q values
            # perform policy for updating q_values
            if random.random() < epsilon:
                all_q_values[range(len(all_q_values)), actions] = rewards + gamma * all_q_values_next[1][rand_action]
            else:
                all_q_values[range(len(all_q_values)), actions] = rewards + gamma * torch.max(all_q_values_next,
                                                                                              axis=1).values
            all_q_values[is_dones_indices.tolist(), actions_tensor[is_dones].tolist()] = rewards[
                is_dones_indices.tolist()]

            self.update(states.tolist(), all_q_values.tolist())


def sarsa(env, model, episodes, gamma,
          epsilon, eps_decay=0.99,
          replay=True, replay_size=20, verbose=True):
    """Deep Sarsa alg."""
    final = []
    memory = []
    episode_i = 0
    sum_total_replay_time = 0
    for episode in range(episodes):
        episode_i += 1
        # Reset state
        state = env.reset()
        done = False
        total = 0
        env.render()
        while not done:
            # Implement greedy search policy to explore the state space
            if random.random() < epsilon:
                action = env.action_space.sample()
            else:
                q_values = model.predict(state)
                action = torch.argmax(q_values).item()

            # Take action and add reward to total
            next_state, reward, done, _ = env.step(action)

            # Update total and memory
            total += reward
            memory.append((state, action, next_state, reward, done))
            q_values = model.predict(state).tolist()

            if done:
                if not replay:
                    q_values[action] = reward
                    # Update network weights
                    model.update(state, q_values)
                break

            if replay:
                # Update network weights using replay memory
                model.replay(memory, replay_size, gamma, epsilon)


            else:
                # Update network weights using the last step only
                q_values_next = model.predict(next_state)
                actions = np.array([0, 1])
                rand_action = np.random.choice(actions)
                # perform policy for updating q_values
                if random.random() < epsilon:
                    q_values[action] = reward + gamma * q_values_next[rand_action]
                else:
                    q_values[action] = reward + gamma * torch.max(q_values_next).item()
                model.update(state, q_values)
                env.render()
            state = next_state

        # Update epsilon; eps_decay parameter indicates the speed at which the epsilon decreases
        epsilon = max(epsilon * eps_decay, 0.001)
        final.append(total)

        if verbose:
            print("episode: {}, total reward: {}".format(episode_i, total))
    env.close()
    plot_res(final, 'Deep Sarsa')
    return final


def plot_res(values, title='Deep Sarsa'):
    ''' Plot the reward curve and histogram of results over time.'''

    # Define the figure
    f, ax = plt.subplots(figsize=(12, 5))
    f.suptitle(title)
    ax.plot(values, label='score per run')
    ax.axhline(195, c='red', ls='--', label='goal')
    ax.set_xlabel('Episodes')
    ax.set_ylabel('Reward')
    ax.legend()
    plt.show()


# Set environment
env = gym.envs.make("CartPole-v1")
# Number of states
n_state = env.observation_space.shape[0]
# Number of actions
n_action = env.action_space.n
# Number of episodes
episodes = 200
# Number of hidden nodes in the DQN
n_hidden = 64
# Learning rate
lr = 0.001

if __name__ == '__main__':
    # remove this comment to run task 3a)
    # interactive_cart_pole()
    deep_sarsa = Deep_Sarsa(n_state, n_action, n_hidden, lr)
    simple = sarsa(env, deep_sarsa, episodes, gamma=0.9, epsilon=0.05)
