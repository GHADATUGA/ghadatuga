import sys
import json
import random
import matplotlib.pyplot as plt
import numpy as np


def apply_action(state, action):
    x, y = state.x, state.y
    if action == "left":
        x -= 1
    elif action == "right":
        x += 1
    elif action == "up":
        y -= 1
    elif action == "down":
        y += 1

    return x, y


class Cell:
    """
    Cell base class. This corresponds to the behaviour of a blank cell
    """

    def __init__(self, world, x, y):
        self.world = world
        self.x = x
        self.y = y
        self.reachable = True
        self.terminal = False

    def to_dict(self):
        res = self.__dict__.copy()
        res["__cls__"] = type(self).__name__
        del res["world"]
        return res

    def from_dict(self, dictionary):
        if "__cls__" in dictionary:
            del dictionary["__cls__"]

        self.__dict__.update(dictionary)

    def step(self, action):
        """
        Sample the next state when an agent performs the action while in this state.

        :param action: The action that the agent wants to take
        :return: the resulting state
        """
        if self.terminal:
            return self

        x, y = apply_action(self, action)

        state = self.world.get_state(x, y)

        return state

    def allow_enter(self, old_state, action):
        """
        Specify whether the agent can enter this state. The base implementation simply checks if the old state is next
        to the current state.
        :param old_state: The previous state that the agent is coming from
        :param action: The action that the agent wants to take
        :return: bool that specifies if the agent can enter or not
        """
        if not self.reachable:
            return False

        return (abs(self.x - old_state.x) + abs(self.y - old_state.y)) <= 1

    def get_afterstates(self, action):
        """
        This should return all possible states that can be output by step
        if that method is called with parameter action
        """
        if self.terminal:
            return [self]

        x, y = apply_action(self, action)
        return [self.world.get_state(x, y)]

    def p_step(self, action, new_state):
        """
        Compute the probability that self.step(action) will return new_state
        """
        if self.terminal:
            return int(new_state == self)

        x, y = apply_action(self, action)

        return int(new_state == self.world.get_state(x, y))

    def p_enter(self, old_state, action):
        """
        Compute the probability that self.allow_enter(old_state, action) will return True
        """
        if self.allow_enter(old_state, action):
            return 1

        return 0

    def __eq__(self, other):
        """
        Overwriting __eq__ and __hash__ means that Cells can be used as dictionary keys.
        """
        if not isinstance(other, Cell):
            return False

        return self.x == other.x and self.y == other.y

    def __hash__(self):
        """
        Overwriting __eq__ and __hash__ means that Cells can be used as dictionary keys.
        """
        return hash((self.x, self.y))

    def __str__(self):
        return f"{type(self).__name__} at ({self.x},{self.y})"

    def __repr__(self):
        return str(self)


class BlankCell(Cell):
    pass


class StartCell(Cell):
    def allow_enter(self, old_state, action):
        return (
            super(StartCell, self).allow_enter(old_state, action) or old_state.terminal
        )


class GoalCell(Cell):
    def __init__(self, world, x, y):
        super(GoalCell, self).__init__(world, x, y)
        self.terminal = True


class WallCell(Cell):
    def __init__(self, world, x, y):
        super(WallCell, self).__init__(world, x, y)

        self.reachable = False


class ArrowCell(Cell):
    def __init__(self, world, x, y, direction="left"):
        super(ArrowCell, self).__init__(world, x, y)

        self.direction = direction

    def step(self, action):
        return Cell.step(self, self.direction)

    def get_afterstates(self, action):
        x, y = apply_action(self, self.direction)
        return [self.world.get_state(x, y)]

    def p_step(self, action, new_state):
        if new_state == self.step(action):
            return 1

        return 0


class SwampCell(Cell):
    def __init__(self, world, x, y, stick_prob=0.5):
        super(SwampCell, self).__init__(world, x, y)

        self.stick_prob = stick_prob

    def step(self, action):
        if self.world.rng.random() < self.stick_prob:
            return self

        return super(SwampCell, self).step(action)

    def get_afterstates(self, action):
        x, y = apply_action(self, action)
        return [self, self.world.get_state(x, y)]

    def p_step(self, action, new_state):
        if new_state == Cell.step(self, action):
            return 1 - self.stick_prob
        elif new_state == self:
            return self.stick_prob

        return 0


class PitCell(Cell):
    def __init__(self, world, x, y):
        super(PitCell, self).__init__(world, x, y)
        self.terminal = True


class DefaultReward:
    possible_rewards = [0, -1, -1000]

    @staticmethod
    def reward_f(old_state, action, new_state):
        if old_state.terminal:
            return 0
        if isinstance(new_state, PitCell):
            return -1000

        return -1

    @staticmethod
    def reward_p(reward, new_state, old_state, action):
        """
        Computes p(R_{t+1} | S_{t+1}=new_state, S_t=old_state, A_t=action)
        """
        if old_state.terminal:
            true_r = 0
        elif isinstance(new_state, PitCell):
            true_r = -1000
        else:
            true_r = -1
        return int(true_r == reward)


class RewardWin:
    possible_rewards = [0, 1, -1000]

    @staticmethod
    def reward_f(old_state, action, new_state):
        if old_state.terminal:
            return -1000 if isinstance(new_state, PitCell) else 1
        if isinstance(new_state, PitCell) or isinstance(new_state, GoalCell):
            return -1000 if isinstance(new_state, PitCell) else 1
        return 0

    @staticmethod
    def reward_p(reward, new_state, old_state, action):
        """
        Computes p(R_{t+1} | S_{t+1}=new_state, S_t=old_state, A_t=action)
        """
        if old_state.terminal:
            true_r = -1000 if isinstance(new_state, PitCell) else 1
        elif isinstance(new_state, PitCell):
            true_r = -1000 if isinstance(new_state, PitCell) else 1
        else:
            true_r = 0

        return int(true_r == reward)


class CustomReward:
    possible_rewards = [0, 1, -1000]

    @staticmethod
    def reward_f(old_state, action, new_state):
        if old_state.terminal:
            return -1000 if isinstance(new_state, PitCell) else 100
        if isinstance(new_state, PitCell) or isinstance(new_state, GoalCell):
            return -1000 if isinstance(new_state, PitCell) else 100
        return -1

    @staticmethod
    def reward_p(reward, new_state, old_state, action):
        """
        Computes p(R_{t+1} | S_{t+1}=new_state, S_t=old_state, A_t=action)
        """
        if old_state.terminal:
            true_r = -1000 if isinstance(new_state, PitCell) else 100
        elif isinstance(new_state, PitCell):
            true_r = -1000 if isinstance(new_state, PitCell) else 100
        else:
            true_r = -1

        return int(true_r == reward)


class HammingDistanceReward:
    @staticmethod
    def reward_f(old_state, action, new_state):
        if old_state.terminal:
            return 0
        if isinstance(new_state, PitCell):
            return -1000
        return 0

    @staticmethod
    def reward_p(reward, new_state, old_state, action):
        """
        Computes p(R_{t+1} | S_{t+1}=new_state, S_t=old_state, A_t=action)
        """
        if old_state.terminal:
            true_r = 0
        elif isinstance(new_state, PitCell):
            true_r = -1000
        else:
            true_r = 0

        return int(true_r == reward)


class World:
    actions = ["right", "left", "up", "down"]

    def __init__(self, reward_class=DefaultReward):
        self.reward_class = reward_class
        self.rng = random.Random()
        self.grid = None
        self.start_states = None
        self.current_state = None
        self.look_up_table = None
        self.epsilon = 0.01
        self.goal = None

    def save_to_file(self, path):
        g_list = self.grid.flatten().tolist()
        g_list = [o for o in g_list if not isinstance(o, BlankCell)]

        world_info = dict(
            size=self.grid.shape, reward_class=self.reward_class.__name__, grid=g_list
        )

        class CellEncoder(json.JSONEncoder):
            def default(self, obj):
                if hasattr(obj, "to_dict"):
                    return obj.to_dict()

                return json.JSONEncoder.default(self, obj)

        with open(path, mode="w") as f:
            json.dump(world_info, f, cls=CellEncoder, ensure_ascii=False, indent=4)

    @classmethod
    def load_from_file(cls, path):
        def hook(dct):
            if "__cls__" not in dct:
                return dct

            klass = globals()[dct["__cls__"]]
            obj = klass(None, dct["x"], dct["y"])
            obj.from_dict(dct)
            return obj

        with open(path, mode="r") as f:
            world_info = json.load(f, object_hook=hook)

        rew_class = globals()[world_info["reward_class"]]
        world = World(rew_class)
        world.start_states = []

        world.grid = np.array(
            [
                [BlankCell(world, x, y) for x in range(world_info["size"][0])]
                for y in range(world_info["size"][1])
            ],
            dtype=object,
        )
        for cell in world_info["grid"]:
            cell.world = world
            if isinstance(cell, StartCell):
                world.start_states.append(cell)
            if isinstance(cell, GoalCell):
                world.goal = cell
            world.grid[cell.y, cell.x] = cell

        return world

    def get_state(self, x, y):
        if self.grid is None:
            raise RuntimeError
        if not (0 <= x < self.grid.shape[0] and 0 <= y < self.grid.shape[1]):
            raise ValueError

        return self.grid[y, x]

    def reset(self):
        self.current_state = self.rng.choice(self.start_states)

    def step(self, action):
        proposed_state = self.current_state.step(action)

        if proposed_state.allow_enter(self.current_state, action):
            new_state = proposed_state
        else:
            new_state = self.current_state
        if isinstance(self.reward_class(), HammingDistanceReward) and not isinstance(
            new_state, PitCell
        ):
            reward = -abs(self.goal.x - new_state.x) - abs(self.goal.y - new_state.y)
        else:
            reward = self.reward_class.reward_f(self.current_state, action, new_state)

        done = new_state.terminal
        self.current_state = new_state

        return new_state, reward, done

    def p(self, new_state, reward, old_state, action):
        """
        Computes p(S_{t+1}=new_state, R_{t+1}=reward | S_t=old_state, A_t=action)
        """
        if isinstance(self.reward_class, HammingDistanceReward) and isinstance(
            new_state, GoalCell
        ):
            reward_p = int(
                reward
                == -abs(self.goal.x - new_state.x) - abs(self.goal.y - new_state.y)
            )
        else:
            reward_p = self.reward_class.reward_p(reward, new_state, old_state, action)

        if reward_p == 0:
            return 0

        step_p = old_state.p_step(action, new_state)
        if new_state != old_state:
            enter_p = new_state.p_enter(old_state, action)
            return reward_p * step_p * enter_p

        if step_p == 1:
            return reward_p

        enter_p = 0
        for s in old_state.get_afterstates(action):
            if s != old_state:
                enter_p += old_state.p_step(action, s) * s.p_enter(old_state, action)
        return reward_p * (1 - enter_p)

    def performPolicy(self, state):
        random_value = np.random.random()
        if random_value < self.epsilon:
            action = random.choice(["left", "right", "up", "down"])
        else:
            arg_max = np.argmax(np.array(list(self.look_up_table[state].values())))
            action = list(self.look_up_table[state].keys())[arg_max]
        return action


def random_walk(steps=100):
    world = World.load_from_file("world.json")
    world.reset()
    print(f"Starting at pos. ({world.current_state.x}, {world.current_state.y}).")
    ep_return = 0
    last_termination = 0
    for s in range(steps):
        print(f"Step {s+1}/{steps}...")
        action = random.choice(["left", "right", "up", "down"])
        print(f"Going {action}!")
        new_state, reward, done = world.step(action)
        ep_return += reward
        print(f"Received a reward of {reward}!")
        print(
            f"New position is ({new_state.x}, {new_state.y}) on a {type(new_state).__name__}."
        )
        if done:
            print(
                f"Episode terminated after {s+1-last_termination} steps. Total Return was {ep_return}."
            )
            ep_return = 0
            last_termination = s + 1
            print(f"Resetting the world...")
            world.reset()


def sarsa(world, epsilon, max_episode_num, step_size, discount_factor):
    print("Learning in progress...")
    random.seed(128)
    states, episode_rewards, wins, losses = [], [], [], []
    win, loss = 0, 0
    for i in range(world.grid.shape[0]):
        for j in range(world.grid.shape[1]):
            states.append(world.get_state(i, j))
    actions = ["left", "right", "up", "down"]
    look_up_table = {
        state: {action: 0 for action in actions}
        if isinstance(state, GoalCell) or isinstance(state, PitCell)
        else {action: 10 for action in actions}
        for state in states
    }
    for _ in range(max_episode_num):
        episode_reward = 0
        world.reset()
        initial_state = world.start_states[0]
        probabilty = random.random()
        if probabilty < epsilon:
            action = random.choice(actions)
        else:
            arg_max = np.argmax(np.array(list(look_up_table[initial_state].values())))
            action = list(look_up_table[initial_state].keys())[arg_max]
        while True:
            next_state, reward, _ = world.step(action)
            probabilty = random.random()
            if probabilty < epsilon:
                next_action = random.choice(actions)
            else:
                arg_max = np.argmax(
                    np.array(list(look_up_table[initial_state].values()))
                )
                next_action = list(look_up_table[initial_state].keys())[arg_max]
            look_up_table[initial_state][action] = look_up_table[initial_state][
                action
            ] + step_size * (
                reward
                + discount_factor * look_up_table[next_state][next_action]
                - look_up_table[initial_state][action]
            )
            action = next_action
            initial_state = next_state
            episode_reward += reward
            if episode_reward < -2000:
                break
            if isinstance(initial_state, GoalCell) or isinstance(
                initial_state, PitCell
            ):
                if isinstance(next_state, GoalCell):
                    win += 1
                elif isinstance(next_state, PitCell):
                    loss += 1
                break
        episode_rewards.append(episode_reward)
        wins.append(win)
        losses.append(loss)
    plt.scatter(
        [i for i in range(len(episode_rewards))],
        episode_rewards,
        label="Rewards/Ep",
        color="k",
        marker=".",
        alpha=0.5,
    )
    plt.plot(wins, label="Wins", color="g")
    plt.plot(losses, label="Losses", color="r")
    plt.legend()
    plt.xlabel("Episode number")
    plt.ylabel("Cummulative reward and wins")
    plt.show()
    return look_up_table


def q_learning(world, epsilon, max_episode_number, step_size, discount_factor):
    print("Learning in progress...")
    random.seed(128)
    states, episode_rewards, wins, losses = [], [], [], []
    win, loss = 0, 0
    for i in range(world.grid.shape[0]):
        for j in range(world.grid.shape[1]):
            states.append(world.get_state(i, j))
    actions = [
        "left",
        "right",
        "up",
        "down",
    ]
    look_up_table = {
        state: {action: 0 for action in actions}
        if isinstance(state, GoalCell) or isinstance(state, PitCell)
        else {action: 1 / (actions.index(action) + 1) for action in actions}
        for state in states
    }
    for _ in range(max_episode_number):
        episode_reward = 0
        world.reset()
        initial_state = world.start_states[0]
        probabilty = random.random()
        if probabilty < epsilon:
            action = random.choice(actions)
        else:
            arg_max = np.argmax(np.array(list(look_up_table[initial_state].values())))
            action = list(look_up_table[initial_state].keys())[arg_max]
        while True:
            next_state, reward, done = world.step(action)
            probabilty = random.random()
            if probabilty < epsilon:
                next_action = random.choice(actions)
            else:
                arg_max = np.argmax(
                    np.array(list(look_up_table[initial_state].values()))
                )
                next_action = list(look_up_table[initial_state].keys())[arg_max]
            look_up_table[initial_state][action] = look_up_table[initial_state][
                action
            ] + step_size * (
                reward
                + discount_factor * max(list(look_up_table[next_state].values()))
                - look_up_table[initial_state][action]
            )
            action = next_action
            initial_state = next_state
            episode_reward += reward
            if episode_reward < -1500:
                break
            if isinstance(initial_state, GoalCell) or isinstance(
                initial_state, PitCell
            ):
                if isinstance(initial_state, GoalCell):
                    win += 1
                elif isinstance(initial_state, PitCell):
                    loss += 1
                break
        episode_rewards.append(episode_reward)
        wins.append(win)
        losses.append(loss)
    plt.scatter(
        [i for i in range(len(episode_rewards))],
        episode_rewards,
        label="Rewards/Ep",
        color="k",
        marker=".",
        alpha=0.5,
    )
    plt.plot(wins, label="Wins", color="g")
    plt.plot(losses, label="Losses", color="r")
    plt.ylim([-3000, 500])
    plt.legend()
    plt.xlabel("Episode number")
    plt.ylabel("Cummulative reward and wins")
    plt.show()
    return look_up_table


def example_walk(
    world,
    max_episode_number,
    step_size,
    discount_factor,
    learning_function=sarsa,
    optimal_plicy=False,
    reward_function=DefaultReward,
):
    world.reward_class = reward_function
    if optimal_plicy:
        world.look_up_table = policy_iteration(world)
        world.epsilon = 0
    else:
        world.look_up_table = learning_function(
            world, world.epsilon, max_episode_number, step_size, discount_factor
        )
    world.reset()
    wins, returns = 0, []
    """100 runs"""
    # sys.exit()
    for _ in range(100):
        world.epsilon = 0
        episode_return, number_steps = 0, 0
        print(f"Starting at pos. ({world.current_state.x}, {world.current_state.y}).")
        while True:
            print(f"Step {number_steps}...")
            x = world.current_state
            action = world.performPolicy(x)
            print(f"Going {action}!")
            new_state, reward, done = world.step(action)
            episode_return += reward
            print(f"Received a reward of {reward}!")
            print(
                f"New position is ({new_state.x}, {new_state.y}) on a {type(new_state).__name__}."
            )
            if done or number_steps > 2000:
                returns.append(episode_return)
                print(
                    f"Episode terminated after {number_steps + 1} steps. Total Return was {episode_return}."
                )
                if isinstance(new_state, GoalCell):
                    wins += 1
                break
            number_steps += 1
        print(
            f"The agent won {wins} rounds with a win rate of {round(wins/100 * 100, 2)}% with an average return of {sum(returns) / 100}"
        )
        print(
            "------------------------------------------------------------------------"
        )
        print(f"Resetting the world...")
        world.reset()


def policy_evaluation(world, policy, threshold=0.1, discount_factor=1):
    states, rewards = [], [-1, 1000, 0]
    for i in range(1, world.grid.shape[0]):
        for j in range(1, world.grid.shape[1]):
            states.append(world.get_state(i, j))
    evaluation_fucntion = {state: 1 for state in states}
    print("Beginning policy evaluation algorithm")
    while True:
        delta = 0
        for state in states:
            current_evaluation_of_state = evaluation_fucntion[state]
            arg_max = np.argmax(np.array(list(policy[state].values())))
            action = list(policy[state].keys())[arg_max]
            for next_state, reward in zip(states, rewards):
                try:
                    evaluation_fucntion[state] += world.p(
                        next_state,
                        reward,
                        state,
                        action,
                    )
                except ValueError:
                    continue  # Hit a wall
                evaluation_fucntion[state] *= (
                    reward + discount_factor * evaluation_fucntion[next_state]
                )
            delta = max(
                delta, abs(current_evaluation_of_state - evaluation_fucntion[state])
            )
        print(delta)
        if delta < threshold:
            break
    return evaluation_fucntion


def policy_iteration(world):
    actions = ["left", "right", "up", "down"]
    rewards = [-1, 0, -1000]
    states = []
    for i in range(1, world.grid.shape[0]):
        for j in range(1, world.grid.shape[1]):
            states.append(world.get_state(i, j))
    policy = {state: {action: 0.25 for action in actions} for state in states}
    print("Beginning Policy improvement algorithm")
    while True:
        policy_is_stable = True
        current_policy_evaluation = policy_evaluation(world, policy)
        for state in states:
            if not (isinstance(state, GoalCell) or isinstance(state, PitCell)):
                arg_max = np.argmax(np.array(list(policy[state].values())))
                old_action = list(policy[state].keys())[arg_max]
                best_action, maximum_action_value = None, -(10 ** 6)
                for action in actions:
                    try:
                        action_value = sum(
                            world.p(
                                state_p,
                                reward,
                                state,
                                action,
                            )
                            * (reward + current_policy_evaluation[state])
                            for state_p, reward in zip(states, rewards)
                        )
                        if action_value > maximum_action_value:
                            best_action, maximum_action_value = action, action_value
                    except ValueError:
                        continue  # Arrived to the wall
                if old_action != best_action:
                    policy_is_stable = False
        if policy_is_stable:
            print(policy)
            return policy


def main():
    while True:
        print(
            "To visualize the outcome and process of an algorithm",
            "We need some parameters to begin the lerning choose to b and to exit choose e",
            end="",
        )
        input_ = input()
        if input_ in ["s", "q"]:
            break
        else:
            print("Input Error, Try again")
    while input_ != "e":
        print("Lets begin our journey")
        while True:
            input_ = input(
                "To choose the learning algorithm, choose s for Sarsa and to Q-Learning choose q "
            )
            if input_ in ["s", "q"]:
                lerning_function = sarsa if input_ == "s" else q_learning
                break
            else:
                print("Input Error, Try again")
        while True:
            input_ = input("Now enter the number of episodes")
            if input_.isdigit():
                number_episodes = int(input_)
                break
            else:
                print("Input Error, Try again")
        while True:
            input_ = input("Now enter epsilon, value between 0 and 1")
            try:
                epsilon = float(input_)
                if epsilon > 1 or epsilon < 0:
                    print("Input Error, Try again")
                    continue
                else:
                    break
            except ValueError:
                print("Input Error, Try again")
        while True:
            print(
                "We have implemented four reward classes",
                "1. Default Reward",
                "2. RewardWin",
                "3. HammingReward",
                "4. CustomReward",
            )
            input_ = input("Now choose the reward fuction, value between 0 and 1")
            try:
                if int(input_) == 1:
                    reward_class = DefaultReward
                    break
                elif int(input_) == 2:
                    reward_class = RewardWin
                    break
                elif int(input_) == 3:
                    reward_class = HammingDistanceReward
                    break
                elif int(input_) == 4:
                    reward_class = CustomReward
                    break
                else:
                    print("Input Error, Try again")
            except ValueError:
                print("Input Error, Try again")
        while True:
            input_ = input("Now enter the step-size, value between 0 and 1")
            try:
                step_size = float(input_)
                if step_size > 1 or step_size < 0:
                    print("Input Error, Try again")
                    continue
                else:
                    break
            except ValueError:
                print("Input Error, Try again")
        while True:
            input_ = input("Now enter the discount factor, value between 0 and 1")
            try:
                discount_factor = float(input_)
                if discount_factor > 1 or discount_factor < 0:
                    print("Input Error, Try again")
                    continue
                else:
                    break
            except ValueError:
                print("Input Error, Try again")
        print(
            "------------------------------------------------------------------------"
        )
        print("Now we will train the model and visualize the training process.")
        print("Loading the world...")
        print(
            "------------------------------------------------------------------------"
        )
        world = World.load_from_file("world.json")
        world.epsilon = epsilon
        world.reset()
        example_walk(
            world,
            number_episodes,
            step_size,
            discount_factor,
            learning_function=lerning_function,
            optimal_plicy=False,
            reward_function=reward_class,
        )
        print(
            "------------------------------------------------------------------------"
        )
    """while True:
        print(
            "To visualize the outcome and process of an algorithm",
            "We need some parameters to begin the lerning choose to b and to exit choose e",
            end="",
        )
        input_ = input()
        if input_ in ["s", "q"]:
            break
        else:
            print("Input Error, Try again")"""


if __name__ == "__main__":
    world = World.load_from_file("world.json")
    world.reset()
    main()
