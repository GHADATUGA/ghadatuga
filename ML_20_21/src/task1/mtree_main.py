
import numpy as np
from dataset import *
import matplotlib.pyplot as plt
from mtree_code import *
train_x, train_y = get_strange_symbols_train_data()


def chebyshev_distance(a,b):
    return np.max(np.abs(np.subtract(a , b)))

def euclidean_distance(a, b):
    return np.sqrt(np.sum(np.square(a - b)))
#def euclidean_distance(a, b):
    #return (np.sum((a - b) ** 2))**1/2

    # Create an Euclidean Distance Matrix
   # part1 = np.sum (a**2, axis=1)[:, np.newaxis]
   # part2 = np.sum(b**2, axis =1)
    #part3 = -2 * np.dot(a, b.T)

    #return np.round(np.sqrt(part1+part2+part3),2)  
    

class KNN:
    def __init__(self, k=5, dist_function=euclidean_distance):
        self.k = k
        self.dist_function = dist_function
        #self.X=None
        #self.labels_train=None

    def fit(self, X, y):
        self.X_train=X
        self.labels_train=y
        
        
         #initialisation of mtree with euclidean distance
        self.mtree=MTree(euclidean_distance)
        # Insert the training data into Mtree
        self.mtree.add_all(self.X_train)
        
         
        

    def predict(self, X):
        
        nbr_test=X.shape[0]
        nbr_train=self.X_train.shape[0]
        dists=np.zeros((nbr_test, nbr_train))
        ypred=np.zeros(nbr_test)
        kneighbors=np.zeros((nbr_test, self.k), dtype="i4")
        
        #The model is starting the prediction
        for i in range(nbr_test): 
            
            #Perform k-NN queries by finding NN using search method in mtree_code
            neighbor=self.mtree.search(X[i],self.k)
            neighbors=np.array([j for j in neighbor])
            if None not in neighbors:
                yyy=self.labels_train[neighbors]
                ypred[i]=self.mode(yyy)
            
        return ypred
        
    def mode(self,arr):
        vals, counts=np.unique(arr, return_counts=True)
         # Out of the k-closest images, the class in majority is assigned to that new test image
        index=np.argmax(counts)
        return vals[index]
        
        


def accuracy(predicted, actual):
    return np.mean(predicted == actual)


def cross_validation(clf, X, Y, m=5, metric=accuracy):
    
    avg_metric=0
    # Split training_data and training_labels into m folds for crossvalidation
    for train_index, test_index in mfold(X,n_splits=m):
        x_train,y_train, x_test, y_test=X[train_index], Y[train_index], X[test_index], Y[test_index]
        clf.fit(x_train,y_train)
        y_pred=clf.predict(x_test)
        avg_metric+=metric(y_pred,y_test)
    return avg_metric/m
   
    #split int 5 folds
def mfold(X,random_state=None,n_splits=5,y=None):
    
    def test_mask(X,y, n_splits):
        for index in test_index(X,y, n_splits):
            mask=np.zeros(X.shape[0], dtype=np.bool)
            mask[index]=True
            yield mask
    def test_index(X=None, y=None, n_splits=5):
        nbr_samples=X.shape[0]
        indices=np.arange(nbr_samples)
        fold_sizes=np.full(n_splits,nbr_samples//n_splits, dtype=np.int)
        fold_sizes[:nbr_samples%n_splits]+=1
        current=0
        for fold_size in fold_sizes:
            start, stop=current, current+fold_size
            yield indices[start:stop]
            current=stop
    nbr_samples=X.shape[0]
    indices=np.arange(nbr_samples)
    for index in test_mask(X,y, n_splits):
        train=indices[np.logical_not(index)]
        test=indices[index]
        yield train, test
    
    
    

def main(args):
    # Set up data
    train_x, train_y = get_strange_symbols_train_data(root=args.train_data)
    train_x = train_x.numpy()
    train_y = np.array(train_y)
    train_x, train_y = get_strange_symbols_train_data()
    
    
    #dataset=np.arange(1000)
    #indices=np.random.choice(dataset,100)
    
    classifiers=[KNN(k=i+1) for i in range(10)]
    counter=0
    scores = []
    for clf in classifiers:
        
        #metric=cross_validation(clf, train_x.numpy()[indices], np.array(train_y)[indices], m=5, metric=accuracy)
        metric=cross_validation(clf, train_x.numpy(), np.array(train_y), m=5, metric=accuracy)
        counter +=1
        scores.append(metric)
        print("Accuracy at k=",counter," is :", metric)
        
    neighbors = np.arange(1, clf.k+1)
    plt.title('k-NN: Varying Number of Neighbors')
    plt.plot(neighbors, scores, label='Testing Accuracy')
    plt.legend()
    plt.xlabel('Number of Neighbors')
    plt.ylabel('Accuracy')
    plt.show()


def plotImages(data):
    '''
    Plotting of images
    :param data: train_x data
    '''
    rows = 5
    cols = 3
    axes = []
    fig = plt.figure()
    plt.title('Plot that contains images from each classe')
    for i in range(rows * cols):
        img = data[i][0]
        axes.append(fig.add_subplot(rows, cols, i + 1))
        plt.imshow(img)
    fig.tight_layout()
    plt.show()

    
    
    
    

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='This script computes cross validation scores for a kNN                                                                     classifier.')

    parser.add_argument('--folds', '-m', type=int, default=5,
                        help='The number of folds that the data is partitioned in for cross validation.')
    parser.add_argument('--train-data', type=str, default=DEFAULT_ROOT,
                        help='Directory in which the training data and the corresponding labels are located.')
    parser.add_argument('--k', '-k', type=int, default=list(range(1, 11))+[15], nargs='+',
                        help='The k values that should be evaluated.')

    args = parser.parse_args()
    main(args)
    
