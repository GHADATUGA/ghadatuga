import numpy as np
from matplotlib import pyplot as plt
from src.task1.dataset import *
from scipy.spatial.distance import cdist # For Manhatten-Distance


def euclidean_distance(a, b):
    #return (np.sum((a - b) ** 2))**1/2

    # Create an Euclidean Distance Matrix
    part1 = np.sum (a**2, axis=1)[:, np.newaxis]
    part2 = np.sum(b**2, axis =1)
    part3 = -2 * np.dot(a, b.T)

    return np.round(np.sqrt(part1+part2+part3),2)

def manhattan_distance(a, b):
    #return np.sum(abs(a - b))

    # Create Manhattan-Distance with scipy
    return cdist(a, b, metric='cityblock')


class KNN:
    #train_x = []
    #train_y = []

    def __init__(self, k=5, dist_function= euclidean_distance):          #change  to euclidean/manhatten/..
        self.k = k
        self.dist_function = dist_function

    def fit(self, X, y):
        """
        Train the k-NN classifier.

        :param X: Training inputs. Array of shape (n, ...)
        :param y: Training labels. Array of shape (n,)
        """
        self.train_x = X
        self.train_y = y

    def predict(self, X):
        """
        Predict labels for new, unseen data.

        :param X: Test data for which to predict labels. Array of shape (n', ..) (same as in fit)
        :return: Labels for all points in X. Array of shape (n',)
        """
        predictions = []
        nof_classes = np.amax(self.train_y) + 1
        nof_test_vectors = X.shape[0]
        nof_train_vectors = self.train_x.shape[0]

        # Reshape data into (<Nof_Images>, 28*28)
        test_x = np.reshape(X,(3000, 28*28))
        train_x = np.reshape(self.train_x,(12000, 28*28))

        # Calculate Distance Matrix
        dists = self.dist_function(test_x, train_x)

        # Predict label for test image
        for i in range(nof_test_vectors):
            # Initialize matrix for votes (15 classes)
            votes = np.zeros(nof_classes, dtype=np.int)
            # Sorts the distances and votes for the k labels with the smallest distance
            for neighbor_index in np.argsort(dists[i, :])[:self.k]:
                    neighbor_label = self.train_y[neighbor_index]
                    votes[neighbor_label] += 1

            # Out of the k-closest images, the class in majority is assigned to that new test image
            predictions.append(np.argmax(votes))

        return predictions


def accuracy(predicted, actual):
    return np.mean(predicted == actual)


def cross_validation(clf, X, Y, m=5, metric=accuracy):
    """
    Performs m-fold cross validation.

    :param clf: The classifier which should be tested.
    :param X: The input data. Array of shape (n, ...).
    :param Y: Labels for X. Array of shape (n,).
    :param m: The number of folds.
    :param metric: Metric that should be evaluated on the test fold.
    :return: The average metric over all m folds.
    """

    avg_metric = 0
    # Split training_data and training_labels into m folds for crossvalidation
    for train_index, test_index in mfold(X, n_splits=5):
        x_train, y_train, x_test, y_test = X[train_index], Y[train_index], X[test_index], Y[test_index]
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)
        avg_metric += metric(y_pred, y_test)
    return avg_metric / m


def mfold(X, n_splits=5):
    '''
    Separates data into five equally sized index ranges

    :param X: data (training_data)
    :param n_splits: Number of Folds
    :return: Five different Indexsplits for CrossValidation
    '''

    def test_mask(X, n_splits):
        for index in test_index(X, n_splits):
            mask = np.zeros(X.shape[0], dtype=np.bool)
            mask[index] = True
            yield mask

    def test_index(X=None, n_splits=5):
        nbr_samples = X.shape[0]
        indices = np.arange(nbr_samples)
        fold_sizes = np.full(n_splits, nbr_samples // n_splits, dtype=np.int)
        fold_sizes[:nbr_samples % n_splits] += 1
        current = 0
        for fold_size in fold_sizes:
            start, stop = current, current + fold_size
            yield indices[start:stop]
            current = stop

    nbr_samples = X.shape[0]
    indices = np.arange(nbr_samples)
    #print(nbr_samples)
    for index in test_mask(X, n_splits):
        train = indices[np.logical_not(index)]
        test = indices[index]
        yield train, test


def main(args):
    # Set up data
    train_x, train_y = get_strange_symbols_train_data(root=args.train_data)
    train_x = train_x.numpy()
    train_y = np.array(train_y)
    # plotImages(train_x)                                               # Plotting some images to get an understanding of the data
    # TODO: Load and evaluate the classifier for different k

    # Setting up classifiers for every k = (1..10)
    classifiers = [KNN(k=i + 1) for i in range(10)]
    counter = 0
    scores = []
    for clf in classifiers:
        metric = cross_validation(clf, train_x, np.array(train_y), m=5, metric=accuracy)
        counter += 1
        scores.append(metric)
        print("Accuracy at k={} is: {:.2f}".format(counter, metric))

    # TODO: Plot results
    neighbors = np.arange(1, clf.k+1)
    plt.title('k-NN: Varying Number of Neighbors')
    plt.plot(neighbors, scores, label='Testing Accuracy')
    plt.legend()
    plt.xlabel('Number of Neighbors')
    plt.ylabel('Accuracy')
    plt.show()


def plotImages(data):
    '''
    Plotting of images
    :param data: train_x data
    '''
    rows = 5
    cols = 3
    axes = []
    fig = plt.figure()
    for i in range(rows * cols):
        img = data[i][0]
        axes.append(fig.add_subplot(rows, cols, i + 1))
        plt.imshow(img)
    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='This script computes cross validation scores for a kNN classifier.')

    parser.add_argument('--folds', '-m', type=int, default=5,
                        help='The number of folds that the data is partitioned in for cross validation.')
    parser.add_argument('--train-data', type=str, default=DEFAULT_ROOT,
                        help='Directory in which the training data and the corresponding labels are located.')
    parser.add_argument('--k', '-k', type=int, default=list(range(1, 11)) + [15], nargs='+',
                        help='The k values that should be evaluated.')

    args = parser.parse_args()
    main(args)
