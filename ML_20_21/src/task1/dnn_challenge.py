from dnn import DeepNeuralNetwork
from dataset import get_strange_symbols_test_data
import csv
import torch

if __name__ == '__main__':
    # Paramerters of the model
    input_size = 784
    hidden_size = 256
    number_classes = 15
    # Initializing the model    
    model = DeepNeuralNetwork(input_size, hidden_size, number_classes)
    # Loading the model
    model.load_state_dict(torch.load('Models/trained_dnn_model'), strict=False)
    test_data = get_strange_symbols_test_data()[0] 
    # writing the csv file
    with open('test_predictions.csv', 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(['# image id', 'class 0', 'class 1', 'class 2', 'class 3', 'class 4', 'class 5', 'class 6', 'class 7', 'class 8', 'class 9', 'class 10', 'class 11', 'class 12', 'class 13', 'class 14'])
        for item, i in zip(enumerate(test_data), range(len(test_data))):
            _, image = item
            # Adding the image id
            next_row = ["{:.18e}".format(i)]
            # Appending the vector to the current row
            for x in model.forward(image).tolist():
                for y in x:
                    next_row += ["{:.18e}".format(y)]
            # Writting the row
            writer.writerow(next_row) 