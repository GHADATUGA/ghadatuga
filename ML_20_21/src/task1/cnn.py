# imports a getter for the StrangeSymbol Dataset loader and the test data tensor
from dataset import get_strange_symbols_train_loader, get_strange_symbols_test_data, get_strange_symbols_train_data
# import the library pytorch
import torch
# for ploting the changes of loss and accuracies depending on the parameters..
import matplotlib.pyplot as plt
from matplotlib.transforms import BlendedGenericTransform
# the module for all functions that can be used as loss or activation function to train a network
import torch.nn.functional as F
# import the base class for all neural network modules and ...
from torch.nn import Conv2d, ReLU, MaxPool2d, Module, Linear, Flatten
# to calculate the confusion matrix
from sklearn.metrics import confusion_matrix
from torch.utils.data.dataloader import DataLoader
from torch.utils.data import random_split



# the class of the deep neural network
class ConvolutionalNeuralNetwork(Module):
    def __init__(self, number_classes, loss_function=F.cross_entropy, optimizer=torch.optim.SGD):
        super().__init__()
        self.network = torch.nn.Sequential(
            Conv2d(1, 16, kernel_size=4, padding=1),
            ReLU(),
            MaxPool2d(2, 2), 
            Conv2d(16, 32, kernel_size=4, padding=1),
            ReLU(),
            MaxPool2d(2, 2),
            Conv2d(32, 64, kernel_size=4, padding=1),
            ReLU(),
            MaxPool2d(2, 2),
            Conv2d(64, 128, kernel_size=4, padding=1),
            ReLU(),
            Flatten(),
            Linear(128, 256),
            ReLU(),
            Linear(256, number_classes))
        self.num_classes = number_classes
        self.loss_function = loss_function
        self.optimizer = optimizer
    # feeding the input data to the network
    def forward(self, inpt):
        return self.network(inpt)
    # calculation the loss in this batch
    def trainingStep(self, batch):
        images, labels = batch
        out = self(images)
        loss = self.loss_function(out, labels)
        return loss
    # calculating the loss in this batch and accuracy
    def validationStep(self, batch):
        images, labels = batch
        out = self(images)
        loss = self.trainingStep(batch)
        acc = accuracy(out, labels)
        return {'valueLoss': loss, 'valueAccuracy': acc}
    # calculationg the value loss and accuracy of the model after the end of an epoch 
    def validationEpochEnd(self, outputs):
        batch_losses = [x['valueLoss'] for x in outputs]
        epoch_loss = torch.stack(batch_losses).mean()
        batch_accs = [x['valueAccuracy'] for x in outputs]
        epoch_acc = torch.stack(batch_accs).mean()
        return {'valueLoss': epoch_loss.item(), 'valueAccuracy': epoch_acc.item()}
    # Printer of the current loss value and accuracy of the model
    def epochEnd(self, epoch, result):
        print("{}. epoch, the loss value: {:.2f} with model accuracy: {:.2f}%".format(epoch, result['valueLoss'], result['valueAccuracy']*100))
    # Evaluating the model using valuation set
    def evaluate(self, valuationSetLoader):
        outputs = [self.validationStep(batch) for batch in valuationSetLoader]
        return self.validationEpochEnd(outputs)
    # Fitting the model using the optimiser stochastic gradient descent as default optimizer
    def fit(self, epochs, learning_rate, train_loader, valuation_loader):
        history = []
        optimizer = self.optimizer(model.parameters(), learning_rate)
        for epoch in range(epochs):
            for batch in train_loader:
                loss = model.trainingStep(batch)
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()
            result = self.evaluate(valuation_loader)
            self.epochEnd(epoch, result)
            history.append(result)
        return history
# to calculate the accuracies
def accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))

def to_device(data, device):
    if isinstance(data, (list, tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)


if __name__ == '__main__':
    new_or_warmStart = input("new or warm start? type (y) for intializing a new model else (n) ")
    # Paramerters of the model
    number_classes = 15
    epochs = int(input("What is the number of epochs? "))
    # Initialising the model
    if new_or_warmStart == 'y':
        model = ConvolutionalNeuralNetwork(15)
    else:
        model = ConvolutionalNeuralNetwork(15)
        model.load_state_dict(torch.load('Models/trained_model'), strict=False)
    # Loading the the training set
    trainLoader = get_strange_symbols_train_loader(batch_size=512)
    dataSet = trainLoader.dataset
    # Split the training data to a training set and a validation set with a ratio (.8, .2)
    train_data_set, validation_data_set = random_split(dataSet, [int(len(dataSet)*.9), int(len(dataSet)*.1)])
    train_loader= DataLoader(train_data_set, batch_size=128,shuffle=True,drop_last=True, num_workers=8)
    validation_loader= DataLoader(validation_data_set, batch_size=256, drop_last=True,num_workers=8)
    # First evaluation of the model
    history = [model.evaluate(validation_loader)]
    # Fitting the model
    history += model.fit(epochs, .12 , train_loader, validation_loader)
    # Extract value losses and accuracies
    losses = [x['valueLoss'] for x in history]
    accuracies = [x['valueAccuracy'] for x in history]
    # mode 01 from one case
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    line1, = ax1.plot(range(epochs+1), losses, label='losses per epoch')
    # mode 01 from other case
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    line2, = ax2.plot(range(epochs+1), accuracies, label='accuracies per epoch')
    ax2.set_ylim([0, 1])
    ax1.set_xlabel('epochs')
    ax2.set_xlabel('epochs')
    ax1.set_ylabel('losses')
    ax2.set_ylabel('accuracies')
    # Create new figure and two subplots, sharing both axes
    fig3, (ax3, ax4) = plt.subplots(1,2,sharey=False, sharex=True,figsize=(10,5))
    # Plot data from fig1 and fig2
    line3, = ax3.plot(line1.get_data()[0], line1.get_data()[1], color='r')
    line4, = ax4.plot(line2.get_data()[0], line2.get_data()[1], color='b')
    ax3.set_xlabel('epochs')
    ax4.set_xlabel('epochs')
    ax3.set_ylabel('losses')
    ax4.set_ylabel('accuracies')
    # Add legend
    fig3.legend((line3, line4), ('losses', 'accuracies'),loc = 'upper center',bbox_to_anchor = [0.5, -0.05],bbox_transform = BlendedGenericTransform(fig3.transFigure, ax3.transAxes))
    # Make space for the legend beneath the subplots
    plt.subplots_adjust(bottom = 0.2)
    # Creating the confusion matrix for the model
    images, labels = get_strange_symbols_train_data()
    # Generating outouts of the model and converting them to predictions
    outputs = model(images)
    _, predictions = torch.max(outputs, dim=1)
    m = confusion_matrix(labels, predictions, labels=list(range(15)))
    # Ploting the confusion matrix
    fig, ax = plt.subplots()
    im = ax.imshow(m)
    # Setting the axises as the dimention of the confusion matrix
    ax.set_xticks(list(range(15)))
    ax.set_yticks(list(range(15)))
    # Rotating the tick labels and seting their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    # Looping over data dimensions
    for i in range(15):
        for j in range(15):
            # Creating text annotations
            text = ax.text(j, i, m[i, j], ha="center", va="center", color="w")
    # Creating a title to the confusion matrix plot
    ax.set_title("Confusion matrix for the trained model")
    fig.tight_layout()
    showTheFigures = input("Show the figures? type (y) for y else (n) ")
    if showTheFigures is "y":
        plt.show()
    torch.save(model.state_dict(), 'Models/trained_cnn_model')