import abc
from heapq import heappush, heappop
import collections
import numpy as np
from itertools import combinations, islice

# CALCULATE THE DISTANCE BETWEEN TWO OBJECT
def euclidean_distance(a, b):
    return np.sqrt(np.sum(np.square(a - b)))

    
     #return object which is far away from current routing object
    
def far_away_object(entries, current_routing_entry, d):
    
    
    if current_routing_entry is None or any(e.distance_to_parent is None for e in entries):  
        return select_two_more_apart(entries,current_routing_entry,d) # if current obj is none we call non_current_obj
    
    new_entry = max(entries, key=lambda e: e.distance_to_parent)
    return (current_routing_entry.obj, new_entry.obj)
     
    
    # select two obj which are furthest apart (because there was no current routing obj)
    
def select_two_more_apart(entries, unused_current_routing_entry, d): 
    
    
    objs = map(lambda e: e.obj, entries)
    return max(combinations(objs, 2), key=lambda two_objs: d(*two_objs))
    

    #return two set of entries rooting to both object
def to_which_entry(entries, routing_object1, routing_object2, d):
    
    separations = (set(), set())
    for entry in entries:
        separations[d(entry.obj, routing_object1) >d(entry.obj, routing_object2)].add(entry)

    if not separations[0] or not separations[1]:
        separations = (set(islice(entries, len(entries)//2)), set(islice(entries, len(entries)//2, len(entries))))

    return separations

     #construction of Mtree only distance function must be 
     #given others are optional
     #d is distance
    
class MTree(object): 
    def __init__(self,d,size_of_node=20,representer_in_parent=far_away_object,separations=to_which_entry):
        
        self.size = 0
        self.root = LeafNode(self)
        self.current_index=0
        self.d = d
        self.size_of_node = size_of_node
        self.representer_in_parent = representer_in_parent
        self.separations = separations
    
    def __len__(self): return self.size
    def add(self, obj, index):#add an object into mtree
        self.root.add(obj, index)
        self.size += 1
        

    def add_all(self, iterable): #add all element in Tree
        for obj in iterable:
            self.add(obj, self.current_index)
            self.current_index+=1

            # the search method receive k for how many obj to be
            #returned and q_obj for object to be referred to.
            #Return the k objects the most close to query obj(q_ojb).
    def search(self, q_obj, k): 
        
        k = min(k, len(self))
        if k == 0: return []
        priority_queue = []
        heappush(priority_queue, priority_queue_data(self.root, 0, 0))

        nn = NN(k)

        while priority_queue:
            prEntry = heappop(priority_queue)
            if(prEntry.dmin > nn.search_radius()):
                
                break
          
            prEntry.tree.search(q_obj, priority_queue, nn, prEntry.d_query)
            
        return nn.list_of_res()
    
NNEntry = collections.namedtuple('NNEntry', 'obj dmax')
      # class of Nearest Neighbars
class NN(object):
    def __init__(self, size):
        self.elems = [NNEntry(None, float("inf"))] * size
        self.dmax = float("inf")

    def __len__(self): return len(self.elems)

    #find the radius.
    def search_radius(self): return self.dmax

    def update(self, obj, dmax):
        if obj is None: #internal node
            
            self.dmax = min(self.dmax, dmax)
            
            return
        self.elems.append(NNEntry(obj, dmax))
        for i in range(len(self)-1, 0, -1):
            if self.elems[i].dmax < self.elems[i-1].dmax:
                self.elems[i-1], self.elems[i] = self.elems[i], self.elems[i-1]
            else:
                break
        self.elems.pop()

    def list_of_res(self):
        result = map(lambda entry: entry.obj, self.elems)
        return result

    def __repr__(self): return "NN(%r)" % self.elems
            

class priority_queue_data(object):
    def __init__(self, tree, dmin, d_query): #d_query represent the distance to the searched object
        self.tree = tree
        self.dmin = dmin
        self.d_query = d_query
    def __lt__(self, other): return self.dmin < other.dmin
    def __repr__(self): return "priority_queue_data(tree:%r, dmin:%r)" % (self.tree, self.dmin)
    

    #class of entry the nodes contain the instance of the class.
class Entry(object):
    def __init__(self,obj,distance_to_parent=None,radius=None,subtree=None, index=0):
        self.obj = obj
        self.distance_to_parent = distance_to_parent
        self.radius = radius
        self.subtree = subtree
        self.index=index
    def __repr__(self): return "Entry(obj: %r, dist: %r, radius: %r, subtree: %r)" % ( self.obj,self.distance_to_parent,
            self.radius,self.subtree.repr_class() if self.subtree else self.subtree)
    

      # abstract leaf of tree to keep references to mtree
class Ab_Node_of_tree(object):
    __metaclass__ = abc.ABCMeta
    def __init__(self,mtree,parent_node=None,parent_entry=None,entries=None):
        self.mtree = mtree
        self.parent_node = parent_node
        self.parent_entry = parent_entry
        self.entries = set(entries) if entries else set()
        
     # choose the first few elements
    def __repr__(self): 
        entries_str = '%s' % list(islice(self.entries, 2))
        if len(self.entries) > 2:
            entries_str = entries_str[:-1] + ', ...]'
            
        return "%s(parent_node: %s, parent_entry: %s, entries:%s)" % (
            self.__class__.__name__,
            self.parent_node.repr_class() \
                if self.parent_node else self.parent_node,self.parent_entry, entries_str)

    def repr_class(self): return self.__class__.__name__ + "()"
    
    def __len__(self): return len(self.entries)
    
    @property
    def d(self): return self.mtree.d
    def is_full(self): return len(self) == self.mtree.size_of_node
    def is_empty(self):return len(self) == 0
    def is_root(self):return self is self.mtree.root
         
        #delete entry from node
    def remove_entry(self, entry):
        self.entries.remove(entry)
          
        #add new entry to node
    def add_entry(self, entry):
        if self.is_full(): raise ValueError('Trying to add %s into a full node' % str(entry))
        self.entries.add(entry)

    #here we set the entries and their parents
    def children_and_father(self, new_entries, new_parent_entry):
        self.entries = new_entries
        self.parent_entry = new_parent_entry
        self.parent_entry.radius = self.c_radius(self.parent_entry.obj)
        self.dis_update_()
       
        # update of the distance between child and parent node
    def dis_update_(self):
        if self.parent_entry:
            for entry in self.entries:
                entry.distance_to_parent = self.d(entry.obj, self.parent_entry.obj)

         #method to add obj into this subtree       
    @abc.abstractmethod
    def add(self, obj): pass
        
        #method to calculate the radius which cover all entries of node
    @abc.abstractmethod         
    def c_radius(self, obj): pass
    @abc.abstractmethod
    def search(self, q_obj, priority_queue, nn, d_parent_query): pass
    
    # Leaf of the tree
class LeafNode(Ab_Node_of_tree):
   
    def __init__(self,mtree,parent_node=None,parent_entry=None,entries=None):

        Ab_Node_of_tree.__init__(self,mtree,parent_node,parent_entry,entries)
    def add(self, obj,index):
        distance_to_parent = self.d(obj, self.parent_entry.obj) \
            if self.parent_entry else None
        new_entry = Entry(obj, distance_to_parent,index=index)
        if not self.is_full():
            self.entries.add(new_entry)
        else:
            split(self, new_entry, self.d)
        assert self.is_root() or self.parent_node        

    def c_radius(self, obj):# compute minimal radius to cover all object
        if not self.entries:
            return 0
        else:
            return max(map(lambda e: self.d(obj, e.obj), self.entries))

        #determine if there is object in this subtree which is contained in the result
    def belong_to_results(self,q_obj,search_radius,distance_to_parent, d_parent_query):
       #looking up if there obj in subtree which belong to the result.
        if self.is_root():
            return True
        return abs(d_parent_query - distance_to_parent)\
                <= search_radius
        
    def search(self, q_obj, priority_queue, nn, d_parent_query):
        for entry in self.entries:
            if self.belong_to_results(q_obj,nn.search_radius(),entry.distance_to_parent,d_parent_query):
                distance_entry_to_q = self.d(entry.obj, q_obj)
                if distance_entry_to_q <= nn.search_radius():
                    nn.update(entry.index, distance_entry_to_q)
    
class InternalNode(Ab_Node_of_tree):# internal node of tree
    def __init__(self,mtree,parent_node=None,parent_entry=None,entries=None):
        Ab_Node_of_tree.__init__(self,mtree,parent_node,parent_entry,entries)
    
    def add(self, obj, index):     
        dist_to_obj = {}
        for entry in self.entries:
            dist_to_obj[entry] = self.d(obj, entry.obj)
            
        def find_best_entry_requiring_no_covering_radius_increase():
            valid_entries = [e for e in self.entries
                             if dist_to_obj[e] <= e.radius]
            return min(valid_entries, key=dist_to_obj.get) \
                if valid_entries else None     
        def find_best_entry_minimizing_radius_increase():
            entry = min(self.entries,key=lambda e: dist_to_obj[e] - e.radius)
            #enlarge radius so that obj is in the covering radius of e 
            entry.radius = dist_to_obj[entry]
            return entry

        entry = find_best_entry_requiring_no_covering_radius_increase() or \
            find_best_entry_minimizing_radius_increase()
        entry.subtree.add(obj, index)
        assert self.is_root() or self.parent_node

    def c_radius(self, obj):# minimal radius for the object to cover routing objs of node
        if not self.entries:
            return 0
        else:
            return max(map(lambda e: self.d(obj, e.obj) + e.radius,
                           self.entries))

    def children_and_father(self, new_entries, new_parent_entry):
        Ab_Node_of_tree.children_and_father(self,new_entries,new_parent_entry)
        for entry in self.entries:
            entry.subtree.parent_node = self

            #determine if there objs in subtree which belong to result
    def belong_to_results(self,q_obj,search_radius,entry,d_parent_query):
        if self.is_root():
            return True
        
        parent_obj = self.parent_entry.obj
        return abs(d_parent_query - entry.distance_to_parent) <= search_radius + entry.radius
            
    def search(self, q_obj, priority_queue, nn, d_parent_query):
        for entry in self.entries:
            if self.belong_to_results(q_obj,nn.search_radius(),entry,d_parent_query):
                d_entry_query = self.d(entry.obj, q_obj)
                entry_dmin = max(d_entry_query - entry.radius, 0)
                if entry_dmin <= nn.search_radius():
                    heappush(priority_queue, priority_queue_data(entry.subtree, entry_dmin, d_entry_query))
                    entry_dmax = d_entry_query + entry.radius
                    if entry_dmax < nn.search_radius():
                        nn.update(None, entry_dmax)
         
        
        
       # to split the node into two parts when node is full 
       # the parameter are the node (ex_node)to be splitted, new obj to be inserted and the distance function d
        
def split(ex_node, entry, d):
    assert ex_node.is_full()
    mtree = ex_node.mtree
    new_node = type(ex_node)(ex_node.mtree)
    all_entries = ex_node.entries | set((entry,))
    routing_object1, routing_object2 = mtree.representer_in_parent(all_entries, ex_node.parent_entry, d)
    entries1, entries2 = mtree.separations(all_entries,routing_object1,routing_object2,d)
    
    assert entries1 and entries2, "Error during split operation."
    #save old entry of the existing node, later it will be removed from the parent
    old_ex_node_parent_entry = ex_node.parent_entry
    ex_node_entry = Entry(routing_object1,
                                None,#distance_to_parent set later
                                None,#covering_radius set later
                                ex_node)    
    ex_node.children_and_father(entries1,ex_node_entry)

    new_node_entry = Entry(routing_object2, None,None,new_node)
    new_node.children_and_father(entries2, new_node_entry)
                                          
    if ex_node.is_root():
        new_root_node = InternalNode(ex_node.mtree)
        ex_node.parent_node = new_root_node
        new_root_node.add_entry(ex_node_entry)
        new_node.parent_node = new_root_node
        new_root_node.add_entry(new_node_entry)
        mtree.root = new_root_node
    else:
        parent_node = ex_node.parent_node

        if not parent_node.is_root():
            ex_node_entry.distance_to_parent = \
                d(ex_node_entry.obj, parent_node.parent_entry.obj)
            new_node_entry.distance_to_parent = \
                d(new_node_entry.obj, parent_node.parent_entry.obj)

        parent_node.remove_entry(old_ex_node_parent_entry)
        parent_node.add_entry(ex_node_entry)
        
        if parent_node.is_full():
            split(parent_node, new_node_entry, d)
        else:
            parent_node.add_entry(new_node_entry)
            new_node.parent_node = parent_node
    assert ex_node.is_root() or ex_node.parent_node
    assert new_node.is_root() or new_node.parent_node

    
    
