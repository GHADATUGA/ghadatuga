from dataset import get_strange_symbols_train_loader, get_strange_symbols_test_data
import torch as tr
import matplotlib.pyplot as plt
import torch.functional as f
from torchvision.utils import make_grid


trainLoader = get_strange_symbols_train_loader(batch_size=128)
for images, labels in trainLoader:
    plt.figure(figsize=(16, 8))
    plt.axis('off')
    plt.imshow(make_grid(images, nrow=16).permute((1, 2, 0)), cmap='viridis')
    break
x = tr.linspace(-2, 2, 100)
y = f.F.relu(x)
plt.plot(x, y, label="RELU")
x = tr.linspace(-4, 4, 100)
y = f.F.softmax(x, dim=-1)
plt.plot(x, y, label="Softmax")
plt.legend()
plt.show()
