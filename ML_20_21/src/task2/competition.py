import pandas as pd
import numpy as np
from regression import get_cleaned_wine_reviews_data, check_exists, DEFAULT_ROOT, complete_ridge_regression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import PCA


def get_regression_model():
    X, y = get_transformed_data()
    return complete_ridge_regression(X, y)


def get_transformed_data():
    data_frame = get_cleaned_wine_reviews_data()
    data_frame.reset_index(drop=True, inplace=True)
    data_frame_important = data_frame.copy()
    data_frame_important = data_frame_important.drop(
        ['country', 'region_2', 'points', 'taster_twitter_handle', 'title'],
        axis=1)
    tfidf = TfidfVectorizer(min_df=0.05,
                            max_df=0.3)
    data_frame_important['description'] = tfidf.fit_transform(data_frame.description)
    X = tfidf.fit_transform(data_frame.description).toarray()
    data_frame_important['description'] = list(X)
    description = X
    provinces = pd.get_dummies(data_frame_important.province)
    varieties = pd.get_dummies(data_frame_important.variety)
    taster_names = pd.get_dummies(data_frame_important.taster_name)
    designations = pd.get_dummies(data_frame_important.designation)
    wineries = pd.get_dummies(data_frame_important.winery)
    prices = data_frame_important.price.values
    points = data_frame.points.values
    X = np.hstack((description, provinces, prices[:, None], taster_names, varieties))
    X_prices = np.hstack((prices[:, None]))
    X_province = np.hstack((description, provinces))
    X_taster_names = np.hstack((description, taster_names))
    X_varieties = np.hstack((description, varieties))
    y = data_frame['points'].values.reshape((-1, 1))
    pca = PCA(1)
    pca.fit(X_varieties)
    _varieties = pca.transform(X_varieties)
    pca.fit(X_taster_names)
    _taster_name = pca.transform(X_taster_names)
    pca.fit(X_province)
    _province = pca.transform(X_province)
    return X, y
