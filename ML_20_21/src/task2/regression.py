import os
import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split

DEFAULT_ROOT = '../../data/wine'


class RidgeRegression:
    def __init__(self, regularisation_factor=1):
        self.regularisation_factor = regularisation_factor
        self.predictions = None
        self.W = None

    def fit(self, x_train, y_train):
        X = np.c_[np.ones((x_train.shape[0], 1)), x_train]
        X_transposed = np.transpose(X)
        X_transposed_dot_X = X_transposed @ X
        C_I = self.regularisation_factor * np.identity(X.shape[1])
        C_I[0, 0] = 0
        X_transposed_dot_X_inverse = np.linalg.inv(X_transposed_dot_X + C_I)
        self.W = (X_transposed_dot_X_inverse @ X_transposed) @ y_train

    def predict(self, X):
        X_predictor = np.c_[np.ones((X.shape[0], 1)), X]
        self.predictions = X_predictor.dot(self.W)
        return self.predictions


class RidgeRegressionBias:
    def __init__(self, learnable_bias=0.0, regularisation_factor=2, with_varying_regularisation_term=False):
        self.learnable_bias = learnable_bias
        self.regularisation_factor = regularisation_factor
        self.predictions = None
        self.W = None
        self.with_varying_regularisation_term = with_varying_regularisation_term

    def fit(self, x_train, y_train):
        X = np.c_[np.ones((x_train.shape[0], 1)), x_train]
        C_I = (1 / self.regularisation_factor) * np.identity(X.shape[1])
        if self.with_varying_regularisation_term:
            for i in range(X.shape[1]):
                variance = np.var(X[:, i], axis=0, ddof=1)
                C_I[i, i] = 1 / variance if variance else 10
        X_transposed = np.transpose(X)
        X_transposed_dot_X = X_transposed @ X
        b = np.full(y_train.shape, self.learnable_bias)
        X_transposed_dot_X_inverse = np.linalg.inv(X_transposed_dot_X + C_I)
        self.W = (X_transposed_dot_X_inverse @ X_transposed) @ (y_train - b)

    def predict(self, X):
        X_predictor = np.c_[np.ones((X.shape[0], 1)), X]
        self.predictions = X_predictor.dot(self.W)
        return self.predictions


def plot_graph(X, y, x_axis, title, with_learned_bias=False, learnable_bias=0.0):
    fig, ax = plt.subplots(figsize=(6, 6))
    model = RidgeRegressionBias(learnable_bias=learnable_bias) if with_learned_bias else RidgeRegression()
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42)
    model.fit(X_train, y_train)
    predictions = model.predict(X_test)
    ax.scatter(X_test, y_test)
    ax.plot(X_test, predictions, color='red')
    ax.set_ylabel('points')
    ax.set_xlabel(x_axis)
    fig.suptitle(title, size=10)
    return fig


def check_exists(root):
    return os.path.isfile(os.path.join(root, 'cleaned_winemag-data.csv'))


def get_cleaned_wine_reviews_data(root=DEFAULT_ROOT):
    if not check_exists(root):
        subprocess.run("python3 wine_dataset.py", shell=True)
    file_path = os.path.join(root, 'cleaned_winemag-data.csv')
    data = pd.read_csv(file_path, index_col=0)
    return data


def error_metrics(predictions, y_test):
    print("Mean squared error: %.2f" % np.square(y_test - predictions).mean())


def complete_ridge_regression(X, y):
    model = RidgeRegression()
    x_train, x_test, y_train, y_test = train_test_split(X, y, random_state=42)
    model.fit(x_train, y_train)
    predictions = model.predict(x_test)
    error_metrics(predictions, y_test)


def complete_ridge_regression_bias(X, y, learnable_bias=2, with_varying_regularization_term=False):
    model = RidgeRegressionBias(learnable_bias=learnable_bias,
                                with_varying_regularisation_term=with_varying_regularization_term)
    x_train, x_test, y_train, y_test = train_test_split(X, y, random_state=42)
    model.fit(x_train, y_train)
    predictions = model.predict(x_test)
    error_metrics(predictions, y_test)
    return model


def cross_validation_n_folds(X, y, n=5, with_bias=False, learnable_bias=0, with_varying_regularization_term=False):
    X_groups = np.vsplit(X, 5)
    y_groups = np.vsplit(y, 5)
    result = 0
    for i in range(n):
        X_test = X_groups[i]
        y_test = y_groups[i]
        X_training = np.vstack([group for j, group in enumerate(X_groups) if j != i])
        y_training = np.vstack([group for j, group in enumerate(y_groups) if j != i])
        model = RidgeRegressionBias(learnable_bias=learnable_bias,
                                    with_varying_regularisation_term=with_varying_regularization_term) if with_bias else RidgeRegression()
        model.fit(X_training, y_training)
        predictions = model.predict(X_test)
        result += np.square(y_test - predictions).mean()
    return round(result / 5, 2)


if __name__ == '__main__':
    data_frame = get_cleaned_wine_reviews_data()
    data_frame.reset_index(drop=True, inplace=True)
    data_frame_important = data_frame.copy()
    data_frame_important = data_frame_important.drop(
        ['country', 'region_2', 'points', 'taster_twitter_handle', 'title'],
        axis=1)
    print("________________________________________________________________________________________")
    print("Ignoring terms that appear in less than 5% of samples, which reduces the feature space")
    tfidf = TfidfVectorizer(min_df=0.05,
                            max_df=0.3)
    print("________________________________________________________________________________________")
    print("Transforming String values to vector values")
    data_frame_important['description'] = tfidf.fit_transform(data_frame.description)
    X = tfidf.fit_transform(data_frame.description).toarray()
    data_frame_important['description'] = list(X)
    description = X
    print("________________________________________________________________________________________")
    print("Encoding the provinces and varieties")
    provinces = pd.get_dummies(data_frame_important.province)
    varieties = pd.get_dummies(data_frame_important.variety)
    taster_names = pd.get_dummies(data_frame_important.taster_name)
    designations = pd.get_dummies(data_frame_important.designation)
    wineries = pd.get_dummies(data_frame_important.winery)
    print("________________________________________________________________________________________")
    print("Stacking the matrices")
    prices = data_frame_important.price.values
    points = data_frame.points.values
    X = np.hstack((description, provinces, prices[:, None], taster_names, varieties))
    print("________________________________________________________________________________________")
    print("Creating the variables to train the model")
    X_prices = np.hstack((prices[:, None]))
    X_province = np.hstack((description, provinces))
    X_taster_names = np.hstack((description, taster_names))
    X_varieties = np.hstack((description, varieties))
    y = data_frame['points'].values.reshape((-1, 1))
    print("________________________________________________________________________________________")
    print("Creating a PCA instance")
    pca = PCA(1)
    fig_plot = plot_graph(X_prices, y, x_axis='price', title='Ridge Regression model fits for Prices')
    print("________________________________________________________________________________________")
    print("Fitting the model for the varieties")
    pca.fit(X_varieties)
    print("________________________________________________________________________________________")
    print("Transforming the data")
    _varieties = pca.transform(X_varieties)
    plot_graph(_varieties, y, x_axis='varietes', title='Ridge Regression model fits for varietes')
    print("________________________________________________________________________________________")
    print("Fitting the model for the taster_names")
    pca.fit(X_taster_names)
    _taster_name = pca.transform(X_taster_names)
    plot_graph(_taster_name, y, x_axis='taster_name', title='Ridge Regression model fits for taster_name')
    print("________________________________________________________________________________________")
    print("Fitting the model for the provinces")
    pca.fit(X_province)
    _province = pca.transform(X_province)
    plot_graph(_province, y, x_axis='province', title='Ridge Regression model fits for province')
    print("________________________________________________________________________________________")
    print("Computing MSE of Ridge Regression",
          "for columns description, province, price,",
          "taster_name, variety", sep='\n')
    complete_ridge_regression(X, y)
    print("________________________________________________________________________________________")
    print("Cross validation")
    print(cross_validation_n_folds(X, y))
    command = input("Show the plots? (y[n]) ")
    if command == 'y':
        plt.show()
    plt.close('all')
    plot_graph(X_prices, y,
               x_axis='price',
               title='Ridge Regression with learnable bias model fits for Prices',
               with_learned_bias=True,
               learnable_bias=.5)
    print("________________________________________________________________________________________")
    print("Fitting the model with learnable bias for the varieties")
    pca.fit(X_varieties)
    _varieties = pca.transform(X_varieties)
    plot_graph(_varieties, y,
               x_axis='varieties',
               title='Ridge Regression with learnable bias model fits for varieties',
               with_learned_bias=True,
               learnable_bias=.5)
    print("________________________________________________________________________________________")
    print("Fitting the model with learnable bias for the taster_names")
    pca.fit(X_taster_names)
    _taster_name = pca.transform(X_taster_names)
    plot_graph(_taster_name, y,
               x_axis='taster_name',
               title='Ridge Regression with learnable bias model fits for taster_name',
               with_learned_bias=True,
               learnable_bias=.5)
    print("________________________________________________________________________________________")
    print("Fitting the model with learnable bias for the provinces")
    pca.fit(X_province)
    _province = pca.transform(X_province)
    plot_graph(_province, y,
               x_axis='province',
               title='Ridge Regression with learnable bias model fits for province',
               with_learned_bias=True,
               learnable_bias=.5)
    print("________________________________________________________________________________________")
    print("Computing MSE of Ridge Regression with learnable bias",
          "for columns description, province, price,",
          "taster_name, variety", sep='\n')
    complete_ridge_regression_bias(X, y, learnable_bias=.8)
    print("________________________________________________________________________________________")
    print("Cross validation with learnable bias")
    print(cross_validation_n_folds(X, y, with_bias=True, learnable_bias=.8))
    command = input("Show the plots? (y[n]) ")
    if command == 'y':
        plt.show()
    plt.close('all')
    print("________________________________________________________________________________________")
    print("Computing MSE of Ridge Regression with learnable bias and varying regularization term",
          "for columns description, province, price,",
          "taster_name, variety", sep='\n')
    complete_ridge_regression_bias(X, y, learnable_bias=.8, with_varying_regularization_term=True)
    print("________________________________________________________________________________________")
    print("Cross validation with learnable bias and varying regularization term")
    print(cross_validation_n_folds(X, y, with_bias=True, learnable_bias=.8, with_varying_regularization_term=True))
    print('[END PROCESS]')
