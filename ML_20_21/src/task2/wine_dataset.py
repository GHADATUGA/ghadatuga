import errno
import hashlib
import os
import shutil
import tempfile
import urllib.request
import zipfile
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import nltk

DEFAULT_ROOT = '../../data/wine'

URL = r'http://ml.cs.uni-kl.de/download/wine-reviews.zip'
CHECKSUM = 'c7fa81ba06ed48f290463fbf3bfff4229b68fce8aa94d6a200e1e59002e9a83c'
BUFFERSIZE = 16 * 1024 * 1024


def check_exists(root):
    return os.path.isfile(os.path.join(root, 'winemag-data-130k-v2.csv'))


def download(root=DEFAULT_ROOT):
    if check_exists(root):
        return

    # download files
    try:
        os.makedirs(root)
    except OSError as e:
        if e.errno == errno.EEXIST:
            pass
        else:
            raise

    print(f'Downloading "{URL}"...')
    with tempfile.TemporaryFile() as tmp:
        with urllib.request.urlopen(URL) as data:
            shutil.copyfileobj(data, tmp, BUFFERSIZE)
        tmp.seek(0)

        print('Checking SHA-256 checksum of downloaded file...')
        hasher = hashlib.sha256()
        while True:
            data = tmp.read(BUFFERSIZE)
            if len(data) == 0:
                break
            hasher.update(data)
        if hasher.hexdigest() == CHECKSUM:
            print('OK')
        else:
            print('FAILURE!')
            raise RuntimeError('The SHA-256 Hash of the downloaded file is not correct!')

        print('Extracting data...')
        with zipfile.ZipFile(tmp, 'r') as zip:
            zip.extract('winemag-data-130k-v2.csv', path=root)
        print('Done!')


def get_wine_reviews_data(root=DEFAULT_ROOT):
    if not check_exists(root):
        download(root)
    file_path = os.path.join(root, 'winemag-data-130k-v2.csv')
    data = pd.read_csv(file_path, index_col=0)
    return data


# Extract year from the title
def extract_year(title):
    for part in title.split(" "):
        part = part.strip(',. ')
        if part.isnumeric() and int(part) < 2020 and len(part) == 4:
            return int(part)
    return None


# Extract the vintage from the title of each row
def extract_years(column_title):
    years = []
    for title in column_title:
        years += [extract_year(title)]
    return years


# Add the extracted vintages as a new column
def append_vintage(data):
    data = data.assign(year=lambda x: extract_years(x['title']))
    return data


# Plot the distribution of values in a column with a possible keyword argument maximum
def plot_count(data, column, maximum=None, rotate=False):
    plt.subplots(figsize=(8, 8))
    if max:
        if rotate:
            sns.countplot(y=column, data=data, order=data[column].value_counts().iloc[:maximum].index, color=None)
        else:
            sns.countplot(x=column, data=data, order=data[column].value_counts().iloc[:maximum].index, color=None)
    else:
        if rotate:
            sns.countplot(y=column, data=data, order=data[column].value_counts().index, color=None)
        else:
            sns.countplot(x=column, data=data, order=data[column].value_counts().index, color=None)
    sns.despine()
    plt.show()


if __name__ == '__main__':
    download()
    print('_____________________________________________________________________')
    nltk.download('stopwords')
    print('_____________________________________________________________________')
    print("Extracting the year from the title and appending it as a new column")
    data_frame = get_wine_reviews_data()
    data_frame = append_vintage(data_frame)
    print('_____________________________________________________________________')
    while True:
        # Menu to choose which data to plot
        print('1) Plot the histogram of vintages',
              '2) Plot the histogram of prices',
              '3) Plot the histogram of points',
              '4) Plot 16 most occurring countries',
              '5) Plot taster name',
              '6) Plot 40 most occurring provinces',
              '7) Plot 15 most occurring designations',
              '8) Plot 20 most occurring wineries',
              '9) Plot 20 most occurring varieties',
              '10) Plot 20 most occurring region_1',
              '11) Plot 20 most occurring region2',
              '12) Plot 20 most occurring taster_twitter_handle',
              '0) Next',
              sep='\n')
        command = input()
        if command == '1':
            # Plot year
            fig, ax = plt.subplots(figsize=(8, 8))
            sns.histplot(data_frame['year'], bins=500)
            ax.set(xlim=(1990, 2020))
            ax.set(ylabel="density")
            sns.despine()
            plt.show()
        elif command == '2':
            # Plot Price
            fig, ax = plt.subplots(figsize=(8, 8))
            sns.histplot(data_frame['price'], bins=500)
            ax.set(xlim=(0, 250))
            ax.set(ylabel="density")
            sns.despine()
            plt.show()
        elif command == '3':
            # Plot Points
            plot_count(data_frame, 'points')
        elif command == '4':
            # Plot 16 most occurring countries
            plot_count(data_frame, 'country', maximum=16, rotate=True)
        elif command == '5':
            # Plot taster name
            plot_count(data_frame, 'taster_name', rotate=True)
        elif command == '6':
            # Plot 40 most occurring provinces
            plot_count(data_frame, 'country', maximum=40, rotate=True)
        elif command == '7':
            # Plot 15 most occurring designations
            plot_count(data_frame, 'designation', maximum=40, rotate=True)
        elif command == '8':
            # Plot 20 most occurring wineries
            plot_count(data_frame, 'winery', maximum=40, rotate=True)
        elif command == '9':
            # Plot 20 most occurring varieties
            plot_count(data_frame, 'variety', maximum=20, rotate=True)
        elif command == '10':
            # Plot 20 most occurring region_1
            plot_count(data_frame, 'region_1', maximum=20, rotate=True)
        elif command == '11':
            # Plot 20 most occurring region2
            plot_count(data_frame, 'region_2', maximum=20, rotate=True)
        elif command == '12':
            # Plot 20 most occurring taster_twitter_handle
            plot_count(data_frame, 'taster_twitter_handle', maximum=20, rotate=True)
        elif command == '0':
            break
    print('_____________________________________________________________________')
    print('Plotting the distribution of missing values.')
    x = data_frame.isnull().sum()
    x.plot.bar(rot=90)
    plt.show()
    print('_____________________________________________________________________')
    print('Now we can begin the process of cleaning the data and missing values.')
    # Impute price by mean
    print('First imputing price by mean')
    data_frame['price'] = data_frame['price'].fillna(data_frame['price'].mean())
    # Impute designation by most frequent
    print('First imputing designation by most frequent')
    data_frame['designation'] = data_frame['designation'].fillna('Reserve')
    # Impute region_1 by most frequent regions
    print('First imputing region_1 by most frequent regions')
    fill_list = ['Napa Valley', 'Columbia Valley (WA)']
    data_frame['region_1'] = data_frame['region_1'].fillna(pd.Series(np.random.choice(fill_list,
                                                                                      size=len(data_frame.index))))
    # Impute region_2 by most frequent regions
    print('Imputing region_2 by most frequent regions')
    fill_list = ['Central Coast', 'Sonoma', 'Central Coast']
    data_frame['region_2'] = data_frame['region_2'].fillna(pd.Series(np.random.choice(fill_list,
                                                                                      size=len(data_frame.index))))
    # Impute taster_name by most frequent regions
    print('Imputing taster_name by most frequent regions')
    data_frame['taster_name'] = data_frame['taster_name'].fillna('Roger Voss')
    # Impute taster_name_handle by most frequent regions
    print('Imputing taster_name_handle by most frequent regions')
    data_frame['taster_twitter_handle'] = data_frame['taster_twitter_handle'].fillna('@vossroger')
    # Impute year by most frequent years
    print('Imputing year by most frequent years')
    fill_list = [2013.0, 2012.0, 2014.0]
    data_frame['year'] = data_frame['year'].fillna(pd.Series(np.random.choice(fill_list,
                                                                              size=len(data_frame.index))))
    # Computing how many rows still have missing values after the clean up
    print('How many values are still missing')
    print(data_frame.isnull().sum())
    # Dropping rows with missing values
    print('Dropping rows with missing values')
    data_frame = data_frame.dropna()
    print('_____________________________________________________________________')
    # Remove duplicate descriptions
    print("Removing duplicate in the column descriptions")
    data_frame = data_frame.drop_duplicates("description")
    # Make important columns lower case
    print("Making columns with text values lower case")
    data_frame["designation"] = data_frame["designation"].str.lower()
    data_frame["province"] = data_frame["province"].str.lower()
    data_frame["region_1"] = data_frame["region_1"].str.lower()
    data_frame["variety"] = data_frame["variety"].str.lower()
    data_frame["winery"] = data_frame["winery"].str.lower()
    data_frame["taster_name"] = data_frame["taster_name"].str.lower()
    # variety name shouldn't occur in the description, so add it to the list of stop words
    print("Removing redundant and unimportant values")
    stopWords_variety = set()
    for wine_variety in data_frame.variety:
        wine_variety = wine_variety.split()
        for word in wine_variety:
            stopWords_variety.add(word)
    variety_list = sorted(stopWords_variety)
    extras = ['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', 'cab', "%", "that's"]
    stopwords = set(nltk.corpus.stopwords.words('english'))
    stopwords.update(variety_list)
    stopwords.update(extras)
    data_frame["description"] = data_frame["description"].apply(lambda x: x.lower())
    data_frame["description"] = data_frame["description"].apply(
        lambda x: ' '.join([word for word in x.split()
                            if word not in stopwords]))
    print('_____________________________________________________________________')
    print('Plotting the distribution of after adding missing values.')
    x = data_frame.isnull().sum()
    x.plot.bar(rot=90)
    plt.show()
    print('_____________________________________________________________________')
    # Making a statistic of the entire dataset
    statistics = data_frame.describe(include='all')
    print(statistics)
    command = input('Do you want to save the statistic? (y/[n]) ')
    if command == 'y':
        print('Saving the processed data in stats_cleaned_dataset.csv')
        data_frame.describe(include='all').to_csv(os.path.join(DEFAULT_ROOT, 'stats_cleaned_dataset.csv'), header=True, index=True)
    command = input('Do you want to save the statistic of numerical data? (y/[n]) ')
    if command == 'y':
        print('Saving the processed data in stats_cleaned_dataset.csv')
        data_frame.describe().to_csv(os.path.join(DEFAULT_ROOT, 'numerical_stats_cleaned_dataset.csv'), header=True, index=True)
    command = input('Do you want to save the cleaned data? (y/[n]) ')
    if command == 'y':
        print('Saving the processed data in cleaned_winemag-data.csv')
        data_frame.to_csv(os.path.join(DEFAULT_ROOT, 'cleaned_winemag-data.csv'), header=True, index=True)
    print('[END PROCESS]')
