# Submitting Solutions

This document contains detailed instructions on how to submit your solutions.

## Formalities
* We expect your submission to be complete in the sense that it contains an answer to every question from the task sheet.
* You don't have to upload your code or report to any website. Instead, you should push everything to your git repository.
* Every task sheet contains a deadline date. We will only consider the last commit on your repository's `master` branch before the deadline date at 23:59.
* All files relevant for your submission should be in the corresponding task's folder, so that we can easily find them. Exceptions to this rule might be a package of Python utility functions that you want to reuse in different tasks or a LaTeX header file containing macros and definitions.

## Report
You must submit a report together with the code for every task. The goal is for you to present your work and for us to understand what you did. Usually, it should be clear from the task what needs to be included in the report. For example, if it says "plot XXX and YYY" in the assignment text, then you should include and explain those plots in your report. Sometimes, we also ask questions that don't require any coding and should test your understanding of the concepts instead. Your answers to those questions should also be present in the report.

A few more things to consider:
* The report **must** be written in LaTeX.
* Your submission must contain the report's LaTeX source files as well as the compiled .pdf file.
* Make sure to write your report properly, imagine you are writing a scientifc report for a conference, put effort in.
* It can be written in English or in German, though we prefer English.

If you are unsure how to write a report, [this article](https://www.skillsyouneed.com/write/report-writing.html) offers some general guidelines. As far as style is concerned, we don't want to impose any hard restrictions, as long as it still looks decent (no Comic Sans or any other equally horrific abominations, of course). However, we invite you to use the template that comes with the first task.

## Code
First and foremost, your code should work correctly and do the job that it is supposed to do. To make sure that this is the case, we will inspect your code and we might even run and try it. Therefore, it is important that we can exactly reproduce any results that you decided to include in your report. Please include a README file with exact instructions on how to do just that. The [sacred library](https://sacred.readthedocs.io/en/stable/) is a powerful tool that can help you with that.

Some more hints:
* We use Python 3.6 on our test machine. So if you use any feature that was introduced in later versions, like the walrus operator, it will not work on our test machine. Also be aware that the behaviour of built-in functions and objects can change between Python versions. For example, prior to Python 3.7, it was not guaranteed that items in a `dict` would be iterated over in the same order they were inserted.
* If you use any third-party libraries, please include instructions on how to install them, for example a `requirements.txt` file for `pip`.

## Feedback
We will review your code and report some time after you have submitted them. Our feedback will be posted as an issue in your Gitlab repository.

If we don't understand parts of your submission, we might ask you to explain them in a video conference. In such a case, we will contact you beforehand, so that we can agree on a date. All members of your group should participate in that meeting.

If we find that your submission is incomplete or doesn't meet our minimum requirements, we will ask you to add the missing parts or improve them. Usually, you will have one week for that.
