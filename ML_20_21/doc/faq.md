# FAQ
This document contains answers to questions that we thought might come to your mind at some point.

## Passing the Project
* What do I need to do to pass the project?
    - You have to submit acceptable solutions for all three tasks (see [Submission](submission.md)) and present your results at the end of the semester (see below).
* How do you determine the final grade for each group?
    - This project is not going to be graded. The only two alternatives are "pass" or "fail".
* Do the groups pass/fail as a whole? Or is it possible for some members to fail while others pass the project?
    - In general, you should solve all tasks as a group and therefore we will also judge your performance as a group.
    - However, if we are unsure about some of your solutions, we may ask you to explain them in a video conference. Depending on your explanations and participation in that meeting, we might choose to pass/fail group members individually.

## Groups
* Which group size do you recommend?
    - We only allow groups of two or three people to participate in the project.
* Is it possible to change groups during the project?
    - No, the groups are going to be locked after the 3rd November. If you have a really good reason, e.g., your fellow group members dropped the project, please contact us and we will find a solution.
    
## Competitions
* Why do you include competitions as part of the tasks?
    - We want to motivate you to work harder and delve deeper into the material than it would be necessary to pass the project. Additionally, competitions have been an important motor of innovation in the machine learning research community, see e.g. [Kaggle](https://www.kaggle.com/competitions).
* How do I win?
    - Each sheet contains one or more competitions that are worth a certain number of points. Each group will be ranked according to the rules stated in the competition and receive the corresponding amount of points. The group with the highest amount of total points, summed over all competitions on a sheet, wins.
* How does the point system work?
    - The number that is written next to the competition is the max. amount of points for that competition, i.e., the amount of points that the group in first place will receive.
    - The first eight teams will receive points according to the following formula:
      Round(max_points * [1, 0.72, 0.6, 0.48, 0.4, 0.32, 0.24, 0.14])
    - For example, if the max. amount of points is 15 for a competition, the follwoing points will be awarded from 1st to 8th place: 15, 11, 9, 7, 6, 5, 4, 2
    - The max. reachable points in each sheet are 25.
* What kind of prize does the winner receive?
    - You can choose between a crate of beer, a bottle of wine or a box of chocolate. If you don't like any of those, we'll figure something out.
* Do I need to participate in the competitions to pass the project?
    - No.

## Debriefings
* Will we get the chance to ask questions in a live session?
    - Yes, there will be a debriefing in the form of a video conference some time after we have reviewed everyone's solutions to a task.
* What are the debriefings going to be about?
    - We will recap the last task and summarise the most important points that you should take away from it. The results of the competitions will also be revealed.
    - You can ask questions about the next task or the feedback that you recieved on the last one.
    - You'll also have the opportunity to tell us what you liked and didn't like about the last task, we are very interested in hearing your feedback!
* When are the debriefings going to take place?
    - This is not set in stone. Since we don't need to book a room, we are actually quite flexible and will probably set up a poll to determine a date. Once a date is fixed, it will also be broadcast via Mattermost and OLAT.
* Do I need to participate in the debriefings to pass the project?
    - No.
* I missed one of the debriefings but I would like to know what happend. What can I do?
    - We plan to record the debriefing sessions and upload the videos to OLAT. Our Presentation slides will also be upload.

## Final Presentations
* What should we include in the final presentation?
    - Explain your solutions to your fellow students and why you came with them. Was there anything you found particularly interesting or was there a problem that you thought was really hard? How did you reach 2nd place in the competition on task 1?
* Are there any formal guidelines that we need to follow?
    - Your presentation should be 15 minutes long.
    - We will provide a LaTeX template for the sildes, but you are also free to use any other software to create your presentation.
* Are the presentations going to be in person at the university or are they going to be online?
    - We would prefer to have the presentations in person, but depending on the Covid situation that might not be possible. At the moment, online presentations are the most likely case. We will inform you as soon as we have made our decision.
* When are the final presentations going to take place?
    - We don't know that yet. Most likely end of February or beginning of March.
* Do I need to participate in the final presentation to pass the project?
    - Yes.
