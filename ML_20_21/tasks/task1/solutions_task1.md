# Task1 

## d)

Accuracy is probably the best known approach to measure the performance of a classifier. 
If you have balanced data, this is actually a good approach. 
However, if the data is unbalanced, e.g. 90% of the images belong to class 1 and the classifier always returns class 1,
the accuracy is 90% even though the classifier is not "smart".
So accuracy overestimates the performance,
so that especially with unbalanced data you have to calculate additional performance metrics.

