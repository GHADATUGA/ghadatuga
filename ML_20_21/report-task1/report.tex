\documentclass[12pt]{article}
\input{header}
\addbibresource{references.bib}
\setlength{\abovecaptionskip}{0pt}

\title{Machine Learning (Project)\\Task 1 Report}
\author{Mohamed Ghazal \\ m\_ghazal19@cs.uni-kl.de\and
	Nico Müller\\ muelleni@rhrk.uni-kl.de\and
	Ghad Niyomwungeri\\
	niyomwun@rhrk.uni-kl.de}
\date{\today}
\begin{document}
\maketitle
\section{\textit{k}-Nearest Neighbour Classification}
Classical k-nearest neighbour (\textit{k-NN}) is perhaps one of the simplest algorithms to accomplish the classification task. Given a training dataset $Dtr := \{(\mathbf{x}_i, y_i)\}^n_{i=1} \subseteq \mathcal{X} \times \mathcal{Y}$, we assign a label to an unseen test point $\mathbf{x}_{te}\in \mathbfcal{X}$ by finding the $k\in N$ closest training points to it and choosing the most frequent label from those points.
Formally, we can define the set of nearest neighbours as:
\begin{equation}
    \mathcal{N} (\mathbf{x}_{te}, k) := \underset{\{(\mathbf{x}_{i_1},y_{i_1}),...,(\mathbf{x}_{i_k},y_{i_1})\}
    \subseteq D_{tr}}
    {\text{arg min}}
    \sum_{l=1}^{k} d(\mathbf{x}_{te}, \mathbf{x}_{il}),
\end{equation}
where $d:\mathcal{X}\times\mathcal{X}\rightarrow \mathbb{R}^+_0$ is a distance function, quantifying the closeness of datapoints in $\mathbfcal{X}$. This allows us to predict the label of $\mathbf{x}_{te}$ as
\begin{equation}
    y_{te} =\hat{f}(\mathbf{x}_{te}):=\underset{y'\in\mathcal{Y}}
    {\text{arg max}} |\{(\mathbf{x}, y)\in\mathcal{N}(\mathbf{x}_{te},k)|y=y'\}|.
\end{equation}
\subsection{Getting Started}
First I wanted to get a feeling of the data. I started with a simple plot of each class (see \cref{fig:classes}) and recognized that these are symbols.

\begin{figure}[H]
\begin{center}
    \includegraphics[width=0.8\textwidth]{figures/examplePlotClasses.png}
    \caption{A figure that shows each class of the dataset.}
    \label{fig:classes}
    \end{center}
\end{figure}


\subsection{Setting up the KNN Classifier}

One KNN Classifier has it`s own class "KNN". The constructor initializes both the number of neighbors k and the distance function. "KNN" is divided into two parts, called "prediction" and "fit". The classifier is trained in the "fit" function. Here (in the simplest case) the training data with their corresponding labels are only stored.

\begin{lstlisting}[language=iPython]
    def fit(self, X, y):
    
        self.train_x = X
        self.train_y = y
\end{lstlisting}
In the "predict" function, the corresponding labels are "predicted" for the given test data. 
For this purpose you first put the test and training data into two-dimensional shape for easier calculation.
Then a "Distance Matrix" is created, which calculates the distance between a test image and all training images using a distance function and stores these distances in rows in the distance matrix.
After that we iterate over all test images and sort the distances. Out of the k-closest images, the class in majority is assigned to that new test image.

\begin{lstlisting}[language=iPython]
    def predict(self, X):
  
        predictions = []
        nof_classes = np.amax(self.train_y) + 1
        nof_test_vectors = X.shape[0]
        nof_train_vectors = self.train_x.shape[0]

        test_x = np.reshape(X,(3000, 28*28))
        train_x = np.reshape(self.train_x,(12000, 28*28))

        dists = self.dist_function(test_x, train_x)

        for i in range(nof_test_vectors):
            votes = np.zeros(nof_classes, dtype=np.int)
            for neighbor_index in np.argsort(dists[i, :])[:self.k]:
                    neighbor_label = self.train_y[neighbor_index]
                    votes[neighbor_label] += 1

            predictions.append(np.argmax(votes))

        return predictions
\end{lstlisting}
we have used \emph{cross\_validation} function to evaluate the performance of our model, where we have decided to train our model using 5 \emph{folds}. we have called \emph{mfold} function in order to split the dataset into those 5 \emph{folds}. \emph{Test\_mask} and \emph{test\_indix} methods hold hand in hand to generate the indices of cross validation.
\begin{lstlisting}[language=iPython]
   def cross_validation(clf, X, Y, m=5, metric=accuracy):
   avg_metric=0
    
    for train_index, test_index in mfold(X,n_splits=m):
        x_train,y_train, x_test, y_test=X[train_index],
        Y[train_index], X[test_index],Y[test_index]
        clf.fit(x_train,y_train)
        y_pred=clf.predict(x_test)
        avg_metric+=metric(y_pred,y_test)
    return avg_metric/m
   
    #split int 5 folds
def mfold(X,random_state=None,n_splits=5,y=None):
        #generate the indices
    def test_mask(X,y, n_splits):
        for index in test_index(X,y, n_splits):
            mask=np.zeros(X.shape[0], dtype=np.bool)
            mask[index]=True
            yield mask
    def test_index(X=None, y=None, n_splits=5):
        nbr_samples=X.shape[0]
        indices=np.arange(nbr_samples)
        fold_sizes=np.full(n_splits,nbr_samples//n_splits,
        dtype=np.int)
        fold_sizes[:nbr_samples%n_splits]+=1
        current=0
        for fold_size in fold_sizes:
            start, stop=current, current+fold_size
            yield indices[start:stop]
            current=stop
    nbr_samples=X.shape[0]
    indices=np.arange(nbr_samples)
    for index in test_mask(X,y, n_splits):
        train=indices[np.logical_not(index)]
        test=indices[index]
        yield train, test
    
    

    
\end{lstlisting}

\subsection{Distance function}
\subsubsection{Euclidean distance}

In the first run we used the Euclidean distance. As already mentioned, a distance matrix was created, more precisely the "Euclidean Distance Matrix"."The basic concept is that it represents a table in which the rows are “source” entities and columns are “target” entities upon which you want to calculate a distance (in euclidean way)." \autocite{euclideandistancematrix} 
In brief, the distance matrix can be divided into three components: 
\begin{Large}
    \begin{equation}
      D^2 = (||a_i||^2_2|1|)+(||b_i||^{2T}_2|1|)-2AB^T 
    \end{equation}
\end{Large}
\noindent
Here "D" corresponds to the distance matrix and "A" or "B" is the training or test matrix.
Or in code:

\begin{lstlisting}[language=iPython]
def euclidean_distance(a, b):

    part1 = np.sum (a**2, axis=1)[:, np.newaxis]
    part2 = np.sum(b**2, axis =1)
    part3 = -2 * np.dot(a, b.T)

    return np.round(np.sqrt(part1+part2+part3),2)
\end{lstlisting}

This is a very fast and efficient way to compute a Distance Matrix.
\newline
\vspace{.3cm}

\textbf{Accuracy Euclidean distance}
\vspace{.3cm}
\noindent

For a simple algorithm like KNN, the results are pretty good. As you can see in  \cref{fig:euclideanAccuracy}) the accuracy of the classifier using the Euclidean distance is 0.84-0.865.
\begin{figure}[H]
\begin{center}
    \includegraphics[width=0.8\textwidth]{figures/accuracyPlotEuclidean.png}
    \caption{This figure shows the accuracy of the KNN classifier for different k using the euclidean distance.}
    \label{fig:euclideanAccuracy}
    \end{center}
\end{figure}
\noindent
This raises the question whether the corresponding accuracy value is a good
estimate for the performance of the classifier on fresh, unseen data?
\\
Accuracy is probably the best known approach to measure the performance of a classifier. 
If you have balanced data, this is actually a good approach. 
However, if the data is unbalanced, e.g. 90\% of the images belong to class 1 and the classifier always returns class 1,
the accuracy is 90\% even though the classifier is not "smart".
So accuracy overestimates the performance,
so that especially with unbalanced data you have to calculate additional performance metrics.


\subsubsection{Manhattan distance}


The second distance function is the \emph{Manhattan distance}, also known as 'city block distance'. It is calculated in the way that we sum up the differences (in absolute values) of coordinates. This is the formula once we consider two dimensional points \emph{$(x_1,y_1)$} and \emph{$(x_2,y_2)$} :\newline


\hspace{5cm} \textbf{$|x_1-x_2|+|y_1-y_2|$}
\\
\newline\begin{lstlisting}[language=iPython]
from scipy.spatial.distance import cdist
def manhattan_distance(a, b):
    return cdist(a, b, metric='cityblock')
\end{lstlisting}
\vspace{.3cm}
\textbf{Accuracy of manhattan distance}
\vspace{.3cm}


\noindent
The \textbf{Figure \ref{fig:manhat}} represents the acccuracy graph obtained once the KNN executed using Manhattan distance. The hightest accuracy is at \emph{k=5} and lowest at \emph{k=2}, around $85.6\%$ and $83.4\%$ respectively.
\begin{figure}[H]
\begin{center}
    \includegraphics[width=0.7\textwidth]{figures/Accuracy_Plot(Manhattan).png}
    \caption{This figure shows the accuracy of the KNN classifier for different k using the manhattan distance.}
    \label{fig:manhat}
    \end{center}
\end{figure}
If you are interested in seeing the exact values in all $k\in \{1, \cdots ,10\}$, \newline look at \textbf{Figure \ref{fig:acc_manhat}}
 \begin{figure}[H]
\begin{center}
    \includegraphics[width=0.8\textwidth]{figures/Accuracy(Manhattan).png}
    \caption{This figure shows the accuracies at all k when using manhattan distance.}
    \label{fig:acc_manhat}
    \end{center}
\end{figure}

\subsubsection{chebyshev distance}

chebyshev between two vectors is calculated in the way that, we make the absolute distance between \emph{$x_s$-coordinates}, \emph{$y_s$-coordinate} etc depending on dimension, then we pick the biggest value. This is formula once we consider two dimensional points \emph{$(x_1,y_1)$} and \emph{$(x_2,y_2)$} :\newline


\hspace{5cm} \textbf{max($|x_1-x_2|,|y_1-y_2|$)}
\newline\begin{lstlisting}[language=iPython]
def chebyshev_distance(a,b):
    return np.max(np.abs(np.subtract(a , b)))
\end{lstlisting}
\vspace{.3cm}
\textbf{Accuracy of chebyshev distance}
\vspace{.2cm}
\newline
The \textbf{Figure \ref{fig:chebyshev}} represent the acccuracy graph obtained once the KNN executed using chebyshev distance. The hightest accuracy was at \emph{K=5} and lowest at \emph{K=2}, around $81\%$ and $79\%$ respectively.
\begin{figure}[H]
\begin{center}
    \includegraphics[width=0.8\textwidth]{figures/graph_chebyshev.PNG}
    \caption{This figure shows the accuracy of the KNN classifier for different k using the chebyshev distance.}
    \label{fig:chebyshev}
    \end{center}
\end{figure}
 If you are interested in seeing the exact values in all $k\in \{1, \cdots ,10\}$, \newline look at \textbf{Figure \ref{fig:acc_chev}}
 \begin{figure}[H]
\begin{center}
    \includegraphics[width=0.8\textwidth]{figures/accuracy_chebyshev.PNG}
    \caption{This figure shows the accuracies at all k when using chebyshev distance.}
    \label{fig:acc_chev}
    \end{center}
\end{figure}
\subsection{The use of Mtree}

In this section we have implemented an index-structure \emph{M-tree} in file called \textbf{\emph{mtree\_code.py}} and inserted all the training data. We first initialized our \emph{Mtree} code with \textit{euclidean distance}  and then added the training data as you can see it in \textbf{\emph{mtree\_main.py}} code
\begin{lstlisting}[language=iPython]
   def fit(self, X, y):
        self.X_train=X
        self.labels_train=y
        
        
         #initialisation of mtree with euclidean distance
        self.mtree=MTree(euclidean_distance)
        # Insert the training data into Mtree
        self.mtree.add_all(self.X_train)
        
\end{lstlisting}
and then while our model start to predict we  call method \textbf{search(x,k)}  from \textbf{\emph{mtree\_code.py}}, where \textit{x} is the image and \textit{k} is the number of neighbors we want.
\begin{lstlisting}[language=iPython]
   def predict(self, X):
        
        nbr_test=X.shape[0]
        nbr_train=self.X_train.shape[0]
        dists=np.zeros((nbr_test, nbr_train))
        ypred=np.zeros(nbr_test)
        kneighbors=np.zeros((nbr_test, self.k), dtype="i4")
        
        #The model is starting the prediction
        for i in range(nbr_test): 
            
            #Perform k-NN queries by finding NN using 
            #search method in mtree_code
            neighbor=self.mtree.search(X[i],self.k)
            neighbors=np.array([j for j in neighbor])
            if None not in neighbors:
                yyy=self.labels_train[neighbors]
                ypred[i]=self.mode(yyy)
            
        return ypred
        
\end{lstlisting}
the other methods of \textbf{\emph{mtree\_main.py}} follow the same order like in \textbf{\emph{knn.py}}. The \textbf{\emph{mtree\_code.py}} is to be supplied with distance \textit{d}. Here we present the main functions within our \textbf{\emph{mtree\_code.py}} the others are there for the constructions like adding a single entry, splitting the tree, managing the leave and internal node, looking for object within a radius etc \cite{moreinfos} ( we pushed code in git). Let show the \textbf{Class of Mtree and its search method}. To run the code we run \textbf{\emph{mtree\_main.py}} which import the code of \textbf{\emph{mtree\_code.py}}
\newline
\begin{lstlisting}[language=iPython]
     #construction of Mtree only distance function must be 
     #given others are there to hold the properties on Mtree
     #d is distance
  class MTree(object): 
   def __init__(self,d,size_of_node=20,representer_in_parent=
            far_away_object,separations=to_which_entry):
        self.size = 0
        self.root = LeafNode(self)
        self.current_index=0
        self.d = d
        self.size_of_node = size_of_node
        self.representer_in_parent = representer_in_parent
        self.separations = separations
    
    def __len__(self): return self.size
    #add one image into mtree
    def add(self, obj, index):
        self.root.add(obj, index)
        self.size += 1
        #add all images in Tree
    def add_all(self, iterable): 
        for obj in iterable:
            self.add(obj, self.current_index)
            self.current_index+=1

        # the search method receive k for how many obj to be
        #returned and q_obj for object to be referred to.
        #Return the k objects the most close to query obj(q_ojb).
    def search(self, q_obj, k): 
        
        k = min(k, len(self))
        if k == 0: return []
        priority_queue = []
        heappush(priority_queue, priority_queue_data(
                            self.root, 0, 0))
        nn = NN(k)
        while priority_queue:
            prEntry = heappop(priority_queue)
            if(prEntry.dmin > nn.search_radius()):
                
                break
          
            prEntry.tree.search(q_obj, priority_queue, 
                                nn, prEntry.d_query)
            
        return nn.list_of_res()
        
\end{lstlisting}
\vspace{.3cm}
\textbf{Accuracy at all k when using Mtree}
\vspace{.2cm}


The \textbf{Figure \ref{fig:mtreeaccuracy}} represent the acccuracy graph obtained once Mtree intervene. The hightest accuracy was at \emph{k=3} and lowest at \emph{k=9}, around $85.3\%$ and $82.5\%$ respectively.
\begin{figure}[H]
\begin{center}
    \includegraphics[width=0.5\textwidth]{figures/Figure_1mtree.png}
    \caption{This figure shows the accuracy of the classifier using mtree.}
    \label{fig:mtreeaccuracy}
    \end{center}
\end{figure}
If you are interested in seeing the exact values in all $k\in \{1, \cdots ,10\}$, \newline look at \textbf{Figure \ref{fig:mt}}
 \begin{figure}[H]
\begin{center}
    \includegraphics[width=0.6\textwidth]{figures/Unbenannt_mtree.PNG}
    \caption{This figure shows the accuracies at all k when using mtree.}
    \label{fig:mt}
    \end{center}
\end{figure}
\section{Deep Neural Networks}

In Deep Learning the classifier function $\hat{f}$ is represented by a Neural Network (NN), usually also called Deep Neural Network (DNN). It is of the form:
\begin{equation}\label{eq:1}
	\hat{f} = \text{softmax}(f_l(\sigma(f_{l-1}(\dots (\sigma(f_1(\mathbf{x}))) \dots )))), 
\end{equation}
\myequations{The classifier function $\hat{f}$}
where $f_i(\mathbf{x}) = W_i\mathbf{x} + \mathbf{b}_i, W_i\in\mathbb{R}^
{d_i\times d_{i-1}} , \mathbf{b}_i \in \mathbb{R}^{d_i},\sigma$ is the activation function 
and 
\begin{equation}\label{eq:2}
	\text{softmax}(\mathbf{x})=
	\bigg ( \cfrac{e^{\mathbf{x}_i}}{\sum_{j}{e^{\mathbf{x}_j}}} \bigg )_i
\end{equation}
\myequations{Softmax activation function} 
 where $\mathbf{x}_i$ is the i-th element of the vector $
\mathbf{x}$.
\subsection{Definitions}
\paragraph{Activation function} In an artificial neural network activation function of a neuron defines the output of said neuron given an input or a set of inputs. Neutral networks without these functions act like a regression model, which isn't suitable for every situation and use case. These are known also as squashing functions such that is squashes the amplitude of output to a finite value \cite{sharma2017activation}. These are some types of activation functions:
\begin{itemize}
	\item[-] Binary step function, which models the behaviour of the neuron as switch, either active or inactive and it can be defined as:
			\begin{equation}\label{eq:3}
			f(x)=\begin{cases}
    					1,& \text{ if } x \ge 0\\
    					0,& \text{ if } x < 0 \\
				 \end{cases}
			\end{equation}
			\myequations{Binary step function}
	\item[-] Linear, it makes the output of the neuron proportional to the input and it can be defined as:\begin{equation}\label{eq:4}
		f(x)=ax
	\end{equation}
	\myequations{Linear activation function}
	\item[-] Sigmoid, this functions transforms the input into a value in range [$0, 1$] and it can be defined as:\begin{equation}\label{eq:5}
			f(x)=\cfrac{1}{e^x}
		\end{equation}
		\myequations{Sigmoid activation function}
	\item[-] Tanh, this function is similar to sigmoid, but in contrast to the later it is symmetric to around 0, and the output value is in range [$-1, 1$], and it can be defined as:
		\begin{equation}\label{eq:5}
			f(x)=2\text{sigmoid}(2x)-1
		\end{equation}
		\myequations{Tanh activation function}
	\item[-] ReLU is a hybrid between the identity function and binary step function. For negative values the neuron isn't activated and after above 0 it is a linear function. It can be defined as:
		\begin{equation}\label{eq:relu}
			f(x)=\text{max}(0,x)
		\end{equation}
		\myequations{ReLU activation function}
	\item[-] Leaky ReLU is an extended ReLU, in which instead of giving negative numbers the value 0, it is defined as an extremely small linear component. It can be defined as:
		\begin{equation}\label{eq:3}
			f(x)=\begin{cases}
    					x,& \text{ if } x \ge 0\\
    					0,.001x& \text{ if } x < 0 \\
				 \end{cases}
		\end{equation}
		\myequations{Leaky ReLU function}
	\item[-] Parametrised ReLU is a parametrised variant of ReLU, a new parameter is introduced to the negative part of the function. It can be defined as:
	\begin{equation}\label{eq:3}
			f(x)=\begin{cases}
    					x,& \text{ if } x \ge 0\\
    					ax& \text{ if } x < 0 \\
				 \end{cases}
		\end{equation}
		\myequations{Parametrised ReLU function}
	\item[-] Softmax is combination of multiple sigmoid functions. Knowing that sigmoid functions are used in binary classification problems, such that the output is in range [$0, 1$]. Softmax is used in multiclass classification problems. It can be defined as in \ref{eq:2} \cite{sharma2017activation}.
\end{itemize}
\paragraph{Loss function} A loss function is a function that associates values of one or more variables onto a real number representing some "cost". An optimization problem seeks to minimise such a function\cite{wiki:1}. An example of a loss function is \textbf{cross entropy}, which can be approximated over a test set $T$ and $q(x)$ the probability of event $x$ estimated by the trained model by \cite{wiki:3}
\begin{equation}\label{eq:ce}
	H(T, q) = \sum^{|T|}_{i=1}\cfrac{1}{|T|}log_2(q(x_i))
\end{equation}
\myequations{Cross entropy}
\paragraph{Hyperparameters} Hyperparameters are a set of parameters that parameterise a learning method, which must be set appropriately by the user to maximise the usefulness of the learning approach. These parameters represent various aspects of the learning algorithms and have varying effects on the resulting model and its performance \cite{claesen2015hyperparameter}.
\paragraph{Optimizer} Optimizers are algorithms responsible for reducing the losses and to provide the most accurate models. An example of an optimizer is \textit{Stochastic Gradient Descent} \cite{8595534}.
\paragraph{Epoch} An epoch refers to one cycle through the full training dataset. In each epoch a neural network is feed a part of the training data. On the other hand an iterations is the number of batches or steps through partitioned packets of the training data, needed to complete one epoch \cite{deepai:1}.
\paragraph{Underfitting} The goal of using neural networks is to produce a model that can reflect the real world. A model is underfitted, when it can neither model the training data nor generalize to new data. Obviously this will result in poor performance of the model \cite{kubat2017introduction}.
\paragraph{Overfitting} Similar to underfitting, it is a cause of underperformance of a model. A model is overfitted, when it fits the training data too well. This happens when it picks up the noise or random fluctuations in the training data to the extent that it negatively impacts the performance of said model on new data \cite{kubat2017introduction}.
\paragraph{Training set} In the practice data sets are divided in multiple parts. The training set is the part that is used to train the model \cite{kubat2017introduction}.
\paragraph{Test set} On the other hand the test set is used to evaluate the trained model for fitness. This evaluation is done by feeding the set ti the model and calculate the number of samples that are classified correctly for example \cite{kubat2017introduction}.
\paragraph{Validation set} A validation dataset is a dataset of examples used to tune the hyperparameters of a model. An example of a hyperparameter for artificial neural networks includes the number of hidden units in each layer \cite{wiki:2}.
\subsection{Setting up the deep neural network}\label{subsec:2-2}
\paragraph{}
Using the library \texttt{pytorch} grants the user an extensive library of implemented activation functions, loss functions and optimizers etc... In addition to that the library has an implementation of a base class of all neural networks, which can be adjusted to implement a specific architecture. This does not only immensely simplify the task, but grants the user a high level of flexibility. 
\subparagraph{}\label{sub:2-2-1}
In the submitted implementation, we used the base class \texttt{torch.nn.Module} and \texttt{torch.nn.Linear} to initialize the neural network as the input, output and hidden layers.  In addition to that we give the model RELU, cross entropy and stochastic gradient descent as default parameters.  
\lstinputlisting[language = iPython, firstline={24}, lastline={32}]{../src/task1/dnn.py}
Assuming in the next code snippets that the loss function is cross entropy, RELU is the activation function and SGD is the optimizer.  
\paragraph{}
The data to classify are images that can't be feed to a linear function as a vector. Having two dimensions to use a linear function on them, they need to be converted to a vector without loss of any data. This can be achieved using the function \texttt{view} as in the code snippet bellow. Then the vector is as an input to the first linear function the output of \texttt{Linear} as a parameter of the activation function to calculate the activation values of the neutrons of the next layer. Here the activation function is RELU as defined in \ref{eq:relu}.
\lstinputlisting[language = iPython, firstline={34}, lastline={39}]{../src/task1/dnn.py} 
\paragraph{}
Next the loss in each training step must be calculated using a loss function. This can be implemented as in the code snippet bellow. Here we have used the loss function cross entropy as defined in \ref{eq:ce}.
\lstinputlisting[language = iPython, firstline={41}, lastline={45}]{../src/task1/dnn.py} 
\paragraph{}
After the end of each epoch, the value loss and accuracy of the model must be calculated, to give a sense of progress during the training of the model. This can be implemented as in the code snippet bellow. In this implementation the calculated values are passed as a \texttt{dictionary} object containing them.
\lstinputlisting[language = iPython, firstline={54}, lastline={59}]{../src/task1/dnn.py} 
\paragraph{}
Finally fitting the model can be done using the above mentioned methods. This can be implemented as in the code snippet bellow. Here preserving the history of the model for evaluation purposes is important, so in each epoch a \texttt{list history} parameter is appended with the current value loss and accuracy of the model.
\lstinputlisting[language = iPython, firstline={69}, lastline={81}]{../src/task1/dnn.py} 
\paragraph{}
To test the accuracy and evaluate the trained model we need the accuracy function, which takes the predictions and the labels and provides the percentage of predictions that where true. It is defined with $d\in\mathbb{N}$ the cardinality of the valuation set, $\mathbf{p}\in\mathbb{R}^d$ the vector representing the predictions and $\mathbf{l}\in\mathbb{R}^d$.
\begin{equation}
\begin{split}
	g(x, y) := 	\begin{cases}
				1, \text{ if } x = y\\
				0, \text{ if } x \neq y
			\end{cases},
	f(\mathbf{p}, \mathbf{l}) := \cfrac{\sum^{d}_{i = 0} g( \mathbf{p}_i,  \mathbf{l}_i)}{d}
\end{split}
\end{equation}
and the implementation of such function can be as in the code snippet bellow
\lstinputlisting[language = iPython, firstline={20}, lastline={22}]{../src/task1/dnn.py} 
\subsection{Training a deep neural network}\label{subsec:2-3}
\paragraph{Overview of the data set}
The problem in the task is that we want to train a model to classify images in $15$ classes . Asample of these images is shown in fig \ref{fig:smp}.
\begin{figure}[htbp]
\begin{center}
	\includegraphics[width=\textwidth]{figures/sample}
\caption{Sample of images in the data set}
\label{fig:smp}
\end{center}
\end{figure}
\paragraph{}
Now that everything is set up. Training a deep neural network can be done. Assuming that in the following that the default values as described in \ref{sub:2-2-1}. First dividing the data set into a validation set and a training set is important to avoid overfitting the model. Then a \texttt{DeepNeuralNetwork} object must be initialized with the parameters
\begin{itemize}
	\item[-] \texttt{input\_size} The size of the input layer.
	\item[-] \texttt{hidden\_size} The size of the hidden layer.
	\item[-] \texttt{number\_classes} The size of the outpuz layer.
\end{itemize}
 This can be implemented as in the code snippet bellow, 
\lstinputlisting[language = iPython, firstline={95}, lastline={105}]{../src/task1/dnn.py} 
Then the to fit the initialized model, the method \texttt{fit} is called on the training set and validation set with a learning rate $learning\_rate=0.12$ in this case.
\lstinputlisting[language = iPython, firstline={110}, lastline={110}]{../src/task1/dnn.py} 
By executing this we get a trained model with accuracy as shown in fig. \ref{fig:acc-dnn}.
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.75\textwidth]{figures/accuracy-of-dnn}
\caption{Achieved accuracy}
\label{fig:acc-dnn}
\end{center}
\end{figure}
The history of the training is summed up in the plots in fig. \ref{fig:laov} , where the accuracy of the model stagnates over $.9$ and the value loss under $.4$ in the $10.$ epoch  
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.8\textwidth]{figures/losses__accuracies_over_epochs}
\caption{Plotted losses and accuracies over epochs}
\label{fig:laov}
\end{center}
\end{figure}
After $100$ epochs the confusion matrix of the trained model is in fig. \ref{fig:cm}. From the resulting confusion matrix it can be deduced that for the images labeled $0, 1, 2\ and\ 3$ the model is most confident. On the other hand the model is least confident for images labeled $14, 8, 4, 12$.
 \begin{figure}[H]
\begin{center}
	\includegraphics[width=0.6\textwidth]{figures/confusion_matrix}
\caption{Plotted losses and accuracies over epochs}
\label{fig:cm}
\end{center}
\end{figure}
\subsection{Another architechture}
\paragraph{}
In image classification there is a lot of architectures to choose from. Such an example would be CNN short for convolutional neural networks, that use a different method classify images. This architecture has become dominant in computer vision tasks. It is designed to automatically and adaptively learn spatial hierarchies of features through backpropagation by using multiple building blocks, such as convolution layers, pooling layers, and fully connected layers \cite{yamashita2018convolutional}.
\subsubsection{Setting up the convolutional neural network}
Just like the DNN using \texttt{pytorch} simplify the task of creating CNNs and grants us a great deal of flexibility. In the code snippet bellow it can be seen that the implemented neural network has the following structure with 15 layers in the following order:
\begin{itemize}
	\item[-] 17 layers in the.
	\item[-] Convolutional layer with an initial out with $16$ channels in the output.
	\item[-] ReLU layer.
	\item[-] Pooling layer
	\item[-] Convolutional layer doubling the number of channels.
	\item[-] ReLU layer.
	\item[-] Pooling layer
	\item[-] Convolutional layer doubling the number of channels.
	\item[-] ReLU layer.
	\item[-] Pooling layer
	\item[-] ReLU layer.
	\item[-] At the end $3$ Fully connected layer.
\end{itemize}
\lstinputlisting[language = iPython, firstline={20}, lastline={41}]{../src/task1/cnn.py} 
\subsubsection{Training the CNN}
Just like the DNN in \ref{subsec:2-2} and \ref{subsec:2-3} the same methods can be applied to train the model with a valuation set and a training set.
By executing this we get a trained model with accuracy as shown in fig. \ref{fig:acc-cnn}.
It can be remarked that the model achieves an accuracy of over $92\%$ in only half the number of epochs compared to DNNs.
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.9\textwidth]{figures/accuracy-of-cnn}
\caption{Achieved accuracy}
\label{fig:acc-cnn}
\end{center}
\end{figure}
The history of the training is summed up in the plots in fig. \ref{fig:laov} , where the accuracy of the model stagnates over $.9$ and the value loss under $.3$ in the $10.$ epoch  
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.9\textwidth]{figures/losses_accuracies_over_epochs_cnn}
\caption{Plotted losses and accuracies over epochs}
\label{fig:laov}
\end{center}
\end{figure}
\newpage
\printbibliography
\end{document}