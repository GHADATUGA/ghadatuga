
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">
</head>
<link rel="stylesheet" type="text/css" href="styles.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body style="background-image: url(upload/backfoto.jpg);">
	<div class="container">
        <div class="row">
            <div class="col-lg-6 m-auto">
                <div class="card bg-dark mt-5">
                    <div class="card-title bg-primary text-white mt-5">
                        <h3 class="text-center py-3">Welcome to Ghad_Atuga !</h3>
                    </div>
 
                    <?php 
                    
                 
                        if(@$_GET['Empty']==true)
                        {
                    ?>
                        <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Empty'] ?></div>                                
                    <?php
                        }
                    ?>
 
 
                    <?php 
                        if(@$_GET['Invalid']==true)
                        {
                    ?>
                        <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Invalid'] ?></div>                                
                    <?php
                        }
                    ?>
 
                    <div class="card-body ">
                        <form action="<?php echo htmlspecialchars ("Processing.php")?>" method="post">
                        <i style="position: absolute; left:0;  padding: 15px 30px; " class="fa fa-user fa-lg f-fw" aria-hidden="true" ></i>
                            <input type="text" name="UName" placeholder=" User Name"  maxlength = '35' class="form-control mb-3">
                            <i style="position: absolute; left:0; padding: 15px 30px; " class="fa fa-lock fa-lg f-fw" aria-hidden="true" ></i>
                            <input type="password" name="Password" placeholder=" Password" maxlength = '35' class="form-control mb-3">
                            <button class="button1"class="btn btn-success mt-3" name="Login">Login</button>
                             <a style="right: 0;" href="Signup.php">Create a New Account </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
                
</body>
</html>