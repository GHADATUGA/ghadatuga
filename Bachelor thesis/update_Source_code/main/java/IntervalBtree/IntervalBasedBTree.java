package IntervalBtree;

import java.util.LinkedList;
import java.util.List;

//interval based B-tree datastructure
public class IntervalBasedBTree<Key extends Comparable<Key>, Value> implements java.io.Serializable {
    // max children per interval based B-tree node = M-1
    // (must be even and greater than 2)
    private static final int M = 20;

    private Node root;       // root of the interval based B-tree
    private int height;      // height of the B-tree
    private int n;           // number of key-value pairs in the B-tree


    private static final class Node implements java.io.Serializable {
        private int m;                             // number of children
        private Entry[] children = new Entry[M];   // the array of children

        // create a node with k children
        private Node(int k) {
            m = k;
        }
    }

    // internal nodes: only use key and next
    // external nodes: only use key and value
    private static class Entry implements java.io.Serializable{
        private Comparable key;
        private Object val;
        private Node next;     // helper field to iterate over array entries
        public Entry(Comparable key, Object val, Node next) {
            this.key  = key;
            this.val  = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "key=" + key +
                    ", val=" + val +
                    '}';
        }
    }

    /**
     * Initializes an empty B-tree.
     */
    public IntervalBasedBTree() {
        root = new Node(0);
    }

    /**
     * Returns true if this symbol table is empty.
     * @return {@code true} if this symbol table is empty; {@code false} otherwise
     */
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns the number of key-value pairs in this symbol table.
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return n;
    }

    /**
     * Returns the height of this B-tree (for debugging).
     *
     * @return the height of this B-tree
     */
    public int height() {
        return height;
    }


    /**
     * Returns the value associated with the given key.
     *
     * @param  key the key
     * @return the value associated with the given key if the key is in the symbol table
     *         and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public List<Value> get(Key key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        return search(root, key, height);
    }

    private List<Value> search(Node x, Key key, int ht) {
        Entry[] children = x.children;

        // external node
        if (ht == 0) {
            for (int j = 0; j < x.m; j++) {
                if (eq(key, children[j].key)) return (List<Value>) children[j].val;
            }
        }

        // internal node
        else {
            for (int j = 0; j < x.m; j++) {
                if (j+1 == x.m || less(key, children[j+1].key))
                    return search(children[j].next, key, ht-1);
            }
        }
        return null;
    }


    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table.
     * If the value is {@code null}, this effectively deletes the key from the symbol table.
     *
     * @param  key the key
     * @param  val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(Key key, List<Value> val) {
        if (key == null) throw new IllegalArgumentException("argument key to put() is null");
        Node u = insert(root, key, val, height);
        n++;
        if (u == null) return;

        // need to split root
        Node t = new Node(2);
        t.children[0] = new Entry(root.children[0].key, null, root);
        t.children[1] = new Entry(u.children[0].key, null, u);
        root = t;
        height++;
    }

    private Node insert(Node h, Key key, List<Value> val, int ht) {
        int j;
        Entry t = new Entry(key, val, null);

        // external node
        if (ht == 0) {
            for (j = 0; j < h.m; j++) {
                if (less(key, h.children[j].key)) break;
            }
        }

        // internal node
        else {
            for (j = 0; j < h.m; j++) {
                if ((j+1 == h.m) || less(key, h.children[j+1].key)) {
                    Node u = insert(h.children[j++].next, key, val, ht-1);
                    if (u == null) return null;
                    t.key = u.children[0].key;
                    t.val = null;
                    t.next = u;
                    break;
                }
            }
        }

        for (int i = h.m; i > j; i--)
            h.children[i] = h.children[i-1];
        h.children[j] = t;
        h.m++;
        if (h.m < M) return null;
        else         return split(h);
    }

    // split node in half
    private Node split(Node h) {
        Node t = new Node(M/2);
        h.m = M/2;
        for (int j = 0; j < M/2; j++)
            t.children[j] = h.children[M/2+j];
        return t;
    }

    /**
     * Returns a string representation of this B-tree (for debugging).
     *
     * @return a string representation of this B-tree.
     */
    public String toString() {
        return toString(root, height, "") + "\n";
    }

    private String toString(Node h, int ht, String indent) {
        StringBuilder s = new StringBuilder();
        Entry[] children = h.children;

        if (ht == 0) {
            for (int j = 0; j < h.m; j++) {
                s.append(indent + children[j].key + " " + children[j].val + "\n");
            }
        }
        else {
            for (int j = 0; j < h.m; j++) {
                if (j > 0) s.append(indent + "(" + children[j].key + ")\n");
                s.append(toString(children[j].next, ht-1, indent + "     "));
            }
        }
        return s.toString();
    }


    // return *all* intervals in data structure that intersect the given interval
    // running time is proportional to R log N, where R is the number of intersections
    public Iterable<Key> searchAll(Key interval) {
        LinkedList<Key> list = new LinkedList<Key>();
       // int height=0;
        searchAll(root, interval, list, height);

        return list;
    }

    // look in subtree rooted at x
    public boolean searchAll(IntervalBasedBTree.Node x, Key key, LinkedList<Key> list, int ht) {
        boolean found1 = false;
        boolean found2 = false;
        boolean found3 = false;
        if (x == null) {
           // System.out.println("No root");
            return false;
        }

        Entry[] children = x.children;
       // System.out.println(x.m+": "+Arrays.toString(x.children));

        // external node
        if (ht == 0) {
            //System.out.println("Processing External Nodes");
            for (int j = 0; j < x.m; j++) {
                //System.out.println(children[j].key);
                if (intersects((Interval<Key>) key, (Interval<Key>) children[j].key)) {
                    list.add( (Key) children[j].key);
                }
            }
            return true;
        }

        // internal node
        else {
            //System.out.println("Processing Internal Nodes");
            for (int j = 0; j < x.m; j++) {
              //  System.out.println(children[j].key);
                if (j+1 == x.m || less(key, children[j+1].key))
                    return searchAll(children[j].next, key, list,ht-1);
            }
        }
        return false;
    }

    private boolean intersects(Interval<Key> a, Interval<Key> b){
        return a.intersects(b);
    }

    // comparison functions - make Comparable instead of Key to avoid casts
    private boolean less(Comparable k1, Comparable k2) {
        return k1.compareTo(k2) < 0;
    }

    private boolean eq(Comparable k1, Comparable k2) {
        return k1.compareTo(k2) == 0;
    }


    /**
     * Unit tests the {@code IntervalBasedBTree} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        //IntervalBasedBTree<String, String> st = new IntervalBasedBTree<String, String>();


    }

}