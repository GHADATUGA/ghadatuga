package IntervalBtree;
import BtreeAndSplayTree.Datasetgenerator;
import BtreeAndSplayTree.ReadDataset2Dtree;
import randomizedBST.ReadDatasetrBST;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import randomizedBST.Bitmapresult;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class ReadDatasetIntervalBasedBTree{

    public static <obj> void main( String[] args) throws Exception{

        //JSON parser object ot parse read file
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\Thesis\\bachelorthesis\\Source_code\\code\\dataset.json")) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONArray tuples = (JSONArray) obj;
            List<Double> averagesearch = new ArrayList();
            List<Float>averageaccuracy=new ArrayList<>();
            int tupleid = 0;
            for (int segment = 1; segment <= 4; segment++) { // replace 4 with the number of segments
                IntervalBasedBTree<Interval<String>,String> intervalBasedBTree = new IntervalBasedBTree<>();
                List<String> list = new ArrayList();
                long nanotimestart = System.nanoTime();
                for (int id = tupleid; id < (tupleid + 16384); id++) { // change the size of a segment
                    Object ob = tuples.get(id);
                    String values = (String) ((JSONObject) ob).get("Attr1");
                    list.add(values);

                }
                Collections.sort(list);
                ReadDatasetrBST.batches(list, 12).forEach(part -> {
                    // batches(subset, categori_int.get(random.nextInt(categori_int.size()))).forEach(part -> {
                    //System.out.println(part);
                   Interval<String> interval = new Interval<>(part.get(0), part.get(part.size() - 1));
                    ArrayList<String> inse = new ArrayList<>(part);
                    intervalBasedBTree.put(interval, inse);

                });
                String min = list.get(16212);
                String max = list.get(16244);
                Interval<String> searchinterval = new Interval<>(min, max);
                ArrayList<Integer> bitmaps = new ArrayList<>();
                long Searchstart = System.nanoTime();
                //System.out.println(rBST.searchAll(searchinterval));
                intervalBasedBTree.searchAll(searchinterval).forEach(res -> {
                    Bitmapresult bitmapresult = new Bitmapresult();
                    // System.out.println(rBST.get(res));
                    intervalBasedBTree.get(res).forEach(record -> {
                        int bitvalue = bitmapresult.compare(record, min, max);
                        bitmaps.add(bitvalue);
                    });

                });
                long nanotimesearchend = System.nanoTime();
                System.out.println(bitmaps);
                averageaccuracy.add(ReadDatasetrBST.accuracyOfTheSegement(bitmaps));
                averagesearch.add((nanotimesearchend - Searchstart) / 1e6);
                tupleid = tupleid + 16384;
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream("createdfiles/" + Datasetgenerator.random_string() + ".ser");
                    ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                    outputStream.writeObject(intervalBasedBTree);
                    outputStream.close();

                } catch (IOException i) {
                    i.printStackTrace();
                }
            }
            System.out.println("Search time time= " + ReadDataset2Dtree.calculateAverage(averagesearch));
            System.out.println("Average accuracy="+ReadDatasetrBST.finalaverageaccuracy(averageaccuracy));
            File folder = new File("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\Thesis\\bachelorthesis\\Source_code\\code\\createdfiles");
            // replace 4 with the number of the segments
            float memory = ReadDataset2Dtree.memorycapacity(folder) / (1024.00F * 1024.00F * 4.00F);
            System.out.println("Memory space is= " + memory);
            ReadDataset2Dtree.clearfilesindirectory(folder);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
      }}