package kdtree;
import BtreeAndSplayTree.Datasetgenerator;
import BtreeAndSplayTree.ReadDataset2Dtree;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import rangetree.RangeSearch;

import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.io.*;
import java.util.List;

public class ReadDataset {
    public static <obj> void main( String[] args) throws Exception{
        //creation of objects of Kdtree (2dtree) and range tree
        //BTree<String,String>bTree=new BTree<>();

        //JSON parser object ot parse read file
        JSONParser jsonParser=new JSONParser();
        try(FileReader reader=new FileReader("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\Thesis\\bachelorthesis\\Source_code\\code\\dataset.json")) {
            //Read JSON file
            Object obj=jsonParser.parse(reader);
            JSONArray tuples=(JSONArray) obj;
            //
            List<Double> averageinsert = new ArrayList();
            List<Double> averagesearch = new ArrayList();
            int tupleid=0;
            for ( int segment=1;segment<=4;segment++){
                KDTree<String> kdTree=new KDTree<>(2);
                long nanotimestart=System.nanoTime();
                for ( int id=tupleid;id<(tupleid+16384);id++){
                    Object ob=tuples.get(id);
                    String values = (String) ((JSONObject)ob).get("Attr1");
                    double key=(double) ((JSONObject)ob).get("Attr3");
                    double [] po={key,key};
                    kdTree.insert(po,values);
                }
                long nanotimeend=System.nanoTime();
                double[] lower={0.0038690906784998,0.0938690906784998};
                double[] upper={0.8938690906784998,0.9988379862876785};
                System.out.println(kdTree.range(lower,upper).size());
                long nanotimesearchend=System.nanoTime();
                averageinsert.add((nanotimeend-nanotimestart)/1e6);
                averagesearch.add((nanotimesearchend-nanotimeend)/1e6);
                tupleid=tupleid+16384;
                try {
                    FileOutputStream fileOutputStream=new FileOutputStream("createdfiles/"+ Datasetgenerator.random_string()+".ser");
                    ObjectOutputStream outputStream=new ObjectOutputStream(fileOutputStream);
                    outputStream.writeObject(kdTree);
                    outputStream.close();

                }catch (IOException i){
                    i.printStackTrace();
                }
            }

            System.out.println("Insert time= "+ ReadDataset2Dtree.calculateAverage(averageinsert));
            System.out.println("Search time time= "+ ReadDataset2Dtree.calculateAverage(averagesearch));
            File folder = new File("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\Thesis\\bachelorthesis\\Source_code\\code\\createdfiles");
            float memory=ReadDataset2Dtree.memorycapacity(folder)/(1024.00f*1024.00f*4.00f);
            System.out.println("Memory space is= "+memory );
            ReadDataset2Dtree.clearfilesindirectory(folder);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }


    }
}

