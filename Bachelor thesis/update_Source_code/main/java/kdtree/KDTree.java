package kdtree;
import java.util.List;
import java.util.LinkedList;
import java.util.Stack;

/**
 * KDTree is a class supporting KD-tree insertion, range search,
 * Splitting dimension is chosen naively, by
 * depth modulo K.  Semantics are as follows
 */
public class KDTree<T> implements java.io.Serializable{
    // number of milliseconds
    final long m_timeout;

    // K = number of dimensions
    final private int m_K;

    // root of KD-tree
    private KDNode<T> m_root;

    // count of nodes
    private int m_count;

    /**
     * Creates a KD-tree with specified number of dimensions.
     *
     * @param k number of dimensions
     */
    public KDTree(int k) {
        this(k, 0);
    }
    public KDTree(int k, long timeout) {
        this.m_timeout = timeout;
        m_K = k;
        m_root = null;
    }


    /**
     * Insert a node in a KD-tree.  Uses algorithm translated from 352.ins.c of
     *
     * @param key key for KD-tree node
     * @param value value at that key
     *
     * @throws KeySizeException if key.length mismatches K
     * @throws KeyDuplicateException if key already in tree
     */
    public void insert(double [] key, T value)
            throws KeySizeException, KeyDuplicateException {
        this.edit(key, new Editor.Inserter<T>(value));
    }

    /**
     * Edit a node in a KD-tree
     *
     * @param key key for KD-tree node
     * @param editor object to edit the value at that key
     *
     * @throws KeySizeException if key.length mismatches K
     * @throws KeyDuplicateException if key already in tree
     */

    public void edit(double [] key, Editor<T> editor)
            throws KeySizeException, KeyDuplicateException {

        if (key.length != m_K) {
            throw new KeySizeException();
        }

        synchronized (this) {
            // the first insert has to be synchronized
            if (null == m_root) {
                m_root = KDNode.create(new HPoint(key), editor);
                m_count = m_root.deleted ? 0 : 1;
                return;
            }
        }

        m_count += KDNode.edit(new HPoint(key), editor, m_root, 0, m_K);
    }

    /**
     * Find  KD-tree node whose key is identical to key.  Uses algorithm
     * translated from 352.srch.c of Gonnet & Baeza-Yates.
     *
     * @param key key for KD-tree node
     *
     * @return object at key, or null if not found
     *
     * @throws KeySizeException if key.length mismatches K
     */
    public T search(double [] key) throws KeySizeException {

        if (key.length != m_K) {
            throw new KeySizeException();
        }

        KDNode<T> kd = KDNode.srch(new HPoint(key), m_root, m_K);

        return (kd == null ? null : kd.v);
    }

    /**
     * Range search in a KD-tree.  Uses algorithm translated from
     * 352.range.c of Gonnet & Baeza-Yates.
     *
     * @param lowk lower-bounds for key
     * @param uppk upper-bounds for key
     *
     * @return array of Objects whose keys fall in range [lowk,uppk]
     *
     * @throws KeySizeException on mismatch among lowk.length, uppk.length, or K
     */
    public List<T> range(double [] lowk, double [] uppk)
            throws KeySizeException {

        if (lowk.length != uppk.length) {
            throw new KeySizeException();
        }

        else if (lowk.length != m_K) {
            throw new KeySizeException();
        }

        else {
            List<KDNode<T>> found = new LinkedList<KDNode<T>>();
            KDNode.rsearch(new HPoint(lowk), new HPoint(uppk),
                    m_root, 0, m_K, found);
            List<T> o = new LinkedList<T>();
            for (KDNode<T> node : found) {
                o.add(node.v);
            }
            return o;
        }
    }

    public int size() { /* added by MSL */
        return m_count;
    }

    public String toString() {
        return m_root.toString(0);
    }



}