package kdtree;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
//to generate BITMAPS OF THE RESULTS
public class Bitmapresult {
    public int compare(int a, int b,int c){
        return (a>=b && a<=c)?1:0;
    }

    public int compare(String a, String b, String c){

        return (a.compareTo(b)>=0 && a.compareTo(c)<=0)?1:0;
    }
    public int compare(long a, long b, long c){

        return (a>=b && a<=c)?1:0;
    }
    public int compare(float a, float b, float c){

        return (a>=b && a<=c)?1:0;
    }
    public int compare(double a, double b, double c){

        return (a>=b && a<=c)?1:0;
    }
    public int compare(Date a, Date b, Date c){
        Calendar cal1=Calendar.getInstance();
        Calendar cal2=Calendar.getInstance();
        Calendar cal3=Calendar.getInstance();
        cal1.setTime(a);
        cal2.setTime(b);
        cal3.setTime(c);
        return (((cal1.after(cal2)||cal1.equals(cal2)) &&(cal1.before(cal3)||cal1.equals(cal3))))?1:0;
    }
}
