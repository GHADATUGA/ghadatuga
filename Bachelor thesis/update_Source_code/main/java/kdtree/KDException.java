package kdtree;

public class KDException extends Exception implements java.io.Serializable{
    protected KDException(String s) {
        super(s);
    }
    public static final long serialVersionUID = 1L;
}