package randomizedBST;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;


public class IntervalST<Key extends Comparable<Key>, Value>  implements java.io.Serializable{

    private Node root;   // root of the BST

    // BST helper node data type
    private class Node implements java.io.Serializable {
        Interval interval;      // key
        List<Value> value;              // associated data
        Node left, right;         // left and right subtrees
        int N;                    // size of subtree rooted at this node
        Key max;                  // max endpoint in subtree rooted at this node

        Node(Interval interval, List<Value> value) {
            this.interval = interval;
            this.value    = value;
            this.N        = 1;
            this.max      = (Key) interval.max();
        }
    }


    /***************************************************************************
     *  BST search
     ***************************************************************************/

    public boolean contains(Interval interval) {
        return (get(interval) != null);
    }

    // return value associated with the given key
    // if no such value, return null
    public List<Value> get(Interval interval) {
        return get(root, interval);
    }
    private List<Value> get(Node x, Interval interval) {
        if (x == null)                  return null;
        //int cmp = interval.compareTo(x.interval);
        // this is the code i have added in
        int cmp;
       int first= interval.min().compareTo(x.interval.min());
       if (first==0){
           int second=interval.max().compareTo(x.interval.max());
           cmp=second;
       }
       else{
           cmp=first;
       }
        //untill here



        if      (cmp < 0) return get(x.left, interval);
        else if (cmp > 0) return get(x.right, interval);
        else              return x.value;
    }


    /***************************************************************************
     *  randomized insertion
     ***************************************************************************/
    public void put(Interval interval, List<Value> value) {
        if (contains(interval)) { System.out.println("duplicate"); remove(interval);  }
        root = randomizedInsert(root, interval, (Value) value);
    }

    // make new node the root with uniform probability
    private Node randomizedInsert(Node x, Interval interval, Value value) {
        if (x == null) return new Node(interval, (List<Value>) value);
        if (Math.random() * size(x) < 1.0) return rootInsert(x, interval, value);
        ///int cmp = interval.compareTo(x.interval);
        // this is the code i have added in
        int cmp;
        int first= interval.min().compareTo(x.interval.min());
        if (first==0){
            int second=interval.max().compareTo(x.interval.max());
            cmp=second;
        }
        else{
            cmp=first;
        }
        //untill here

        if (cmp < 0)  x.left  = randomizedInsert(x.left,  interval, value);
        else          x.right = randomizedInsert(x.right, interval, value);
        fix(x);
        return x;
    }

    private Node rootInsert(Node x, Interval interval, Value value) {
        if (x == null) return new Node(interval, (List<Value>) value);
       // int cmp = interval.compareTo(x.interval);
        // this is the code i have added in
        int cmp;
        int first= interval.min().compareTo(x.interval.min());
        if (first==0){
            int second=interval.max().compareTo(x.interval.max());
            cmp=second;
        }
        else{
            cmp=first;
        }
        //untill here
        if (cmp < 0) { x.left  = rootInsert(x.left,  interval, value); x = rotR(x); }
        else         { x.right = rootInsert(x.right, interval, value); x = rotL(x); }
        return x;
    }


    /***************************************************************************
     *  deletion
     ***************************************************************************/
    private Node joinLR(Node a, Node b) {
        if (a == null) return b;
        if (b == null) return a;

        if (Math.random() * (size(a) + size(b)) < size(a))  {
            a.right = joinLR(a.right, b);
            fix(a);
            return a;
        }
        else {
            b.left = joinLR(a, b.left);
            fix(b);
            return b;
        }
    }

    // remove and return value associated with given interval;
    // if no such interval exists return null
    public List<Value> remove(Interval interval) {
        Value value = (Value) get(interval);
        root = remove(root, interval);
        return (List<Value>) value;
    }

    private Node remove(Node h, Interval interval) {
        if (h == null) return null;
        //int cmp = interval.compareTo(h.interval);
        // this is the code i have added in
        int cmp;
        int first= interval.min().compareTo(h.interval.min());
        if (first==0){
            int second=interval.max().compareTo(h.interval.max());
            cmp=second;
        }
        else{
            cmp=first;
        }
        //untill here
        if      (cmp < 0) h.left  = remove(h.left,  interval);
        else if (cmp > 0) h.right = remove(h.right, interval);
        else              h = joinLR(h.left, h.right);
        fix(h);
        return h;
    }


    /***************************************************************************
     *  Interval searching
     ***************************************************************************/

    // return an interval in data structure that intersects the given inteval;
    // return null if no such interval exists
    // running time is proportional to log N
    public Interval search(Interval interval) {
        return search(root, interval);
    }

    // look in subtree rooted at x
    public Interval search(Node x, Interval interval) {
        while (x != null) {
            if (interval.intersects(x.interval)) return x.interval;
            else if (x.left == null)             x = x.right;
            //else if (x.left.max < interval.min())  x = x.right;
            else if (x.left.interval.max().compareTo(interval.min())<0){
                x = x.right;
            }
            else     x = x.left;
        }
        return null;
    }


    // return *all* intervals in data structure that intersect the given interval
    // running time is proportional to R log N, where R is the number of intersections
    public Iterable<Interval> searchAll(Interval interval) {
        LinkedList<Interval> list = new LinkedList<Interval>();
        searchAll(root, interval, list);
        return list;
    }

    // look in subtree rooted at x
    public boolean searchAll(Node x, Interval interval, LinkedList<Interval> list) {
        boolean found1 = false;
        boolean found2 = false;
        boolean found3 = false;
        if (x == null)
            return false;
        if (interval.intersects(x.interval)) {
            list.add(x.interval);
            found1 = true;
        }
        if (x.left != null && (x.left.interval.max().compareTo(interval.min()) >=0 ))
            found2 = searchAll(x.left, interval, list);
        if (found2 || x.left == null || x.left.interval.max().compareTo(interval.min()) <0 )
            found3 = searchAll(x.right, interval, list);
        return found1 || found2 || found3;
    }


    /***************************************************************************
     *  useful binary tree functions
     ***************************************************************************/

    // return number of nodes in subtree rooted at x
    public int size() { return size(root); }
    private int size(Node x) {
        if (x == null) return 0;
        else           return x.N;
    }

    // height of tree (empty tree height = 0)
    public int height() { return height(root); }
    private int height(Node x) {
        if (x == null) return 0;
        return 1 + Math.max(height(x.left), height(x.right));
    }


    /***************************************************************************
     *  helper BST functions
     ***************************************************************************/

    // fix auxilliar information (subtree count and max fields)
    private void fix(Node x) {
        if (x == null) return;
        x.N = 1 + size(x.left) + size(x.right);
        x.max = max3((Key) x.interval.max(), max(x.left), max(x.right));
    }

    private Key max(Node x) {
        if (x == null) return null;
        return x.max;
    }

    // precondition: a is not null
    private Key max3(Key a, Key b, Key c) {
        if (a!=null && b!=null && c!=null){
        int ab = a.compareTo(b);
        int ac = a.compareTo(c);
        int bc = b.compareTo(c);
        if (ab == 0) {
            if (ac < 0) return c;
            else return a;
        } else if (ab < 0) {
            if (bc > 0) return b;
            else return c;

        } else if (ab > 0) {
            if (ac > 0) return a;
            else return c;
        }
        // return Math.max(a, Math.max(b, c));
        return null;
    }else if (a==null &&(b!=null && c!=null)){
            int bc=b.compareTo(c);
            if(bc<0) return c;
            else return b;
        }
        else if (b==null &&(a!=null && c!=null)){
            int ac=a.compareTo(c);
            if (ac<0) return c;
            else return a;
        }
        else if (c==null &&(a!=null && b!=null)){
            int ab=a.compareTo(b);
            if (ab<0)return b;
            else return a;
        }
        else if (a==null && b==null && c!=null){
            return c;
        }
        else if (a==null && b!=null && c==null){
            return b;
        }
        else if (a!=null && b==null && c==null){
            return a;
        }


        return null;
    }

    // right rotate
    private Node rotR(Node h) {
        Node x = h.left;
        h.left = x.right;
        x.right = h;
        fix(h);
        fix(x);
        return x;
    }

    // left rotate
    private Node rotL(Node h) {
        Node x = h.right;
        h.right = x.left;
        x.left = h;
        fix(h);
        fix(x);
        return x;
    }


    /***************************************************************************
     *  Debugging functions that test the integrity of the tree
     ***************************************************************************/

    // check integrity of subtree count fields
    public boolean check() { return checkCount() && checkMax(); }

    // check integrity of count fields
    private boolean checkCount() { return checkCount(root); }
    private boolean checkCount(Node x) {
        if (x == null) return true;
        return checkCount(x.left) && checkCount(x.right) && (x.N == 1 + size(x.left) + size(x.right));
    }

    private boolean checkMax() { return checkMax(root); }
    private boolean checkMax(Node x) {
        if (x == null) return true;
        return x.max ==  max3( (Key) x.interval.max(), max(x.left), max(x.right));
    }



}