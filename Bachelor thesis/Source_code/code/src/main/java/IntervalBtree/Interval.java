package IntervalBtree;

import java.io.Serializable;

/******************************************************************************
 *  Implementation of an interval.
 *
 ******************************************************************************/

public class Interval<Key extends Comparable<Key>> implements Serializable, Comparable<Interval<Key>> {
    private final Key min;    // min endpoint
    private final Key max;    // max endpoint

    public Interval(Key min, Key max) {
        if (less(max, min)) throw new RuntimeException("Illegal argument");
        this.min = min;
        this.max = max;
    }

    // return min endpoint
    public Key min() {
        return min;
    }

    // return max endpoint
    public Key max() {
        return max;
    }

    // is x between min and max
    public boolean contains(Key x) {
        return !less(x, min) && !less(max, x);
    }

    // does this interval a intersect interval b?
    public boolean intersects(Interval<Key> b) {
        Interval<Key> a  = this;
        if (less(a.max, b.min)) return false;
        if (less(b.max, a.min)) return false;
        return true;
    }

    // does this interval a equal interval b?
    public boolean equals(Interval<Key> b) {
        Interval<Key> a  = this;
        return a.min.equals(b.min) && a.max.equals(b.max);
    }


    // comparison helper functions
    private boolean less(Key x, Key y) {
        return x.compareTo(y) < 0;
    }

    // return string representation
    public String toString() {
        return "[" + min + ", " + max + "]";
    }

    @Override
    public int compareTo(Interval o) {
        if (this.min.compareTo((Key) o.min)==0){
            if (this.max.compareTo((Key) o.max)==0) return 0;
            else if(this.max.compareTo((Key) o.max)>0) return 1;
            else return -1;
        }else{
            if (this.min.compareTo((Key) o.min)>0) return 1;
            else return -1;

        }
    }

    // test client
    public static void main(String[] args) {

    }


}