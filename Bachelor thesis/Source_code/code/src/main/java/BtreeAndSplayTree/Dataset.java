/*
 * program to read dataset and create obj of data structures to hold data
 *
 */
package BtreeAndSplayTree;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;

// creation of class
public class Dataset {


    public static <obj> void main( String[] args) throws Exception{

        /*
        *creation of objects of data structure
        * Btree, kdtree, range tree etc
         */
        SplayBST<Integer, Integer> splayBST = new SplayBST<>();
        //JSON parser object ot parse read file
        JSONParser jsonParser=new JSONParser();
        try(FileReader reader=new FileReader("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\" +
                "Thesis\\bachelorthesis\\Source_code\\code\\" +
                "dataset2_17.json")) {
           //Read JSON file
           Object obj=jsonParser.parse(reader);
           JSONArray tuples=(JSONArray) obj;
            //keep starting time of the insertion of data into tree.
            long nanotimestart=System.nanoTime();
            for (Object tuple : tuples) {
                Object intproperty= ((JSONObject)tuple).get("segment_id");
                String inttostring=intproperty.toString();
                int intatrribute=Integer.parseInt(inttostring);
                //change to 2,3 or 4 depending on size of segment
                if (intatrribute<2){

                    // identifier of entered record
                    Object identifier= ((JSONObject)tuple).get("id");
                    String identifier_string=identifier.toString();
                    int key_=Integer.parseInt(identifier_string);

                    /*
                     *value corresponding to this identifier
                     * can be changed depending on attribute values we want to insert
                     * String, double, float etc (ten attribute types)
                     * and change to respective object
                     */

               Object intproperty1= ((JSONObject)tuple).get("Attr10");
                 String inttostring1=intproperty1.toString();
              int intatrribute1=Integer.parseInt(inttostring1);
                 splayBST.put(key_,intatrribute1);

                }


            }
            // keep the record of time after insertion of data
            long nanotimeend=System.nanoTime();
            /*
             * searching for values
             * change to respective type and object
             */

            System.out.println(splayBST.get(-1920975425));
            // keep the record of time time after search
            long nanotimetosearch=System.nanoTime();
            // we calculate required time to insert and to search
            System.out.println("Time of  insertion:"+(nanotimeend-nanotimestart)/1e6);
            System.out.println("Time of  search:"+(nanotimetosearch-nanotimeend)/1e6);
            /*
             *  we then serialize the object to see how much memory it occupies.
             *  and change splayBST to respective object
             */
            try {
                FileOutputStream fileOutputStream=new FileOutputStream("memory_.ser");
                ObjectOutputStream outputStream=new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(splayBST);
                outputStream.close();

            }catch (IOException i){
                i.printStackTrace();
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }
}

