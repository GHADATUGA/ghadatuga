package BtreeAndSplayTree;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
    // Program to generate dataset
public class Datasetgenerator {

    public static void main(String[] args) throws IOException {
        JSONArray array=new JSONArray();
        HashMap<String, JSONObject> map=new HashMap<>();
        for (int i=0;i<1638400;i++){
            JSONObject object=new JSONObject();
            Random random=new Random();
            object.put("id",i);
            object.put("Attr1",random_string());
            object.put("Attr2",random.nextFloat());
            object.put("Attr3",random.nextDouble());
            object.put("Attr4",random.nextBoolean());
            object.put("Attr5",random.nextLong());
            object.put("Attr6",categorical_string());
            object.put("Attr7",categorical_int());
            object.put("Attr8",date());
            object.put("Attr9",time());
            object.put("Attr10",random.nextInt());
            map.put("object"+1,object);
            array.add(map.get("object"+1));

        }
        //create a json file
        try(FileWriter file=new FileWriter("dataset2_17.json")) {

           file.write(array.toJSONString());
           file.flush();
        }
    }
    //generate categorical string
    private static String categorical_string(){
        Random random=new Random();
        List<String> categori_String= Arrays.asList("big", "kigali",  "znewdfsyourk", "fnewdfsyourk","kewdfsyourk" ,"cewdfsyourk" ,"anewdfsyourk", "newdfsyourk","newyourk", "small", "extra big", "medium", "extra small",
                "big2", "kigali2",  "2znewdfsyourk", "fnewdfsyourk2","kewdfsyourk2" ,"cewdfsyourk2" ,"anewdfsyourk2", "n2ewdfsyourk","n2ewyourk", "s2mall", "ex2tra big", "me2dium", "ex2tra small");
        int index=ThreadLocalRandom.current().nextInt(0,5);
   return categori_String.get(index);
    }
    //generate categorical integer
   private static int categorical_int(){
       //return random integer between 1 and 100,000
       return (int)((Math.random()*(100000-1))+1);
   }
   //generate random data
   private static String date(){
    int hundredYears=100*365;
    return LocalDate.ofEpochDay(ThreadLocalRandom.current().nextInt(-hundredYears,hundredYears)).toString();
   }
   //generate random time

   private static String time(){

       return between().toString();
   }
private static LocalTime between(){
        int startSeconds= LocalTime.MIN.toSecondOfDay();
        int endSeconds= LocalTime.MAX.toSecondOfDay();
        int randomTime= ThreadLocalRandom.current().nextInt(startSeconds,endSeconds);
        return LocalTime.ofSecondOfDay(randomTime);
}
       // generate random string
    public static String random_string(){
        int lefflimit=97;//letter a
        int righlimit=122;// letter z
        Random random=new Random();
        Random random1=new Random();

        return random.ints(lefflimit,righlimit+1).limit(random1.nextInt(30-7)+7)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint,
                        StringBuilder::append).toString();

    }
}
