
package BtreeAndSplayTree;
import kdtree.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * program to read dataset and create obj of data structures to hold data
 */
public class ReadDataset2Dtree {
   //delete created .ser files
   public static void clearfilesindirectory(File dir) {
        for (File file: dir.listFiles()) {
            if (file.isDirectory())
                clearfilesindirectory(file);
            file.delete();
        }
    }
    //memory space
    public static long memorycapacity(File dir) {
        long size = 0;
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
               // System.out.println(file.getName() + " " + file.length());
                size += file.length();
            }
            else
                size += memorycapacity(file);
        }
        return size;
    }
    public static double calculateAverage(List<Double> marks) {
        double sum = 0;
        if(!marks.isEmpty()) {
            for (Double mark : marks) {
                sum += mark;
            }
            return sum / marks.size();
        }
        return sum;
    }

  public static <obj> void main( String[] args) throws Exception{
              //creation of objects of Kdtree (2dtree) and range tree
      //BTree<String,String>bTree=new BTree<>();

        //JSON parser object ot parse read file
        JSONParser jsonParser=new JSONParser();
        try(FileReader reader=new FileReader("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\Thesis\\bachelorthesis\\Source_code\\code\\dataset.json")) {
            //Read JSON file
            Object obj=jsonParser.parse(reader);
            JSONArray tuples=(JSONArray) obj;
            //
            List<Double> averageinsert = new ArrayList();
            List<Double> averagesearch = new ArrayList();
            int tupleid=0;
            for ( int segment=1;segment<=4;segment++){
                BTree<String,String>bTree=new BTree<>();
                long nanotimestart=System.nanoTime();
                for ( int id=tupleid;id<(tupleid+16384);id++){
                    Object ob=tuples.get(id);
                    String values = (String) ((JSONObject)ob).get("Attr1");
                    bTree.put(values,values);
                }
                long nanotimeend=System.nanoTime();
                System.out.println(bTree.get("gfzzqmttxhr"));
                long nanotimesearchend=System.nanoTime();
                averageinsert.add((nanotimeend-nanotimestart)/1e6);
                averagesearch.add((nanotimesearchend-nanotimeend)/1e6);
                tupleid=tupleid+16384;
                try {
                    FileOutputStream fileOutputStream=new FileOutputStream("createdfiles/"+Datasetgenerator.random_string()+".ser");
                    ObjectOutputStream outputStream=new ObjectOutputStream(fileOutputStream);
                    outputStream.writeObject(bTree);
                    outputStream.close();

                }catch (IOException i){
                    i.printStackTrace();
                }
            }

            System.out.println("Insert time= "+ calculateAverage(averageinsert));
            System.out.println("Search time time= "+ calculateAverage(averagesearch));
            File folder = new File("C:\\Users\\GHAD_ATUGA\\Desktop\\WS2021\\Thesis\\bachelorthesis\\Source_code\\code\\createdfiles");
           float memory=memorycapacity(folder)/(1024.00f*1024.00f*4.00f);
            System.out.println("Memory space is= "+memory );
            clearfilesindirectory(folder);
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }


    }

}

