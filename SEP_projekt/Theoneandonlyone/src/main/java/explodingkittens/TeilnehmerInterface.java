package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TeilnehmerInterface extends Remote {
    String getName() throws RemoteException;
}
