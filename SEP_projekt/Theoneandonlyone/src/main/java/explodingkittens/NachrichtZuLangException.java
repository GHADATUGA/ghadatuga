package explodingkittens;

/**
 * Exception, die ausgelöst wird, wenn die Nachricht länger als 80 Zeichen lang ist.
 */
public class NachrichtZuLangException extends ExplodingkittensException {
}
