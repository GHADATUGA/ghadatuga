package explodingkittens;

import explodingkittens.server.KarteNichtAufHandException;
import explodingkittens.server.KarteNichtAusspielbarException;
import explodingkittens.server.NichtAktuellerTeilnehmerException;
import explodingkittens.server.PositionNichtMoeglichException;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Interface zum Spiel, enthält Methoden des Remote-Objekts Spiel, die der Client aufrufen kann.
 * @author Desiree
 */
public interface SpielInterface extends Remote {

    /**
     * Entfernt den Spieler aus dem Spielablauf.
     * @param teilnehmer Teilnehmer, das Spiel verlassen will.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Liste, bestehend aus einem boolean (Spieler beendet und Gewinner ist ein Spieler) einem String
     * (Spielername des Gewinners oder null, falls es (noch) keinen Gewinner des Typs Spieler gibt).
     * @throws RemoteException  Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    ArrayList verlassen(String teilnehmer, String spiel) throws RemoteException;

    /**
     * Beendet den Zug des Spielers, indem eine Karte vom Spielstapel gezogen wird.
     * @param teilnehmer Teilnehmer, der den Zug beenden will.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return true, wenn gezogene Karte eine Exploding Kitten ist, sonst false.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws NichtAktuellerTeilnehmerException Exception, die geworfen wird wenn der Teilnehmer, der den Zug beenden möchte, nicht der aktuelle
     * Teilnehmer ist. */
    boolean zugBeenden(String teilnehmer,String spiel) throws RemoteException, NichtAktuellerTeilnehmerException;

    /**
     * Gibt die Anzahl Änderungen am Spiel zurück. Nötig zum Vergleich im AktionListener, um zu entscheiden, ob
     * ein Update der GUI erfolgen muss.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Anzahl Änderungen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    int modified(String spiel) throws RemoteException;

    /**
     * Gibt den aktuellen Spielzustand (aus Sicht des Teilnehmers) zurück
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Aktueller Teilnehmer (Name), verbleibende Züge des aktuellen Teilnehmers,
     * Teilnehmerliste in Spielreihenfolge (Namen), oberste Ablagestapelkarte (Name),
     * Exploding-Kitten-Dichte im Spielstapel, Anzahl der Spielstapelkarten und
     * die Handkarten als Hastable mit dem Key Teilnehmername und dem Value ArrayList der Spielkartennamen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    ArrayList getSpielzustand(String spiel) throws RemoteException;

    /**
     * Erzeugt ein neues Spiel mit den angegebenen Teilnehmern.
     * @param name Spielname (==Spielraumname) des zu erzeugenden Spiels.
     * @param spieler Liste der Namen der Spieler, die mitspielen sollen (Spieler: menschliche Mitspieler).
     * @param bots Liste der Namen der Bots, die mitspielen sollen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void erzeugeSpiel(String name, ArrayList<String> spieler, ArrayList<String> bots) throws RemoteException;

    /**
     * Spielt eine Karte "Blick in die Zukunft" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @return Liste der Namen der obersten drei Spielkarten oder null, falls die Aktion durch Nö-Karten verhindert wurde.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    String[] blickInDieZukunft(String spiel, String teilnehmer) throws RemoteException,KarteNichtAufHandException, NichtAktuellerTeilnehmerException;

    /**
     * Spielt eine Karte "Mischen" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    void mischen (String spiel, String teilnehmer) throws RemoteException, KarteNichtAufHandException, NichtAktuellerTeilnehmerException;

    /**
     * Spielt eine Karte "Nö" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws KarteNichtAusspielbarException Wird geworfen, wenn keine Karte, die genöt werden kann, im Moment auf dem Ablagestapel liegt.
     */
    void noe (String spiel, String teilnehmer) throws RemoteException, KarteNichtAufHandException, KarteNichtAusspielbarException;

    /**
     * Spielt die Karte "Wunsch" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param spieler Teilnehmer, der die Karte spielt.
     * @param gegner Teilnehmer, der eine Karte abgeben soll.
     * @return true, wenn der Wunsch nicht durch "Nös" verhindert wird, sonst false.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    boolean gegnerGibtKarte (String spiel, String spieler, String gegner) throws RemoteException, KarteNichtAufHandException, NichtAktuellerTeilnehmerException;

    /**
     * Führt die Übergabe der vom Gegner abgegebenen Karte an den aktuellen Spieler durch (Teil der Aktion "Wunsch").
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param spieler Teilnehmer, der die Karte spielt.
     * @param gegner Teilnehmer, der eine Karte abgeben soll.
     * @param karte Karte, die der Gegner dem Spieler abgibt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Gegner keine Karte des angegebenen Typs auf der Hand hat.
     */
    void gegnerGibtKarteAb (String spiel,String spieler, String gegner,String karte) throws RemoteException, KarteNichtAufHandException;

    /**
     * Spielt eine "Entschärfungskarte".
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @param position Position (0, ..., #Karten(Spielstapel)), an die die Exploding Kitten in den Spielstapel
     * gelegt werden soll. Wobei 0 die unterste Position ist.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws PositionNichtMoeglichException Wird geworfen, wenn die angegebene Position, an die die Exploding Kitten gelegt werden soll, nicht gültig ist.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    void entschaerfen (String spiel, String teilnehmer, int position) throws RemoteException, KarteNichtAufHandException, PositionNichtMoeglichException, NichtAktuellerTeilnehmerException;

    /**
     * Führt das Explodieren eines Spielers durch.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der explodiert.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void explodieren (String spiel, String teilnehmer) throws RemoteException;

    /**
     * Spielt die Karte "Angriff" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    void angriff (String spiel, String teilnehmer) throws RemoteException, KarteNichtAufHandException, NichtAktuellerTeilnehmerException;

    /**
     * Spielt die Karte "Hops" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    void hops (String spiel, String teilnehmer) throws RemoteException, KarteNichtAufHandException, NichtAktuellerTeilnehmerException;

    void loeschen(String location) throws RemoteException;
}

