package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Interface zum SpielraumService, enthält Methoden des Remote-Objekts SpielraumService, die der Client aufrufen kann.
 *
 * @author Nico
 */
public interface SpielraumServiceInterface extends Remote {

    /**
     * Gibt bereits bestehende Spielräume zurück.
     *
     * @return Collection von Spielrauminterfaces.
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    Collection<SpielraumInterface> getSpielraeume() throws RemoteException;

    /**
     * Gibt Spielraumnamen zurück.
     *
     * @return Spielraumnamen.
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    ArrayList<String> getSpielarumNamen() throws RemoteException;

    /**
     * Gibt gesuchten Spielraum zurück.
     *
     * @param spielraumname gesuchter Spielraum.
     * @return SpielraumInterface.
     * @throws ExplodingkittensException Ausnahme, wenn gesuchter Spielraum nicht existiert.
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    SpielraumInterface getSpielraum(String spielraumname) throws ExplodingkittensException, RemoteException;

    /**
     * Erstellt einen Spielraum.
     *
     * @param ersteller Ersteller des spielraums.
     * @param name      Name des Spielraums.
     * @return SpielraumInterface
     * @throws ExplodingkittensException Ausnahme wenn kein Spielraum erstellt werden kann.
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    SpielraumInterface erstellen(SpielerInterface ersteller, String name) throws ExplodingkittensException, RemoteException;

    /**
     * Loescht den mit Name angegebenen Spielraum, falls existent.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param name      Name des Spielraums.
     * @throws ExplodingkittensException genauer: NichtLeerException, SpielraumNichtExistentException.
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void loeschen(SpielerInterface ersteller, String name) throws ExplodingkittensException, RemoteException;

    /**
     * Ändert den Namen des Spielraums, falls der Ersteller dies wünscht.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param oldName   Alter Name des Spielraums.
     * @param newName   Neuer Name des Spielraums.
     * @throws ExplodingkittensException genauer: NameSchonVergebenException, SpielraumNichtExistentException
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void aendern(SpielerInterface ersteller, String oldName, String newName) throws ExplodingkittensException, RemoteException;

    /**
     * Prüft, Spielraumliste aktualisiert wurde.
     *
     * @return Anzahl Änderungen.
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    int aktualisieren() throws RemoteException;

    /**
     * Signalisiert Änderungen auf Spielraumliste.
     *
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void signal() throws RemoteException;
}
