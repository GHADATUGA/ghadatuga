package explodingkittens;

/**
 * Wenn kein Bot im Spielraum vorhanden ist.
 */
public class KeinBotVorhandenException extends Exception {
}
