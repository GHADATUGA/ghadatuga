package explodingkittens;

/**
 * Wenn das neue gewünschte Passwort falsch wiederholt wurde.
 */
public class PasswortFalschWiederholtException extends Exception {
}
