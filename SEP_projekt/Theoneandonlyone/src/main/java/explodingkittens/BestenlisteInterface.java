package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Interface für das Remote-Objekt Bestenliste.
 *
 * @author Hugo, erstellt am 14.06.2020.
 */
public interface BestenlisteInterface extends Remote {
    /**
     * Signalisiert eine Änderung auf die Bestenliste.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void signal() throws RemoteException;


    /**
     * Aktualisiert die Bestenliste und erhöht die Siegeanzahl des Spielers, der das Spiel gewonnen hat. Updates finden am Ende des Spiels statt.
     * @param winner Benutzername des Spielers, der das Spiel gewonnen hat.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws BenutzernameNichtExistentException Wenn der Benutzername nicht in der Bestenliste gespeichert ist.
     */
    void updateList(String winner) throws RemoteException, BenutzernameNichtExistentException;

    ArrayList<ArrayList<String>> getList() throws RemoteException;

    /**
     * Prüft, ob die Bestenliste aktualisiert wurde.
     * @return Gibt die Anzahl der Änderungen zurück.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    int aktualisieren() throws RemoteException;
}
