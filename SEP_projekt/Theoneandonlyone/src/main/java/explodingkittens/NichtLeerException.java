package explodingkittens;

/**
 * Wenn der Spielraum gelöscht werden soll, sich aber noch Spieler in diesem befinden.
 */
public class NichtLeerException extends ExplodingkittensException {
}
