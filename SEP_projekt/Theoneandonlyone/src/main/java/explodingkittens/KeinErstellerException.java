package explodingkittens;

/**
 * Wenn der Benutzer, der den Spielraum konfigurieren will, nicht der Ersteller dieses Spielraums ist.
 */
public class KeinErstellerException extends ExplodingkittensException {
}
