package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote-Interface zur Erzeugung eines Spielers.
 */
public interface SpielerServiceInterface extends Remote {
    /**
     * Erzeugt einen neuen Spieler.
     * @param name Name des Spielers.
     * @return SpielerInterface des erzeugten Spielers.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    SpielerInterface create(String name) throws RemoteException;;
}
