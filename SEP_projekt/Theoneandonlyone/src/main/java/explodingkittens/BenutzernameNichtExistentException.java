package explodingkittens;

/**
 * Wenn der eingegebene Benutzername nicht in der Datenbank existiert.
 */
public class BenutzernameNichtExistentException extends Exception {
}
