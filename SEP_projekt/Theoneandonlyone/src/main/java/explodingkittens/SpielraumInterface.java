package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Set;

/**
 * Interface zum Spielraum, enthält Methoden des Remote-Objekts Spielraum, die der Client aufrufen kann.
 *
 * @author Nico
 */
public interface SpielraumInterface extends Remote {


    /**
     * Beitreten in einen Spielraum, falls dieser nicht voll.
     *
     * @param newSpieler Beitretender Spieler.
     * @throws SpielraumVollException RemoteException
     * @throws RemoteException        Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void beitreten(SpielerInterface newSpieler) throws SpielraumVollException, RemoteException;

    /**
     * Hinzufügen eines Bots zum Spielraum, falls Spielraum nicht voll.
     *
     * @param bot KI.
     * @throws SpielraumVollException Ausnahme, falls Spielraum voll.
     * @throws RemoteException        Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void botHinzufuegen(BotInterface bot) throws SpielraumVollException, RemoteException;

    /**
     * Prüft, ob Spielraum leer ist.
     *
     * @return boolean, ob Spielraum leer ist.
     * @throws RemoteException Ausnahme
     */
    boolean istLeer() throws RemoteException;

    /**
     * Setzt Status auf "gelöscht", falls der Ersteller den Spielraum löschen will.
     *
     * @param ersteller Ersteller des Spielraums.
     * @throws KeinErstellerException Ausnahme, falls man nicht der Ersteller ist.
     * @throws RemoteException        Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void setzeGeloescht(SpielerInterface ersteller) throws KeinErstellerException, RemoteException;

    boolean isGestartet() throws RemoteException;

    void setGestartet() throws RemoteException;

    /**
     * Ändert den Namen des Spielraums, falls der Ersteller dies wünscht.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param newName   Neuer Spielraumname.
     * @throws KeinErstellerException Ausnahme, falls der Veränderer kein Ersteller ist.
     * @throws RemoteException        Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void aenderName(SpielerInterface ersteller, String newName) throws KeinErstellerException, RemoteException;

    /**
     * Entfernt einen Spieler aus dem Spielraum
     *
     * @param spieler Spieler, der Spielraum verlässt.
     * @throws ErstellerException Exception, falls der Verlassende spieler der Ersteller ist.
     * @throws RemoteException    Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void verlassen(SpielerInterface spieler) throws RemoteException, ErstellerException;

    /**
     * Entfernt einen Bot aus dem Spielraum.
     *
     * @param bot zu entfernender Bot.
     * @throws KeinBotVorhandenException Exception, falls kein Bot vorhanden.
     * @throws RemoteException           Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void botEntfernen(BotInterface bot) throws KeinBotVorhandenException, RemoteException;

    /**
     * Gibt Spieler im Spielraum zurück.
     *
     * @return Spieler des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    Set<SpielerInterface> getSpieler() throws RemoteException;

    /**
     * Gibt Bots im Spielraum zurück.
     *
     * @return Bots des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    ArrayList<BotInterface> getBots() throws RemoteException;

    /**
     * Gibt den Namen des Spielraums zurück
     *
     * @return Name.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    String getName() throws RemoteException;

    /**
     * Gibt Ersteller des Spielraums zurück.
     *
     * @return Ersteller des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    SpielerInterface getErsteller() throws RemoteException;

    /**
     * Gibt alle Teilnehmernamen des Spielraums zurück.
     *
     * @return Teilnehmernamen des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    ArrayList<String> getMitSpieler() throws RemoteException;

    void setOffline() throws RemoteException;
}
