/**
 * Hauptpackage des Projekts. Enthält weitere Packages, Remote-Interfaces für die Kommunikation zwischen den
 * Server- und Client-Packages sowie packageübergreifende Exceptions.
 */
package explodingkittens;