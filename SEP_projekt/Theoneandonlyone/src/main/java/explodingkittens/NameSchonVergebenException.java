package explodingkittens;

/**
 * Wenn der Spielraumname bereits vergeben ist.
 */
public class NameSchonVergebenException extends ExplodingkittensException {
}
