package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * Remote-Interface für einen Spieler.
 */
public interface SpielerInterface extends Remote {
    /**
     * Gibt den Namen des Spielers zurück.
     * @return Name des Spielers.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    String getName() throws RemoteException;
}
