package explodingkittens.client;

import explodingkittens.SpielInterface;
import explodingkittens.server.KarteNichtAufHandException;
import explodingkittens.server.KarteNichtAusspielbarException;
import explodingkittens.server.NichtAktuellerTeilnehmerException;
import explodingkittens.server.PositionNichtMoeglichException;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

/**
 * Client des SpielServers.
 * @author Desiree
 */
public class SpielServerClient {
    private SpielInterface spielInterface;

    /**
     * Findet die Registry und durchsucht sie nach dem Remote-Objekt.
     */
    public SpielServerClient(){
        try {
            Registry registry = LocateRegistry.getRegistry(8014);
            this.spielInterface = (SpielInterface) registry.lookup("spielService");
        } catch (Exception e) {
            System.err.println("SpielServerClient-Exception: " + e.toString());
            e.printStackTrace();
        }
    }
    /**
     * Entfernt den Spieler aus dem Spielablauf.
     * @param teilnehmer Teilnehmer, das Spiel verlassen will.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Liste, bestehend aus einem boolean (Spieler beendet und Gewinner ist ein Spieler) einem String
     * (Spielername des Gewinners oder null, falls es (noch) keinen Gewinner des Typs Spieler gibt).
     * @throws RemoteException  Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public ArrayList verlassen(String teilnehmer, String spiel) throws RemoteException {
        return spielInterface.verlassen(teilnehmer, spiel);
    }

    /**
     * Beendet den Zug des Spielers, indem eine Karte vom Spielstapel gezogen wird.
     * @param teilnehmer Teilnehmer, der den Zug beenden will.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return true, wenn gezogene Karte eine Exploding Kitten ist, sonst false.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws NichtAktuellerTeilnehmerException Exception, die geworfen wird wenn der Teilnehmer, der den Zug beenden möchte, nicht der aktuelle
     * Teilnehmer ist. */
    public boolean zugBeenden(String teilnehmer, String spiel) throws RemoteException, NichtAktuellerTeilnehmerException {
        return spielInterface.zugBeenden(teilnehmer,spiel);
    }

    /**
     * Gibt die Anzahl Änderungen am Spiel zurück. Nötig zum Vergleich im AktionListener, um zu entscheiden, ob
     * ein Update der GUI erfolgen muss.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Anzahl Änderungen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public int modified(String spiel) throws RemoteException {
        return spielInterface.modified(spiel);
    }

    /**
     * Gibt den aktuellen Spielzustand (aus Sicht des Teilnehmers) zurück
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Aktueller Teilnehmer (Name), verbleibende Züge des aktuellen Teilnehmers,
     * Teilnehmerliste in Spielreihenfolge (Namen), oberste Ablagestapelkarte (Name),
     * Exploding-Kitten-Dichte im Spielstapel, Anzahl der Spielstapelkarten und
     * die Handkarten als Hastable mit dem Key Teilnehmername und dem Value ArrayList der Spielkartennamen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public ArrayList getSpielzustand(String spiel) throws RemoteException {
        return spielInterface.getSpielzustand(spiel);
    }
    /**
     * Erzeugt ein neues Spiel mit den angegebenen Teilnehmern.
     * @param name Spielname (==Spielraumname) des zu erzeugenden Spiels.
     * @param spieler Liste der Namen der Spieler, die mitspielen sollen (Spieler: menschliche Mitspieler).
     * @param bots Liste der Namen der Bots, die mitspielen sollen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void erzeugeSpiel(String name,ArrayList<String> spieler,ArrayList<String> bots) throws RemoteException {
        spielInterface.erzeugeSpiel(name,spieler,bots);
    }

    /**
     * Spielt eine Karte "Blick in die Zukunft" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @return Liste der Namen der obersten drei Spielkarten oder null, falls die Aktion durch Nö-Karten verhindert wurde.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public String[] blickInDieZukunft(String spiel, String teilnehmer) throws KarteNichtAufHandException, NichtAktuellerTeilnehmerException, RemoteException {
        return spielInterface.blickInDieZukunft(spiel, teilnehmer);
    }

    /**
     * Spielt eine Karte "Mischen" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public void mischen(String spiel, String teilnehmer) throws KarteNichtAufHandException, RemoteException, NichtAktuellerTeilnehmerException {
        spielInterface.mischen(spiel, teilnehmer); }

    /**
     * Spielt eine Karte "Nö" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws KarteNichtAusspielbarException Wird geworfen, wenn keine Karte, die genöt werden kann, im Moment auf dem Ablagestapel liegt.
     */
    public void noe (String spiel, String teilnehmer) throws KarteNichtAufHandException, RemoteException, KarteNichtAusspielbarException {
        spielInterface.noe(spiel, teilnehmer); }

    /**
     * Spielt die Karte "Wunsch" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param spieler Teilnehmer, der die Karte spielt.
     * @param gegner Teilnehmer, der eine Karte abgeben soll.
     * @return true, wenn der Wunsch nicht durch "Nös" verhindert wird, sonst false.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public boolean gegnerGibtKarte (String spiel, String spieler, String gegner) throws KarteNichtAufHandException, RemoteException, NichtAktuellerTeilnehmerException {
        return spielInterface.gegnerGibtKarte(spiel,spieler,gegner);
    }

    /**
     * Führt die Übergabe der vom Gegner abgegebenen Karte an den aktuellen Spieler durch (Teil der Aktion "Wunsch").
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param spieler Teilnehmer, der die Karte spielt.
     * @param gegner Teilnehmer, der eine Karte abgeben soll.
     * @param karte Karte, die der Gegner dem Spieler abgibt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Gegner keine Karte des angegebenen Typs auf der Hand hat.
     */
    public void gegnerGibtKarteAb(String spiel,String spieler, String gegner,String karte) throws KarteNichtAufHandException, RemoteException {
        spielInterface.gegnerGibtKarteAb(spiel, spieler,gegner,karte); }

    /**
     * Spielt eine "Entschärfungskarte".
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @param position Position (0, ..., #Karten(Spielstapel)), an die die Exploding Kitten in den Spielstapel
     * gelegt werden soll. Wobei 0 die unterste Position ist.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws PositionNichtMoeglichException Wird geworfen, wenn die angegebene Position, an die die Exploding Kitten gelegt werden soll, nicht gültig ist.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public void entschaerfen(String spiel, String teilnehmer, int position) throws RemoteException, PositionNichtMoeglichException, KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        spielInterface.entschaerfen(spiel,teilnehmer,position);
    }

    /**
     * Führt das Explodieren eines Spielers durch.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der explodiert.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void explodieren (String spiel, String teilnehmer) throws RemoteException {
        spielInterface.explodieren(spiel,teilnehmer);
    }

    /**
     * Spielt die Karte "Angriff" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public void angriff (String spiel, String teilnehmer) throws KarteNichtAufHandException, RemoteException, NichtAktuellerTeilnehmerException {
        spielInterface.angriff(spiel,teilnehmer); }

    /**
     * Spielt die Karte "Hops" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public void hops (String spiel, String teilnehmer) throws KarteNichtAufHandException, RemoteException, NichtAktuellerTeilnehmerException {
        spielInterface.hops(spiel,teilnehmer);
    }

    public void loeschen(String location) throws RemoteException {
        spielInterface.loeschen(location);
    }




    /*
    public static class AktionListener {
        private int aenderungen;
        private Thread t;
        private SpielServerClient spielServerClient;
        private String spiel;
        private Label explodierChance;
        private ObservableValue<? extends String> explodierChanceObservable;
        private Label amZug;
        private ObservableValue<?extends String> amZugObservable;
        private String spielername;
        private HBox gegner;
        private Label anzSpielstapelkarten;
        private ObservableValue<? extends String> anzahlSK;
        private ImageView imgAblagestapel;
        private ObservableValue<?extends Image> imgAblagestapelObservable;
        private HBox hand;
        private ImageView gewaehlt;




        public AktionListener(SpielServerClient spielServerClient, String spiel, String spielername, Label explodierChance, Label amZug, HBox gegner, Label anzSpielstapelkarten, ImageView imgAblagestapel, HBox hand, ImageView gewaehlt) throws RemoteException {
            this.spielServerClient = spielServerClient;
            this.spiel=spiel;
            this.aenderungen = 0;
            this.explodierChance = explodierChance;
            this.amZug = amZug;
            this.spielername = spielername;
            this.gegner = gegner;
            this.anzSpielstapelkarten = anzSpielstapelkarten;
            this.imgAblagestapel = imgAblagestapel;
            this.hand = hand;
            this.gewaehlt = gewaehlt;

            startAktionListener();
        }




        private void startAktionListener() {

            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() {
                    try {
                        readChanges();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };

            t = new Thread(task);
            t.setDaemon(true);
            t.start();
        }


        private void readChanges() throws RemoteException {
            while (true) {
                int modifications = spielServerClient.modified(spiel);
                if (aenderungen != modifications) {
                    ArrayList zustand =  spielServerClient.getSpielzustand(spiel);

                    amZug.setText(zustand.get(0).toString()+" ("+zustand.get(1).toString()+")");
                    List<String> teilnehmerList = (ArrayList<String>) zustand.get(2);
                    imgAblagestapel = new ImageView(getKartenbild((String)zustand.get(3)));
                    explodierChance.setText(zustand.get(4).toString());
                    anzSpielstapelkarten.setText(zustand.get(5).toString());
                    HashMap<String,ArrayList<String>> handkartenAll = (HashMap<String,ArrayList<String>>)zustand.get(6);

                    ArrayList<String> myhandkarten = handkartenAll.get(spielername);
                    hand.getChildren().clear();
                    for(String handkarte : myhandkarten){
                        ImageView imageKarte = new ImageView(getKartenbild(handkarte));
                        imageKarte.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent mouseEvent) {
                                handKarteGewaehlt(mouseEvent);
                            }
                        });
                        hand.getChildren().add(imageKarte);
                    }

                    int split = teilnehmerList.indexOf(spielername);
                    for(int reihe = 0; reihe < split; reihe++){
                        teilnehmerList.add(teilnehmerList.get(0));
                        teilnehmerList.remove(0);
                    }
                    teilnehmerList.remove(0);
                    List vboxList = gegner.getChildren();
                    for(int j = 0; j < teilnehmerList.size(); j++){
                        String name = teilnehmerList.get(j);
                        VBox vb = (VBox) vboxList.get(j);
                        vb.setVisible(true);
                        ((Label) vb.getChildren().get(0)).setText(name);
                        ((Label) ((HBox) vb.getChildren().get(2)).getChildren().get(1)).setText(String.valueOf(handkartenAll.get(name).size()));
                    }
                    aenderungen = modifications;
                }
            }
        }

        private Image getKartenbild(String name){
            Image bild = new Image("TestKarte.png");
            if(name.equals("angriff")){
                bild = new Image("Angriff.png");
            } else if(name.equals("blick_in_die_zukunft")){
                bild = new Image("Blick_In_Die_Zukunft.png");
            } else if(name.equals("entschaerfung")){
                bild = new Image("Entschaerfen.png");
            } else if(name.equals("hops")){
                bild = new Image("Hops.png");
            } else if(name.equals("mischen")){
                bild = new Image("Mischen.png");
            } else if(name.equals("noe")){
                bild = new Image("Noe.png");
            } else if(name.equals("wunsch")){
                bild = new Image("Wunsch.png");
            }
            return bild;
        }

        private void handKarteGewaehlt(MouseEvent event) {
            ImageView gewaehlteKarte = (ImageView) event.getSource();
            DropShadow gewaehlt = new DropShadow();

            for (Node handkarte : hand.getChildren()) {
                handkarte.setEffect(null);
            }
            gewaehlteKarte.setEffect(gewaehlt);
            this.gewaehlt = gewaehlteKarte;
        }
    }

         */


}
