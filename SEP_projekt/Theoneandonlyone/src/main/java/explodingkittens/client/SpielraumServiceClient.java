package explodingkittens.client;

import explodingkittens.ExplodingkittensException;
import explodingkittens.SpielerInterface;
import explodingkittens.SpielraumInterface;
import explodingkittens.SpielraumServiceInterface;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.ListView;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client des SpielraumService
 * Zuständing für die Verwaltung der Spielräume
 *
 * @author Nico
 */
public class SpielraumServiceClient {
    private static final Logger logger = Logger.getLogger(SpielraumServiceClient.class.getName());
    private SpielraumServiceInterface look_up;
    private boolean online = true;

    /**
     * Konstruktor des SpielraumServiceClient.
     * Findet die Registry und ruft mittels look_up() den Verweis auf das Server-Objekt (Spielraumservice) ab.
     */
    public SpielraumServiceClient() {
        try {
            Registry registry = LocateRegistry.getRegistry(8014);
            this.look_up = (SpielraumServiceInterface) registry.lookup("spielraumService");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Fehler beim Lookup von <spielraumService>.", e);
        }
    }

    /**
     * Erstellt einen Spielraum.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param name      Name des Spielraums.
     * @return SpielraumInterface
     * @throws ExplodingkittensException genauer: NameSchonVergebenException.
     * @throws RemoteException           Ausnahme
     */
    public SpielraumInterface erstellen(SpielerInterface ersteller, String name) throws ExplodingkittensException, RemoteException {
        return look_up.erstellen(ersteller, name);
    }

    /**
     * Loescht den mit Name angegebenen Spielraum, falls existent.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param name      Name des Spielraums.
     *
     * @throws ExplodingkittensException genauer: NichtLeerException, SpielraumNichtExistentException.
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void loeschen(SpielerInterface ersteller, String name) throws ExplodingkittensException, RemoteException {
        look_up.loeschen(ersteller, name);
    }

    /**
     * Ändert den Namen des Spielraums, falls der Ersteller dies wünscht.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param oldName   Alter Name des Spielraums.
     * @param newName   Neuer Name des Spielraums.
     * @throws ExplodingkittensException genauer: NameSchonVergebenException, SpielraumNichtExistentException
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void aendern(SpielerInterface ersteller, String oldName, String newName) throws ExplodingkittensException, RemoteException {
        look_up.aendern(ersteller, oldName, newName);
    }

    /**
     * Gibt bereits bestehende Spielraume zurück.
     *
     * @return eine Collection von Spielrauminterfaces
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public Collection<SpielraumInterface> getSpielraeume() throws RemoteException {
        return look_up.getSpielraeume();
    }

    /**
     * Gibt Namen der Spielräume zurück.
     *
     * @return Spielraumnamen.
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public ArrayList<String> getSpielarumNamen() throws RemoteException {
        return look_up.getSpielarumNamen();
    }

    /**
     * Gibt gesuchten Spielraum zurück.
     *
     * @param spielraumname Name des gesuchten Spielraums.
     * @return SpielraumInterface
     * @throws ExplodingkittensException Wenn Spielraum nicht existiert.
     * @throws RemoteException           Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public SpielraumInterface getSpielraum(String spielraumname) throws ExplodingkittensException, RemoteException {
        return look_up.getSpielraum(spielraumname);
    }

    /**
     * Prüft, ob der SpielraumService-Client noch aktiv ist.
     *
     * @return true, wenn der BestenlisteClient noch ausgeführt wird; sonst falsch
     */
    private boolean isOnline() {
        return online;
    }

    /**
     * Setzt aktivität des SpielraumService-Client auf offline
     */
    //entfernt: @return true, wenn der BestenlisteClient noch ausgeführt wird; sonst falsch
    public void setOffline() {
        this.online = false;
    }

    public void setOnline() {
        this.online = true;
    }

    /**
     * Setzt Spielräume in die GUI und startet Thread, der nach neuen Spielräumen schaut und ggf. die GUI aktualisiert.
     *
     * @param spielraumListView ListView mit Spielräumen.
     */
    public void startChangeListener(ListView<String> spielraumListView) {
        logger.log(Level.INFO, "start changeListener: {0}", spielraumListView);
        ObservableList<String> spielraumList = FXCollections.observableArrayList();

        spielraumListView.setItems(spielraumList);
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    readChanges(spielraumList);
                } catch (RemoteException e) {
                    logger.log(Level.SEVERE, "changeListener error", e);
                }
                return null;
            }
        };

        Thread t = new Thread(task);
        t.setDaemon(true);
        t.start();
    }

    /**
     * Lauscht auf Änderungen und aktualisiert die GUI.
     *
     * @param spielraumList ObservableList von Spielräumen.
     * @throws RemoteException      Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    private void readChanges(ObservableList<String> spielraumList) throws RemoteException {
        int old = -1;
        while (isOnline()) {
            int current = look_up.aktualisieren();
            if (old < current) {
                ArrayList<String> namen = look_up.getSpielarumNamen();
                spielraumList.clear();
                spielraumList.addAll(namen);

                old = current;
                //Thread.sleep(2000);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Signalisiert Änderung auf Spielraumliste
     *
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void signal() throws RemoteException {
        look_up.signal();
    }

    /**
     * Prüft, ob Spielraumliste verändert wurde.
     *
     * @return Anzahl der Änderungen.
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public int aktualisieren() throws RemoteException {
        return look_up.aktualisieren();
    }
}
