package explodingkittens.client;

import explodingkittens.NachrichtZuLangException;
import explodingkittens.ChatInterface;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.ListView;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

/**
 * Client des Chats.
 *
 * @author Hugo, erstellt am 05.05.2020
 */
public class ChatClient {
    private ChatInterface look_up;
    private boolean online = true;

    /**
     * Konstruktor der ChatClient-Klasse.
     *
     * Findet die Registry, ruft sie ab und sucht in der Registry nach dem Remote-Objekt.
     */
    public ChatClient(){
        try {
            Registry registry = LocateRegistry.getRegistry(8014);
            this.look_up = (ChatInterface) registry.lookup("chat");
        } catch (Exception e) {
            System.err.println("ChatClient exception: " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Fügt der Liste der Nachrichten an einem vorhandenen Speicherort eine Nachricht hinzu.
     * Fügt der Benutzerliste einen Benutzer hinzu, wenn der Speicherort spielerList ist.
     *
     * @param location Ort, zu dem die Nachricht gehört. oder spielerList.
     * @param txt Nachricht, die hinzugefügt wird. oder Benutzername.
     * @throws NachrichtZuLangException Exception, die ausgelöst wird, wenn die Nachricht mehr als 80 Zeichen enthält.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void verfassen(String location, String txt) throws RemoteException, NachrichtZuLangException {
        look_up.verfassen(location, txt);
    }

    /**
     * Gibt die Liste der Nachrichten an einem vorhandenen Speicherort zurück.
     * Gibt die Liste der Benutzer zurück, wenn Speicherort spielerList ist.
     *
     * @param location Ort, zu dem die Liste der Nachrichten gehört. oder spielerList.
     * @return Liste der Nachrichten oder Liste der Benutzer.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public ArrayList<String> getNachrichten(String location) throws RemoteException {
        return look_up.getNachrichten(location);
    }

    /**
     * Löscht die Liste der Nachrichten an einem dem Server bekannten Ort.
     *
     * Entfernt einen Benutzernamen in der Benutzerliste, wenn Speicherort spielerList ist.
     *
     * @param location Ort, an dem die Nachrichtenliste gelöscht werden soll; oder spielerList.
     * @param username Benutzername, der aus der Liste der Benutzer entfernt werden soll.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void nachrichtenLoeschen(String location, String username) throws RemoteException{
        look_up.nachrichtenLoeschen(location, username);
    }

    /**
     * Erstellen eines Standorts und einer dazugehörigen Nachrichtenliste.
     * @param location Neuer Speicherort, zu dem die Nachrichten gehören sollen.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void erstellNachrichtLocation(String location) throws RemoteException{
        look_up.erstellNachrichtenLocation(location);
    }

    /**
     * Prüft, ob die Daten auf dem Server aktualisiert wurden.
     * @param location Ort, zu dem die Nachrichten gehören.
     * @return Gibt die Anzahl der Änderungen zurück.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public int aktualisieren(String location) throws RemoteException {
        return look_up.aktualisieren(location);
    }

    /**
     * Prüft, ob der Chatclient noch aktiv ist.
     * @return true, wenn der Chatclient noch ausgeführt wird; sonst false
     */
    private boolean isOnline() {
        return online;
    }

    /**
     * Setzt den ChatClient auf inaktiv.
     */
    public void setOffline(){
        this.online = false;
    }

    /**
     * Lauscht auf Änderungen auf dem Server und aktualisiert die Liste der Benutzer oder Nachrichten.
     */
    public static class MessageListener {
        private Thread t;
        private ChatClient chatClient;
        private ListView<String> messageListView;
        private ObservableList<String> messageList;
        private String location;
        private int prev;

        /**
         * Konstruktor für die MessageListener-Klasse.
         *
         * Initialisiert und startet den MessageListener.
         *
         * @param chatClient Chatclient, der Client-Chat-Funktionen verwaltet.
         * @param location Standort des Chatclients.
         * @param messageListView FX Listview für Nachrichten.
         * @param messageList FX ObservableListe der Nachrichten.
         * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
         */
        public MessageListener(ChatClient chatClient, String location,ListView<String> messageListView, ObservableList<String> messageList) throws RemoteException {
            this.chatClient = chatClient;
            this.location = location;
            this.messageList = messageList;
            this.messageListView = messageListView;
            this.prev = chatClient.aktualisieren(location);

            startMessageListener();
        }

        /**
         * Erstellt einen Thread, der auf Änderungen auf dem Server wartet und die GUI aktualisiert.
         */
        private void startMessageListener(){

            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() {
                    try {
                        readChanges();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };

            t = new Thread(task);
            t.setDaemon(true);
            t.start();
        }

        /**
         * Lauscht auf Änderungen und aktualisiert die GUI.
         * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
         */
        private void readChanges() throws RemoteException {
            while (chatClient.isOnline()){
                int aktual = chatClient.aktualisieren(location);
                if(prev != aktual){
                    messageList = FXCollections.observableList(chatClient.getNachrichten(location));
                    messageListView.setItems(messageList);
                    prev = aktual;
                }
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

}
