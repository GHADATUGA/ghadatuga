package explodingkittens.client;
import explodingkittens.*;
import explodingkittens.BenutzerInterface;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Zuständig für die Verwaltung von Clients, die das System benutzen wollen.
 * @author Ghad
 */
public class BenutzerClient {
    private BenutzerInterface look_up;
    private String benutzername = null;

    /**
     * Konstruktor der BenutzerClient-Klasse.
     */
    public BenutzerClient() {
        try {
            Registry registry= LocateRegistry.getRegistry("192..xxxx.xx",8014);
            this.look_up= (BenutzerInterface) registry.lookup("benutzer");
        }catch (Exception e){
            System.err.println("Client Exception:"+ e.toString());
            e.printStackTrace();
        }
    }


    /**
     * Erhält den eingegebenen Namen und das eingegebene Passwort des neuen Benutzers und leitet sie weiter an die Benutzer-Klasse(remote serverworker).
     * @param username Gewünschter Benutzername des neuen Clients (Benutzer).
     * @param kennwort Gewünschtes Passwort.
     * @param kennwort2 Wiederholung des gewünschten Passworts.
     * @throws PasswortFalschWiederholtException Passwort ist Falsch Wiederholt Exception
     * @throws LeereEingabeException Leere Eingabe Exception
     * @throws BenutzernameSchonVergebenException Benutzername ist Schon Vergeben Exception
     * @throws EingabeZuLangException Eingabe ist ZuLang Exception
     * @throws RemoteException   wird geworfen
     */

    public void registrieren(String username, String kennwort, String kennwort2) throws RemoteException, BenutzernameSchonVergebenException,LeereEingabeException,PasswortFalschWiederholtException,EingabeZuLangException{
        look_up.registrieren(username, kennwort, kennwort2);
    }

    /**
     * Beim Anmelden wird Benutzername und Passwort übergeben.
     * @param username  Benutzername.
     * @param kennwort Entsprechendes Passwort.
     * @throws BenutzernameNichtExistentException Benutzername ist Nicht Existent Exception
     * @throws PasswortFalschException Passwort ist Falsch Exception
     * @throws SchonAngemeldetException Wenn der Benutzer schon angemeldet ist und sich nochmal anmelden will, wird diese Exception geworfen.
     * @throws BenutzernameNichtExistentException Wird geworfen, wenn der eingegebene Benutzername nicht im System vorhanden ist.
     * @throws RemoteException wird geworfen
     * @throws LeereEingabeException wenn ein Feld Leer ist.
     */
    public void anmelden(String username, String kennwort) throws RemoteException, PasswortFalschException, BenutzernameNichtExistentException, SchonAngemeldetException, LeereEingabeException {
        look_up.anmelden(username,kennwort);
        this.benutzername = username;
    }

    /**
     * Beim Ändern der Daten wwerden ein neuer Benutzername und ein neues Passwort gefordert.
     * @param neuBenutzername Neuer Benutzername.
     * @param neuPassword Neues Passwort.
     * @param passwordwiederholen Wiederholung des Passwortes.
     * @throws LeereEingabeException Leere Eingabe Exception
     * @throws EingabeZuLangException Eingabe ist ZuLang Exception
     * @throws PasswortFalschWiederholtException Passwort ist Falsch Wiederholt Exception
     * @throws BenutzernameSchonVergebenException Benutzername ist Schon Vergeben Exception
     * @throws BenutzernameNichtExistentException Benutzername ist Nicht Existent Exception
     * @throws RemoteException wird geworfen
     * @author Ghad
     */
    public void aendern( String neuBenutzername, String neuPassword, String passwordwiederholen) throws RemoteException, BenutzernameSchonVergebenException, BenutzernameNichtExistentException, EingabeZuLangException, LeereEingabeException, PasswortFalschWiederholtException {
        look_up.aendern(this.benutzername,neuBenutzername, neuPassword, passwordwiederholen);
    }

    /**
     * Beim Löschen der Daten müssen die aktuellen Benutzerdaten eingeben werden.
     * Das System löscht die gegebenen Daten.
     * @param name Benuztername
     * @param passwort Passwort
     * @throws BenutzernameNichtExistentException Benutzername ist Nicht Existent Exception
     * @throws PasswortFalschException Passwort ist Falsch Exception
     * @throws RemoteException wird geworfen
     * @throws LeereEingabeException wenn ein Feld leer ist
     * @author Ghad
     */
    public void loeschen(String name, String passwort) throws RemoteException, BenutzernameNichtExistentException, PasswortFalschException, LeereEingabeException {
        look_up.loeschen(name,passwort);
    }

    /**
     * Der BenutzerClient wird abgemeldet und aus der Liste der angemeldeten Benutzer gelöscht.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void abmelden() throws RemoteException {
        look_up.abmelden(this.benutzername);
    }

    /**
     * Getter für den Benutzernamen.
     * @return Name des Benutzers.
     */
    public String getBenutzername() {
        return benutzername;
    }
}