package explodingkittens.client;

import explodingkittens.BenutzernameNichtExistentException;
import explodingkittens.gui.BestenlisteGUI;
import explodingkittens.BestenlisteInterface;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

/**
 * Bestenliste-Client-Klasse
 * @author Hugo, erstellt am 12.06.2020.
 */
public class BestenlisteClient {
    private BestenlisteInterface look_up;
    private boolean online = true;
    public BestenlisteClient(){
        try {
            Registry registry = LocateRegistry.getRegistry(8014);
            this.look_up = (BestenlisteInterface) registry.lookup("bestenlist");
        } catch (Exception e) {
            System.err.println("BestenlisteClient exception: " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Signalisiert eine Änderung der Bestenliste.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void signal() throws RemoteException{
        look_up.signal();
    }

    /**
     * Aktualisiert die Bestenliste und erhöht die Anzahl der Siege des Spielers, der das Spiel gewonnen hat. Die Updates erfolgen am Ende des Spiels.
     * @param winner Benutzername des Spielers, der das Spiel gewonnen hat.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws BenutzernameNichtExistentException Exception, die ausgelöst wird, wenn der Benutzername nicht in der Bestenliste gespeichert ist.
     */
    public void updateList(String winner) throws RemoteException, BenutzernameNichtExistentException {
        look_up.updateList(winner);
    }

    /**
     * Gibt die aktuelle Bestenliste zurück
     * @return Bestenliste.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public ArrayList<BestenlisteGUI> getList() throws RemoteException{
        ArrayList<ArrayList<String>> usersInfo = look_up.getList();

        ArrayList<BestenlisteGUI> list = new ArrayList<>();
        for(ArrayList<String> userInfo : usersInfo){
            list.add(new BestenlisteGUI(userInfo.get(0), userInfo.get(1)));
        }
        return list;
    }

    /**
     * Prüft, ob die Daten auf dem Server aktualisiert wurden.
     * @return Gibt die Anzahl der Änderungen zurück.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public int aktualisieren() throws RemoteException {
        return look_up.aktualisieren();
    }

    /**
     * Prüft, ob der Bestenliste-Client noch aktiv ist.
     * @return true, wenn der BestenlisteClient noch ausgeführt wird; sonst falsch
     */
    private boolean isOnline() {
        return online;
    }

    /**
     * Setzt den Bestenliste-Client auf inaktiv.
     */
    public void setOffline(){
        this.online = false;
    }

    /**
     * Lauscht auf Änderungen auf dem Server und aktualisiert die Bestenliste.
     */
    public static class UpdateListener {
        private Thread t;
        private BestenlisteClient bestenlisteClient;
        private ObservableList<BestenlisteGUI> bestenlist;
        private TableView<BestenlisteGUI> table;
        private TableColumn<BestenlisteGUI, String> usernameBestenlist;
        private TableColumn<BestenlisteGUI, String> scoreBestenlist;
        private int prev;

        /**
         * Konstruktor für die UpdateListener-Klasse.
         * Initialisiert und startet einen Updatelistener auf die Bestenliste.
         * @param bestenlisteClient Bestenlisten-Client, der die Bestenlisten-Funktionen verwaltet.
         * @param table FX TableView für Bestenliste.
         * @param bestenlist Bestenlíste
         * @param usernameBestenlist FX TableColumn für Benutzername.
         * @param scoreBestenlist FX TableColumn für Punkte.
         * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
         */
        public UpdateListener(BestenlisteClient bestenlisteClient, TableView<BestenlisteGUI> table, ObservableList<BestenlisteGUI> bestenlist, TableColumn<BestenlisteGUI, String> usernameBestenlist, TableColumn<BestenlisteGUI, String> scoreBestenlist) throws RemoteException {
            this.bestenlisteClient = bestenlisteClient;
            this.table = table;
            this.bestenlist = bestenlist;
            this.usernameBestenlist = usernameBestenlist;
            this.scoreBestenlist = scoreBestenlist;
            this.prev = bestenlisteClient.aktualisieren();

            startUpdateListener();
        }

        /**
         * Erstellt einen Thread, der auf Änderungen auf dem Server wartet und die GUI aktualisiert.
         */
        private void startUpdateListener(){

            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() {
                    try {
                        readChanges();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };

            t = new Thread(task);
            t.setDaemon(true);
            t.start();
        }

        /**
         * Lauscht auf Änderungen und aktualisiert die GUI
         * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
         */
        private void readChanges() throws RemoteException {
            while (bestenlisteClient.isOnline()){
                int aktual = bestenlisteClient.aktualisieren();
                if(prev != aktual){
                    bestenlist = FXCollections.observableArrayList(bestenlisteClient.getList());
                    usernameBestenlist.setCellValueFactory(new PropertyValueFactory<>("username"));
                    scoreBestenlist.setCellValueFactory(new PropertyValueFactory<>("score"));
                    table.setItems(bestenlist);
                    prev = aktual;
                }
                try {
                    Thread.sleep(2000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

}
