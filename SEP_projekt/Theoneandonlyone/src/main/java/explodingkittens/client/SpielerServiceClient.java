package explodingkittens.client;

import explodingkittens.SpielerInterface;
import explodingkittens.SpielerServiceInterface;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Client für den SpielerService.
 */
public class SpielerServiceClient {

    private SpielerServiceInterface look_up;

    /**
     * Konstruktor der SpielerClient-Klasse.
     */
    public SpielerServiceClient() {
        try {
            Registry registry= LocateRegistry.getRegistry(8014);
            this.look_up = (SpielerServiceInterface) registry.lookup("spielerService");
        }catch (Exception e){
            throw new IllegalStateException("Fehler beim Verbinden mit dem Spielerservice", e);
        }
    }

    /**
     * Erzeugt einen neuen Spieler.
     * @param name Name des zu erstellenden Spielers.
     * @return SpielerInterface (erstellter Spieler)
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden konnte.
     */
    public SpielerInterface createSpieler(String name) throws RemoteException {
        return look_up.create(name);
    }
}
