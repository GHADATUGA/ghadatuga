package explodingkittens;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface, das von Benutzer implementiert wird.
 * @author Ghad
 */
public interface BenutzerInterface extends Remote {

    /**
     * Erhält den eingegebenen Namen und das Passwort vom neuen Benutzer und schickt leitet diese an Benutzer(remote serverworker) weiter.
     * @param name Gewünschter Benutzername des neuen Clients(Spieler).
     * @param kennwort  Gewünschtes Passwort.
     * @param kennwort2 Passwort Wiederholung.
     * @throws LeereEingabeException Leereingabe exception.
     * @throws BenutzernameSchonVergebenException Benutzername ist Schon VergebenException.
     * @throws EingabeZuLangException Eingabe ist ZuLang Exception.
     * @throws PasswortFalschWiederholtException Passwort ist falsch wiederholt.
     * @throws RemoteException Remote Exception wird gewolfen.
     */
        void registrieren(String name, String kennwort, String kennwort2) throws  RemoteException, EingabeZuLangException, BenutzernameSchonVergebenException, LeereEingabeException, PasswortFalschWiederholtException;

    /**
     * Beim Anmelden wird der Benutzername und das Passwort eingegeben.
     * @param name Benutzername.
     * @param kennwort Entsprechendes Passwort.
     * @throws BenutzernameNichtExistentException Benutzername ist nicht existent Exception.
     * @throws PasswortFalschException Passwort ist falsch Exception.
     * @throws  SchonAngemeldetException schon angemeldet Exception.
     * @throws RemoteException Remote Exception wird gewolfen.
     * @throws LeereEingabeException Leer Eingabe Exception
     */
         void anmelden(String name, String kennwort) throws RemoteException, PasswortFalschException, BenutzernameNichtExistentException, SchonAngemeldetException, LeereEingabeException;

    /**
     * Beim Ändern der Daten werden ein neuer Benutzername und ein neues Passwort gefordert.
     * @param altename aktueller Benutzername.
     * @param neuBenutzername Neuer Benutzername.
     * @param neuPassword Neues Passwort.
     * @param passwordwiederholen Wiederholung des Passwortes.
     * @throws LeereEingabeException Benutzername oder Passwort ist leer.
     * @throws EingabeZuLangException Benutzername oder Passwort ist zu lang.
     * @throws BenutzernameSchonVergebenException Benutzername ist schon vergeben Exception.
     * @throws BenutzernameNichtExistentException Benutzername ist nicht existent Exception.
     * @throws PasswortFalschWiederholtException Passwort falsch wiederholt Exception
     * @throws RemoteException Remote Exception wird gewolfen.
     */
         void aendern(String altename,String neuBenutzername,String neuPassword,String passwordwiederholen) throws RemoteException, BenutzernameSchonVergebenException, LeereEingabeException, PasswortFalschWiederholtException, EingabeZuLangException, BenutzernameNichtExistentException;

    /**
     * Beim Löschen der Daten müssen die aktuellen Benutzerdaten eingegeben werden.
     * Das System löscht die gegebenen Daten.
     * @param name Benutzername.
     * @param passwort Passwort.
     * @throws BenutzernameNichtExistentException Benutzername ist nicht existent Exception.
     * @throws PasswortFalschException Passwort ist falsch Exception.
     * @throws RemoteException Remote Exception wird gewolfen.
     * @throws  LeereEingabeException LeereEingabe Exception
     */
        void loeschen( String name, String passwort) throws RemoteException, PasswortFalschException, BenutzernameNichtExistentException, LeereEingabeException;
    /**
     * Benutzerclient wird abgemeldet.
     * @param name Name des abzumeldenden Benutzers.
     * @throws RemoteException Remote Exception.
     */
     void abmelden(String name) throws RemoteException;
    }


