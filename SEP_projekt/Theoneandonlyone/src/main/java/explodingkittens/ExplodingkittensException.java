package explodingkittens;

/**
 * Sammel-Exception.
 */
public class ExplodingkittensException extends Exception {
    /**
     * Konstruktor der Exploding-Kitten-Exception ohne Nachricht.
     */
    public ExplodingkittensException() {
        super();
    }

    /**
     * Konstruktor der Exploding-Kitten-Exception mit Nachricht.
     * @param message Exception-Nachricht.
     */
    public ExplodingkittensException(String message) {
        super(message);
    }
}
