package explodingkittens;

/**
 * Wenn die Eingabe (Benutzername oder Passwort) leer ist.
 */
public class LeereEingabeException extends ExplodingkittensException {
}
