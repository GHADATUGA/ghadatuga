package explodingkittens;

/**
 * Wenn der gewünschte neue Benutzername bereits vergeben ist.
 */
public class BenutzernameSchonVergebenException extends Exception {
}
