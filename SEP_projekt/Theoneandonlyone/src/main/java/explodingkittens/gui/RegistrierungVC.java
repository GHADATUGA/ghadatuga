package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.rmi.RemoteException;

/**
 * Controller-Klasse für den Registrierung. Steuert die logischen Abläufe, die über das GUI-Interface indiziert werden können.
 * @author Nico
 */
public class RegistrierungVC {
    private RegistrierungView view;
    private MainModel model;
    @FXML
    public Label exception_text;
    @FXML
    public PasswordField passwort1_entryfield;
    @FXML
    public PasswordField passwort2_entryfield;
    @FXML
    public TextField benutzername_entryfield;
    private BenutzerClient benutzerClient;
    private BestenlisteClient bestenlistClient;



    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger View-Klasse [RegistrierungView])
     */
    public RegistrierungVC(){

    }

    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     */
    public void initialize(MainModel model, RegistrierungView view){
        this.benutzerClient =  model.getBenutzerClient();
        this.bestenlistClient = model.getBestenlisteClient();
        this.view = view;
        this.model = model;
        this.view.getStage().show();
    }

    /**
     * Erstelle ein Objekt der View-Klasse, die für die Konstruktion der Registrierungs-Szene zuständig ist.
     * @param actionEvent Ereignis "Drücken des Abbrechen-Buttons"
     */
    @FXML
    public void abbrechen(ActionEvent actionEvent) {
        new VorraumView(model,this.view.getStage());
    }

    /**
     * Liest die Textfelder Benutzername und Passwort aus und führt einen Registrierungsversuch mit diesen
     * Daten aus. Je nach Ergebnis des Reg.-versuchs wird das Vorraum-Fenster geöffnet oder eine
     * spezifische Fehlermeldung angezeigt. Ausgelöst wird diese Aktion durch das Drücken des AccountErstellen-buttons.
     * @param actionEvent Ereignis "Drücken des AccountErstellen-Buttons"
     */
    @FXML
    public void registrieren(ActionEvent actionEvent) {
        this.view.hideExceptionText();
        String resultat="";
        String username = this.benutzername_entryfield.getText();
        String password1 = this.passwort1_entryfield.getText();
        String password2 = this.passwort2_entryfield.getText();
        try {
            this.benutzerClient.registrieren(username, password1, password2);
            this.bestenlistClient.signal();

            resultat = "Registrierung erfolgreich";
            new VorraumView(model, this.view.getStage()).setExceptionText(resultat);
        }
        catch (PasswortFalschWiederholtException e) {
            resultat = "Passwort falsch wiederholt!";
        } catch (BenutzernameSchonVergebenException e) {
            resultat ="Benutzername schon vergeben!";
        } catch (LeereEingabeException e) {
            resultat = "Leere Eingabe!!";
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (EingabeZuLangException e) {
            resultat = "Eingabe ist zu lang. Max. 20 Zeichen erlaubt!";
        }

        if (resultat != ""){
            this.view.setExceptionText(resultat);
        }


    }

    /**
     * @return Referenz zum Label, das ggf. dem User Feedback bei nicht erfolgreicher Anmeldung gibt
     */
    public Label getException_text() {
        return exception_text;
    }

}

