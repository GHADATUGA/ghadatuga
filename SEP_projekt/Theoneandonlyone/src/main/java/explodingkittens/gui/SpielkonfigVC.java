package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import explodingkittens.server.Bot;
import explodingkittens.server.SpielraumNichtExistentException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.rmi.RemoteException;
import java.util.*;

/**
 * Controller-Klasse für das Spielkonfig Fenster.
 * @author Kevin
 */
public class SpielkonfigVC {
    private Stage stage;
    private SpielkonfigView view;
    private MainModel model;
    private BenutzerClient benutzerClient;
    private String erstellerName;
    private Set<SpielerInterface> spielerList ;
    private ArrayList<BotInterface> botList ;
    //private String spielraumname;
    private SpielraumInterface sr;

    @FXML
    private TextField txtSpielraumname;
    @FXML
    private Button btnSpielraumloeschen;
    @FXML
    private Button btnKonfigverlassen;
    @FXML
    private Button btnSpielraumaendern;
    @FXML
    private Label labelSpielerList;
    @FXML
    private Label labelBotList;
    @FXML
    private Button btnSpielstarten;
    @FXML
    private Button btnBothinzufuegen;
    @FXML
    private Button btnSitzauswaehlen;
    @FXML
    private Label labelErsteller;
    @FXML
    private Button btnBotloeschen;

    @FXML
    private Label errorReport;

    @FXML
    private ListView<String> mitSpielerListView;

    private ObservableList<String> mitSpieler;
    private String currentSpielraumName;

    /**
     * Konstruktor, wird nur vom FXML-Loader benutzt.
     */
    public SpielkonfigVC() {
    }

    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     * Setzt den Namen des Erstellers fest, denn Ersteller und Mitspieler sehen unterschiedliche Buttons.
     * Falls der Benutzer der Ersteller ist, werden ihm alle Buttons außer "Sitz auswählen" angezeigt, da der Ersteller
     * automatisch einen Sitz im Spiel hat.
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     * @param stage Referenz zur Stage.
     * @param spielraumname Name des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws ExplodingkittensException Sammel-Exception, wird geworfen, wenn eine sonstige Exception auftritt.*/
    public void initialize(MainModel model, SpielkonfigView view, Stage stage, String spielraumname) throws RemoteException, ExplodingkittensException {
        this.stage = stage;
        this.model = model;
        this.currentSpielraumName= spielraumname;
        this.sr = model.getSpielraumServiceClient().getSpielraum(spielraumname);
        spielerList= sr.getSpieler();
        botList = sr.getBots();
        //spielerList.add(model.getSpieler());

        /*this.spielraumname = spielraumname;
        this.benutzerClient =  model.getBenutzerClient();
        if (this.erstellerName == null) {
            this.erstellerName = this.benutzerClient.getBenutzername();
            labelErsteller.setText(this.erstellerName);
            btnSitzauswaehlen.setVisible(false);
        } else {
            btnBotloeschen.setVisible(false);
            btnSpielraumloeschen.setVisible(false);
            btnSpielraumaendern.setVisible(false);
            btnBothinzufuegen.setVisible(false);
            btnSpielstarten.setVisible(false);
        }*/

        //TODO Timer/new Thread der spielerHinzufügen() jede Sekunde aufruft.

        mitSpieler = FXCollections.observableArrayList(sr.getMitSpieler());
        mitSpielerListView.setItems(mitSpieler);
        startListener();

        this.view = view;
        this.model = model;
        this.view.getStage().show();
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Bot hinzufügen".
     * Die Beschriftung der ToggleButtons wird geändert, falls mindestens ein freier Platz vor dem Knopfdruck
     * ausgewählt war. Diese Beschriftung wird später ausgelesen um zu ermitteln, wie viele Bots am Spiel teilnehmen.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
    */
    public void botHinzufuegen() throws RemoteException {
        if(botList.size()+spielerList.size() < 4){
            ArrayList<BotInterface> botList = sr.getBots();
            Bot b = new Bot("Bot_" + (botList.size()+1));
            try {
                sr.botHinzufuegen(b);
            } catch (SpielraumVollException e) {
                errorReport.setText("Spielraum voll");
            }
            //botList.add(b);
            //String labeltxt = labelBotList.getText()+"  "+b.getName();
            //labelBotList.setText(labeltxt);
        }
        //spielerHinzufuegen();
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Bot löschen".
     * Falls mindestens ein ToggleButton mit der Beschriftung "[Bot]" ausgewählt wurde, wird die Beschriftung wieder zu
     * frei geändert.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void botLoeschen() throws RemoteException {
        ArrayList<BotInterface> botList = sr.getBots();
        if(!botList.isEmpty()){
            int last = botList.size()-1;
            try {
                sr.botEntfernen(botList.get(last));
            } catch (KeinBotVorhandenException e) {
                errorReport.setText("Kein Bot vorhanden");
            }
            //botList.remove(last);
            //String labeltxt = labelBotList.getText();
            //int x = labeltxt.length();
            //labelBotList.setText(labeltxt.substring(0,x-7));
        }
        //spielerHinzufuegen();
    }

    /**
     * Spieler hinzufügen aus der Lobby
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */

    public void spielerHinzufuegen() throws RemoteException {
        Set<SpielerInterface> spielerList = sr.getSpieler();
        String listString = "";
        for(SpielerInterface spieler : spielerList){
            listString = listString + spieler.getName()+"  ";
        }
        labelSpielerList.setText(listString);
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Spielraum ändern".
     * Falls etwas in das entsprechende TextFeld eingetragen wurde, wird der Stage-Title angepasst.
     */
    public void spielraumNameAendern() {
        /*String neuerName = txtSpielraumname.getCharacters().toString();
        if (!neuerName.isEmpty()) {
            txtSpielraumname.setPromptText(neuerName);
            ((Stage) btnSpielstarten.getScene().getWindow()).setTitle("[Konfig] " + neuerName);
            txtSpielraumname.clear();
            txtSpielraumname.getParent().requestFocus();
        }*/

        String neuName = txtSpielraumname.getText();
        if(neuName!= null && !neuName.equals("")){
            try {
                model.getSpielraumServiceClient().aendern(model.getSpieler(), currentSpielraumName, neuName );
                model.getSpielraumServiceClient().signal();
                this.currentSpielraumName = neuName;
            } catch (NameSchonVergebenException e) {
                errorReport.setText("Name Schon Vergeben Exception");
            }catch (SpielraumNichtExistentException e){
                errorReport.setText("SpielraumNichtExistentException");
            } catch (RemoteException exception) {
                exception.printStackTrace();
            } catch (ExplodingkittensException e) {
                e.printStackTrace();
            }
            ((Stage) btnSpielstarten.getScene().getWindow()).setTitle("[Konfig] " + neuName);
            txtSpielraumname.setText("");
        }
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Spielraum verlassen".
     * Wechselt auf das Fenster der Lobby-Sicht. Vorher wird der Sitz des Spielers wieder freigegeben, wenn einer gewählt wurde.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws ErstellerException Wird geworfen, wenn der Ersteller den Spielraum verlassen will.*/
    public void konfigVerlassen() throws RemoteException, ErstellerException {
        //model.getSpielraumServiceClient().loeschen(model.getSpieler(), spielraumname);
        sr.verlassen(model.getSpieler());
        new LobbyView(model,this.view.getStage());
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Spiel starten".
     * Falls mindestens ein weiterer Teilnehmer einen Sitz im Spiel hat, wird das Spiel Fenster gestartet und die Szene gewechselt.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void spielStarten() throws RemoteException {
        if (mitSpieler.size()>1) {
            ArrayList<String> botListe = new ArrayList<>();
            ArrayList<BotInterface> botList = sr.getBots();
            for(BotInterface b : botList){
                botListe.add(b.getName());
            }
            Set<SpielerInterface> spielerSet = sr.getSpieler();
            ArrayList<String> spielerListe = new ArrayList<>();
            for(SpielerInterface sp : spielerSet){
                spielerListe.add(sp.getName());
            }
            sr.setGestartet();
            System.out.println(sr.getName()+ sr.isGestartet());
            //model.getSpielraumServiceClient().getSpielraum(currentSpielraumName).setGestartet();
            new SpielraumView(this.model, this.view.getStage(), currentSpielraumName, spielerListe, botListe);
        }
    }

    /**
     * Erstellt einen Thread, der auf Änderungen auf dem Server wartet und die GUI aktualisiert.
     */
    private void startListener(){
        Thread t;
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    readChanges();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        t = new Thread(task);
        t.setDaemon(true);
        t.start();
    }

    /**
     * Lauscht auf Änderungen und aktualisiert die GUI.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    private void readChanges() throws RemoteException {
        while (true){
            //mitSpieler.clear();
            mitSpieler= FXCollections.observableArrayList(sr.getMitSpieler());
            mitSpielerListView.setItems(mitSpieler);
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public void spielraumLoeschen() {
        try {
            model.getSpielraumServiceClient().getSpielraum(currentSpielraumName).verlassen(model.getSpieler());
            model.getSpielraumServiceClient().loeschen(model.getSpieler(), currentSpielraumName);
            model.getSpielraumServiceClient().signal();
            new LobbyView(model,this.view.getStage());
        }catch (NichtLeerException e) {
            errorReport.setText("NichtLeerException");
        }catch (SpielraumNichtExistentException e){
            errorReport.setText("SpielraumNichtExistentException");
        } catch (ExplodingkittensException e) {
            e.printStackTrace();
        } catch (RemoteException exception) {
            exception.printStackTrace();
        } catch (ErstellerException e) {
            e.printStackTrace();
        }
    }
}
