package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import explodingkittens.client.ChatClient;
import explodingkittens.client.SpielraumServiceClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller für die Spielraumerstellung (Zur Übergabe des Namens).
 *
 * @author Nico
 */
public class SpielraumErstellenVC {
    private static final Logger LOGGER = Logger.getLogger(SpielraumErstellenVC.class.getName());

    private SpielraumErstellenView view;
    private MainModel model;
    @FXML
    public Label exception_text;
    @FXML
    public TextField spielraumname_entryfield;

    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger View-Klasse [SpielraumErstellenView])
     */
    public SpielraumErstellenVC() {
    }

    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     *
     * @param model                  Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param spielraumErstellenView Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     */
    public void initialize(MainModel model, SpielraumErstellenView spielraumErstellenView) {
        this.view = spielraumErstellenView;
        this.model = model;
        //Zeige von der View-Klasse erzeugt Szene in der Stage
        view.getStage().show();
    }

    /**
     * Erstelle ein Objekt der View-Klasse, die für die Konstruktion der Lobby-Szene zuständig ist.
     *
     * @param actionEvent Ereignis "Drücken des Abbrechen-Buttons"
     */
    @FXML
    public void abbrechen(ActionEvent actionEvent) {
        new LobbyView(model, this.view.getStage());
    }

    @FXML
    public void bestaetigen(ActionEvent actionEvent) {
        //this.view.hideExceptionText();
        String resultat = "";
        String spielraumname = this.spielraumname_entryfield.getText();
        System.out.println(spielraumname);
        if(spielraumname == null || spielraumname.equals("")){
            exception_text.setText("Leer eingabe");
        }else {
            //new SpielkonfigView(model, this.view.getStage(), spielraumname);

            try {
                model.getSpielraumServiceClient().erstellen(model.getSpieler(), spielraumname);
                //resultat = "Registrierung erfolgreich";
                model.getSpielraumServiceClient().signal();
                //new SpielkonfigView(model, this.view.getStage(),spielraumname).setExceptionText(resultat);

                new LobbyView(model, view.getStage());

            } catch (NameSchonVergebenException e) {
                exception_text.setText("Spielraumname schon vergeben");
                spielraumname_entryfield.setText("");
            } catch (ExplodingkittensException | RemoteException e) {
                /*LOGGER.log(Level.WARNING, "spieler=" + model.getUsername() + ", spielraum=" + spielraumname, e);
                resultat = "Spielraumname schon vergeben";
                if (resultat != "") {
                    this.view.setExceptionText(resultat);
                }*/
            }
        }
    }


    /**
     * @return Referenz zum Label, das ggf. dem User Feedback bei nicht erfolgreicher Anmeldung gibt
     */
    public Label getException_text() {
        return exception_text;
    }

}
