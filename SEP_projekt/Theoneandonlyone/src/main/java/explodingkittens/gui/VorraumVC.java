package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.rmi.RemoteException;

/**
 * Controller-Klasse für das "Vorraum"-Fenster - steuert die logischen Abläufe, die über das GUI-Interface iniziiert werden können.
 * @author Desiree
 */
public class VorraumVC {
    private VorraumView view;
    private MainModel model;
    @FXML
    private Label exception_text;
    @FXML
    private PasswordField passwort_entryfield;
    @FXML
    private TextField benutzername_entryfield;
    private BenutzerClient benutzerClient;


    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger View-Klasse [VorraumView])
     */
    public VorraumVC(){
    }

    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     */

    public void initialize(MainModel model, VorraumView view){
        this.view = view;
        this.model = model;
        this.benutzerClient =  model.getBenutzerClient();
        //Zeige von der View-Klasse erzeugt Szene in der Stage
        view.getStage().show();
    }

    /**
     * Wechsel zum Registrierungsfenster, der beim Drücken des Registrierungsbuttons stattfindet.
     * @param mouseEvent Ereignis "Drücken des Registrieren-Buttons"
     */

    @FXML
    void changeToRegistration(MouseEvent mouseEvent) {
        new RegistrierungView(model,this.view.getStage());
    }

    /**
     * Liest die Textfelder Benutzername und Passwort aus und führt einen Anmeldeversuch mit diesen
     * Daten aus. Je nach Ergebnis des Anmeldeversuchs wird das Lobby-Fenster geöffnet oder eine
     * spezifische Fehlermeldung angezeigt. Ausgelöst wird diese Aktion durch das Drücken des Anmeldebuttons.
     * @param mouseEvent Ereignis "Drücken des Anmelde-Buttons"
     */
    @FXML
    void tryToLogIn(MouseEvent mouseEvent) {
        this.view.hideExceptionText();
        String result= "";

       try{
            //Textfelder auslesen
            String username = this.benutzername_entryfield.getText();
            String password = this.passwort_entryfield.getText();
            this.benutzerClient.anmelden(username,password);
            //falls Anmelden erfolgreich: Username im Model speichern und Aufbau des Lobby-Fensters
            model.setUsername(username);
            new LobbyView(model,this.view.getStage());
       }

        catch (PasswortFalschException e) {
            result = "Passwort falsch!";
        } catch (BenutzernameNichtExistentException e) {
            result ="Benutzername nicht registriert!";
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (SchonAngemeldetException e) {
           result="Schon angemeldet!!";
       }catch (LeereEingabeException e){
           result="Leer Eingabe!";
       }

        //zeige den Exception-Text an, falls ein Fehler beim Anmelden aufgetreten ist
        if (!result.equals("")){
            this.view.setExceptionText(result);
        }
    }

    // getter für die Nodes der Lobbyszene
    public Label getException_text() {
        return exception_text;
    }


}