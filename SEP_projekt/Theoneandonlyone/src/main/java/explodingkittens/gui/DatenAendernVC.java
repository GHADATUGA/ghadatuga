package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import explodingkittens.client.ChatClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Controller für die Datenänderung.
 *
 * @author Nico
 */
public class DatenAendernVC {
    @FXML
    public Label exception_text;
    @FXML
    public PasswordField nPasswort1_entryfield;
    @FXML
    public PasswordField nPasswort2_entryfield;
    @FXML
    public TextField nBenutzername_entryfield;
    private DatenAendernView view;
    private MainModel model;
    private BenutzerClient benutzerClient;
    private ChatClient chatClient;
    private BestenlisteClient bestenlisteClient;

    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger View-Klasse [DatenAendernView])
     */
    public DatenAendernVC() {
    }

    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     *
     * @param model            Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param datenAendernView Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     * @param chatClient       Referenz zum ChatClient.
     */
    public void initialize(MainModel model, DatenAendernView datenAendernView, ChatClient chatClient) {
        this.view = datenAendernView;
        this.model = model;
        this.benutzerClient = model.getBenutzerClient();
        this.chatClient = chatClient;
        this.bestenlisteClient = model.getBestenlisteClient();
        //Zeige von der View-Klasse erzeugt Szene in der Stage
        view.getStage().show();
    }

    /**
     * Erstelle ein Objekt der View-Klasse, die für die Konstruktion der Lobby-Szene zuständig ist.
     *
     * @param actionEvent Ereignis "Drücken des Abbrechen-Buttons"
     */
    @FXML
    public void abbrechen(ActionEvent actionEvent) {
        new LobbyView(model, this.view.getStage());
    }


    @FXML
    public void aenderungenSpeichern(ActionEvent actionEvent) {

        this.view.hideExceptionText();
        String resultat = "";
        String username = this.nBenutzername_entryfield.getText();
        String password1 = this.nPasswort1_entryfield.getText();
        String password2 = this.nPasswort2_entryfield.getText();

        try {
            this.benutzerClient.aendern(username, password1, password2);
            chatClient.setOffline();
            chatClient.nachrichtenLoeschen("spielerList", model.getUsername());
            bestenlisteClient.signal();
            bestenlisteClient.setOffline();
            model.getSpielraumServiceClient().setOffline();
            try {
                benutzerClient.abmelden();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new VorraumView(model, this.view.getStage());
            resultat = "\u00e4nderung erfolgreich";


        } catch (PasswortFalschWiederholtException e) {
            resultat = "Passwort falsch wiederholt!";
        } catch (BenutzernameSchonVergebenException e) {
            resultat = "Benutzername schon vergeben!";
        } catch (LeereEingabeException e) {
            resultat = "Leere Eingabe!!";
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (EingabeZuLangException e) {
            resultat = "Eingabe ist zu lang. Max. 20 Zeichen erlaubt!";
        } catch (BenutzernameNichtExistentException e) {
            resultat = "Benutzername ist schon vergeben!!";
        }

        if (!resultat.equals("")) {
            this.view.setExceptionText(resultat);
        }

    }

    /**
     * @return Referenz zum Label, das ggf. dem User Feedback bei nicht erfolgreicher Anmeldung gibt
     */
    public Label getException_text() {
        return exception_text;
    }

}
