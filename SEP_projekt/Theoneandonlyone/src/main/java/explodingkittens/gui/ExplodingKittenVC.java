package explodingkittens.gui;

import explodingkittens.client.SpielServerClient;
import explodingkittens.server.KarteNichtAufHandException;
import explodingkittens.server.NichtAktuellerTeilnehmerException;
import explodingkittens.server.PositionNichtMoeglichException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Controller-Klasse für das "Exploding Kitten"-Fenster - steuert die logischen Abläufe, die über das GUI-Interface iniziiert werden können.
 * @author Desiree
 */
public class ExplodingKittenVC {
    private MainModel model;
    private ExplodingKittenView view;
    @FXML
    private ProgressBar progress_bar;
    @FXML
    private Text restzeit_text;
    @FXML
    private Button entschaerfen_button;  //eventuell disablen ??
    private boolean button_pressed;
    private SpielServerClient ssc;
    private String location;

    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger View-Klasse [ExplodingKittenView])
     */
    public ExplodingKittenVC(){

    }
    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     * @param location Spielname.
     */
    public void initialize(MainModel model, ExplodingKittenView view, String location) {
        this.model=model;
        this.view = view;
        this.location = location;
        ssc = new SpielServerClient();
        button_pressed=false;
        //Zeige von der View-Klasse erzeugt Szene in der Stage
        view.getStage().show();
        updateProgressbar();

    }
    //getter für den Prozessbalken
    public ProgressBar getProgress_bar(){
        return this.progress_bar;
    }

    //getter für den Restzeit-Text
    public Text getRestzeit_text(){
        return this.restzeit_text;
    }
    //getter für den Entschärfungs-Button
    public Button getEntschaerfen_button(){
        return entschaerfen_button;
    }
    /**
     * Updatet den Prozessbalken kontinuierlich (jede Sekunde) nach Aufbau des Exploding-Kitten-Fensters.
     * Falls der Entschärfen-Button in dem Zeitfenster nicht gedrückt wurde, wird das GameOver-Fenster erzeugt.
     */
    private void updateProgressbar(){
        Integer rem_time = 20;
        Timer t = new Timer(true);
        class myTasks extends TimerTask{
            Integer time;
            public myTasks(Integer time){
                    this.time = time;}
            public void run(){
                if (time>=1 && button_pressed==false){
                    time = time - 1;
                    view.updateProgress((float) time/20);
                    view.changeRestzeit(time.toString());
                }
                else if (time<1 && button_pressed==false){
                    Platform.runLater(() -> {
                        try {
                            ssc.explodieren(location, model.getUsername());
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        ArrayList spieler=new ArrayList();
                        ArrayList bots=new ArrayList();
                        new SpielraumView(model,view.getStage(),location,spieler, bots);
                    });
                    this.cancel();
                }
                else{
                    view.disableButton();
                    this.cancel();
                }
        }}
        myTasks task = new myTasks(rem_time);

        t.scheduleAtFixedRate(task,1000,1000);


    }

    /**
     * Entschärfungsprozess, der durch das Drücken des "Entschärfen"-Buttons angestoßen wird.
     * @param actionEvent Betätigen des Entschärfen-Buttons
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void entscharfen(ActionEvent actionEvent) throws RemoteException {
        ZurueckBox posWaehler = new ZurueckBox((int)ssc.getSpielzustand(location).get(5));
        int pos=posWaehler.display();
        try {
            ssc.entschaerfen(location,model.getUsername(),pos);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (PositionNichtMoeglichException e) {
            e.printStackTrace();
        } catch (KarteNichtAufHandException e) {
            ssc.explodieren(location, model.getUsername());
        } catch (NichtAktuellerTeilnehmerException e) {
            e.printStackTrace();
        }
        button_pressed = true;
        ArrayList spieler=new ArrayList();
        ArrayList bots=new ArrayList();

        new SpielraumView(model,view.getStage(),location,spieler, bots);

    }
}
class ZurueckBox {
    static int maxstelle;
    static int stelle;

    public ZurueckBox(int maxstelle){
        this.maxstelle = maxstelle;
    }
    public int display() {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("");

        Label label1 = new Label();
        label1.setText("W\u00e4hle Position f\u00fcr die Exploding Kitten:");

        Label label2 = new Label();
        label2.setText("(Zwischen 0 (unten) und " + maxstelle + "(oben))");

        TextField textField = new TextField();

        Button okbutton = new Button("OK");
        okbutton.setOnAction(e -> {
            if (!textField.getText().isEmpty()) {
                int temp = Integer.parseInt(textField.getText());
                if(temp <= maxstelle){
                    stelle = temp;
                    window.close();
                }
            }
        });

        HBox layout = new HBox();
        layout.getChildren().addAll(label1, label2, textField, okbutton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return stelle;
    }
}
