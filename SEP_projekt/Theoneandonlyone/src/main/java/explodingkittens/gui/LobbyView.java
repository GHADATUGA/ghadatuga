package explodingkittens.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Lobby-Gui
 *
 * @author Nico
 */
public class LobbyView {
    private final MainModel model;
    private final Stage stage;
    private LobbyVC c;


    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     *
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage (1.)Stage, in der Szenen gesetzt und gewechselt werden können
     */

    public LobbyView(MainModel model, Stage stage) {
        this.model = model;
        this.stage = stage;

        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/Lobby1.fxml"));
            AnchorPane apane = loader.load();
            LobbyVC controller = loader.getController();
            this.c = controller;
            this.c.userInfo(model.getUsername());
            this.stage.setTitle("Lobby");

            Scene scene = new Scene(apane, 1000, 610);
            this.stage.setScene(scene);
            stage.setY(scene.getY());
            stage.setHeight(635);
            this.stage.centerOnScreen();
            stage.sizeToScene();
            //this.stage.resizableProperty().setValue(Boolean.FALSE);
            c.initialize(this.model, this);


            this.getReferences();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * gibt Referenzen
     */
    private void getReferences() {
    }

    /**
     * Gibt Aktuelle stage zurück.
     *
     * @return aktuelle stage.
     */
    public Stage getStage() {
        return this.stage;
    }
}

