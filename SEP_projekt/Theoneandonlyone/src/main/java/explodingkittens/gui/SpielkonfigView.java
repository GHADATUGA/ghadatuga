package explodingkittens.gui;

import explodingkittens.ExplodingkittensException;
import explodingkittens.client.BenutzerClient;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * View-Klasse des Spielkonfigfensters.
 * @author Kevin
 */
public class SpielkonfigView {
    private MainModel model;
    private Stage stage;
    private SpielkonfigVC c;

    /**
     * Konstruktor der SpielkonfigView-Klasse .
     * @param model globale Daten der GUI.
     * @param stage neue Szene wird in die Stage gesetzt, der Titel wird entsprechend geändert.
     * @param spielname Name des Spiels/Spielraums.
     */
    public SpielkonfigView(MainModel model, Stage stage, String spielname) {
        this.model = model;
        this.stage = stage;

        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/SpielekonfigErsteller.fxml"));
            Parent konfig = loader.load();
            SpielkonfigVC controller_konfig = loader.getController();
            this.c = controller_konfig;
            this.stage.setTitle("[Konfig] "+spielname);
            Scene scene = new Scene(konfig);
            this.stage.setScene(scene);
            stage.setY(0);
            stage.setHeight(300);
            this.stage.centerOnScreen();
            //this.stage.sizeToScene();
            controller_konfig.initialize(this.model, this, this.stage, spielname);
        }
        catch(IOException | ExplodingkittensException e){}
    }

    /**
     * Getter-Methode für die Stage.
     * @return die aktuelle Stage.
     */
    public Stage getStage() {
        return this.stage;
    }

    public void setExceptionText(String resultat) {
    }
}
