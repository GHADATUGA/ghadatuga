package explodingkittens.gui;

import explodingkittens.ExplodingkittensException;
import explodingkittens.NachrichtZuLangException;
import explodingkittens.SpielraumInterface;
import explodingkittens.SpielraumVollException;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import explodingkittens.client.ChatClient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

import java.awt.*;
import java.net.URI;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller-Klasse für die Lobby. Steuert die logischen Abläufe, die über das GUI-Interface indiziert werden können.
 *
 * @author Nico
 */
public class LobbyVC {//implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(LobbyVC.class.getName());
    private final String location = "Lobby";
    public TextField inputMsg;
    public ScrollPane chatScrollPane;
    public ScrollPane onlineBenutzer;
    public ScrollPane spielraumScrollPane;
    public Label benutzername;
    public Button abmelden;
    public Label errorReport;
    public ListView<String> messageListView;
    public ListView<String> userListView;
    private LobbyView view;
    private MainModel model;
    private ChatClient chatClient;
    private ListView<String> spielraumListView;
    private ObservableList<String> messageList;
    private ObservableList<String> userList;
    private ObservableList<BestenlisteGUI> bestenList;
    private String username;
    private BenutzerClient benutzerClient;
    private BestenlisteClient bestenlisteClient;

    @FXML
    private TableView<BestenlisteGUI> table;

    @FXML
    private TableColumn<BestenlisteGUI, String> usernameBestenlist;

    @FXML
    private TableColumn<BestenlisteGUI, String> scoreBestenlist;

    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger Lobby-Klasse [LobbyView])
     */
    public LobbyVC() {
    }

    /**
     * Initialisierung der Model-, Username- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     *
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     */
    public void initialize(MainModel model, LobbyView view) {
        LOGGER.log(Level.INFO, "init(model={0}, view={1})", new Object[]{model, view});

        this.benutzerClient = model.getBenutzerClient();
        this.bestenlisteClient = model.getBestenlisteClient();

        this.view = view;
        this.model = model;
        this.username = model.getUsername();

        try {
            chatClient.verfassen("spielerList", username);
            bestenList = FXCollections.observableArrayList(bestenlisteClient.getList());
            usernameBestenlist.setCellValueFactory(new PropertyValueFactory<>("username"));
            scoreBestenlist.setCellValueFactory(new PropertyValueFactory<>("score"));
            table.setItems(bestenList);
            new BestenlisteClient.UpdateListener(bestenlisteClient, table, bestenList, usernameBestenlist, scoreBestenlist);
        } catch (NachrichtZuLangException | RemoteException e) {
            LOGGER.log(Level.WARNING, "username=" + username, e);
        }

        model.getSpielraumServiceClient().setOnline();
        model.getSpielraumServiceClient().startChangeListener(spielraumListView);


        this.view.getStage().setOnCloseRequest(e -> {
            bestenlisteClient.setOffline();
            model.getSpielraumServiceClient().setOffline();
            chatClient.setOffline();
            try {
                chatClient.nachrichtenLoeschen("spielerList", username);
                benutzerClient.abmelden();
            } catch (RemoteException exception) {
                exception.printStackTrace();
            }

        });
        this.view.getStage().show();
    }

    /**
     * Beitreten eines Spielraums, falls ausgewählt.
     *
     * @param actionEvent Ereignis "Drücken des "Beitreten"-Buttons"
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @FXML
    void requestToJoin(ActionEvent actionEvent) throws RemoteException {
        errorReport.setText("");
        String spielraum = spielraumListView.getSelectionModel().getSelectedItem();
        if (spielraum != null) {
            try {
                SpielraumInterface sr = null;
                try {
                    sr = model.getSpielraumServiceClient().getSpielraum(spielraum);
                } catch (ExplodingkittensException e) {
                    e.printStackTrace();
                }
                assert sr != null;
                sr.beitreten(model.getSpieler());

                if (sr.getErsteller().getName().equalsIgnoreCase(model.getUsername())) {
                    new SpielkonfigView(model, this.view.getStage(), spielraum);
                } else {
                    try {
                        new BeitretenView(model, this.view.getStage(), spielraum);
                    } catch (ExplodingkittensException e) {
                        e.printStackTrace();
                    }
                    //errorReport.setText("your request to Join the Game was Successfull");
                }
            } catch (SpielraumVollException e) {
                errorReport.setText(" Spielraum schon voll!!");
            }
        }
        /*
        ArrayList<String> spieler = new ArrayList<>();
        spieler.add("sp1");
        spieler.add(model.getUsername());
        ArrayList<String> bots = new ArrayList();
        bots.add("bot1");
        new SpielraumView(model, view.getStage(),spielraum,spieler,bots);

         */

    }

    /*public void join() throws RemoteException, ExplodingkittensException {
        String spielraum = spielraumListView.getSelectionModel().getSelectedItem();
        if (spielraum != null) {
            SpielraumInterface sr = model.getSpielraumServiceClient().getSpielraum(spielraum);
            if (sr.isGestartet()) {
                System.out.println(sr.getName() + "Start");
                ArrayList<String> botListe = new ArrayList<>();
                ArrayList<BotInterface> botList = sr.getBots();
                for (BotInterface b : botList) {
                    botListe.add(b.getName());
                }
                Set<SpielerInterface> spielerSet = sr.getSpieler();
                ArrayList<String> spielerListe = new ArrayList<>();
                for (SpielerInterface sp : spielerSet) {
                    spielerListe.add(sp.getName());
                }
                new SpielraumView(this.model, this.view.getStage(), spielraum, spielerListe, botListe);
            }else{
                errorReport.setText("Spiel noch nicht gestartet");
            }
        }
    }*/

    /**
     * Laden einer neuen FXML-file (SpielekonfigErsteller) und erzeugen einer neuen Scene auf Basis dieser.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     *
     * @param actionEvent Ereignis "Drücken des "Erstellen"-Buttons"
     */
    @FXML
    public void erstellen(ActionEvent actionEvent) {
        new SpielraumErstellenView(model, this.view.getStage());
    }

    /**
     * Öffnet den Link zum Regelwerk im Browser.
     *
     * @param actionEvent Ereignis "Drücken des Spielregeln-Buttons"
     */
    @FXML
    public void zeigeRegeln(ActionEvent actionEvent) {
        try {
            Desktop.getDesktop().browse(new URI("https://explodingkittens.com/downloads/rules/translations/Exploding-Kittens_Rules_DE.pdf"));
        } catch (Exception /* IOException, URISyntaxException */ e) {
            e.printStackTrace();
        }
       /* try {
            new ProcessBuilder("x-www-browser", "https://explodingkittens.com/downloads/rules/translations/Exploding-Kittens_Rules_DE.pdf").start();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    /**
     * Erstelle ein Objekt der View-Klasse, die für die Konstruktion der DatenAendernszene zuständig ist.
     *
     * @param actionEvent Ereignis "Drücken des "Anmeldedaten Ändern"-Buttons"
     */
    @FXML
    public void anmeldedatenAendern(ActionEvent actionEvent) {
        new DatenAendernView(model, this.view.getStage(), chatClient);
    }

    /**
     * Erstelle ein Objekt der View-Klasse, die für die Konstruktion der Vorraumszene zuständig ist.
     * Außerdem werden alle Nachrichten gelöscht.
     *
     * @param actionEvent Ereignis "Drücken des abmelden-Buttons"
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @FXML
    public void abmelden(ActionEvent actionEvent) throws RemoteException {
        bestenlisteClient.setOffline();
        model.getSpielraumServiceClient().setOffline();
        chatClient.setOffline();
        chatClient.nachrichtenLoeschen("spielerList", username);
        benutzerClient.abmelden();
        new VorraumView(model, this.view.getStage());
    }

    /**
     * Erstelle ein Objekt der View-Klasse, die für die Konstruktion der DatenLoeschenszene zuständig ist.
     *
     * @param actionEvent Ereignis "Drücken des "Account Löschen"-Buttons"
     */
    @FXML
    public void accountLoeschen(ActionEvent actionEvent) {
        new DatenLoeschenView(model, this.view.getStage(), chatClient);
    }

    /**
     * Chatclient verfasst die Nachricht von Spieler
     * und wenn die Nachricht zu lang ist, wird eine NachrichtZuLangException geworfen.
     *
     * @param actionEvent Ereignis "Drücken der Enter-Taste im Textfield"
     */
    @FXML
    public void enterSendChat(ActionEvent actionEvent) {
        errorReport.setText("");
        String textMessage = inputMsg.getText();

        String msg = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")) +
                " - " + username + " :: " + textMessage;
        try {
            chatClient.verfassen(location, msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NachrichtZuLangException e) {
            errorReport.setText(" Nachricht zu lang!!");
        }

        inputMsg.setText("");
    }

    /**
     * Chatclient verfasst die Nachricht von Spieler
     * und wenn die Nachricht zu lang ist, wird eine NachrichtZuLangException geworfen.
     *
     * @param mouseEvent Ereignis "Drücken des Send-Buttons"
     */
    @FXML
    public void send(MouseEvent mouseEvent) {
        errorReport.setText("");
        String textMessage = inputMsg.getText();

        String msg = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")) +
                " - " + username + " :: " + textMessage;
        try {
            chatClient.verfassen(location, msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NachrichtZuLangException e) {
            errorReport.setText(" Nachricht zu lang!!");
        }

        inputMsg.setText("");
    }

    /**
     * Erstelle ein Objekt von Chatclient und zwei MessageListener(Einer lauscht nach neuen Nachrichten
     * und der andere nach neu angemeldeten Benutzern).
     * initialize FX messageList(List von Nachrichten der Spieler) und UserList(List der angemeldeten Spieler).
     */
    public void initialize() {
        LOGGER.log(Level.INFO, "init()");

        chatClient = new ChatClient();

        try {

            messageList = FXCollections.observableArrayList(chatClient.getNachrichten(location));
            userList = FXCollections.observableArrayList(chatClient.getNachrichten("spielerList"));

        } catch (RemoteException e) {
            e.printStackTrace();
        }

        messageListView = new ListView<>(messageList);
        messageListView.setPrefWidth(458);
        messageListView.setPrefHeight(567);

        userListView = new ListView<>(userList);
        userListView.setPrefWidth(500);
        userListView.setPrefHeight(242);

        try {
            new ChatClient.MessageListener(chatClient, location, messageListView, messageList);
            new ChatClient.MessageListener(chatClient, "spielerList", userListView, userList);
        } catch (RemoteException exception) {
            exception.printStackTrace();
        }

        spielraumListView = new ListView<>();
        spielraumListView.setPrefWidth(496);
        spielraumListView.setPrefHeight(608);

        onlineBenutzer.setContent(userListView);
        chatScrollPane.setContent(messageListView);

        spielraumScrollPane.setContent(spielraumListView);
    }


    /**
     * beschriftet den Benutzername in der ChatBox-Anwendung.
     *
     * @param username Benutzername des angemeldeten Benutzers.
     */
    public void userInfo(String username) {
        benutzername.setText("Angemeldet als " + username);
    }


}
