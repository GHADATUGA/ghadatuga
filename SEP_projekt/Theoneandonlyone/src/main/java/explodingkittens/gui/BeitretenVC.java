package explodingkittens.gui;

import explodingkittens.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Set;

/**
 * Controller für das Beitritts-Fenster (für Nicht-Ersteller).
 */
public class BeitretenVC {
    private Stage stage;
    private BeitretenView view;
    private MainModel model;
    private SpielraumInterface sr;
    private String spielraumname;

    @FXML
    private Label beitrittID;
    @FXML
    private Button btnVerlassen;
    @FXML
    private Label errorReport;

    /**
     * Konstruktor. Wird nur vom FXML-Loader benutzt.
     */
    public BeitretenVC(){
    }

    /**
     * Initialisiert die benötigten Referenzen.
     * @param model Referenz zu  Model (speichert global in der GUI benötigte Daten).
     * @param view Refernz zur View (Nodemanipulation)
     * @param spielraumname Name des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt aufgebaut werden kann.
     * @throws ExplodingkittensException Sammel-Exception.
     */
    public void initialize(MainModel model, BeitretenView view, String spielraumname ) throws RemoteException, ExplodingkittensException {
        this.stage = stage;
        this.model = model;
        this.view = view;
        this.sr = model.getSpielraumServiceClient().getSpielraum(spielraumname);

        this.spielraumname = spielraumname;

        this.beitrittID.setText(spielraumname);
        this.view.getStage().show();



        //updateBeitrete();


    }

    /*private void updateBeitrete(){
        Timer t = new Timer(true);
        class myTasks extends TimerTask {
            public myTasks(){
            }
            public void run(){
                try {
                    if(sr.isGestartet()){
                       beitreten();
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
        myTasks task = new myTasks();

        t.scheduleAtFixedRate(task,100,500);

    }*/

    /**
     * Verlassen des Wartefensters.
     * @throws ErstellerException Wird geworfen, falls der Ersteller den Spielraum verlassen will.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt aufgebaut werden kann.
     */
    @FXML
    void verlassen() throws ErstellerException, RemoteException {
        sr.verlassen(model.getSpieler());
        new LobbyView(model,this.view.getStage());
    }

    /**
     * Betreten des Spielraums.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt aufgebaut werden kann.
     * @throws ExplodingkittensException Sammel-Exception.
     */
    public void join() throws RemoteException, ExplodingkittensException {
        SpielraumInterface sr = model.getSpielraumServiceClient().getSpielraum(spielraumname);
        if (sr.isGestartet()) {
            System.out.println(sr.getName() + "Start");
            ArrayList<String> botListe = new ArrayList<>();
            ArrayList<BotInterface> botList = sr.getBots();
            for (BotInterface b : botList) {
                botListe.add(b.getName());
            }
            Set<SpielerInterface> spielerSet = sr.getSpieler();
            ArrayList<String> spielerListe = new ArrayList<>();
            for (SpielerInterface sp : spielerSet) {
                spielerListe.add(sp.getName());
            }
            new SpielraumView(this.model, this.view.getStage(), spielraumname, spielerListe, botListe);
        }else{
            errorReport.setText("Spiel noch nicht gestartet");
        }
    }

    /*void beitreten() throws RemoteException {
        System.out.println("Start");
        ArrayList<String> botListe = new ArrayList<>();
        ArrayList<BotInterface> botList = sr.getBots();
        for(BotInterface b : botList){
            botListe.add(b.getName());
        }
        System.out.println("NachBot");
        Set<SpielerInterface> spielerSet = sr.getSpieler();
        ArrayList<String> spielerListe = new ArrayList<>();
        for(SpielerInterface sp : spielerSet){
            spielerListe.add(sp.getName());
            System.out.println(sp.getName());
        }
        new SpielraumView(this.model, this.view.getStage(), this.stage.getTitle(), spielerListe, botListe);
    }*/

}