package explodingkittens.gui;


import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
/**
 * View-Klasse des Registrierungs-Fensters. Erzeugt die entsprechende Szene und modifiziert Nodes der Szene.
 * @author Nico
 */
public class RegistrierungView {
    private MainModel model;
    private Stage stage;
    private RegistrierungVC c;
    private Label exception_text;

    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage, in der Szenen gesetzt und gewechselt werden können
     */
    public RegistrierungView(MainModel model, Stage stage) {
        this.model = model;
        this.stage = stage;

        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/reg.fxml"));
            AnchorPane apane = loader.load();
            RegistrierungVC controller_registrierung = loader.getController();
            this.c = controller_registrierung;
            this.stage.setScene(new Scene(apane, 400, 350));
            stage.sizeToScene();
           // this.stage.resizableProperty().setValue(Boolean.FALSE);
            controller_registrierung.initialize(this.model, this);

            this.getReferences();

        }
        catch(IOException e){}
    }

    /**
     * Setzt Referenz zum Label für Exceptiontext.
     */
    private void getReferences() {
        this.exception_text = c.getException_text();


    }

    /**
     * @return aktuelle stage.
     */
    public Stage getStage() {return this.stage;
    }

    /**
     * Setzt das Label Exception-Text auf unsichtbar.
     */
    public void hideExceptionText() {
        exception_text.setVisible(false);
    }

    /**
     * setzt den Exception-Text.
     * @param  resultat enthält gewünschten Text.
     */
    public void setExceptionText(String resultat) {
        exception_text.setText(resultat);
        exception_text.setVisible(true);
    }
}
