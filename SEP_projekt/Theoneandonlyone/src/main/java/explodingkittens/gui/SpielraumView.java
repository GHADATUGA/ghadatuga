package explodingkittens.gui;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * View-Klasse für die Spiel-GUI.
 */
public class SpielraumView {
    private MainModel model;
    private Stage stage;
    private SpielraumVC c;
    private String erstellerName;
    private List<String> spielerListe;

    /**
     * Konstruktor der SpielkonfigView-Klasse.
     * @param model globale Daten der GUI.
     * @param location Spielraumname
     * @param stage neue Szene wird in die Stage gesetzt, der Titel wird entsprechend geändert.
     * @param spieler Liste der Spieler, die mitspielen wollen.
     * @param bots Liste der Bots, die mitspielen.
     */
    public SpielraumView(MainModel model, Stage stage, String location, ArrayList<String> spieler, ArrayList<String> bots) {
        this.model = model;
        this.stage = stage;

        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Spielraum.fxml"));
            Parent spielraum = loader.load();
            SpielraumVC controller_spielraum = loader.getController();
            this.c = controller_spielraum;

            controller_spielraum.initialize(this.model, this,stage, location, spieler, bots);

           //set Stage boundaries to visible bounds of the main screen
            Screen screen = Screen.getPrimary();
            Rectangle2D bounds = screen.getVisualBounds();
            stage.centerOnScreen();
            stage.setX(bounds.getMinX());
            stage.setY(bounds.getMinY());
            stage.setWidth(bounds.getWidth());
            stage.setHeight(bounds.getHeight());

            stage.setScene(new Scene(spielraum));
            //String fenstername = this.stage.getTitle();
            //this.stage.setTitle("[Spiel]" + fenstername.substring(9));


            stage.sizeToScene();

        }
        catch(IOException e){}
    }

    /**
     * Getter-Methode für die Stage.
     * @return die aktuelle Stage.
     */
    public Stage getStage() {
        return this.stage;
    }

}
