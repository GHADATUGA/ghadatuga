package explodingkittens.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * View-Klasse des Vorraum-Fensters - erzeugt die Stage und ist für die Node-Manipulation zuständig.
 * @author Desiree
 */
public class VorraumView {
    private MainModel model;
    private Stage stage;
    private Label exception_text;
    private VorraumVC c;

    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage (1.)Stage, in der Szenen gesetzt und gewechselt werden können
     */

    public VorraumView(MainModel model, Stage stage){
        this.model = model;
        this.stage = stage;
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Vorraum.fxml"));
            Parent root = loader.load();
            VorraumVC controller_vorraum = loader.getController();
            this.c = controller_vorraum;

            this.stage.setScene(new Scene(root, 400, 430));
          // setting auflösung
            //Scene scene = new Scene(root, 400, 400);
            //this.stage.setScene(scene);
            this.stage.setY(400);
          this.stage.setHeight(430);
            this.stage.centerOnScreen();
            //this.stage.resizableProperty().setValue(Boolean.FALSE);
            stage.sizeToScene();
            controller_vorraum.initialize(this.model, this);
            this.getExceptionLabelReference();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Setzt das Label Exception-Text auf unsichtbar.
     */
    public void hideExceptionText(){
        exception_text.setVisible(false);
    }

    private void getExceptionLabelReference() {
        this.exception_text = c.getException_text();
    }

    public Stage getStage() {
        return this.stage;
    }

    public void setExceptionText(String result) {
        exception_text.setText(result);
        exception_text.setVisible(true);
    }


}
