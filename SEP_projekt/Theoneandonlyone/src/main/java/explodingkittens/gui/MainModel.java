package explodingkittens.gui;

import explodingkittens.SpielerInterface;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import explodingkittens.client.SpielerServiceClient;
import explodingkittens.client.SpielraumServiceClient;

import java.rmi.RemoteException;

/**
 * Modell-Klasse (enthält global benötigte Daten des gesamten GUI-Interfaces)
 */
public class MainModel {
    private String username;
    private BenutzerClient benutzerClient;
    private BestenlisteClient bestenlisteClient;
    private SpielerServiceClient spielerServiceClient;
    private SpielraumServiceClient spielraumServiceClient;
    private SpielerInterface spielerInterface;

    /**
     * Konstruktor der Modell-Klasse. Erzeugt neue Objekte des Benutzer- und Bestenlisten-Clients und speichert deren referenzen.
     */
    public MainModel(){
        //Initialisierung der Attribute
        this.benutzerClient = new BenutzerClient();
        this.bestenlisteClient = new BestenlisteClient();
        this.spielerServiceClient = new SpielerServiceClient();
        this.spielraumServiceClient = new SpielraumServiceClient();
    }

    /**
     * Setzt den Benutzernamen.
     * @param username Zu setzender Benutzername.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void setUsername(String username) throws RemoteException {
        this.username = username;
        spielerInterface = spielerServiceClient.createSpieler(username);
    }

    /**
     * @return Gespeicherter Benutzername.
     */
    public String getUsername(){
        return username;
    }


    public SpielerInterface getSpieler() {
        if (spielerInterface == null) {
            throw new IllegalStateException("Spieler wurde noch nicht erzeugt!");
        }
        return spielerInterface;
    }

    /**
     *
     * @return Referenz zum Benutzer-Client.
     */
    public BenutzerClient getBenutzerClient(){
        return benutzerClient;
    }

    /**
     *
     * @return Referenz zum Bestenlisten-Client.
     */
    public BestenlisteClient getBestenlisteClient(){
        return bestenlisteClient;
    }

    public SpielraumServiceClient getSpielraumServiceClient(){return  spielraumServiceClient;}


}
