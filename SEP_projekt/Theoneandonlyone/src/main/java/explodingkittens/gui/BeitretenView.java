package explodingkittens.gui;

import explodingkittens.ExplodingkittensException;
import explodingkittens.SpielraumInterface;
import explodingkittens.client.ChatClient;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * View-Klasse für das "Beitreten"-Fenster - erzeugt die Stage und ist für die Node-Manipulation zuständig.
 * @author Kevin
 */
public class BeitretenView {
    private MainModel model;
    private Stage stage;
    private BeitretenVC c;

    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage (1.)Stage, in der Szenen gesetzt und gewechselt werden können
     * @param spielraumname Name des Spielraums.
     * @throws ExplodingkittensException Sammel-Exception, wird geworfen, wenn eine sonstige Exception auftritt.
     */

    public BeitretenView(MainModel model, Stage stage, String spielraumname) throws ExplodingkittensException {
        this.model = model;
        this.stage = stage;
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Beitreten.fxml"));
            Parent root = loader.load();
            BeitretenVC controller = loader.getController();
            this.c = controller;

            Scene scene = new Scene(root,400,350);
            this.stage.setScene(scene);
            stage.setY(400);
            stage.setHeight(350);
            this.stage.centerOnScreen();
           // this.stage.resizableProperty().setValue(Boolean.FALSE);
            controller.initialize(this.model, this, spielraumname);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public Stage getStage() {
        return this.stage;
    }

}