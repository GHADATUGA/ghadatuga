package explodingkittens.gui;

/**
 * @author Hugo
 */
public class BestenlisteGUI {
    String username;
    String score;
    public BestenlisteGUI(String username, String score){
        this.username = username;
        this.score = score;
    }
    public String getUsername(){
        return this.username;
    }
    public String getScore(){
        return this.score;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public void setScore(String score){
        this.score = score;
    }
}
