package explodingkittens.gui;

import explodingkittens.client.ChatClient;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * View-Klasse des Datenaenderungs-Fensters. Erzeugt die entsprechende Szene und modifiziert Nodes der Szene.
 *
 * @author Nico
 */
public class DatenAendernView {
    private final MainModel model;
    private final Stage stage;
    private final ChatClient chatClient;
    private DatenAendernVC c;
    private Label exception_text;


    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     *
     * @param model      Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage,     in der Szenen gesetzt und gewechselt werden können
     * @param chatClient Referenz zum ChatClient.
     */
    public DatenAendernView(MainModel model, Stage stage, ChatClient chatClient) {
        this.model = model;
        this.stage = stage;
        this.chatClient = chatClient;


        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/Daten_aendern.fxml"));
            AnchorPane apane = loader.load();
            DatenAendernVC controller = loader.getController();
            this.c = controller;

            this.stage.setTitle("Anmeldedaten \u00e4ndern");
            this.stage.setScene(new Scene(apane, 400, 350));
            // Scene scene = new Scene(apane,400,400);
            //this.stage.setScene(scene);
            this.stage.setY(400);
            this.stage.setHeight(400);
            this.stage.centerOnScreen();
            //this.stage.resizableProperty().setValue(Boolean.FALSE);
            stage.sizeToScene();
            controller.initialize(this.model, this, chatClient);

            this.getReferences();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setzt Referenz zum Label für Exceptiontext.
     */
    private void getReferences() {
        this.exception_text = c.getException_text();
    }

    /**
     * Gibt Referenz zum Label für Exceptiontext.
     *
     * @return aktuelle stage.
     */
    public Stage getStage() {
        return this.stage;
    }

    /**
     * Setzt das Label Exception-Text auf unsichtbar.
     */
    public void hideExceptionText() {
        exception_text.setVisible(false);
    }

    /**
     * Setzt das Label Exception-Text.
     *
     * @param resultat enthält gewünschten Text.
     */
    public void setExceptionText(String resultat) {
        exception_text.setText(resultat);
        exception_text.setVisible(true);
    }


}

