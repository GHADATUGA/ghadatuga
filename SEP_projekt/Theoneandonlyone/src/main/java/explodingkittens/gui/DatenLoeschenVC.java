package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import explodingkittens.client.ChatClient;

import java.io.IOException;
import java.rmi.RemoteException;
/**
 * Controller-Klasse für das "Daten löschen"-Fenster - steuert die logischen Abläufe, die über das GUI-Interface iniziiert werden können.
 * @author Desiree
 */
public class DatenLoeschenVC {
    private BenutzerClient benutzerClient;
    private DatenLoeschenView view;
    private MainModel model;
    private ChatClient chatClient;
    @FXML
    private Label exception_label;
    @FXML
    private PasswordField passwort_entryfield;
    @FXML
    private TextField benutzername_entryfield;
    private BestenlisteClient bestenlisteClient;

    /**
     * Controller-Konstruktor, der ausschließlich vom FXML-Loader benutzt wird (in zugehöriger View-Klasse [DatenLoeschenView])
     */
    public DatenLoeschenVC(){
    }

    /**
     * Initialisierung der Model- und View-Referenz. Wird über die View-Klasse aufgerufen. Kann nicht im Konstruktor
     * aufgerufen werden, da Objekt der Klasse vom FXML-Loader erzeugt wird und dieser diese Referenzen nicht mitgeben kann.
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     * @param chatClient Referenz zum Chat-Client (zuständig für die Chatmanipulation/-anzeige)
     */

    public void initialize(MainModel model, DatenLoeschenView view, ChatClient chatClient){
        this.view = view;
        this.model = model;
        this.benutzerClient = model.getBenutzerClient();
        this.bestenlisteClient = model.getBestenlisteClient();
        this.chatClient = chatClient;
        //Zeige von der View-Klasse erzeugt Szene in der Stage
        view.getStage().show();
    }
    /**
     * Wechsel zur Lobby, der beim Drücken des Abbrechenbuttons stattfindet.
     * @param actionEvent Ereignis "Drücken des Abbrechen-Buttons"
     */

    @FXML
    void return_to_lobby(ActionEvent actionEvent) {
        new LobbyView(model,this.view.getStage());
    }

    /**
     * Liest die Textfelder Benutzername und Passwort aus und führt einen Löschversuch mit diesen
     * Daten aus. Je nach Ergebnis des Löschversuchs wird das Vorraum-Fenster geöffnet oder eine
     * spezifische Fehlermeldung angezeigt. Ausgelöst wird diese Aktion durch das Drücken des Löschenbuttons.
     * @param actionEvent Ereignis "Drücken des Löschen-Buttons"
     */
    @FXML
    void try_to_delete(ActionEvent actionEvent) {
        this.view.hideExceptionText();
        String result= "";

        try{
            //Textfelder auslesen
            String username = this.benutzername_entryfield.getText();
            String password = this.passwort_entryfield.getText();
            this.benutzerClient.loeschen(username,password);
            //falls Löschen erfolgreich: Username im Model löschen und Aufbau des Vorraum-Fensters
            chatClient.setOffline();
            chatClient.nachrichtenLoeschen("spielerList", model.getUsername());
            bestenlisteClient.signal();
            bestenlisteClient.setOffline();
            model.getSpielraumServiceClient().setOffline();
            try{
                benutzerClient.abmelden();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            new VorraumView(model,this.view.getStage());
        }

        catch (PasswortFalschException e) {
            result = "Passwort falsch!";
        } catch (BenutzernameNichtExistentException e) {
            result ="Benutzername nicht registriert!";
        } catch (RemoteException e) {
            e.printStackTrace();
        }catch (LeereEingabeException e){
            result="Leer Eingabe!";
        }

        //zeige den Exception-Text an, falls ein Fehler beim Löschen aufgetreten ist
        if (!result.equals("")){
            this.view.setExceptionText(result);
        }
    }

    // getter für das Exception_Label
    public Label getException_text() {
        return exception_label;
    }

}
