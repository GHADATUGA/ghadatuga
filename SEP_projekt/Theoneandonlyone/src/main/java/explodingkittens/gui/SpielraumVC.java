package explodingkittens.gui;

import explodingkittens.*;
import explodingkittens.client.BenutzerClient;
import explodingkittens.client.BestenlisteClient;
import explodingkittens.client.ChatClient;
import explodingkittens.client.SpielServerClient;
import explodingkittens.server.KarteNichtAufHandException;
import explodingkittens.server.KarteNichtAusspielbarException;
import explodingkittens.server.NichtAktuellerTeilnehmerException;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Controller für die GUI des Kern-Spiels.
 * @author Kevin, Desiree
 */
public class SpielraumVC {
    private SpielraumView view;
    private MainModel model;
    private BenutzerClient benutzerClient;
    private BestenlisteClient bestenlisteClient;
    private ArrayList<String> spielerListe;
    private ArrayList<ImageView> ablagestapel;
    private ImageView gewählteHandkarte;
    private SpielServerClient ssc;
    private String spielraumName;
    private int stand;
    @FXML
    private HBox gegner;
    @FXML
    private Label labelExplodierChance;
    @FXML
    private Button btnKartespielen;
    @FXML
    private Button btnZugbeenden;
    @FXML
    private Button btnSpielraumverlassen;
    @FXML
    private Label anzSpielstapelkarten;
    @FXML
    private ImageView imgAblagestapel;
    @FXML
    private Label anzAblagestapelkarten;
    @FXML
    private Label labelAmZug;
    @FXML
    private VBox vbxSpielerA;
    @FXML
    private Label labelSpielerA;
    @FXML
    private Label anzKartenSpielerA;
    @FXML
    private VBox vbxSpielerB;
    @FXML
    private Label labelSpielerB;
    @FXML
    private Label anzKartenSpielerB;
    @FXML
    private VBox vbxSpielerC;
    @FXML
    private Label labelSpielerC;
    @FXML
    private Label anzKartenSpielerC;
    @FXML
    private HBox hand;
    @FXML
    private Label exceptionText;



    private ListView<String> messageListView;

    @FXML
    private TextField inputMsg;

    @FXML
    private Label errorReport;

    @FXML
    private ScrollPane chatScrollPane;
    private ChatClient chatClient;
    private ObservableList<String> messageList;
    private String location;
    private Stage stage;


    /**
     * Konstruktor, wird nur vom FXML-Loader benutzt.
     */
    public SpielraumVC() {
    }


    /**
     * Methode um anzuzeigen, welche Karten sich im Ablagestapel befinden.
     * @param event bei Klick auf den Ablegestapel.
     */
    @FXML
    void klickAblagestapel(MouseEvent event) {
        KartenBox.display(ablagestapel);
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Spielraum verlassen".
     * Wechsel des Fensters zur LobbyView um das Spiel zu verlassen.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @FXML
    public void spielraumVerlassen() throws RemoteException {
        ArrayList ergebnis = ssc.verlassen(model.getUsername(), location);




        try {
            model.getSpielraumServiceClient().getSpielraum(location).verlassen(model.getSpieler());
        } catch (ErstellerException | ExplodingkittensException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            if(model.getSpielraumServiceClient().getSpielraum(location).getSpieler().size()==0){
                model.getSpielraumServiceClient().getSpielraum(location).setOffline();
                ssc.loeschen(location);
            }
        } catch (ExplodingkittensException e) {
            e.printStackTrace();
        }

        new LobbyView(model,stage);
    }

    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Zug beenden".
     * Die oberste Karte des Decks wird in die Hand des Spielers befördert.
     *
     */
    @FXML
    public void zugBeenden() {
        try {
            boolean ergebnis = ssc.zugBeenden(model.getUsername(),location);
            exceptionText.setVisible(false);
            if (ergebnis){
                new ExplodingKittenView(model,stage,location);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NichtAktuellerTeilnehmerException e) {
            exceptionText.setText("Aktuell nicht am Zug!");
            exceptionText.setVisible(true);
            e.printStackTrace();

        }
    }


    /**
     * Aufruf dieser Methode aus dem FXML nach drücken des Buttons "Karte spielen".
     * Gewählte Handkarte wird aus der Hand gelöscht und in den Ablagestapel befördert.
     */
    @FXML
    public void karteSpielen() {
        for (Node handkarte : hand.getChildren()) {
            if(handkarte.equals(gewählteHandkarte)) {
                String kartenname = (String) handkarte.getUserData();
                if(kartenname.equals("angriff")){
                    try {
                        ssc.angriff(location, model.getUsername());
                        exceptionText.setVisible(false);
                        gewählteHandkarte = null;
                    } catch (KarteNichtAufHandException e) {
                        exceptionText.setText("Karte nicht auf Hand!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    } catch (NichtAktuellerTeilnehmerException e) {
                        exceptionText.setText("Aktuell nicht am Zug!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    }
                } else if(kartenname.equals("mischen")){
                    try {
                        ssc.mischen(location, model.getUsername());
                        exceptionText.setVisible(false);
                        gewählteHandkarte = null;
                    } catch (KarteNichtAufHandException e) {
                        e.printStackTrace();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    } catch (NichtAktuellerTeilnehmerException e) {
                        exceptionText.setText("Aktuell nicht am Zug!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    }
                } else if(kartenname.equals("hops")){
                    try {
                        ssc.hops(location, model.getUsername());
                        exceptionText.setVisible(false);
                        gewählteHandkarte = null;
                    } catch (KarteNichtAufHandException e) {
                        exceptionText.setText("Karte nicht auf Hand!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    } catch (NichtAktuellerTeilnehmerException e) {
                        exceptionText.setText("Aktuell nicht am Zug!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    }
                } else if(kartenname.equals("blick_in_die_zukunft")){
                    try {
                        String[] blickKarten = ssc.blickInDieZukunft(location, model.getUsername());
                        exceptionText.setVisible(false);
                        if(blickKarten!=null){
                            ArrayList<ImageView> karten = new ArrayList<>();
                            for(String karte : blickKarten){
                                karten.add(new ImageView(getKartenbild(karte)));
                            }
                            KartenBox.display(karten);
                        }
                        gewählteHandkarte = null;

                    } catch (RemoteException e) {
                        e.printStackTrace();
                    } catch (NichtAktuellerTeilnehmerException e) {
                        exceptionText.setText("Aktuell nicht am Zug!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    } catch (KarteNichtAufHandException e) {
                        exceptionText.setText("Karte nicht auf Hand!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    }
                } else if(kartenname.equals("noe")){
                    try{
                        ssc.noe(location,model.getUsername());
                        gewählteHandkarte = null;
                        exceptionText.setVisible(false);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    } catch (KarteNichtAusspielbarException e) {
                        exceptionText.setText("Karte nicht ausspielbar!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    } catch (KarteNichtAufHandException e) {
                        exceptionText.setText("Karte nicht auf Hand!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    }
                }else if(kartenname.equals("wunsch")){
                    ArrayList<String> gegnerList = new ArrayList<>();
                    if(vbxSpielerA.isVisible()){
                        gegnerList.add(labelSpielerA.getText());
                    }
                    if(vbxSpielerB.isVisible()){
                        gegnerList.add(labelSpielerB.getText());
                    }
                    if(vbxSpielerC.isVisible()){
                        gegnerList.add(labelSpielerC.getText());
                    }

                    String wunschZiel = WunschkBox.display(gegnerList);
                    try{
                        ssc.gegnerGibtKarte(location,model.getUsername(),wunschZiel);
                        exceptionText.setVisible(false);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    } catch (KarteNichtAufHandException e) {
                        exceptionText.setText("Karte nicht auf Hand!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    } catch (NichtAktuellerTeilnehmerException e) {
                        exceptionText.setText("Aktuell nicht am Zug!");
                        exceptionText.setVisible(true);
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Initialisiert die GUI mit Abhängigkeiten bzw. übergebenen Infos der View-Klasse. Erzeugt zudem einen neuen ChatClient und einen SpielServerClient (Synchronisation + Interaktion mit Spiel).
     * @param model Referenz zum Modell (enthält global benötigte Daten des GUI-Interfaces)
     * @param view  Referenz zur View (Konstruktion der Szene und Manipulation der Nodes der Szene)
     * @param stage Referenz zur Stage
     * @param location Spiel(raum)name.
     * @param spieler Spieler, die an Spiel teilnehmen (sollen).
     * @param bots Bots, die an Spiel teilnehmen (sollen).
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public void initialize(MainModel model, SpielraumView view,Stage stage, String location, ArrayList<String> spieler, ArrayList<String> bots) throws RemoteException {
        this.location = location;
        this.view = view;
        this.model = model;
        this.stage = stage;
        this.ablagestapel = new ArrayList<>();
        this.bestenlisteClient=new BestenlisteClient();
        this.stand =bestenlisteClient.aktualisieren();
        //chat
        chatClient = new ChatClient();
        try {
            chatClient.erstellNachrichtLocation(location);
            messageList = FXCollections.observableArrayList(chatClient.getNachrichten(location));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        messageListView = new ListView<>(messageList);
        messageListView.setPrefWidth(704);
        messageListView.setPrefHeight(151);
        chatScrollPane.setContent(messageListView);

        try {
            new ChatClient.MessageListener(chatClient, location, messageListView, messageList);
        } catch (RemoteException exception) {
            exception.printStackTrace();
        }
        //ssc
        ssc = new SpielServerClient();
        try {
            ssc.erzeugeSpiel(location,spieler, bots);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        hand.getChildren().add(new ImageView(getKartenbild("noe")));

        this.view.getStage().show();
        //ssc.zugBeenden("sp1",location);
        updateSpielGUI();
    }

    /**
     * Updatet die Spiel-GUI (regelmäßige Prüfung auf Änderungen des Spielzustandes und ggf. Aktualisierung der GUI).
     */
    private void updateSpielGUI(){
        Timer t = new Timer(true);
        class myTasks extends TimerTask {
            private int aenderungen;
            public myTasks(){
                aenderungen=0;
            }
            public void run(){
                int modifications = 0;
                try {
                    modifications = ssc.modified(location);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                if (aenderungen != modifications) {
                    ArrayList zustand = null;
                    try {
                        zustand = ssc.getSpielzustand(location);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    ArrayList finalZustand = zustand;
                    Platform.runLater(()->{

                        if ((boolean)finalZustand.get(8)) {
                            if(finalZustand.get(9)!=null){
                                // = new BestenlisteClient();
                                try {
                                    if(bestenlisteClient.aktualisieren()==stand){
                                        try {
                                            bestenlisteClient.updateList((String) finalZustand.get(9));
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        } catch (BenutzernameNichtExistentException e) {
                                            e.printStackTrace();
                                    }}
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                            }

                            //spielraumVerlassen
                            try {
                                model.getSpielraumServiceClient().getSpielraum(location).verlassen(model.getSpieler());
                            } catch (ErstellerException | ExplodingkittensException | RemoteException e) {
                                e.printStackTrace();
                            }

                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(model.getSpielraumServiceClient().getSpielraum(location).getSpieler().size()==0){
                                    model.getSpielraumServiceClient().getSpielraum(location).setOffline();
                                    ssc.loeschen(location);
                                }
                            } catch (ExplodingkittensException | RemoteException e) {
                                e.printStackTrace();
                            }

                            new LobbyView(model,view.getStage());
                        }

                        labelAmZug.setText(finalZustand.get(0).toString()+" (vebleibende Z\u00fcge: "+ finalZustand.get(1).toString()+")"+"    Angemeldet als: "+model.getUsername());
                        ArrayList<String> teilnehmerList = new ArrayList(Arrays.asList((String[]) finalZustand.get(2)));

                        if(!(finalZustand.get(3)==null)){
                            String ablageKarte = (String) finalZustand.get(3);
                            Image bild = getKartenbild(ablageKarte);
                            ablagestapel.add(new ImageView(bild));
                            imgAblagestapel.setImage(bild);

                        }

                        if ((boolean)finalZustand.get(10) && ((String)finalZustand.get(12)).equals(model.getUsername())){
                            ArrayList<String> handkarten = ((Hashtable<String,ArrayList<String>>)(finalZustand.get(6))).get(model.getUsername());
                            String gewaehlteKarte = WunschauswahlBox.display(handkarten);
                            try {
                                ssc.gegnerGibtKarteAb(location, (String) finalZustand.get(11),model.getUsername(),gewaehlteKarte);
                            } catch (KarteNichtAufHandException e) {
                                e.printStackTrace();
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }

                        }

                        anzAblagestapelkarten.setText(finalZustand.get(7).toString());
                        labelExplodierChance.setText(finalZustand.get(4).toString());
                        anzSpielstapelkarten.setText(finalZustand.get(5).toString());
                        Hashtable<String,ArrayList<String>> handkartenAll = (Hashtable<String,ArrayList<String>>)(finalZustand.get(6));

                        ArrayList<String> myhandkarten = handkartenAll.get(model.getUsername());
                        hand.getChildren().clear();
                        for(String handkarte : myhandkarten){
                            ImageView imageKarte = new ImageView(getKartenbild(handkarte));
                            imageKarte.setUserData(handkarte);
                            imageKarte.setOnMouseClicked(new EventHandler<MouseEvent>() {
                                @Override
                                public void handle(MouseEvent mouseEvent) {
                                    handKarteGewaehlt(mouseEvent);
                                }
                            });
                            hand.getChildren().add(imageKarte);
                        }

                        ablagestapel.clear();
                        ArrayList<String> ablagenamen = (ArrayList<String>) finalZustand.get(13);
                        for(String ablageKarte : ablagenamen){
                            Image bild = getKartenbild(ablageKarte);
                            ablagestapel.add(new ImageView(bild));
                        }

                        int split = teilnehmerList.indexOf(model.getUsername());
                        for(int reihe = 0; reihe < split; reihe++){
                            teilnehmerList.add(teilnehmerList.get(0));
                            teilnehmerList.remove(0);
                        }
                        teilnehmerList.remove(0);
                        List vboxList = gegner.getChildren();
                        for(int j = 0; j < teilnehmerList.size(); j++){
                            String name = teilnehmerList.get(j);
                            VBox vb = (VBox) vboxList.get(j);
                            vb.setVisible(true);
                            ((Label) vb.getChildren().get(0)).setText(name);
                            ((Label) ((HBox) vb.getChildren().get(2)).getChildren().get(1)).setText(String.valueOf(handkartenAll.get(name).size()));
                        }
                    });
                    aenderungen = modifications;
                }

            }}
        myTasks task = new myTasks();

        t.scheduleAtFixedRate(task,0,100);

    }

    /**
     * Gibt das passende Spielkartenbild zurück.
     * @param name Name der Spielkarte.
     * @return Bild entsprechend der Spielkarte.
     */
    private Image getKartenbild(String name){
        Image bild = new Image("Exploding_Kitten.png");
        if(name.equals("angriff")){
            bild = new Image("Angriff.png");
        } else if(name.equals("blick_in_die_zukunft")){
            bild = new Image("Blick_In_Die_Zukunft.png");
        } else if(name.equals("entschaerfung")){
            bild = new Image("Entschaerfen.png");
        } else if(name.equals("hops")){
            bild = new Image("Hops.png");
        } else if(name.equals("mischen")){
            bild = new Image("Mischen.png");
        } else if(name.equals("noe")){
            bild = new Image("Noe.png");
        } else if(name.equals("wunsch")){
            bild = new Image("Wunsch.png");
        }
        return bild;
    }

    /**
     * Setzt die Objektvariable gewähhlteHandkarte, wenn eine Spielkarte angeklickt wird.
     * @param event Anklicken einer Spielkarte.
     */
    private void handKarteGewaehlt(MouseEvent event) {
        ImageView gewaehlteKarte = (ImageView) event.getSource();
        DropShadow gewaehlt = new DropShadow();

        for (Node handkarte : hand.getChildren()) {
            handkarte.setEffect(null);
        }
        gewaehlteKarte.setEffect(gewaehlt);
        this.gewählteHandkarte = gewaehlteKarte;
    }
    /**
     * Chatclient verfasst die Nachricht von Spieler
     * und wenn die Nachricht zu lang ist, wird eine NachrichtZuLangException geworfen.
     * @param actionEvent Ereignis "Drücken der Enter-Taste im Textfield"
     */
    @FXML
    public void enterSendChat(ActionEvent actionEvent) {
        errorReport.setText("");
        String textMessage = inputMsg.getText();

        String msg = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")) +
                " - " + model.getUsername() + " :: " + textMessage;
        try {
            chatClient.verfassen(location, msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NachrichtZuLangException e) {
            errorReport.setText(" Nachricht zu lang!!");
        }

        inputMsg.setText("");
    }

    /**
     * Chatclient verfasst die Nachricht von Spieler
     * und wenn die Nachricht zu lang ist, wird eine NachrichtZuLangException geworfen.
     * @param mouseEvent Ereignis "Drücken des Send-Buttons"
     */
    @FXML
    public void send(MouseEvent mouseEvent) {
        errorReport.setText("");
        String textMessage = inputMsg.getText();

        String msg = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm")) +
                " - " + model.getUsername() + " :: " + textMessage;
        try {
            chatClient.verfassen(location, msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NachrichtZuLangException e) {
            errorReport.setText(" Nachricht zu lang!!");
        }

        inputMsg.setText("");
    }
}

/**
 * Kreierung eines Popups von Spielkarten.
 */
class KartenBox {
    /**
     * Erzeugt und zeigt ein Popup an, das eine Reihe von Spielkarten je nach Kontext enthält.
     * @param karten Anzuzeigende Spielkarten in Popup.
     */
    public static void display(ArrayList<ImageView> karten){
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("");

        HBox layout = new HBox();
        layout.getChildren().addAll(karten);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

    }
}

/**
 * Popup für Auswahl des Gegners für die Aktion "Wunsch".
 */
class WunschkBox {
    static String gegner;

    /**
     * Erzeugt und zeigt ein Popup an, das die Gegner auflistet, die im Kontext der Aktion "Wunsch" ausgewählt werden können.
     * @param gegnerList Liste von anzuzeigenden Gegnern.
     * @return Ausgewählter Gegner.
     */
    public static String display(ArrayList<String> gegnerList) {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("");

        Label label1 = new Label();
        label1.setText("W\u00e4hle Gegner f\u00fcr Wunsch:");

        ArrayList<Button> buttonList = new ArrayList<>();

        for(String name : gegnerList){
            Button button = new Button(name);
            button.setOnAction(e -> {
                gegner = button.getText();
                window.close();
            });
            buttonList.add(button);
        }

        VBox layout = new VBox();
        layout.getChildren().add(label1);
        layout.getChildren().addAll(buttonList);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return gegner;
    }
}

/**
 * Popup für Auswahl der abzugebenden Karte für die Aktion "Wunsch".
 */
class WunschauswahlBox {
    static String gewaehlteKarte;
    /**
     * Gibt das passende Spielkartenbild zurück.
     * @param name Name der Spielkarte.
     * @return Bild entsprechend der Spielkarte.
     */
    private static Image getKartenbild(String name) {
        Image bild = new Image("Exploding_Kitten.png");
        if (name.equals("angriff")) {
            bild = new Image("Angriff.png");
        } else if (name.equals("blick_in_die_zukunft")) {
            bild = new Image("Blick_In_Die_Zukunft.png");
        } else if (name.equals("entschaerfung")) {
            bild = new Image("Entschaerfen.png");
        } else if (name.equals("hops")) {
            bild = new Image("Hops.png");
        } else if (name.equals("mischen")) {
            bild = new Image("Mischen.png");
        } else if (name.equals("noe")) {
            bild = new Image("Noe.png");
        } else if (name.equals("wunsch")) {
            bild = new Image("Wunsch.png");
        }
        return bild;
    }

    /**
     * Erzeugt und zeigt ein Popup an, das die Spielkarten auflistet, die im Kontext der Aktion "Wunsch" ausgewählt werden können.
     * @param hand Spielkarten, die auf der Hand des Gegners sind.
     * @return Ausgewählte abzugebende Spielkarte.
     */
    public static String display(ArrayList<String> hand) {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("");

        Label label1 = new Label();
        label1.setText("W\u00e4hle Karte f\u00fcr Wunsch:");

        ArrayList<ImageView> kartenList = new ArrayList<>();

        for (String handkarte : hand) {
            ImageView imageKarte = new ImageView(getKartenbild(handkarte));
            imageKarte.setUserData(handkarte);
            imageKarte.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    gewaehlteKarte = (String) imageKarte.getUserData();
                    window.close();
                }
            });
            kartenList.add(imageKarte);
        }

        VBox layout = new VBox();
        HBox bilder = new HBox();
        bilder.getChildren().addAll(kartenList);
        layout.getChildren().addAll(label1, bilder);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return gewaehlteKarte;
    }
}