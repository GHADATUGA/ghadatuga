package explodingkittens.gui;

import explodingkittens.client.ChatClient;
import explodingkittens.client.SpielraumServiceClient;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * View-Klasse des SpielraumErstellen-Fensters. Erzeugt die entsprechende Szene und modifiziert Nodes der Szene.
 * @author Nico
 */
public class SpielraumErstellenView {
    private final MainModel model;
    private final Stage stage;
    private SpielraumErstellenVC c;
    private Label exception_text;


    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage, in der Szenen gesetzt und gewechselt werden können
     */
    public SpielraumErstellenView(MainModel model, Stage stage) {
        this.model = model;
        this.stage = stage;

        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/SpielraumErstellen.fxml"));
            AnchorPane apane = loader.load();
            SpielraumErstellenVC controller = loader.getController();
            this.c = controller;

            this.stage.setTitle("Spielraum Erstellen");
            //this.stage.setScene(new Scene(apane, 400, 350));
            Scene scene = new Scene(apane,400,380);
            this.stage.setScene(scene);
            stage.setY(400);
            stage.setHeight(380);
            this.stage.centerOnScreen();
            stage.sizeToScene();
            controller.initialize(this.model, this);
            this.getExceptionLabelReference();
            this.getReferences();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Setzt Referenz zum Label für Exceptiontext.
     */
    private void getReferences() {
        this.exception_text = c.getException_text();
    }

    /**
     * Gibt Referenz zum Label für Exceptiontext.
     * @return  aktuelle stage.
     */
    public Stage getStage() {
        return this.stage;
    }

    /**
     * Setzt das Label Exception-Text auf unsichtbar.
     */
    //public void hideExceptionText() {
    //  exception_text.setVisible(false);
    // }

    private void getExceptionLabelReference() {
        this.exception_text = c.getException_text();
    }
    /**
     * Setzt das Label Exception-Text.
     * @param resultat enthält gewünschten Text.
     */
    public void setExceptionText(String resultat) {
        exception_text.setText(resultat);
        exception_text.setVisible(true);
    }



}

