package explodingkittens.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * View-Klasse des "Exploding Kitten"-Fensters - erzeugt die Stage und ist für die Node-Manipulation zuständig.
 * @author Desiree
 */
public class ExplodingKittenView {
    private MainModel model;
    private Stage stage;
    private ExplodingKittenVC controller;
    private Text restzeit;
    private ProgressBar progress_bar;
    private Button button;
    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage, in der Szenen gesetzt und gewechselt werden können
     * @param location Spielname.
     */
    public ExplodingKittenView(MainModel model, Stage stage,String location) {
        this.model = model;
        this.stage = stage;

        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("/Exploding_Kitten.fxml"));
            Parent root = loader.load();
            ExplodingKittenVC controller = loader.getController();
            this.controller = controller;

            this.stage.setTitle("Exploding Kitten");
            this.stage.setScene(new Scene(root, 600, 400));
            stage.centerOnScreen();
            stage.sizeToScene();
            controller.initialize(this.model, this,location);
            this.progress_bar = controller.getProgress_bar();
            this.restzeit = controller.getRestzeit_text();
            this.button = controller.getEntschaerfen_button();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ändert den Zeitanteil des Textes, der die verbleibende Zeit zum Entschärfen angibt, auf den angegebenen Wert.
     * @param zeit Restzeit in Sekunden, die eingetragen werden soll (Bsp. "23")
     */
    public void changeRestzeit(String zeit){
        restzeit.setText(zeit+" Sekunden");
    }

    /**
     * Updatet den Fortschritt des Prozessbalkens auf den angegebenen Prozentwert.
     * @param percentage Prozentsatz, der für den Prozessbalken gesetzt werden soll (z.B. 0.5)
     */
    public void updateProgress(float percentage){
        progress_bar.setProgress(percentage);
    }

    public Stage getStage() {
        return stage;
    }

    public void disableButton() {
        button.setDisable(true);
    }
}
