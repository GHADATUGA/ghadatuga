package explodingkittens.gui;

import javafx.application.Application;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Die Klasse Main erzeugt die Stage, ein Objekt der Modell-Klasse (enthält global benötigte Daten des gesamten GUI-Interfaces)
 * und ein Objekt der ersten View-Klasse.
 * Von der View-Klasse aus werden die Szenen gesetzt und manipuliert und der Controller initialisiert.
 * Der Controller steuert dabei die logischen Abfolgen, u.a. den Szenenwechsel.
 */
public class Main extends Application {
    /**
     * Implementierte abstrakte Methode der Oberklasse Application
     * @param stage (1.)Stage, in der Szenen gesetzt und gewechselt werden können
     * @throws Exception Kann ggf. Exceptions werfen
     */
    @Override
    public void start(Stage stage) throws Exception {

        MainModel model1 = new MainModel();
        new VorraumView(model1, stage);

    }

    /**
     * Startet die JavaFX-Anwendung
     * @param args Kommandozeilenargumente
     */
    public static void main(String[] args) {
        launch(args);
    }

}
