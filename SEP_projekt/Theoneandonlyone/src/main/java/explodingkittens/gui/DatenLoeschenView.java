package explodingkittens.gui;

import explodingkittens.client.ChatClient;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * View-Klasse für das "Daten löschen"-Fenster - erzeugt die Stage und ist für die Node-Manipulation zuständig.
 * @author Desiree
 */
public class DatenLoeschenView {
    private MainModel model;
    private Stage stage;
    private Label exception_label;
    private DatenLoeschenVC c;
    private ChatClient chatClient;

    /**
     * Konstruktor der View-Klasse. Lädt das entsprechende FXML-Dokument und erzeugt eine neue Szene
     * auf Basis dieses.
     * Initialisiert die nötige Werte des vom FXMl-Loader erzeugten Controller-Objektes und setzt
     * eine neue Szene für die Stage auf.
     * @param model Referenz zum Objekt der Model-Klasse (enthält global benötigte Daten des GUI-Interfaces)
     * @param stage (1.)Stage, in der Szenen gesetzt und gewechselt werden können
     * @param chatClient Referenz zum zugehörigen Chat-Client, der beim Löschen der Benutzerdaten geupdatet werden muss.
     */

    public DatenLoeschenView(MainModel model, Stage stage, ChatClient chatClient){
        this.model = model;
        this.stage = stage;
        this.chatClient = chatClient;
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Daten_Loeschen.fxml"));
            Parent root = loader.load();
            DatenLoeschenVC controller = loader.getController();
            this.c = controller;

           //this.stage.setScene(new Scene(root, 400, 350));
            Scene scene = new Scene(root,400,350);
            this.stage.setScene(scene);
            stage.setY(400);
            stage.setHeight(350);
            this.stage.centerOnScreen();
            stage.sizeToScene();
            //this.stage.resizableProperty().setValue(Boolean.FALSE);
            controller.initialize(this.model, this, chatClient);
            this.getExceptionLabelReference();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Setzt das Label Exception auf unsichtbar.
     */
    public void hideExceptionText(){
        exception_label.setVisible(false);
    }

    private void getExceptionLabelReference() {
        this.exception_label = c.getException_text();
    }

    public Stage getStage() {
        return this.stage;
    }

    public void setExceptionText(String result) {
        exception_label.setText(result);
        exception_label.setVisible(true);
    }

}
