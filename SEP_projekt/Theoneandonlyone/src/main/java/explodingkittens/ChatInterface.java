package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Interface für das Remote-Objekt Chat.
 *
 * @author Hugo, erstellt am 05.06.2020.
 */
public interface ChatInterface extends Remote {

    /**
     * Fügt der Liste der Nachrichten an einem vorhandenen Speicherort eine Nachricht hinzu.
     * Fügt der Benutzerliste einen Benutzer hinzu, wenn der Speicherort spielerList ist.
     *
     * @param location Ort, zu dem die Nachricht gehört. Oder spielerList.
     * @param text Nachricht, die hinzugefügt wird. Oder Benutzername.
     * @throws NachrichtZuLangException Ausnahme, die ausgelöst wird, wenn die Nachricht mehr als 80 Zeichen enthält.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void verfassen(String location, String text) throws RemoteException, NachrichtZuLangException;

    /**
     * Gibt die Liste der Nachrichten an einem vorhandenen Speicherort zurück.
     * Gibt die Liste der Benutzer zurück, wenn Speicherort spielerList ist.
     *
     * @param location Ort, zu dem die Liste der Nachrichten gehört. Oder spielerList.
     * @return Liste der Nachrichten oder Liste der Benutzer.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    ArrayList<String> getNachrichten(String location) throws RemoteException;

    /**
     * Löscht die Liste der Nachrichten an einem, dem Server bekannten, Ort.
     *
     * Entfernt einen Benutzernamen in der Benutzerliste, wenn Speicherort spielerList ist.
     *
     * @param location Ort, an dem die Nachrichtenliste gelöscht werden soll; oder spielerList.
     * @param username Benutzername, der in der Liste der Benutzer entfernt werden soll.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void nachrichtenLoeschen(String location, String username) throws RemoteException;

    /**
     * Erstellen eines Standorts und einer dazugehörigen Nachrichtenliste.
     * @param location Neuer Speicherort, zu dem die Nachrichten gehören würden.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    void erstellNachrichtenLocation(String location) throws RemoteException;

    /**
     * Prüft, ob die Daten auf dem Server aktualisiert wurden.
     * @param location Ort, zu dem die Nachrichten gehören.
     * @return Gibt die Anzahl der Änderungen zurück.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    int aktualisieren(String location) throws RemoteException;
}
