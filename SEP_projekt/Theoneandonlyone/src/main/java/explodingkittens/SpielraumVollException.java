package explodingkittens;

/**
 * Wenn keine freien Plätze mehr im Spielraum vorhanden sind.
 */
public class SpielraumVollException extends Exception {
}
