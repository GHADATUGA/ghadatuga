package explodingkittens.server;

/**
 * Exception, die angibt, dass eine Karte in einem bestimmten Kontext nicht ausgespielt werden kann.
 */
public class KarteNichtAusspielbarException extends Exception {
}
