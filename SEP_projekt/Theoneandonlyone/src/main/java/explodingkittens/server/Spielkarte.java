package explodingkittens.server;

/**
 *  Die Klasse Spielkarte repräsentiert die Karten, mit denen gespielt wird.
 * @author Desiree
 */
public class Spielkarte {
    private String name;

    /**
     * Erzeugt ein neues Objekt der Klasse Spielkarte.
     *
     * @param name  Name der Spielkarte (erlaubte Werte: "exploding_kitten", "entschaerfung", "noe", "angriff", "hops", "wunsch", "mischen", "blick_in_die_zukunft")
     * @throws UngueltigerSpielkartenname Falls der eingegebene Name keinem gültigen Spielkartennamen entspricht (s. Parameter name).
     */
    public Spielkarte(String name) throws UngueltigerSpielkartenname {
        if (name == "exploding_kitten" | name == "entschaerfung" | name == "noe" | name == "angriff" | name == "hops" | name == "wunsch" | name == "mischen" | name == "blick_in_die_zukunft") {
            this.name = name;
        } else {
            throw new UngueltigerSpielkartenname();
        }
    }

    /**
     * @return Gibt den Namen der Spielkarte zurück.
     */
    public String getName() {
        return name;
    }

}
