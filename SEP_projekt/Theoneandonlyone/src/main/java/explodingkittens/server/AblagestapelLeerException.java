package explodingkittens.server;

/**
 * Exception, die indiziiert, dass der Ablagestapel leer ist, d.h. keine Spielkarte mehr enthält.
 */
public class AblagestapelLeerException extends Exception {
}
