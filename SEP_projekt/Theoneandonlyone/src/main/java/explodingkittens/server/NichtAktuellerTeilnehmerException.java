package explodingkittens.server;

/**
 * Exception, die angibt, dass eine Aktion nicht ausgeführt werden kann, da det Teilnehmer nicht am Zug ist.
 */
public class NichtAktuellerTeilnehmerException extends Exception {
}
