package explodingkittens.server;

import explodingkittens.*;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Diese Klasse ist zuständig für die Verwaltung (erstellen, loeschen, aendern) der Spielraeume.
 *
 * @author Nico
 */
class SpielraumService extends UnicastRemoteObject implements SpielraumServiceInterface {
    private static final Logger LOGGER = Logger.getLogger(SpielraumService.class.getName());
    private final AtomicInteger countChanges = new AtomicInteger(0);
    private final Map<String, SpielraumInterface> spielraeume = new HashMap<>();

    SpielraumService() throws RemoteException {
    }

    /**
     * Gibt bereits bestehende Spielräume zurück.
     *
     * @return Collection von Spielrauminterfaces.
     */
    @Override
    public Collection<SpielraumInterface> getSpielraeume() {
        List<SpielraumInterface> list = new ArrayList<>(spielraeume.values());
        if (list.isEmpty()) {
            return Collections.emptyList();
        }
        return list;
    }

    /**
     * Gibt Spielraumnamen zurück.
     *
     * @return ArrayList von Spielraumnamen..
     */
    @Override
    public ArrayList<String> getSpielarumNamen() {
        ArrayList<String> raeume = new ArrayList<>();
        raeume.addAll(spielraeume.keySet());
        if (raeume.isEmpty()) {
            return new ArrayList<>();
        }
        return raeume;
    }

    /**
     * Sucht Spielraum über den Namen.
     *
     * @param spielraumname gesuchter Spielraum.
     * @return SpielraumInterface.
     * @throws ExplodingkittensException Ausnahme falls Spielraum nicht existiert.
     */
    @Override
    public SpielraumInterface getSpielraum(String spielraumname) throws ExplodingkittensException {
        for (Map.Entry<String, SpielraumInterface> spielraum : spielraeume.entrySet()
        ) {
            if (spielraum.getKey().equals(spielraumname)) {
                return spielraum.getValue();
            }

        }
        throw new SpielraumNichtExistentException(spielraumname);
    }

    /**
     * Erstellt einen Spielraum.
     *
     * @param ersteller Ersteller des spielraums.
     * @param name      Name des Spielraums.
     * @return SpielraumInterface
     * @throws ExplodingkittensException genauer: NameSchonVergebenException.
     * @throws RemoteException           Ausnahme
     */
    @Override
    public SpielraumInterface erstellen(SpielerInterface ersteller, String name) throws ExplodingkittensException, RemoteException {
        if (spielraeume.containsKey(name)) {
            throw new NameSchonVergebenException();
        } else {
            Spielraum spielraum = new Spielraum(ersteller, name);
            spielraeume.put(name, spielraum);
            return spielraum;
        }
    }

    /**
     * Loescht den mit Name angegebenen Spielraum, falls existent.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param name      Name des Spielraums.
     * @throws ExplodingkittensException genauer: NichtLeerException, SpielraumNichtExistentException.
     */
    @Override
    public void loeschen(SpielerInterface ersteller, String name) throws ExplodingkittensException, RemoteException {
        if (spielraeume.containsKey(name)) {
            SpielraumInterface spielraum = spielraeume.get(name);
            if (!spielraum.istLeer()) {
                throw new NichtLeerException();
            } else {
                spielraum.setzeGeloescht(ersteller);
                LOGGER.info("Spielaum <" + name + "> gelöscht: " + spielraeume.remove(name));
            }
        } else {
            throw new SpielraumNichtExistentException(name);
        }
    }

    /**
     * Ändert den Namen des Spielraums, falls der Ersteller dies wünscht.
     * Löscht alten Spielraum aus der Spielraummap und fügt bestehenden mit neuem Namen hinzu.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param oldName   Alter Name des Spielraums.
     * @param newName   Neuer Name des Spielraums.
     * @throws ExplodingkittensException genauer: NameSchonVergebenException, SpielraumNichtExistentException
     */
    @Override
    public void aendern(SpielerInterface ersteller, String oldName, String newName) throws ExplodingkittensException, RemoteException {
        if (spielraeume.containsKey(oldName)) {
            SpielraumInterface spielraum = spielraeume.get(oldName);
            if (spielraeume.containsKey(newName)) {
                throw new NameSchonVergebenException();
            } else {
                spielraum.aenderName(ersteller, newName);
                spielraeume.remove(oldName);
                spielraeume.put(newName, spielraum);
                LOGGER.log(Level.INFO, "Spielaum: alterName=%0, neuerName=%1", new Object[]{oldName, newName});
            }
        } else {
            throw new SpielraumNichtExistentException(oldName);
        }
    }

    /**
     * Prüft, Spielraumliste aktualisiert wurde.
     *
     * @return Anzahl Änderungen.
     */
    @Override
    public int aktualisieren() {
        return countChanges.get();
    }

    /**
     * Signalisiert Änderungen auf Spielraumliste.
     */
    @Override
    public void signal() {
        countChanges.incrementAndGet();
    }
}
