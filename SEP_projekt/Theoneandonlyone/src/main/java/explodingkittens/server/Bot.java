package explodingkittens.server;

import explodingkittens.BotInterface;

import java.rmi.RemoteException;
import java.util.*;

public class Bot extends Teilnehmer implements BotInterface {
    private LinkedList<String> bekannteSpielstapel;
    private int[] ablagestapelMarker = {0,0,0,0,0,0,0};
    private ArrayList<Spielkarte> handkarten;
    private int anzahlEK;
    private String[] prioritätsList = {"blick_in_die_zukunft","mischen","hops","angriff","entschaerfung","wunsch","noe"};

    /**
     * Konstruktor des Bots, der den Namen des Bots und das Spiel in dem er mitspielt als Parameter bekommt.
     *
     * @param name Name des Bots.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public Bot(String name) throws RemoteException {
        super(name);
    }

    /**
     * Bot spielt gegebenenfalls Karten aus und beendet seinen Zug.
     * @param spiel Referenz zum Spiel.
     */
    public void zugAusfuehren(Spiel spiel) {
        if(bekannteSpielstapel.isEmpty()){
            resetteStapel(spiel);
        }
        if(bekannteSpielstapel.size() == 1){
            probiereKartenAusspielen(spiel, new String[] {"hops","angriff"});
        }
        int handzahlNaechster = spiel.getAnzahlHandkarten().get(spiel.getNaechsterTeilnehmer());
        float dichte = spiel.getExplodingKittenDichte();            //bestimme die tatsächliche Dichte, falls bekannte EK im Spielstapel
        int ekBekannt = 0;
        for(String stapelkarte : bekannteSpielstapel){
            if(stapelkarte.equals("exploding_kitten")){
                ekBekannt++;
            }
        }
        int anzUnbekannte = 0;
        for(String stapelkarte : bekannteSpielstapel){
            if(stapelkarte.equals("unbekannt") || stapelkarte.equals("markiert")){
                anzUnbekannte++;
            }
        }
        if(ekBekannt > 0 && anzUnbekannte > 0){
            dichte = (float)(anzahlEK-ekBekannt)/(float)anzUnbekannte;
        }

        int anzahlWunsch = zaehleHandkartentyp("wunsch");
        int anzahlAbwehr = zaehleHandkartentyp("mischen") + zaehleHandkartentyp("hops") + zaehleHandkartentyp("angriff");
        int anzahlEntschaerfung = zaehleHandkartentyp("entschaerfung");
        int anzahlBIDZ = zaehleHandkartentyp("blick_in_die_zukunft");
        if(anzahlBIDZ > 0 && anzahlAbwehr > 0 && ((bekannteSpielstapel.getLast().equals("markiert") || (bekannteSpielstapel.getLast().equals("unbekannt")) && spiel.getExplodingKittenDichte() > 0.2))){
            try{
                String[] blickKarten = spiel.blickInZukunft(this);
                if(!blickKarten.equals(null)){
                    for(int i=0; i < blickKarten.length; i++) {

                    }
                }
            } catch (NichtAktuellerTeilnehmerException | KarteNichtAufHandException e){
                e.printStackTrace();
            }
        }
        if(bekannteSpielstapel.getLast().equals("exploding_kitten") && anzahlWunsch > 0 && anzahlEntschaerfung == 0){
            Teilnehmer next = spiel.getNaechsterTeilnehmer();
            try{
                spiel.gegnerGibtKarte(this, next);
            } catch (NichtAktuellerTeilnehmerException | KarteNichtAufHandException e){
                e.printStackTrace();
            }
        }
        Set<Map.Entry<Teilnehmer,Integer>> handkartenSet = spiel.getAnzahlHandkarten().entrySet();
        for(Map.Entry<Teilnehmer,Integer> handkarten : handkartenSet){
            if (handkarten.getValue() <= anzahlWunsch){
                for(int i = 0; i < handkarten.getValue();i++){
                    try{
                        spiel.gegnerGibtKarte(this, handkarten.getKey());
                    } catch (NichtAktuellerTeilnehmerException | KarteNichtAufHandException e){
                        e.printStackTrace();
                    }
                    anzahlWunsch--;
                }
            }
        }
        // Zug beenden
        if((!bekannteSpielstapel.getLast().equals("exploding_kitten") && !bekannteSpielstapel.getLast().equals("unbekannt") && !bekannteSpielstapel.getLast().equals("markiert")) || dichte < 0.15){
            probiereKartenAusspielen(spiel,new String[] {"Zug beenden"});
        } else if(bekannteSpielstapel.getLast().equals("exploding_kitten")){
            if(dichte < 0.2){
                probiereKartenAusspielen(spiel, new String[] {"mischen","hops","angriff"});
            } else if(dichte <0.35){
                probiereKartenAusspielen(spiel, new String[] {"hops","angriff","mischen"});
            } else {
                probiereKartenAusspielen(spiel, new String[] {"angriff","hops","mischen"});
            }
        }else if(bekannteSpielstapel.getLast().equals("markiert")){
          if(anzahlEntschaerfung > 0){
              probiereKartenAusspielen(spiel,new String[] {"Zug beenden"});
          } else if(dichte < 0.26){
              probiereKartenAusspielen(spiel, new String[] {"hops","angriff","mischen"});
          } else {
              probiereKartenAusspielen(spiel, new String[] {"angriff","hops","mischen"});
          }
        } else {
            if(bekannteSpielstapel.size() > 1 && bekannteSpielstapel.get(bekannteSpielstapel.size()-2).equals("exploding_kitten") && (handzahlNaechster < 4 || zaehleAbwehrGlobal() == 12 )){
                probiereKartenAusspielen(spiel, new String[] {"angriff"});
            } else if(dichte < 0.21){
                probiereKartenAusspielen(spiel, new String[] {"hops"});
            } else if(dichte < 0.34){
                if(anzahlEntschaerfung > 0){
                    probiereKartenAusspielen(spiel, new String[] {"hops"});
                } else {
                    probiereKartenAusspielen(spiel, new String[] {"hops","angriff"});
                }
            } else if(anzahlEntschaerfung > 0){
                if(zaehleAbwehrGlobal() == 12){
                    probiereKartenAusspielen(spiel, new String[] {"angriff","hops"});
                } else if (handzahlNaechster > 5){
                    probiereKartenAusspielen(spiel, new String[] {"hops","angriff"});
                } else {
                    probiereKartenAusspielen(spiel, new String[] {"hops"});
                }
            } else {
                probiereKartenAusspielen(spiel, new String[] {"hops","angriff"});
            }
        }
    }

    /**
     * Reduziert den bekannten Spielstapel um 1 wenn jmd eine Karte zieht.
     */
    public void updateSpielstapel(){
        if(!bekannteSpielstapel.isEmpty()){
            bekannteSpielstapel.remove(bekannteSpielstapel.size()-1);
        }
    }

    /**
     * Bot reagiert auf gespielte Karten indem er seinen Ablagestapel anpasst, ggf den bekannten Spielstapel verändert und evtl ein Noe spielt.
     * @param gespielteKarte Karte auf die der Bot reagiert. Wenn dies eine EK ist, setzt er lediglich den EK-Zähler eins herab.
     * @param spiel Referenz zum Spiel.
     */
    public void reagieren(Spielkarte gespielteKarte, Spiel spiel){
        int anzahlNoe = zaehleHandkartentyp("noe");
        int anzahlBIDZ = zaehleHandkartentyp("blick_in_die_zukunft");
        if(gespielteKarte.getName().equals("blick_in_die_zukunft")){
            ablagestapelMarker[0] += 1;
            markiereTopDrei();
        }else if(gespielteKarte.getName().equals("mischen")){
            ablagestapelMarker[1] += 1;
            resetteStapel(spiel);
        }else if(gespielteKarte.getName().equals("hops")){
            ablagestapelMarker[2] += 1;
            if(bekannteSpielstapel.getLast().equals("exploding_kitten") && anzahlNoe > 0){
                try{
                    spiel.noe(this);
                } catch (KarteNichtAusspielbarException | KarteNichtAufHandException e){
                    e.printStackTrace();
                }
            }
        }else if(gespielteKarte.getName().equals("angriff")){
            ablagestapelMarker[3] += 1;
            if((bekannteSpielstapel.getLast().equals("exploding_kitten") || (spiel.getNaechsterTeilnehmer().equals(this) && spiel.getExplodingKittenDichte() > 0.2)) && anzahlNoe > 0){
                try{
                    spiel.noe(this);
                } catch (KarteNichtAusspielbarException | KarteNichtAufHandException e){
                    e.printStackTrace();
                }
            }
        }else if(gespielteKarte.getName().equals("entschaerfung")){
            ablagestapelMarker[4] += 1;
            bekannteSpielstapel.add("unbekannt");
            resetteStapel(spiel);
            markiereTopDrei();
        }else if(gespielteKarte.getName().equals("wunsch")){
            ablagestapelMarker[5] += 1;
            if(anzahlBIDZ == 0 && anzahlNoe > 0){
                try{
                    spiel.noe(this);
                } catch (KarteNichtAusspielbarException | KarteNichtAufHandException e){
                    e.printStackTrace();
                }
            }
        }else if(gespielteKarte.getName().equals("noe")){
            ablagestapelMarker[6] += 1;
        }else { //gespielteKarte ist EK
            anzahlEK -= 1;
        }

    }

    /**
     * Wählt aus, an welche Stelle im Spielstapel die EK zurückgelegt werden soll. Updatet den bekannten Spielstapel an der Position.
     * @return Stelle im Spielstapel an die die EK zurückgelegt werden soll.
     * @param spiel Referenz zum Spiel.
     */
    public int ekZuruecklegen(Spiel spiel){
        int obersterPlatz = spiel.getSpielstapel().getKarten().size();
        int platzierung = 0;
        Teilnehmer opfer = this;
        Set<Map.Entry<Teilnehmer,Integer>> handkartenSet = spiel.getAnzahlHandkarten().entrySet();
        for(Map.Entry<Teilnehmer,Integer> elem : handkartenSet){
            if(elem.getValue() < 4 && !elem.getKey().equals(this)){
                opfer = elem.getKey();
                break;
            }
        }
        if(spiel.getAnzahlHandkarten().size() > 2 || zaehleAbwehrGlobal() == 12){
            platzierung = obersterPlatz;
        } else if(!opfer.equals(this)){
            platzierung = Math.max(0,obersterPlatz - spiel.getReihenfolge().indexOf(opfer)); }
        else{
            if((obersterPlatz+1) % spiel.getAnzahlHandkarten().size() == 0) {
                platzierung = Math.min(obersterPlatz,1);
            }
        }
        if(bekannteSpielstapel.size() == obersterPlatz-1) {
            if(platzierung < bekannteSpielstapel.size()){
                bekannteSpielstapel.add(platzierung, "exploding_kitten");
            } else {
                bekannteSpielstapel.add("exploding_kitten");
            }
        } else {
            resetteStapel(spiel);
        }
        return platzierung;
    }

    /**
     * Markiert die obersten drei unbekannten Spielkarten im bekannten Spielstapel
     */
    private void markiereTopDrei(){
        int obersterPlatz = bekannteSpielstapel.size()-1;
        for(int i = 0; i < 3; i++){
            if(bekannteSpielstapel.get((obersterPlatz - i)).equals("unbekannt")){
                bekannteSpielstapel.set((obersterPlatz - i),"markiert");
            }
        }
    }

    /**
     * Setzt den kompletten bekannten Spielstapel auf unbekannt, nach Mischen oder Mitspieler entschärft EK.
     */
    public void resetteStapel(Spiel spiel){
        bekannteSpielstapel.clear();
        for(int i = 0; i < spiel.getSpielstapel().getKarten().size(); i++){
            bekannteSpielstapel.add("unbekannt");
        }
    }

    /**
     * Zählt Anzahl an bekannten Angriff, Hops und Mischen auf der Hand, dem Ablagestapel und dem Spielstapel.
     * @return Anzahl.
     */
    private int zaehleAbwehrGlobal(){
        int anzahl = ablagestapelMarker[1]+ablagestapelMarker[2]+ablagestapelMarker[3];
        for(String stapelkarte : bekannteSpielstapel){
            if(stapelkarte.equals("mischen") || stapelkarte.equals("angriff") || stapelkarte.equals("hops")){
                anzahl++;
            }
        }
        anzahl += zaehleHandkartentyp("mischen");
        anzahl += zaehleHandkartentyp("hops");
        anzahl += zaehleHandkartentyp("angriff");
        return anzahl;
    }

    /**
     * Zählt wie oft sich eine bestimmte Kartenart auf der Hand befindet.
     * @param kartenname Kartentyp, der gezählt werden soll.
     * @return Anzahl auf der Hand
     */
    public int zaehleHandkartentyp(String kartenname){
        int anzahl = 0;
        for(Spielkarte handkarte : handkarten){
            if(handkarte.getName().equals(kartenname)){
                anzahl++;
            }
        }
        return anzahl;
    }

    /**
     * Versucht eine Karte aus der Liste zu spielen. Wenn keine solche Karte auf der Hand, beende den Zug indem man eine Karte zieht.
     *@param spiel Referenz zum Spiel.
     * @param kartennamen Array von Karten, die probiert werden sollen auszuspielen. */
    private void probiereKartenAusspielen(Spiel spiel, String[] kartennamen)  {

        ausgespielt: {
            for (int i = 0; i < kartennamen.length; i++) {
                String name = kartennamen[i];
                for (Spielkarte karte : handkarten) {
                    if (karte.getName().equals(name)) {
                        if (name.equals("mischen")) {
                            try {
                                spiel.mischen(this);
                            } catch (NichtAktuellerTeilnehmerException | KarteNichtAufHandException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(spiel.zugBeenden(this)){
                                    if(zaehleHandkartentyp("entschaerfung") > 0){
                                        try {
                                            spiel.entschaerfen(this, ekZuruecklegen(spiel));
                                        } catch (PositionNichtMoeglichException  | KarteNichtAufHandException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        spiel.explodieren(this);
                                    }
                                }
                            } catch (NichtAktuellerTeilnehmerException e) {
                                e.printStackTrace();
                            }
                        } else if (name.equals("angriff")) {
                            try {
                                spiel.angriff(this);
                            } catch (NichtAktuellerTeilnehmerException | KarteNichtAufHandException e) {
                                e.printStackTrace();
                            }
                        } else if (name.equals("hops")) {
                            try {
                                spiel.hops(this);
                            } catch (NichtAktuellerTeilnehmerException | KarteNichtAufHandException e) {
                                e.printStackTrace();
                            }
                        }
                        break ausgespielt;
                    }
                }
            }
            try {
                if(spiel.zugBeenden(this)){
                    if(zaehleHandkartentyp("entschaerfung") > 0){
                        try {
                            spiel.entschaerfen(this, ekZuruecklegen(spiel));
                        } catch (PositionNichtMoeglichException  | KarteNichtAufHandException e) {
                            e.printStackTrace();
                        }
                    } else {
                        spiel.explodieren(this);
                    }
                }
            } catch (NichtAktuellerTeilnehmerException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gibt die Spielkarte aus, die der Bot als Wunschziel abgibt.
     * @return gewählte Spielkarte.
     * @param spiel Referenz zum Spiel.
     */
    public Spielkarte wunschWahl(Spiel spiel){
        for(int i = 0; i<7; i++){
            for(Spielkarte handkarte : handkarten) {
                if(handkarte.getName().equals(prioritätsList[i])){
                    return handkarte;
                }
            }
        }
        return handkarten.get(0);
    }

    /**
     * Wird aufgerufen nachdem das Spiel vollständig erstellt ist. Initialisiert Daten wie Hand und bekannter Spielstapel.
     * @param spiel Referenz zum Spiel.
     */
     public void initialisiereDaten(Spiel spiel){
        handkarten = spiel.getHand(this);
        anzahlEK = spiel.getAnzahlHandkarten().size()-1;
        bekannteSpielstapel = new LinkedList<>();
        for(int i = 0; i < spiel.getSpielstapel().getKarten().size(); i++){
            bekannteSpielstapel.add("unbekannt");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BotInterface)) { //Check against the interface
            return false;
        }
        //Only use the interface. obj might be a stub, not only an impl.
        BotInterface other = (BotInterface) obj;
        try {
            return getName().equals(other.getName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public ArrayList<Spielkarte> getHandkarten() {
        return handkarten;
    }

    public void setHandkarten(ArrayList<Spielkarte> karten) {
         handkarten = karten;
    }

    public LinkedList<String> getBekannteSpielstapel() {
        return bekannteSpielstapel;
    }
    public void setBekannteSpielstapel(LinkedList<String> list) {
        bekannteSpielstapel = list;
    }
}
