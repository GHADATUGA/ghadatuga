package explodingkittens.server.datenbankTest;

import explodingkittens.*;

import java.util.*;


public class DatenbankServer {
    private Hashtable<String, String> datenbank ;
    private HashMap<String,Integer> bestenlist;

    public DatenbankServer() {
        super();
        datenbank = new Hashtable<>();
        bestenlist = new HashMap<>();
    }


    public void checkUserData(String benutzername_eingegeben, String passwort_eingegeben) throws LeereEingabeException, BenutzernameNichtExistentException, PasswortFalschException{
        if (benutzername_eingegeben.equals("")||passwort_eingegeben.equals("")){
            throw new LeereEingabeException();
        }
        if(checkExistent(benutzername_eingegeben)){
            String passw = datenbank.get(benutzername_eingegeben);
            if(!passw.equalsIgnoreCase(passwort_eingegeben)) {
                throw new PasswortFalschException();
            }
        }else{
            throw new BenutzernameNichtExistentException();
        }
    }

    public void changeUserName(String alter_benutzername, String neuer_benutzername) {

        datenbank.keySet().removeIf(username -> username.equalsIgnoreCase(alter_benutzername));

        //bestenlist
        Iterator<String> iteratorBestenlist = bestenlist.keySet().iterator();
        int punkte = 0;
        while (iteratorBestenlist.hasNext()){
            String username = iteratorBestenlist.next();
            if(username.equalsIgnoreCase(alter_benutzername)){
                punkte = bestenlist.get(username);
                iteratorBestenlist.remove();
            }
        }
        bestenlist.put(neuer_benutzername,punkte);

    }


    public void changePassword(String benutzername_eingegeben, String neues_passwort){
        if(!checkExistent(benutzername_eingegeben)){
            datenbank.put(benutzername_eingegeben, neues_passwort);
        }
    }


    public void createUser(String benutzername_eingegeben, String passwort_eingegeben) throws BenutzernameSchonVergebenException, EingabeZuLangException, LeereEingabeException {
        if (benutzername_eingegeben.equals("")||passwort_eingegeben.equals("")){
            throw  new LeereEingabeException();
        } if (benutzername_eingegeben.length()>20||passwort_eingegeben.length()>20){
            throw new EingabeZuLangException();
        }
        if(!checkExistent(benutzername_eingegeben)){
            datenbank.put(benutzername_eingegeben, passwort_eingegeben);
            bestenlist.put(benutzername_eingegeben,0);

        } else if(checkExistent(benutzername_eingegeben)){
            throw new BenutzernameSchonVergebenException();
        }



    }


    public void deleteUser(String benutzername_eingegeben) throws BenutzernameNichtExistentException {
        if(checkExistent(benutzername_eingegeben)){
            datenbank.remove(benutzername_eingegeben);
            bestenlist.remove(benutzername_eingegeben);
        }
        else{
            throw new BenutzernameNichtExistentException();
        }
    }

    public void updateLeaderboard(String benutzername_eingegeben)throws BenutzernameNichtExistentException {
        if(bestenlist.containsKey(benutzername_eingegeben)){
            bestenlist.put(benutzername_eingegeben,bestenlist.get(benutzername_eingegeben) + 1);
        }else{
            throw new BenutzernameNichtExistentException();
        }
    }


    public ArrayList<ArrayList<String>> getLeaderboard()  {
        HashMap<String, Integer> sortedMap = sortByComparator(bestenlist);
        ArrayList<ArrayList<String>> list = new ArrayList<>();
        for(String username : sortedMap.keySet()){
            ArrayList<String> userInfo = new ArrayList<>();
            userInfo.add(username);
            userInfo.add(sortedMap.get(username)+"");
            list.add(userInfo);
        }
        return list;
    }

    /**
     * Bestenliste werden anhand der Anzahl der Punkte des Spielers sortiert aufgelistet.
     * @param unsortMap unsortiert Bestenliste.
     * @return Besteliste in absteigender Reihenfolge sortiert.
     */
    private HashMap<String,Integer> sortByComparator(
            HashMap<String,Integer> unsortMap) {

        List<Map.Entry<String ,Integer>> list = new LinkedList<>(
                unsortMap.entrySet());

        list.sort(Collections.reverseOrder(Map.Entry.comparingByValue()));

        HashMap<String,Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String,Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    private boolean checkExistent(String username){
        return datenbank.contains(username);
    }

}
