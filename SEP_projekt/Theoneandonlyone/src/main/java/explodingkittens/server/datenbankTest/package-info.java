/**
 * Package, das die Dummy-Datenbank enthält, die ausschließlich zum Testen des Programms (ohne Nutzung der
 * echten Datenbank) dient.
 */
package explodingkittens.server.datenbankTest;