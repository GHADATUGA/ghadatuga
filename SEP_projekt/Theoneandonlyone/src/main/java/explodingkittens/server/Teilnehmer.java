package explodingkittens.server;

import explodingkittens.TeilnehmerInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Diese Klasse repräsentiert die Teilnehmer.
 * @author Ghad
 */
public class Teilnehmer extends UnicastRemoteObject implements TeilnehmerInterface {

    private static final long serialVersionUID = 1L;

    private final String name;

    /**
     * Teilnehmer
     *
     * @param name Name des Teilnehmer
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public Teilnehmer(String name) throws RemoteException {
        this.name = name;
    }

    /**
     * Gibt Namen zurück.
     *
     * @return Name.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Überschriebene Equals-Methode.
     *
     * @param obj zu vergleichendes Objekt.
     * @return boolean, ob Objekt gleich dem TeilnehmerInteface.
     */
    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            //Should also return true for stubs pointing to this
            return true;
        }
        if (obj == null || !(obj instanceof TeilnehmerInterface)) { //Check against the interface
            return false;
        }
        //Only use the interface. obj might be a stub, not only an impl.
        TeilnehmerInterface other = (TeilnehmerInterface) obj;
        try {
            return getName().equals(other.getName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Überschriebene hashCode-Methode (Zum Vergleich 2er Objekte)
     *
     * @return hashcode.
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
