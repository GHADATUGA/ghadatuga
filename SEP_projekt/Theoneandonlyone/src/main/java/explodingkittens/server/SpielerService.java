package explodingkittens.server;

import explodingkittens.SpielerInterface;
import explodingkittens.SpielerServiceInterface;

import java.rmi.RemoteException;
import java.rmi.server.RemoteObject;
import java.rmi.server.UnicastRemoteObject;


/**
 * Remote-Objekt zur Erzeugung eines Spielers.
 */
public class SpielerService extends UnicastRemoteObject implements SpielerServiceInterface {
    /**
     * Konstruktor des Spieler-Services.
     * @throws RemoteException Wenn es ein Problem bei der Verbindungsherstellung gab.
     */
    protected SpielerService() throws RemoteException {
    }

    @Override
    /**
     * Erzeugt einen neuen Spieler.
     * @param name Name des Spielers.
     * @return SpielerInterface des erzeugten Spielers.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public SpielerInterface create(String name) throws RemoteException {
        return new Spieler(name);
    }
}
