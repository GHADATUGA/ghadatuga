package explodingkittens.server;

import explodingkittens.BenutzernameNichtExistentException;
import explodingkittens.BestenlisteInterface;
import explodingkittens.server.datenbankTest.DatenbankServer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Bestenliste, Verwaltung und Manipulation der Bestenliste.
 * @author Hugo, erstellt am 14.06.2020.
 */
public class Bestenliste extends UnicastRemoteObject implements BestenlisteInterface {
    private AtomicInteger countChanges = new AtomicInteger(0);
    private DatenbankServer datenbank;

    /**
     * Konstruktor für die Klasse Bestenliste. Initialisiert die Bestenliste-Hashmap.
     * @param datenbank Datenbank für das Spiel.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public Bestenliste(DatenbankServer datenbank) throws RemoteException {
        this.datenbank = datenbank;
    }

    /**
     * Signalisiert eine Änderung auf die Bestenliste.
     */
    @Override
    public void signal() {
        countChanges.incrementAndGet();
    }


    /**
     * Aktualisiert die Bestenliste und erhöht die Punktzahl des Spielers, der das Spiel gewonnen hat. Updates am Ende des Spiels.
     * @param winner Benutzername des Spielers, der das Spiel gewonnen hat.
     * @throws BenutzernameNichtExistentException Wenn der Benutzername nicht in der Bestenliste gespeichert ist.
     */
    @Override
    public void updateList(String winner) throws BenutzernameNichtExistentException {
        datenbank.updateLeaderboard(winner);
        countChanges.incrementAndGet();
    }

    @Override
    public ArrayList<ArrayList<String>> getList()  {
        return datenbank.getLeaderboard();
    }

    /**
     * Prüft, ob die Bestenliste aktualisiert wurde.
     * @return Gibt die Anzahl der Änderungen zurück.
     */
    @Override
    public int aktualisieren() {
        return countChanges.get();
    }
}
