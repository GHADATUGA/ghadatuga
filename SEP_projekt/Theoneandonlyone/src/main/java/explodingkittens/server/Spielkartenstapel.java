package explodingkittens.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
/**
 * Klasse, die zuständig für die Verwaltung und Operationen des Spielstapels ist.
 * @author Desiree
 */
public class Spielkartenstapel {
    private Stack<Spielkarte> karten;
    /**
     * Konstruktor, der einen Ziehstapel für die Spielkarten initialisiert.
     * @param karten Karten, die zu Beginn im Spielstapel sind
     */
    public Spielkartenstapel(Spielkarte[] karten){
        this.karten = new Stack<>();
        for (int i=0;i<karten.length;i++){
            this.karten.push(karten[i]);
        }
    }
    /**
     * Gibt die oberste Spielstapelkarte zur?ck ohne sie vom Stapel zu entfernen.
     * @return oberste Spielstapelkarte
     */
    public Spielkarte top(){
        return karten.peek();
    }
    /**
     * Fügt dem Spielstapel eine Karte an oberster Stelle hinzu.
     * @param karte Karte, die auf den Spielstapel gelegt werden soll
     */
    public void push(Spielkarte karte){
        karten.push(karte);
    }
    /**
     * Gibt die oberste Spielstapelkarte zurück und entfernt sie vom Stapel.
     * @return oberste Spielstapelkarte
     */
    public Spielkarte pop(){
        return karten.pop();
    }
    /**
     * Gibt den kompletten Spielstapel als ArrayList zurück.
     * @return Spielstapel als ArrayList von Spielkarten, wobei an Position 0 die oberste Spielstapelkarte ist
     */
    public ArrayList<Spielkarte> getKarten(){
        ArrayList<Spielkarte> kartenStapel = new ArrayList<Spielkarte>(karten);
        Collections.reverse(kartenStapel);
        return kartenStapel;
    }

    /**
     * Mischt den Spielkartenstapel annähernd zufällig.
     */
    public void mischen(){
        Collections.shuffle(karten);
    }

    /**
     * Fügt eine Spielkarte an die angegebene Position (beginnend mit 0=unterste Stelle) ein.
     * @param karte Einzufügende Spielkarte
     * @param position Position, an der die Spielkarte eingefügt werden soll
     * @throws PositionNichtMoeglichException Wenn die Position keine gültige Einfügeposition ist
     */
    public void insert (Spielkarte karte, int position) throws PositionNichtMoeglichException{
        if (karten.size()<position){
            throw new PositionNichtMoeglichException();
        }
        else{
            karten.insertElementAt(karte,position);
        }
    }
}
