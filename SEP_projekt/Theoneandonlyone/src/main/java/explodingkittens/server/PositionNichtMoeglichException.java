package explodingkittens.server;

/**
 * Gibt an, das die Einfügeposition in den Spielkartenstapel nicht gültig ist.
 */
public class PositionNichtMoeglichException extends Exception {
}
