package explodingkittens.server;

import explodingkittens.ExplodingkittensException;

/**
 * Wird geworfen, wenn ein Spielraum modifiziert werden soll, den es gar nicht gibt.
 */
public class SpielraumNichtExistentException extends ExplodingkittensException {
    public SpielraumNichtExistentException(String unbekannterSpielraum) {
        super("Unbekannter Spielraum: Spielraum=" + unbekannterSpielraum);
    }
}
