package explodingkittens.server;

/**
 * Exception, die angibt, dass sich eine Karte nicht auf der hand befindet und daher nicht ausgespielt werden kann.
 */
public class KarteNichtAufHandException extends Exception {
}
