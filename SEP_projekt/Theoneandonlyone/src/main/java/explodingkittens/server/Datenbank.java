package explodingkittens.server;

import explodingkittens.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Die Klasse Datenbank stellt alle nötigen Methoden zur Interaktion mit der Datenbank bereit.
 * @author Desiree
 */

public class Datenbank {
    String datenbankName;
    private Connection connection;

    //kann gelöscht werden, wenn mockedTest fertig ist
    /**
     * Alternativer Konstruktor der Klasse Datenbank. Parameter für Testdatenbank. Nur für Tests zu benutzen.
     * @param datenbankName Name der Testdatenbank
     * */
    Datenbank(String datenbankName){
        this.datenbankName = datenbankName;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl =
                    "jdbc:sqlserver://DESKTOP-H3N658L;"
                            + "databaseName=" + this.datenbankName
                            + ";user=sa;"
                            + "password=sa;";

            this.connection = DriverManager.getConnection(connectionUrl);


        } catch (ClassNotFoundException|SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }
    /**
     * Konstruktor der Klasse Datenbank. Parameterlos für Produktivdatenbank.
     */
    Datenbank(){
        this.datenbankName = "ek_produktiv";
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl =
                    "jdbc:sqlserver://DESKTOP-H3N658L;"
                            + "databaseName=" + this.datenbankName
                            + ";user=sa;"
                            + "password=sa;";

            this.connection = DriverManager.getConnection(connectionUrl);


        } catch (ClassNotFoundException|SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }

    /**
     * Überprüft, ob der angegebene Benutzername schon in der Datenbank vorhanden ist. Falls ja wird BenutzernameSchonVergebenException geworfen
     * @param benutzername_eingegeben Benutzername, der in der Datenbank nachgeschlagen werden soll
     * @throws BenutzernameSchonVergebenException Wenn der Benutzername bereits in der Datenbank vorhanden ist
     */
    public void checkUserName (String benutzername_eingegeben) throws BenutzernameSchonVergebenException{
        try {
            String query = "select *" +" from dbo.benutzer" + " where benutzername = "+ "'"+benutzername_eingegeben+"'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if (rs.isBeforeFirst()) {
                throw new BenutzernameSchonVergebenException();
            }

        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }
    /**
     * Überprüft, ob der eingegebene Benutzername in der entsprechenden Datenbanktabelle existiert
     * und ob das eingebene Passwort dem des Benutzers entspricht.
     *
     * @author Desiree
     * @param benutzername_eingegeben eingegebener und zu überprüfender Benutzername
     * @param passwort_eingegeben eingegebenes und zu überprüfendes Passwort
     * @throws BenutzernameNichtExistentException - Wenn der Benutzername nicht in der Datenbank gespeichert ist
     * @throws PasswortFalschException - Wenn das Passwort nicht dem des eingegebenen Benutzers entspricht
     */
    public void checkUserData (String benutzername_eingegeben, String passwort_eingegeben) throws BenutzernameNichtExistentException, PasswortFalschException{

        try {
            String query = "select passwort" +" from dbo.benutzer" + " where benutzername = "+ "'"+benutzername_eingegeben+"'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            if (!rs.isBeforeFirst()) {
                throw new BenutzernameNichtExistentException();
            } else {
                rs.next();
                String passwort_result = rs.getString("passwort");

                if (!(passwort_result.equals(passwort_eingegeben))) {
                    throw new PasswortFalschException();
                }
            }

        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
        }
    /**
     * Frägt die Einträge der Bestenliste ab und gibt sie zurück.
     *
     * @author Desiree
     * @return Bestenliste als Array von Einträgen, die wieder in einem Array gekapselt sind. Die Element sind als String
     * kodiert: [['PLATZ', 'SPIELERNAME', 'PUNKTE'], ...] wobei PLATZ, SPIELERNAME und PUNKTE die Platzhalter für die Werte sind.
     */
    public ArrayList<ArrayList<String>> getLeaderboard(){
        ArrayList<ArrayList<String>> bestenliste = new ArrayList<>();
        try {
            String query = "select rank() over (order by punkte DESC) as platz, spieler, punkte" +" from dbo.bestenliste";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs.isBeforeFirst()){

                while (rs.next()) {
                    //Integer platz = rs.getInt("platz");
                    String spieler = rs.getString("spieler");
                    Integer punkte = rs.getInt("punkte");
                    ArrayList<String> eintrag = new ArrayList<>();
                    //eintrag.add(platz.toString());
                    eintrag.add(spieler);
                    eintrag.add(punkte.toString());
                    bestenliste.add(eintrag);
                }

            }

        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }

        return bestenliste;

    }

    /**
     * Ändert den angegebenen alten Benutzernamen in den neuen Benutzernamen.
     * @param alter_benutzername alter Benutzername, der aktuell in der Datenbank gespeichert sein sollte
     * @param neuer_benutzername neuer Benutzername, der den alten ersetzen soll
     * @author Desiree
     * @throws BenutzernameNichtExistentException - wenn der angegebene alte Benutzername nicht in der Datenbank existiert
     * @throws EingabeZuLangException - wenn der übergebne neue Benutzername zu lang (mehr als 20 Zeichen) ist
     * @throws LeereEingabeException - wenn der übergebne neue Benutzername ein leerer String ist
     * @throws BenutzernameSchonVergebenException - wenn der Benutzername bereits in der Datenbank vorhanden ist
     */
    public void changeUserName (String alter_benutzername, String neuer_benutzername) throws BenutzernameNichtExistentException, BenutzernameSchonVergebenException, LeereEingabeException, EingabeZuLangException{
        try {
            this.checkUserName(alter_benutzername);
            throw new BenutzernameNichtExistentException();
        } catch(BenutzernameSchonVergebenException e){
            if (neuer_benutzername.equals(alter_benutzername)==false) {
                System.err.println(alter_benutzername+" "+neuer_benutzername);
                try {
                    try{
                        this.checkUserName(neuer_benutzername);

                        if (neuer_benutzername.length() > 20) {
                            throw new EingabeZuLangException();
                        } else if (neuer_benutzername == "") {
                            throw new LeereEingabeException();
                        } else {
                            String query = "update dbo.benutzer set benutzername = '" + neuer_benutzername + "' where benutzername = '" + alter_benutzername + "'";
                            Statement stmt = connection.createStatement();
                            stmt.execute(query);
                        }
                    } catch (SQLException ex) {
                        System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
                    }}
                catch (BenutzernameSchonVergebenException ex){
                    throw new BenutzernameSchonVergebenException();
                }
            }



    }
    }

    /**
     * Ändert das Passwort des angebenen Benutzers auf das angebene Passwort in der Datenbank.
     *
     * @author Desiree
     * @param benutzername_eingegeben Benutzername dessen zugehöriges Passwort geändert werden soll
     * @param neues_passwort Neues Passwort (max. 20 Zeichen)
     * @throws BenutzernameNichtExistentException - wenn der Benutzer nicht in der Datenbank existiert
     * @throws EingabeZuLangException - wenn das übergebene neue Passwort zu lang (mehr als 20 Zeichen) ist
     * @throws LeereEingabeException - wenn das übergebne neue Passwort ein leerer String ist
     */
    public void changePassword (String benutzername_eingegeben, String neues_passwort) throws BenutzernameNichtExistentException, EingabeZuLangException, LeereEingabeException{
        try {

            try{
                this.checkUserName(benutzername_eingegeben);
                throw new BenutzernameNichtExistentException();

            }
            catch(BenutzernameSchonVergebenException e){
                if (neues_passwort.length()>20){
                    throw new EingabeZuLangException();
                }
                else if(neues_passwort == ""){
                    throw new LeereEingabeException();
                }
                else{
                    String query = "update dbo.benutzer set passwort = '"+neues_passwort+"' where benutzername = '"+benutzername_eingegeben+"'";
                    Statement stmt = connection.createStatement();
                    stmt.execute(query);

                }
            }


        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }

    /**
     * Erstellt einen neuen Benutzer mit den angegebenen Daten in der Datenbank und legt einen neuen Bestenlisteneintrag
     * für den Benutzer an.
     * @param benutzername_eingegeben Gewünschter Benutzername (max. 20 Zeichen)
     * @param passwort_eingegeben Gewünschtes Passwort (max. 20 Zeichen)
     * @author Desiree
     * @throws BenutzernameSchonVergebenException - wenn der gewünschte benutzername schon in der Datenbank existiert
     * @throws EingabeZuLangException - wenn der eingegebene Benutzername ODER das eingegebene Passwort mehr als 20 Zeichen lang ist
     * @throws LeereEingabeException - wenn der eingegebene Benutzername ODER das eingegebene Passwort ein leerer String ist
     */
    public void createUser (String benutzername_eingegeben, String passwort_eingegeben) throws BenutzernameSchonVergebenException, EingabeZuLangException, LeereEingabeException{
        try {
            try{
                this.checkUserName(benutzername_eingegeben);
                if (benutzername_eingegeben.length()>20 | passwort_eingegeben.length()>20){
                    throw new EingabeZuLangException();
                }
                else if((benutzername_eingegeben == "") | (passwort_eingegeben == "")){
                    throw new LeereEingabeException();
                }
                else{
                    String query = "insert dbo.benutzer (benutzername,passwort) values ('"+benutzername_eingegeben+"', '"+passwort_eingegeben+"')";
                    Statement stmt = connection.createStatement();
                    stmt.execute(query);

                    //Add Bestenliste
                    String query2 = "insert dbo.bestenliste (punkte, spieler) values (0, '"+benutzername_eingegeben+"')";
                    Statement stmt2 = connection.createStatement();
                    stmt2.execute(query2);
                }

            }
            catch(BenutzernameSchonVergebenException e){
                throw new BenutzernameSchonVergebenException();
            }


        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }

    /**
     * Löscht angegebenen Benutzer und den zugehörigen Bestenlisteneintrag aus der Datenbank.
     * @param benutzername_eingegeben Benutzername des zu löschenden Spielers
     * @throws BenutzernameNichtExistentException - wenn der Benutzername nicht in der Datenbank vorhanden ist
     */
    public void deleteUser (String benutzername_eingegeben) throws BenutzernameNichtExistentException{
        try {

            try {
                this.checkUserName(benutzername_eingegeben);
                throw new BenutzernameNichtExistentException();



            }
            catch(BenutzernameSchonVergebenException e){
                //löscht Eintrag aus benutzer-Tabelle und aus bestenliste-Tabelle (on delete cascade constraint)
                String query = "delete from dbo.benutzer where benutzername = '"+benutzername_eingegeben+"'";
                Statement stmt = connection.createStatement();
                stmt.execute(query);
            }


        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }

    /**
     * Updatet die Bestenliste in der Datenbank. Die Punktzahl des eingegebenen Spielers wird inkrementiert.
     * @param benutzername_eingegeben Benutzer dessen Bestenlisteneintrag inkrementiert werden soll
     * @throws BenutzernameNichtExistentException Wenn der Benutzername nicht in der Datenbank vorhanden ist
     */
    public void updateLeaderboard (String benutzername_eingegeben) throws BenutzernameNichtExistentException{
        try {
            try {
                this.checkUserName(benutzername_eingegeben);
                throw new BenutzernameNichtExistentException();
            }
            catch(BenutzernameSchonVergebenException e){
                String query_get = "select punkte from dbo.bestenliste where spieler = '"+benutzername_eingegeben+"'";
                Statement stmt_get = connection.createStatement();
                ResultSet rs_get = stmt_get.executeQuery(query_get);
                rs_get.next();
                Integer punkte = rs_get.getInt("punkte");
                punkte =  punkte + 1;
                String punkte2 = punkte.toString();
                String query = "update dbo.bestenliste set punkte = "+punkte2+" where spieler = '"+benutzername_eingegeben+"'";
                Statement stmt = connection.createStatement();
                stmt.execute(query);}


        } catch (SQLException e) {
            System.out.println("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }
    }


}