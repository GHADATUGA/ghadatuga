package explodingkittens.server;

import explodingkittens.*;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Diese Klasse ist für die Konfiguration eines Spielraums verantwortlich.
 *
 * @author Nico
 */
class Spielraum extends UnicastRemoteObject implements SpielraumInterface {
    private static final Logger LOGGER = Logger.getLogger(Spielraum.class.getName());
    private String name;
    private final Set<SpielerInterface> spieler = new HashSet<>();
    private final ArrayList<BotInterface> bots = new ArrayList<>();
    private final SpielerInterface ersteller;
    private String status = "active";
    private boolean gestartet = false;

    /**
     * Konstruktor des Spielraums.
     * Initialisiert den Namen und Ersteller des Spielraums.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param name      Name des Spielraums.
     * @throws RemoteException Ausnahme
     */
    Spielraum(SpielerInterface ersteller, String name) throws RemoteException {
        LOGGER.log(Level.INFO, "Spielraum: ersteller={0}, name={1}", new Object[]{ersteller, name});
        this.name = name;
        this.ersteller = ersteller;
    }

    /**
     * Beitreten in einen Spielraum, falls dieser nicht voll.
     *
     * @param newSpieler Beitretender Spieler.
     * @throws SpielraumVollException Ausnahme
     */
    @Override
    public void beitreten(SpielerInterface newSpieler) throws SpielraumVollException {
        checkVoll();
        if(!spieler.contains(newSpieler)){
            spieler.add(newSpieler);
        }
    }

    /**
     * Entfernt einen Spieler aus dem Spielraum
     *
     * @param spieler Spieler, der Spielraum verlässt.
     * @throws ErstellerException Exception, falls der Verlassende spieler der Ersteller ist.
     */
    @Override
    public void verlassen(SpielerInterface spieler) throws ErstellerException {
       /* if (this.ersteller.equals(spieler)) {
            throw new ErstellerException();
        }
        */
        this.spieler.remove(spieler);
    }

    /**
     * Gibt Gestartetflag zurück.
     *
     * @return Gestartetflag.
     */
    @Override
    public boolean isGestartet()  {
        return gestartet;
    }

    /**
     * Setzt Gestartetflag.
     */
    @Override
    public void setGestartet() {
        gestartet = true;
    }

    /**
     * Hinzufügen eines Bots zum Spielraum, falls Spielraum nicht voll.
     *
     * @param bot KI.
     * @throws SpielraumVollException Ausnahme
     */
    @Override
    public void botHinzufuegen(BotInterface bot) throws SpielraumVollException {
        checkVoll();
        bots.add(bot);
    }

    /**
     * Entfernt einen Bot aus dem Spielraum.
     *
     * @param bot zu entfernender Bot.
     * @throws KeinBotVorhandenException Exception, falls kein Bot vorhanden.
     */
    @Override
    public void botEntfernen(BotInterface bot) throws KeinBotVorhandenException {
        if (bots.size() == 0) {
            throw new KeinBotVorhandenException();
        }
        bots.remove(bot);
    }


    /**
     * Gibt Spielraumnamen zurück.
     *
     * @return Spielraumname.
     */
    public String getName() {
        return name;
    }

    /**
     * Prüft, ob Spielraum leer ist.
     *
     * @return boolean, ob Spielraum leer ist.
     */
    @Override
    public boolean istLeer() {
        return spieler.size() < 1;
    }

    /**
     * Setzt Status auf "gelöscht", falls der Ersteller den Spielraum löschen will.
     *
     * @param ersteller Ersteller des Spielraums.
     * @throws KeinErstellerException Ausnahme
     */
    @Override
    public void setzeGeloescht(SpielerInterface ersteller) throws KeinErstellerException {
        try {
            if (this.ersteller.getName().equals(ersteller.getName())) {
                status = "geloescht";
            } else {
                throw new KeinErstellerException();
            }
        } catch (RemoteException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Ändert den Namen des Spielraums, falls der Ersteller dies wünscht.
     *
     * @param ersteller Ersteller des Spielraums.
     * @param newName   Neuer Spielraumname.
     * @throws KeinErstellerException Ausnahme
     */
    @Override
    public void aenderName(SpielerInterface ersteller, String newName) throws KeinErstellerException {
        if (this.ersteller.equals(ersteller)) {
            name = newName;
        } else {
            throw new KeinErstellerException();
        }
    }

    /**
     * Prüft, ob Spielraum voll (wenn #Teilnehmer=4, mit Ersteller).
     *
     * @throws SpielraumVollException Ausnahme
     */
    private void checkVoll() throws SpielraumVollException {
        if (spieler.size() + bots.size() == 3) {
            throw new SpielraumVollException();
        }
    }

    /**
     * Gibt Spieler im Spielraum zurück.
     *
     * @return Spieler des Spielraums.
     */
    @Override
    public Set getSpieler() {
        return spieler;
    }

    /**
     * Gibt Bots im Spielraum zurück.
     *
     * @return Bots des Spielraums.
     */
    @Override
    public ArrayList getBots() {
        return bots;
    }

    /**
     * Gibt Statusflag zurück (für Test)
     *
     * @return Statusflag.
     */
    String getStatus() {
        return status;
    }

    /**
     * Gibt Ersteller zurück.
     *
     * @return Ersteller.
     */
    @Override
    public SpielerInterface getErsteller() {
        return ersteller;
    }

    /**
     * Gibt Mitspieler zurück.
     *
     * @return Mitspieler.
     */
    @Override
    public ArrayList<String> getMitSpieler() throws RemoteException {
        ArrayList<String> mitSpieler = new ArrayList<>();
        for (SpielerInterface e : spieler) {
            mitSpieler.add(e.getName());
        }
        for (BotInterface e : bots) {
            mitSpieler.add(e.getName());
        }

        return mitSpieler;
    }

    @Override
    public void setOffline()  {
        this.gestartet= false;
    }

}
