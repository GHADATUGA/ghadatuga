package explodingkittens.server;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Klasse, die zuständig für die Verwaltung und Operationen des Ablagestapels ist.
 * @author Desiree
 */

public class Ablagestapel {
    private Stack<Spielkarte> karten;

    /**
     * Konstruktor, der einen leeren Stack f�r die Spielkarten initialisiert.
     */
    public Ablagestapel(){
        karten = new Stack<>();
    }

    /**
     * Gibt die oberste Ablagestapelkarte zurück ohne sie vom Ablagestapel zu entfernen.
     * @return Oberste Ablagestapelkarte.
     * @throws AblagestapelLeerException Falls der Ablagestapel leer ist (oberste Karte kann nicht zurückgegeben werden).
     */
    public Spielkarte top() throws AblagestapelLeerException {
        if (karten.empty()){
            throw new AblagestapelLeerException();
        }
        else{
            return karten.peek();
        }
    }

    /**
     * Fügt dem Ablagestapel eine Karte an oberster Stelle hinzu.
     * @param karte Karte, die auf den Ablagestapel gelegt werden soll.
     */
    public void push(Spielkarte karte){
        karten.push(karte);
    }

    /**
     * Gibt die oberste Ablagestapelkarte zurück und entfernt sie vom Ablagestapel.
     * @return oberste Ablagestapelkarte
     * @throws AblagestapelLeerException - Falls der Ablagestapel leer ist (oberste Karte kann nicht zurückgegeben werden)
     */
    public Spielkarte pull() throws AblagestapelLeerException {
        if (karten.empty()){
            throw new AblagestapelLeerException();
        }
        else{
            return karten.pop();
        }
    }

    /**
     * Gibt den kompletten Ablagestapel als ArrayList zurück.
     * @return Ablagestapel als ArrayList von Spielkarten, wobei an Position 0 die unterste Ablagestapelkarte ist
     */
    public ArrayList<Spielkarte> getAblagestapel(){
        return new ArrayList<>(karten);
    }

}
