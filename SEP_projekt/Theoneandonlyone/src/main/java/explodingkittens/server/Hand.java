package explodingkittens.server;

import java.util.ArrayList;

/**
 * Karten eines Spielers, die nicht durch andere Spieler einsehbar sind.
 * @author Hugo, erstellt am 15.06.2020.
 */
public class Hand {
    private ArrayList<Spielkarte> karten ;
    private Teilnehmer owner;
    private int anzahlKarten;

    /**
     * Konstruktor der Hand-Klasse.
     * @param owner Besitzer der Hand.
     */
    public Hand(Teilnehmer owner){
        this.owner = owner;
        this.karten = new ArrayList<>();
        this.anzahlKarten = 0;
    }

    /**
     * Fügt der Hand eine Karte hinzu.
     * @param spielkarte Karte, die der Hand hinzugefügt werden soll.
     */
    public void addKarte(Spielkarte spielkarte){
        karten.add(spielkarte);
        anzahlKarten ++;
    }

    /**
     * Lösche eine Karte von der Hand.
     * @param spielkarte Karte, die von der Hand abzugeben ist.
     */
    public void subKarte(Spielkarte spielkarte){
        karten.remove(spielkarte);
        anzahlKarten --;
    }

    public ArrayList<Spielkarte> getHand(){
        return this.karten;
    }

    public Teilnehmer getBesitzer(){
        return this.owner;
    }

    public int getAnzahlKarten() {
        return anzahlKarten;
    }
}
