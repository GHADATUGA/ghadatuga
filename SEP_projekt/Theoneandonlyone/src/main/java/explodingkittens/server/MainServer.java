package explodingkittens.server;

import explodingkittens.server.datenbankTest.DatenbankServer;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Main-Klasse des Servers. Zuständig für Initialisierung der Registry.
 */
public class MainServer {
    /**
     *
     * @param args Kommandozeilenargumente.
     * @throws RemoteException Wenn eine Verbindung fehlgeschalgen ist.
     * @throws AlreadyBoundException Wenn der Port bereits verwendet wird.
     */
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {

        Chat chat = new Chat();
        DatenbankServer datenbank = new DatenbankServer();  //in Datenbank ändern, wenn DB angebunden
        Benutzer benutzer = new Benutzer(datenbank);
        Bestenliste bestenliste = new Bestenliste(datenbank);
        SpielraumService spielraumService = new SpielraumService();
        SpielerService spielerService = new SpielerService();
        SpielService spielService = new SpielService();

        Registry registry = LocateRegistry.createRegistry(8014);
        registry.bind("chat", chat);
        registry.bind("benutzer", benutzer);
        registry.bind("bestenlist", bestenliste);
        registry.bind("spielraumService", spielraumService);
        registry.bind("spielerService", spielerService);
        registry.bind("spielService", spielService);

        System.err.println("Server ready");

    }
}
