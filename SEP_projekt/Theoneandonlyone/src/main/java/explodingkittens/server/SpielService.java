package explodingkittens.server;

import explodingkittens.SpielInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Verwaltet die verschiedenen Spiele und Teilnehmerlisten.
 */
public class SpielService extends UnicastRemoteObject implements SpielInterface {
    private Hashtable<String , Spiel> spiele;
    private Hashtable<Spiel, Hashtable<String, Teilnehmer>> teilnehmerlisten;

    /**
     * Konstruktor, des Spiel-Services. Erzeugt eine neue Hashtable in der die verschiedenen Spiele verwaltet werden und
     * eine neue Hashtable für die Zuordnung von Teilnehmernamen zu Teilnehmern.
     * @throws RemoteException  Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public SpielService() throws RemoteException {
        spiele = new Hashtable<>();
        teilnehmerlisten = new Hashtable<>();
    }
    /**
     * Entfernt den Spieler aus dem Spielablauf.
     * @param teilnehmer Teilnehmer, das Spiel verlassen will.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Liste, bestehend aus einem boolean (Spieler beendet und Gewinner ist ein Spieler) einem String
     * (Spielername des Gewinners oder null, falls es (noch) keinen Gewinner des Typs Spieler gibt).
     */
    @Override
    public ArrayList verlassen(String teilnehmer, String spiel) {
        return spiele.get(spiel).verlassen(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer));
    }

    /**
     * Beendet den Zug des Spielers, indem eine Karte vom Spielstapel gezogen wird.
     * @param teilnehmer Teilnehmer, der den Zug beenden will.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return true, wenn gezogene Karte eine Exploding Kitten ist, sonst false.
     */
    @Override
    public boolean zugBeenden(String teilnehmer, String spiel) throws NichtAktuellerTeilnehmerException {
        return spiele.get(spiel).zugBeenden(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer));
    }

    /**
     * Gibt die Anzahl Änderungen am Spiel zurück. Nötig zum Vergleich im AktionListener, um zu entscheiden, ob
     * ein Update der GUI erfolgen muss.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Anzahl Änderungen.
     */
    @Override
    public int modified(String spiel) {
        return spiele.get(spiel).modified();
    }

    /**
     * Gibt den aktuellen Spielzustand (aus Sicht des Teilnehmers) zurück
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @return Aktueller Teilnehmer (Name), verbleibende Züge des aktuellen Teilnehmers,
     * Teilnehmerliste in Spielreihenfolge (Namen), oberste Ablagestapelkarte (Name),
     * Exploding-Kitten-Dichte im Spielstapel, Anzahl der Spielstapelkarten und
     * die Handkarten als Hastable mit dem Key Teilnehmername und dem Value ArrayList der Spielkartennamen.
     */
    @Override
    public ArrayList getSpielzustand(String spiel) {
        return spiele.get(spiel).getSpielzustand();
    }

    /**
     * Erzeugt ein neues Spiel mit den angegebenen Teilnehmern. Dabei werden Tabellen für die
     * Auflösung von Teilnehmernamen zu Teilnehmer-Objekten in der gespeichert.
     * @param name Spielname (==Spielraumname) des zu erzeugenden Spiels.
     * @param spieler Liste der Namen der Spieler, die mitspielen sollen (Spieler: menschliche Mitspieler).
     * @param bots Liste der Namen der Bots, die mitspielen sollen.
     */
    @Override
    public void erzeugeSpiel(String name, ArrayList<String> spieler, ArrayList<String> bots) {
        if(!spiele.containsKey(name)){
        ArrayList<Teilnehmer> teilnehmerListe = new ArrayList<>();

        //erzeuge Spieler
        Spieler[] spielerListe = new Spieler[spieler.size()];
        int counter2 = 0;
        for (String s:spieler){
            try{
                Spieler spieler1 = new Spieler(s);
                spielerListe[counter2]=spieler1;
                teilnehmerListe.add(spieler1);
            }
            catch (RemoteException e){
            }
            counter2 ++;
        }
        //erzeuge Bots

        Bot[] botListe= new Bot[bots.size()];
        int counter = 0;
        for (String b:bots){
            try{
                Bot bot = new Bot(b);
                botListe[counter]= bot;
                teilnehmerListe.add(bot);
            }
            catch (RemoteException e){
            }
            counter ++;
        }
        Spiel s = new Spiel(spielerListe,botListe);
        spiele.put(name,s);
        Hashtable<String, Teilnehmer> tn = new Hashtable<String, Teilnehmer>();
        for (Teilnehmer te:teilnehmerListe){
            tn.put(te.getName(),te);
        }
        teilnehmerlisten.put(s,tn);}
    }

    /**
     * Spielt eine Karte "Blick in die Zukunft" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @return Liste der Namen der obersten drei Spielkarten oder null, falls die Aktion durch Nö-Karten verhindert wurde.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    @Override
    public String[] blickInDieZukunft(String spiel,String teilnehmer) throws KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        return spiele.get(spiel).blickInZukunft(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer));
    }

    /**
     * Spielt eine Karte "Mischen" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    @Override
    public void mischen(String spiel, String teilnehmer) throws KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        spiele.get(spiel).mischen(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer));
    }

    /**
     * Spielt eine Karte "Nö" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws KarteNichtAusspielbarException Wird geworfen, wenn keine Karte, die genöt werden kann, im Moment auf dem Ablagestapel liegt.
     */
    @Override
    public void noe(String spiel, String teilnehmer) throws KarteNichtAufHandException, KarteNichtAusspielbarException {
        spiele.get(spiel).noe(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer)); }

    /**
     * Spielt die Karte "Wunsch" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param spieler Teilnehmer, der die Karte spielt.
     * @param gegner Teilnehmer, der eine Karte abgeben soll.
     * @return true, wenn der Wunsch nicht durch "Nös" verhindert wird, sonst false.
     * @throws RemoteException Exception, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    @Override
    public boolean gegnerGibtKarte(String spiel, String spieler, String gegner) throws RemoteException, KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        return spiele.get(spiel).gegnerGibtKarte(teilnehmerlisten.get(spiele.get(spiel)).get(spieler),teilnehmerlisten.get(spiele.get(spiel)).get(gegner));
    }

    /**
     * Führt die Übergabe der vom Gegner abgegebenen Karte an den aktuellen Spieler durch (Teil der Aktion "Wunsch").
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param spieler Teilnehmer, der die Karte spielt.
     * @param gegner Teilnehmer, der eine Karte abgeben soll.
     * @param karte Karte, die der Gegner dem Spieler abgibt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Gegner keine Karte des angegebenen Typs auf der Hand hat.
     */
    @Override
    public void gegnerGibtKarteAb(String spiel, String spieler, String gegner, String karte) throws KarteNichtAufHandException {
        System.out.println(teilnehmerlisten.get(spiele.get(spiel)).get(spieler));
        System.out.println(spiele.get(spiel));
        System.out.println(teilnehmerlisten.get(spiele.get(spiel)).get(gegner));
        System.out.println(karte);
        spiele.get(spiel).gegnerGibtKarteAb(teilnehmerlisten.get(spiele.get(spiel)).get(spieler),teilnehmerlisten.get(spiele.get(spiel)).get(gegner), karte); }

    /**
     * Spielt eine "Entschärfungskarte".
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @param position Position (0, ..., #Karten(Spielstapel)), an die die Exploding Kitten in den Spielstapel
     * gelegt werden soll. Wobei 0 die unterste Position ist.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws PositionNichtMoeglichException Wird geworfen, wenn die angegebene Position, an die die Exploding Kitten gelegt werden soll, nicht gültig ist.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    @Override
    public void entschaerfen(String spiel, String teilnehmer, int position) throws KarteNichtAufHandException, PositionNichtMoeglichException, NichtAktuellerTeilnehmerException {
        spiele.get(spiel).entschaerfen(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer),position);
    }

    /**
     * Führt das Explodieren eines Spielers durch.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der explodiert.
     */
    @Override
    public void explodieren(String spiel, String teilnehmer){
        spiele.get(spiel).explodieren(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer));
    }

    /**
     * Spielt die Karte "Angriff" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    @Override
    public void angriff(String spiel, String teilnehmer) throws KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        spiele.get(spiel).angriff(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer)); }

    /**
     * Spielt die Karte "Hops" aus.
     * @param spiel Spielname des Spiels aus dem diese Methode aufgerufen wird.
     * @param teilnehmer Teilnehmer, der die Karte spielt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    @Override
    public void hops(String spiel, String teilnehmer) throws KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        spiele.get(spiel).hops(teilnehmerlisten.get(spiele.get(spiel)).get(teilnehmer));
    }

    @Override
    public void loeschen(String location) throws RemoteException {
        spiele.remove(location);
    }

}
