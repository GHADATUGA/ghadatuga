package explodingkittens.server;

/**
 * Wenn der Spielkartenname nicht gültig ist (erlaubte Werte: "exploding_kitten", "entschaerfung", "noe", "angriff", "hops", "wunsch", "mischen", "blick_in_die_zukunft")
 */
public class UngueltigerSpielkartenname extends Exception{
}
