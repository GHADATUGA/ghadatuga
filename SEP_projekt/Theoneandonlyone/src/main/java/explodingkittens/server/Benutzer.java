package explodingkittens.server;

import explodingkittens.*;
import explodingkittens.server.datenbankTest.DatenbankServer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
/**
 * Diese Klasse ist Server von BenutzerClient.
 * @author Ghad
 */

public class Benutzer extends UnicastRemoteObject implements BenutzerInterface {
    private DatenbankServer datenbank;
    private ArrayList<String> OnlinePeople;

    /**
     *
     * @param datenbank Datenbank System.
     * @throws RemoteException wird geworfen.
     */
    public Benutzer(DatenbankServer datenbank) throws RemoteException {
        this.datenbank = datenbank;
        OnlinePeople=new ArrayList<>();
    }
   /*Auskommentieren um richtige DB zu benutzen
    private final Datenbank datenbank;
    private final ArrayList<String > OnlinePeople;
    public Benutzer(Datenbank datenbank) throws RemoteException {
        this.datenbank = datenbank;
        OnlinePeople=new ArrayList<>();
    }*/

    /**
     * Bekommt den eingegebenen Name und das neue Passwort.
     *
     * @param name Gewünschter Benutzername von neuem Client(Spieler).
     * @param kennwort  Gewünschtes Passwort.
     * @param kennwort2 Passwort Wiederholung.
     * @throws LeereEingabeException Leere Eingabe exception.
     * @throws BenutzernameSchonVergebenException Benutzername ist schon vergeben Exception.
     * @throws EingabeZuLangException Eingabe ist zu lang Exception.
     */
    @Override
    public void registrieren(String name, String kennwort, String kennwort2) throws PasswortFalschWiederholtException, BenutzernameSchonVergebenException, LeereEingabeException, EingabeZuLangException {
        if (!kennwort.equals(kennwort2)) {
            throw new PasswortFalschWiederholtException();
        }
            datenbank.createUser(name, kennwort);

    }

    /**
     * Beim Anmelden wird der Benutzername und das Passwort eingegeben.
     * @param name Benutzername.
     * @param kennwort Entsprechendes Passwort.
     * @throws BenutzernameNichtExistentException Benutzername ist nicht existent Exception.
     * @throws PasswortFalschException Passwort ist Falsch Exception
     * @throws LeereEingabeException ein Feld ist leer
     */
    @Override
    public void anmelden(String name, String kennwort) throws SchonAngemeldetException, PasswortFalschException, BenutzernameNichtExistentException, LeereEingabeException {


                 if (OnlinePeople.contains(name)){
                     throw new SchonAngemeldetException();
                 }

                 datenbank.checkUserData(name, kennwort);
                 OnlinePeople.add(name);
    }

    /**
     * Beim Ändern der Daten wird ein neuer Benutzerdaten und ein neues Passwort gefordert.
     *
     * @param neuBenutzername Neuer Benutzername.
     * @param neuPassword Neues Passwort.
     * @param passwordwiederholen Wiederholung des Passwortes.
     * @throws LeereEingabeException Benutzername oder Passwort ist leer.
     * @throws EingabeZuLangException Benutzername oder Passwort ist zu lang
     * @throws BenutzernameSchonVergebenException Benutzername ist Schon Vergeben Exception,
     * @throws BenutzernameNichtExistentException Benutzername ist Nicht Existent Exception
     */
    @Override
    public void aendern(String altename, String neuBenutzername, String neuPassword, String passwordwiederholen) throws PasswortFalschWiederholtException, BenutzernameNichtExistentException, LeereEingabeException, EingabeZuLangException, BenutzernameSchonVergebenException {
        if (!neuPassword.equals(passwordwiederholen)) {
            throw new PasswortFalschWiederholtException();
        }
            datenbank.changeUserName(altename, neuBenutzername);
            datenbank.changePassword(neuBenutzername, neuPassword);
    }

    /**
     * Beim Löschen der Daten werden die aktuellen Benutzerdaten eingeben.
     * Das System löscht die gegebenen Daten
     * @param name Benutzername.
     * @param passwort Passwort.
     * @throws BenutzernameNichtExistentException Benutzername ist nicht existent Exception.
     * @throws PasswortFalschException Passwort ist falsch Exception.
     * @throws LeereEingabeException wenn ein Feld ist leer
     */
    @Override
    public void loeschen(String name, String passwort) throws PasswortFalschException, BenutzernameNichtExistentException, LeereEingabeException {

            datenbank.checkUserData(name, passwort);
            datenbank.deleteUser(name);
            OnlinePeople.remove(name);


    }

    /**
     * Benutzerclient wird abgemeldet.
     * @param name Name des abzumeldenden Benutzers.
     */
    @Override
    public void abmelden(String name) {
        OnlinePeople.remove(name);
    }
    public ArrayList<String> getOnlinePeople(){
        return OnlinePeople;
    }

}