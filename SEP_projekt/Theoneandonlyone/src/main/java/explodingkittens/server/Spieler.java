package explodingkittens.server;

import explodingkittens.SpielerInterface;

import java.rmi.RemoteException;

/**
 * Spieler-Klasse.
 * @author Ghad
 */
public class Spieler extends Teilnehmer implements SpielerInterface {

    private static final long serialVersionUID = 1L;

    /**
     * Spieler-Klasse
     *
     * @param name Name des Spielers
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @author Ghad
     */
    public Spieler(String name) throws RemoteException {
        super(name);
    }

    /**
     * Überschriebene Equals-Methode.
     *
     * @param obj zu vergleichendes Objekt.
     * @return boolean, ob Objekt gleich dem SpielerInterface.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SpielerInterface)) { //Check against the interface
            return false;
        }
        //Only use the interface. obj might be a stub, not only an impl.
        SpielerInterface other = (SpielerInterface) obj;
        try {
            return getName().equals(other.getName());
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }
}