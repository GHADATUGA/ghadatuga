package explodingkittens.server;

import explodingkittens.ChatInterface;
import explodingkittens.NachrichtZuLangException;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Verwaltung und Manipulation der Nachrichten.
 *
 * Behandelt Chat-bezogene Funktionen und Aktualisierung der Liste der angemeldeten Spieler.
 *
 * @author Hugo, erstellt am 05.06.2020
 */
public class Chat extends UnicastRemoteObject implements ChatInterface {

    private ArrayList<String> spielerList;
    private Hashtable<String , ArrayList<String>> nachrichtenTable;
    private Hashtable<String,AtomicInteger> changesCount;
    private volatile AtomicInteger countChangesSpielerList = new AtomicInteger();

    /**
     * Konstruktor der Chat-Klasse.
     *
     * Initialisiert und weist die chatServer hashMap zu.
     * Erstellt einen Standort "Lobby" und dessen Nachrichtenliste.
     * Erstellt eine Liste von Benutzernamen.
     *
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    public Chat() throws RemoteException{
        nachrichtenTable = new Hashtable<>();
        changesCount = new Hashtable<>();
        ArrayList<String> lobbyNachrichten = new ArrayList<>();
        nachrichtenTable.put("Lobby", lobbyNachrichten);
        changesCount.put("Lobby", new AtomicInteger(0));
        spielerList = new ArrayList<>();
        countChangesSpielerList.set(0);
    }

    /**
     * Fügt der Liste der Nachrichten an einem vorhandenen Speicherort eine Nachricht hinzu.
     * Fügt der Benutzerliste einen Benutzer hinzu, wenn der Speicherort spielerList ist.
     *
     * @param location Ort, zu dem die Nachricht gehört. Oder spielerList.
     * @param text Nachricht, die hinzugefügt wird. Oder Benutzername.
     * @throws NachrichtZuLangException Ausnahme, die ausgelöst wird, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Override
    public void verfassen(String location, String text) throws NachrichtZuLangException {
        if(location.equalsIgnoreCase("spielerList")){
            if(!spielerList.contains(text)){
                spielerList.add(text);
                //countChangesSpielerList = countChangesSpielerList +1;
                countChangesSpielerList.incrementAndGet();
            }
        }else if(text.length()>80){
            throw new NachrichtZuLangException();
        }else if (!nachrichtenTable.containsKey(location)) {
            throw new NullPointerException();
        }else{
            nachrichtenTable.get(location).add(text);
            changesCount.put(location, new AtomicInteger(changesCount.get(location).incrementAndGet()));
        }
    }

    /**
     * Gibt die Liste der Nachrichten an einem vorhandenen Speicherort zurück.
     * Gibt die Liste der Benutzer zurück, wenn der Speicherort spielerList ist.
     *
     * @param location Ort, zu dem die Liste der Nachrichten gehört. Oder spielerList.
     * @return Liste der Nachrichten oder Liste der Benutzer.
     */
    @Override
    public ArrayList<String> getNachrichten(String location) {
        if(location.equalsIgnoreCase("spielerList")){
            return spielerList;
        }else if(isInTable(location)){
            return nachrichtenTable.get(location);
        }
        throw new NullPointerException();
    }

    /**
     * Prüft, ob der Standort dem Server bekannt ist.
     *
     * @param location Ort, zu dem die Nachrichten gehört.
     * @return true, wenn der Ort existiert. Sonst false.
     */
    private boolean isInTable(String location) {
        return nachrichtenTable.containsKey(location);
    }

    /**
     * Löscht die Liste der Nachrichten an einem dem Server bekannten Ort.
     *
     * Entfernt einen Benutzernamen in der Benutzerliste, wenn der Speicherort spielerList ist.
     *
     * @param location Ort, an dem die Nachrichtenliste gelöscht werden soll oder spielerList.
     * @param username Benutzername, der in der Liste der Benutzer entfernt werden soll.
     */
    @Override
    public void nachrichtenLoeschen(String location, String username){
        if(location.equalsIgnoreCase("spielerList")){
            spielerList.remove(username);
            countChangesSpielerList.incrementAndGet();
        }else if(isInTable(location)){
            nachrichtenTable.remove(location);
            changesCount.remove(location);
        }else{
            throw new NullPointerException();
        }
    }

    /**
     * Erstellen eines Standorts und einer dazugehörigen Nachrichtenliste.
     * @param location Neuer Speicherort, zu dem die Nachrichten gehören würden.
     */
    @Override
    public void erstellNachrichtenLocation(String location) {
        if(!nachrichtenTable.containsKey(location)){
            ArrayList<String> list = new  ArrayList<>();
            nachrichtenTable.put(location,list);
            changesCount.put(location, new AtomicInteger(0));
        }
    }

    /**
     * Prüft, ob die Daten auf dem Server aktualisiert wurden.
     * @param location Ort, zu dem die Nachrichten gehören.
     * @return Gibt die Anzahl der Änderungen zurück.
     */
    @Override
    public int aktualisieren(String location) {
        if(location.equalsIgnoreCase("spielerList")){
            return countChangesSpielerList.get();
        }else if(isInTable(location)){
            return changesCount.get(location).get();
        }
        throw new NullPointerException();
    }
}
