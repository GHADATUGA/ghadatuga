package explodingkittens.server;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Zuständig für die Verwaltung des Spielablaufs.
 * @author Desiree
 */


public class Spiel{
    private Spieler[] spielerliste;
    private Bot[] botliste;
    private Ablagestapel ablagestapel;
    private Spielkartenstapel spielstapel;
    private Map<Teilnehmer, Hand> handkarten;
    private Teilnehmer aktuellerTeilnehmer;
    private int verbleibendeZuegeAktuellerTeilnehmer;
    private LinkedList<Teilnehmer> reihenfolge;
    private float explodingKittenDichte;
    private int anzahlTeilnehmer;
    private int anzahlSpielstapelkarten;
    private int aenderungen;
    private boolean aktiv_karte; //gibt an, ob gerade versucht wird eine Karte zu spielen
    private int aktiv_noe_karten; //gibt an wieviele Nö-Karte auf aktive Karte gespielt wurden
    private int noezeit = 5000;
    private Spielkarte aktuelleExplodingKitten;
    private boolean beendet;
    private String SpielGewinner;
    private boolean wunschAusstehend;
    private String wunschGegner;
    private String wunschSpieler;


    /**
     * Konstruktor der Klasse Spiel. Initialisiert die Teilnehmerlisten und iniziiert die Verteilung der Spielkarten bzw. Erzeugung der Stapel und Hände.
     * @param spieler Liste von Spielern, die mitspielen.
     * @param bots    Liste von Bots, die mitspielen.
     */

    public Spiel(Spieler[] spieler, Bot[] bots){
        this.spielerliste = spieler;
        this.botliste = bots;
        reihenfolge = new LinkedList<>();
        anzahlTeilnehmer = 0;

        for (Spieler s : spieler) {
            reihenfolge.add(s);
            anzahlTeilnehmer++;
        }
        for (Bot b : bots) {
            reihenfolge.add(b);
            anzahlTeilnehmer++;
        }
        aktuellerTeilnehmer = reihenfolge.pollFirst();
        verbleibendeZuegeAktuellerTeilnehmer = 1;
        reihenfolge.addLast(aktuellerTeilnehmer);
        SpielGewinner=null;
        wunschAusstehend=false;
        wunschGegner=null;
        wunschSpieler=null;
        anzahlSpielstapelkarten = (6 - anzahlTeilnehmer) + (anzahlTeilnehmer - 1) + (26 - anzahlTeilnehmer * 4);
        this.explodingKittenDichte = (float)(anzahlTeilnehmer - 1) / (float) anzahlSpielstapelkarten;
        this.beendet=false;
        try {
            kartenVerteilen();
        } catch (UngueltigerSpielkartenname e) {
            System.out.println("Kartennamen falsch eingegeben beim Initialisieren");

        }
        aktiv_karte=false;
        aktuelleExplodingKitten=null;
        aenderungen = 1;
        for (Bot b :botliste){
            b.initialisiereDaten(this);
        }
        if (aktuellerTeilnehmer.getClass()==Bot.class){
            ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
        }
        aenderungen ++;



    }

    /**
     * Verteilung der Spielkarten auf die Hände und den Spielkartenstapel sowie Erzeugung des Ablagestapels.
     * @throws UngueltigerSpielkartenname Wenn eine Spielkarte falsch initialisiert wurde.
     */
    private void kartenVerteilen() throws UngueltigerSpielkartenname {
        this.ablagestapel = new Ablagestapel();
        //Erstelle alle Spielkarten, die zufällig auf die Hände verteilt werden (keine Exploding Kittens, keine Entschärfungen)
        Spielkarte[] array_austeilstapel = new Spielkarte[26];
        for (int i = 0; i < 5; i++) {
            array_austeilstapel[i] = new Spielkarte("noe");
        }
        for (int i = 5; i < 9; i++) {
            array_austeilstapel[i] = new Spielkarte("angriff");
        }
        for (int i = 9; i < 13; i++) {
            array_austeilstapel[i] = new Spielkarte("hops");
        }
        for (int i = 13; i < 17; i++) {
            array_austeilstapel[i] = new Spielkarte("wunsch");
        }
        for (int i = 17; i < 21; i++) {
            array_austeilstapel[i] = new Spielkarte("mischen");
        }
        for (int i = 21; i < 26; i++) {
            array_austeilstapel[i] = new Spielkarte("blick_in_die_zukunft");
        }
        Spielkartenstapel austeilstapel = new Spielkartenstapel(array_austeilstapel);
        austeilstapel.mischen();
        handkarten = new Hashtable<Teilnehmer,Hand>(5);
        for (Spieler s : spielerliste) {
            handkarten.put(s, new Hand(s));
            for (int i = 0; i < 4; i++) {
                //Füge 4 Karten einer Hand hinzu
                handkarten.get(s).addKarte(austeilstapel.pop());
            }
        }

        for (Bot b : botliste) {
            handkarten.put(b, new Hand(b));
            for (int i = 0; i < 4; i++) {
                //Füge 4 Karten einer Hand hinzu
                handkarten.get(b).addKarte(austeilstapel.pop());
            }
        }
        int entschaerfungen_verteilt = 0;
        //Entschärfungen verteilen
        for (Spieler s : spielerliste) {
            //Füge 1 Entschärfungs-Karte einer Hand hinzu
            handkarten.get(s).addKarte(new Spielkarte("entschaerfung"));
            entschaerfungen_verteilt++;
        }
        for (Bot b : botliste) {
            //Füge 1 Entschärfungs-Karte einer Hand hinzu
            handkarten.get(b).addKarte(new Spielkarte("entschaerfung"));
            entschaerfungen_verteilt++;
        }
        Spielkartenstapel spielkartenstapel = new Spielkartenstapel(new Spielkarte[0]);
        for (int i = 0; i < (6 - entschaerfungen_verteilt); i++) {
            spielkartenstapel.push(new Spielkarte("entschaerfung"));
        }
        // #Teilnehmer-1 Exploding-Kitten dem Spielstapel hinzufügen
        for (int i = 0; i < anzahlTeilnehmer - 1; i++) {
            spielkartenstapel.push(new Spielkarte("exploding_kitten"));
        }
        //Es gibt insgesamt 26 Karten auf dem Austeilstapel, jeder Teilnehmer erhält 4, restliche Karten zum Spielkartenstapel hinzufügen
        for (int i = 0; i < 26 - anzahlTeilnehmer * 4; i++) {
            spielkartenstapel.push(austeilstapel.pop());
        }
        spielkartenstapel.mischen();
        this.spielstapel = spielkartenstapel;


    }


    /**
     * Entfernt den Spieler u.a. aus der Teilnehmerliste und der Reihenfolge-Queue.
     * @param teilnehmer Teilnehmer, der aus dem Spiel entfernt werden soll.
     * @return Liste, bestehend aus einem boolean (Spieler beendet und Gewinner ist ein Spieler) einem String
     * (Spielername des Gewinners oder null, falls es (noch) keinen Gewinner des Typs Spieler gibt).
     */
    public ArrayList verlassen(Teilnehmer teilnehmer)  {
        //aus Reihenfolge und Handkarten entfernen, ggf. aktuellen Teilnehmer anpassen

        for (int i=0;i<reihenfolge.size();i++){
            if (reihenfolge.get(i)==teilnehmer){
                handkarten.remove(reihenfolge.get(i));

                reihenfolge.remove(i);
                anzahlTeilnehmer --;
                break;
            }
        }

        if (teilnehmer.getClass()==Spieler.class || teilnehmer.getClass().getSuperclass()==Spieler.class){
            for (int i=0;i<spielerliste.length;i++){
                if (spielerliste[i]==teilnehmer){
                    spielerliste[i]=null;
                }
            }
        }
        if (teilnehmer.getClass()==Bot.class || teilnehmer.getClass().getSuperclass()==Bot.class){
            int pos=0;
            for (int i=0;i<botliste.length;i++){
                if (botliste[i]==teilnehmer){
                    pos=i;
                }
            }
            ArrayList<Bot> botliste_new=new ArrayList<>();
            for (int i=0;i<pos;i++){
                botliste_new.add(botliste[i]);
            }
            for (int i=pos+1;i<botliste.length;i++){
                botliste_new.add(botliste[i]);
            }
            botliste=new Bot[botliste_new.size()];
            int counter=0;
            for(Bot b: botliste_new){
                botliste[counter]=b;
                counter++;
            }

        }

        //Prüft, ob Spiel beendet werden soll
        ArrayList gewinner = new ArrayList<>();
        if (anzahlTeilnehmer==1){
            if (reihenfolge.getFirst().getClass()==Spieler.class || reihenfolge.getFirst().getClass().getSuperclass()==Spieler.class){
                gewinner.add(true);
                beendet=true;
                SpielGewinner=reihenfolge.getFirst().getName();
                gewinner.add(reihenfolge.getFirst().getName());
            }
            else{
                gewinner.add(false);
                beendet=true;
                gewinner.add(null);
            }
        }
        else{
            gewinner.add(false);
            gewinner.add(null);
            boolean spielergefunden = false;
            for (Teilnehmer t:reihenfolge){
                if (t.getClass()==Spieler.class||t.getClass().getSuperclass()==Spieler.class){
                    spielergefunden=true;
                }
            }
            if(!spielergefunden){
                beendet=true;
            }
            else{
                if (aktuellerTeilnehmer==teilnehmer){
                    aktuellerTeilnehmer=reihenfolge.pollFirst();
                    reihenfolge.addLast(aktuellerTeilnehmer);
                    verbleibendeZuegeAktuellerTeilnehmer=1;
                    aenderungen ++;
                    if (aktuellerTeilnehmer.getClass()==Bot.class){
                        ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
                    }
                }
            }

        }

        aenderungen ++;
        return gewinner;
    }


    /**
     * Gibt die oberste Ablagestapelkarte zurück.
     * @throws AblagestapelLeerException Wird geworfen, wenn der Ablagestapel leer ist.
     * @return Oberste Ablagestapelkarte.
     */
    public Spielkarte obersteAblagestapelkarte() throws AblagestapelLeerException {
        return ablagestapel.top();
    }

    /**
     * Gibt die Spielkarten, die ich auf der Hand des angegebenen Teilnehmers befinden, zurück.
     * @param teilnehmer Teilnehmer, dessen Handkarten zurückgegeben werden sollen.
     * @return Handkarten des angegebenen Teilnehmers.
     */
    public ArrayList<Spielkarte> getHand(Teilnehmer teilnehmer) {
        return handkarten.get(teilnehmer).getHand();
    }

    /**
     * Gibt die Anzahl Handkarten jedes Teilnehmers zurück.
     * @return Liste mit der Anzahl Handkarten aufgeschlüsselt nach Teilnehmer.
     */
    public Hashtable<Teilnehmer, Integer> getAnzahlHandkarten() {
        Hashtable<Teilnehmer, Integer> anzahl = new Hashtable<>();
        for (Teilnehmer t : reihenfolge) {
            anzahl.put(t, handkarten.get(t).getAnzahlKarten());
        }
        return anzahl;
    }

    /**
     * Gibt die Exploding Kitten-Dichte (Anteil Exploding Kittens im Spielkartenstapel) zurück.
     * @return Exploding Kitten-Dichte.
     */
    public float getExplodingKittenDichte() {
        return this.explodingKittenDichte;
    }

    /**
     * Gibt den Ablagestapel zurück.
     * @return Referenz zum Ablagestapel.
     */
    public Ablagestapel getAblagestapel() {
        return ablagestapel;
    }

    /**
     * Gibt den Spielstapel zurück.
     * @return Referenz zum Spielstapel.
     */
    public Spielkartenstapel getSpielstapel() {
        return spielstapel;
    }

    /**
     * Gibt den Teilnehmer zurück, der laut der Reihenfolgen-Queue als nächstes am Zug ist.
     * @return Nächster Teilnehmer.
     */
    public Teilnehmer getNaechsterTeilnehmer() {
        return reihenfolge.getFirst();
    }

    /**
     * Gibt den Teilnehmer zurück, der aktuell am Zug ist.
     * @return Aktueller Teilnehmer.
     */

    public Teilnehmer getAktuellerTeilnehmer() {
        return this.aktuellerTeilnehmer;
    }

    /**
     * Gibt die Reihenfolge zurück.
     * @return Reihenfolge.
     */
    public LinkedList<Teilnehmer> getReihenfolge() {
        return reihenfolge;
    }

    /**
     * Gibt den aktuellen Spielzustand (aus Sicht des Teilnehmers) zurück.
     * @return Aktueller Teilnehmer (Name), verbleibende Züge des aktuellen Teilnehmers,
     * Teilnehmerliste in Spielreihenfolge (Namen), oberste Ablagestapelkarte (Name),
     * Exploding-Kitten-Dichte im Spielstapel, Anzahl der Spielstapelkarten und
     * die Handkarten als Hastable mit dem Key Teilnehmername und dem Value ArrayList der Spielkartennamen.
     */
    public ArrayList getSpielzustand(){
        ArrayList zustand = new ArrayList();

        //aktueller Teilnehmer
        zustand.add(aktuellerTeilnehmer.getName());

        //verbleibende Züge des aktuellen Teilnehmers
        zustand.add(verbleibendeZuegeAktuellerTeilnehmer);

        //Teilnehmerliste (noch im Spiel) -> in Reihenfolge
        String [] teilnehmerListe = new String[reihenfolge.size()];
        int i=0;
        for (Teilnehmer t:reihenfolge){
            teilnehmerListe[i]=t.getName();
            i++;
        }
        zustand.add(teilnehmerListe);

        //oberste Ablagestapelkarte
        try{
            String ablagestapelkarte = obersteAblagestapelkarte().getName();
            zustand.add(ablagestapelkarte);
        }
        catch(AblagestapelLeerException e){
            zustand.add(null);
        }

        //Exploding kitten-Dichte
        zustand.add(explodingKittenDichte);

        //Anzahl Spielstapelkarten
        zustand.add(anzahlSpielstapelkarten);

        //Handkarten
        Hashtable haende = new Hashtable<String,ArrayList<String>>();

        for (Map.Entry<Teilnehmer,Hand> entry : handkarten.entrySet()){
            ArrayList<String> karten = new ArrayList<>();
            String teilnehmername = entry.getKey().getName();

            ArrayList<Spielkarte> hand = entry.getValue().getHand();
            for (Spielkarte karte:hand){
                karten.add(karte.getName());
            }
            haende.put(teilnehmername,karten);
        }
        zustand.add(haende);

        //Anzahl Karten ABlagestapel
        zustand.add(ablagestapel.getAblagestapel().size());

        //Beendet
        zustand.add(beendet);

        //Gewinner
        zustand.add(SpielGewinner);

        //Wunsch ausstehend
        zustand.add(wunschAusstehend);

        //Wunsch Spieler
        zustand.add(wunschSpieler);

        //Wunsch Gegener
        zustand.add(wunschGegner);

        //Ablagestapelkarten
        ArrayList ablagestapel_namen = new ArrayList<>();
        for (Spielkarte k:ablagestapel.getAblagestapel()){
            ablagestapel_namen.add(k.getName());

        }
        zustand.add(ablagestapel_namen);

        return zustand;
    }


    /**
     * Beendet den Zug, indem die oberste Karte vom Ablagestapel gezogen wird.
     * @param teilnehmer Teilnehmer, der den Zug beendet.
     * @throws NichtAktuellerTeilnehmerException Wenn der Teilnehmer, der den Zug beenden will, nicht der aktuelle ist.
     * @return true, wenn die gezogene Karte eine Exploding Kitten ist, sonst false.
     */

    public boolean zugBeenden(Teilnehmer teilnehmer) throws NichtAktuellerTeilnehmerException {
        if (teilnehmer!=getAktuellerTeilnehmer()){
            throw new NichtAktuellerTeilnehmerException();
        }
        else{
            Spielkarte gezogen = spielstapel.pop();
            anzahlSpielstapelkarten--;
            handkarten.get(teilnehmer).addKarte(gezogen);
            for (Bot b: botliste){
                b.updateSpielstapel();
            }
            if (gezogen.getName() == "exploding_kitten") {
                explodingKittenDichte = (explodingKittenDichte * (float)(anzahlSpielstapelkarten + 1) - 1) / (float) anzahlSpielstapelkarten;
                aktuelleExplodingKitten=gezogen;
                aenderungen ++;
                return true;
            } else {
                explodingKittenDichte = (explodingKittenDichte * (float)(anzahlSpielstapelkarten + 1)) / (float)anzahlSpielstapelkarten;
                verbleibendeZuegeAktuellerTeilnehmer--;
                if (verbleibendeZuegeAktuellerTeilnehmer==0){
                    this.aktuellerTeilnehmer=reihenfolge.pollFirst();
                    reihenfolge.addLast(aktuellerTeilnehmer);
                    verbleibendeZuegeAktuellerTeilnehmer=1;
                    aenderungen ++;
                    if (aktuellerTeilnehmer.getClass()==Bot.class){
                        ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
                    }
                }
                aenderungen ++;
                return false;
            }
        }

    }

    /**
     * Führt die Aktion "Blick in die Zukunft" aus, falls die Aktion nicht durch "Nös" verhindert wird.
     * @param teilnehmer Teilnehmer, der die Karte ausspielt.
     * @return Liste der Namen der obersten drei Spielkarten oder null, falls die Aktion durch Nö-Karten verhindert wurde.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     */
    public String[] blickInZukunft(Teilnehmer teilnehmer) throws NichtAktuellerTeilnehmerException, KarteNichtAufHandException{
        if (teilnehmer!=aktuellerTeilnehmer){
            throw new NichtAktuellerTeilnehmerException();
        }
        else {
            aktiv_karte = true;
            aktiv_noe_karten = 0;
            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(teilnehmer).getHand()) {
                if (k.getName() == "blick_in_die_zukunft") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            } else {
                ablagestapel.push(ka);
                handkarten.get(teilnehmer).subKarte(ka);
                if (aktuellerTeilnehmer.getClass()==Bot.class){
                    for(Bot b: getOtherBots((Bot) aktuellerTeilnehmer)){
                        b.reagieren(ka,this);
                    }
                }
                else{
                    for (Bot b: botliste){
                        b.reagieren(ka,this);
                    }
                }
                aenderungen++;
                Thread t = new Thread();
                try {
                    t.sleep(noezeit);
                } catch (InterruptedException e) {
                }
                if (aktiv_noe_karten % 2 == 0) {
                    ArrayList<Spielkarte> stapel = spielstapel.getKarten();
                    String[] kartennamen;
                    if (anzahlSpielstapelkarten>=3) {
                        kartennamen = new String[]{stapel.get(0).getName(), stapel.get(1).getName(), stapel.get(2).getName()};
                    }
                    else if (anzahlSpielstapelkarten==2){
                        kartennamen = new String[]{stapel.get(0).getName(), stapel.get(1).getName()};
                    }
                    else{
                        kartennamen = new String[]{stapel.get(0).getName()};
                    }
                    aktiv_karte=false;
                    aktiv_noe_karten=0;
                    return kartennamen;
                } else {
                    aktiv_karte=false;
                    aktiv_noe_karten=0;
                    return null;
                }
            }
        }

    }

    /**
     * Führt die Aktion "Mischen" aus, falls die Aktion nicht durch "Nös" verhindert wird.
     * @param teilnehmer Teilnehmer, der die Karte ausspielt.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     */
    public void mischen (Teilnehmer teilnehmer) throws NichtAktuellerTeilnehmerException, KarteNichtAufHandException {
        if (teilnehmer!=aktuellerTeilnehmer){
            throw new NichtAktuellerTeilnehmerException();
        }
        else {
            aktiv_karte = true;
            aktiv_noe_karten = 0;

            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(teilnehmer).getHand()) {
                if (k.getName() == "mischen") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            } else {
                ablagestapel.push(ka);
                handkarten.get(teilnehmer).subKarte(ka);
                if (aktuellerTeilnehmer.getClass()==Bot.class){
                    for(Bot b: getOtherBots((Bot) aktuellerTeilnehmer)){
                        b.reagieren(ka,this);
                    }
                }
                else{
                    for (Bot b: botliste){
                        b.reagieren(ka,this);
                    }
                }
                aenderungen++;
                Thread t = new Thread();
                try {
                    t.sleep(noezeit);
                } catch (InterruptedException e) {
                }
                if (aktiv_noe_karten % 2 == 0) {
                    spielstapel.mischen();
                }
                aktiv_karte=false;
                aktiv_noe_karten=0;
            }
        }

    }

    /**
     * Spielt eine Nö-Karte aus.
     * @param teilnehmer Teilnehmer, der die Karte ausspielt.
     * @throws KarteNichtAusspielbarException Wenn derzeit keine Karte genöt werden kann.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     */
    public void noe(Teilnehmer teilnehmer)throws KarteNichtAusspielbarException, KarteNichtAufHandException{
        if (aktiv_karte==false){
            throw new KarteNichtAusspielbarException();
        }
        else{
            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(teilnehmer).getHand()) {
                if (k.getName() == "noe") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            } else {
                aktiv_noe_karten++;
                ablagestapel.push(ka);
                handkarten.get(teilnehmer).subKarte(ka);
                if (aktuellerTeilnehmer.getClass()==Bot.class){
                    for(Bot b: getOtherBots((Bot) aktuellerTeilnehmer)){
                        b.reagieren(ka,this);
                    }
                }
                else{
                    for (Bot b: botliste){
                        b.reagieren(ka,this);
                    }
                }
                aenderungen++;
            }
        }
    }

    /**
     * Spielt die Karte "Wunsch". Falls die Karte nicht genöt wird, muss der gewählte Gegner später eine Karte abgeben.
     * @param spieler Spieler, der die Karte spielt.
     * @param gegner Gegner, der ggf. eine Karte abgeben muss.
     * @return true, wenn der Wunsch nicht durch "Nös" verhindert wird, sonst false.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     */
    public boolean gegnerGibtKarte(Teilnehmer spieler, Teilnehmer gegner) throws NichtAktuellerTeilnehmerException, KarteNichtAufHandException {
        if (spieler!=aktuellerTeilnehmer){
            throw new NichtAktuellerTeilnehmerException();
        }
        else {
            aktiv_karte = true;
            aktiv_noe_karten = 0;

            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(spieler).getHand()) {
                if (k.getName() == "wunsch") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            } else {
                ablagestapel.push(ka);
                handkarten.get(spieler).subKarte(ka);
                if (aktuellerTeilnehmer.getClass()==Bot.class){
                    for(Bot b: getOtherBots((Bot) aktuellerTeilnehmer)){
                        b.reagieren(ka,this);
                    }
                }
                else{
                    for (Bot b: botliste){
                        b.reagieren(ka,this);
                    }
                }
                aenderungen++;
                Thread t = new Thread();
                try {
                    t.sleep(noezeit);
                } catch (InterruptedException e) {
                }
                if (aktiv_noe_karten % 2 == 0 && handkarten.get(gegner).getAnzahlKarten()>0) {
                    aktiv_karte=false;
                    aktiv_noe_karten=0;
                    if (gegner.getClass()==Bot.class){
                        gegnerGibtKarteAb(spieler,gegner,((Bot) gegner).wunschWahl(this).getName());
                    }
                    else{
                        wunschAusstehend=true;
                        wunschGegner=gegner.getName();
                        wunschSpieler=spieler.getName();
                    }
                    aenderungen++;
                    return true;
                }
                else{
                    aktiv_karte=false;
                    aktiv_noe_karten=0;
                    return false;
                }


            }
        }

    }

    /**
     * Führt die Übergabe der vom Gegner abgegebenen Karte an den aktuellen Spieler durch (Teil der Aktion "Wunsch").
     * @param spieler Spieler, der die Wunsch-Karte ausgespielt hat.
     * @param gegner Gegner, der eine Karte seiner Wahl abgeben muss.
     * @param karte Karte, die übertragen wird.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     */
    public void gegnerGibtKarteAb(Teilnehmer spieler, Teilnehmer gegner, String karte) throws KarteNichtAufHandException {
        Spielkarte ka = null;
        for (Spielkarte k : handkarten.get(gegner).getHand()) {
            if (k.getName().equals(karte)) {
                ka=k;
                break;
            }
        }
        if (ka == null) {
            throw new KarteNichtAufHandException();
        }
        else {
            handkarten.get(spieler).addKarte(ka);
            handkarten.get(gegner).subKarte(ka);
            if (wunschSpieler==spieler.getName() && wunschGegner==gegner.getName()){
                wunschAusstehend=false;
                wunschSpieler=null;
                wunschGegner=null;}
            aenderungen++;
        }

    }


    /**
     * Führt die Aktion "entschärfen" aus.
     * @param teilnehmer Teilnehmer, der die Karte ausspielt.
     * @param position Position (0, ..., #Karten(Spielstapel)), an die die Exploding Kitten gelegt werden soll. Wobei 0 die unterste Position ist.
     * @throws PositionNichtMoeglichException Wird geworfen, wenn die angegebene Position, an die die Exploding Kitten gelegt werden soll, nicht gültig ist.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     */
    public void entschaerfen(Teilnehmer teilnehmer,int position) throws PositionNichtMoeglichException, KarteNichtAufHandException {
            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(teilnehmer).getHand()) {
                if (k.getName() == "entschaerfung") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            }
            else{
                handkarten.get(teilnehmer).subKarte(ka);
                ablagestapel.push(ka);
                handkarten.get(teilnehmer).subKarte(aktuelleExplodingKitten);
                if (position<anzahlSpielstapelkarten) {
                    spielstapel.insert(aktuelleExplodingKitten, position);
                }
                else{
                    spielstapel.push(aktuelleExplodingKitten);
                }
                explodingKittenDichte=((explodingKittenDichte*(float)anzahlSpielstapelkarten)+1)/(float)(anzahlSpielstapelkarten+1);
                anzahlSpielstapelkarten++;
                verbleibendeZuegeAktuellerTeilnehmer--;
                if (verbleibendeZuegeAktuellerTeilnehmer==0){
                    aktuellerTeilnehmer=reihenfolge.pollFirst();
                    reihenfolge.addLast(aktuellerTeilnehmer);
                    verbleibendeZuegeAktuellerTeilnehmer=1;
                    if (aktuellerTeilnehmer.getClass()==Bot.class){
                        ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
                    }
                }

                aktuelleExplodingKitten=null;
                aenderungen++;
            }



    }

    /**
     * Wird ausgeführt, wenn eine Exploding-Kitten gezogen wurde, aber keine Entschärfungskarte ausgespielt wurde.
     * @param teilnehmer Teilnehmer, der explodiert
     */
    public void explodieren(Teilnehmer teilnehmer){

        verlassen(teilnehmer);

        aenderungen++;

    }


    /**
     * Führt die Aktion "Angriff" aus, falls sie nicht durch "Nös" verhindert wird.
     * @param teilnehmer Teilnehmer, der die Karte ausspielt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public void angriff(Teilnehmer teilnehmer) throws NichtAktuellerTeilnehmerException, KarteNichtAufHandException {
        if (teilnehmer!=aktuellerTeilnehmer){
            throw new NichtAktuellerTeilnehmerException();
        }
        else {
            aktiv_karte = true;
            aktiv_noe_karten = 0;

            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(teilnehmer).getHand()) {
                if (k.getName() == "angriff") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            } else {
                ablagestapel.push(ka);
                handkarten.get(teilnehmer).subKarte(ka);
                if (aktuellerTeilnehmer.getClass()==Bot.class){
                    for(Bot b: getOtherBots((Bot) aktuellerTeilnehmer)){
                        b.reagieren(ka,this);
                    }
                }
                else{
                    for (Bot b: botliste){
                        b.reagieren(ka,this);
                    }
                }
                aenderungen++;
                Thread t = new Thread();
                try {
                    t.sleep(noezeit);
                } catch (InterruptedException e) {
                }
                if (aktiv_noe_karten % 2 == 0) {
                    aktiv_karte=false;
                    aktiv_noe_karten=0;
                    aktuellerTeilnehmer=reihenfolge.pollFirst();
                    reihenfolge.addLast(aktuellerTeilnehmer);
                    verbleibendeZuegeAktuellerTeilnehmer=2;
                    aenderungen ++;
                    if (aktuellerTeilnehmer.getClass()==Bot.class){
                        ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
                        ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
                    }
                    aenderungen ++;



                }
                else{
                    aktiv_karte=false;
                    aktiv_noe_karten=0;

                }


            }
        }

    }

    /**
     * Führt die Aktion "hops" aus, falls sie nicht durch "Nös" verhindert wird.
     * @param teilnehmer Teilnehmer, der die Karte ausspielt.
     * @throws KarteNichtAufHandException Wird geworfen, falls der Teilnehmer keine Karte des angegebenen Typs auf der Hand hat.
     * @throws NichtAktuellerTeilnehmerException Wird geworfen, wenn der Teilnehmer nicht am Zug ist.
     */
    public void hops(Teilnehmer teilnehmer) throws KarteNichtAufHandException, NichtAktuellerTeilnehmerException {
        if (teilnehmer != aktuellerTeilnehmer) {
            throw new NichtAktuellerTeilnehmerException();
        } else {
            Spielkarte ka = null;
            for (Spielkarte k : handkarten.get(teilnehmer).getHand()) {
                if (k.getName() == "hops") {
                    ka = k;
                    break;
                }
            }
            if (ka == null) {
                throw new KarteNichtAufHandException();
            } else {
                handkarten.get(teilnehmer).subKarte(ka);
                ablagestapel.push(ka);
                if (aktuellerTeilnehmer.getClass()==Bot.class){
                    for(Bot b: getOtherBots((Bot) aktuellerTeilnehmer)){
                        b.reagieren(ka,this);
                    }
                }
                else{
                    for (Bot b: botliste){
                        b.reagieren(ka,this);
                    }
                }
                aenderungen++;
                Thread t = new Thread();
                try {
                    t.sleep(noezeit);
                } catch (InterruptedException e) {
                }
                if (aktiv_noe_karten % 2 == 0) {
                    aktiv_karte=false;
                    aktiv_noe_karten=0;
                    verbleibendeZuegeAktuellerTeilnehmer--;
                    if (verbleibendeZuegeAktuellerTeilnehmer == 0) {
                        aktuellerTeilnehmer = reihenfolge.pollFirst();
                        reihenfolge.addLast(aktuellerTeilnehmer);
                        verbleibendeZuegeAktuellerTeilnehmer=1;
                        aenderungen++;
                        if (aktuellerTeilnehmer.getClass()==Bot.class){
                            ((Bot) aktuellerTeilnehmer).zugAusfuehren(this);
                        }
                    }
                    aenderungen++;

                }
                else{
                    aktiv_karte=false;
                    aktiv_noe_karten=0;

                }

            }

        }
    }

    private Bot[] getOtherBots(Bot b){
        Bot[] otherBots = new Bot[botliste.length-1];
        int otherBotsLength = 0;
        for (Bot bot:botliste){
            if (bot!=b){
                otherBots[otherBotsLength]=bot;
                otherBotsLength++;
            }
        }
        return otherBots;
    }
    /**
     * Gibt die Anzahl Änderungen am Spielzustand zurück.
     * @return Anzahl der Änderungen des Spielzustands.
     */
    public int modified(){
        return aenderungen;
    }

}
