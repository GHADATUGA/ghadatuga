package explodingkittens;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote-Interface für einen Bot.
 */
public interface BotInterface extends Remote {
    /**
     * Gibt den Namen des Bots zurück.
     * @return Name des Bots.
     * @throws RemoteException Wird geworfen, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    String getName() throws RemoteException;

}
