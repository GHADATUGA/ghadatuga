package explodingkittens.server;

import explodingkittens.*;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test der BenutzerKlasse
 * @author  Ghad
 */
class BenutzerTest {
    private StubDatenbank stubDatenbank;

    private Benutzer benutzer=null;

    /**
     * Bevor jede Test wird ein StubDatenbank erstellt
     * @throws RemoteException Remote Exception
     */
    @BeforeEach
    void beforEach() throws RemoteException {
        stubDatenbank = new StubDatenbank();
        benutzer=new Benutzer(stubDatenbank);
    }

    /**
     * Nach jeder Test wird erstellte Datenbank wieder auf null gesetzt.
     */
    @AfterEach
    void afterEacht()  {
        stubDatenbank = null;
        benutzer=null;
    }

    /**
     *  wir testen, ob wenn man sich registriert nicht in Lobby landet bevor man sich anmelden.
     * @throws EingabeZuLangException wenn Eingabe laenger als 20 Buchstaben ist
     * @throws LeereEingabeException wenn nichts eingegeben wird
     * @throws BenutzernameSchonVergebenException wenn Benutzername von andere Benutzer besitzt ist.
     * @throws PasswortFalschWiederholtException Wenn Passwort falsch wiederholt ist
     * @author Ghad
     */

    @Test
    void registrieren() throws EingabeZuLangException, LeereEingabeException, BenutzernameSchonVergebenException, PasswortFalschWiederholtException {
        assertThrows(PasswortFalschWiederholtException.class,()->benutzer.registrieren("name","pass","word"));
     stubDatenbank.createUser("name","password");
        ArrayList<String> names=benutzer.getOnlinePeople();
        benutzer.registrieren("AS","pas","pas");
        Assert.assertFalse(names.contains("AS"));

    }

    /**
     *  Testen, ob man auf Onlinelist steht nach der anmeldung
     * @throws BenutzernameNichtExistentException  wenn Benutzername von andere Benutzer besitzt ist.
     * @throws PasswortFalschException  wenn passwort falsch wiederholt ist
     * @throws SchonAngemeldetException wenn Benutzer sich nochmal anmelden will
     * @throws LeereEingabeException    wird geworfen wenn ein Feld leer ist
     * @author Ghad
     */
    @Test
    void onlinelisttest() throws SchonAngemeldetException, BenutzernameNichtExistentException, PasswortFalschException, LeereEingabeException {
       ArrayList<String> names=benutzer.getOnlinePeople() ;
       names.add("A");
       assertThrows(SchonAngemeldetException.class,()->benutzer.anmelden("A","pas"));
        benutzer.anmelden("AS","pas");
        Assert.assertTrue(names.contains("AS"));



    }

    /**
     * wir ueberpruefen, ob passwort nicht falsch wiederholt ist
     * @author Ghad
     */
    @Test
    void passwordfalschwiederholttest()  {
        assertThrows(PasswortFalschWiederholtException.class,()->benutzer.aendern("name1","name2","pass","word"));


    }

    /**
     * wir ueberpufen ob, wenn man sich loecht auch von List von Leute, die Online sind,geloescht wird.
     * @throws BenutzernameNichtExistentException Benutzername Nicht Existent Exception
     * @throws PasswortFalschException Passwort ist Falsch Exception
     * @throws LeereEingabeException wird geworfen wenn ein Feld leer ist
     * @author Ghad
     */
    @Test
    void loeschenvomOnlinelistTest() throws BenutzernameNichtExistentException, PasswortFalschException, LeereEingabeException {
        ArrayList<String> names=benutzer.getOnlinePeople();
        names.add("AS");
        Assert.assertTrue(names.contains("AS"));
        benutzer.loeschen("AS","pas");
        Assert.assertFalse(names.contains("AS"));

    }

    /**
     * Test ob when man sich abgemeldet hat, Name vom List der angemeldet Benutzer eliminiert wird
     * @author Ghad
     */
    @Test
    void abmelden() {

        ArrayList<String> names=benutzer.getOnlinePeople() ;
        names.add("AB");
        Assert.assertTrue(names.contains("AB"));
        benutzer.abmelden("AB");
        Assert.assertFalse(names.contains("AB"));
    }
}