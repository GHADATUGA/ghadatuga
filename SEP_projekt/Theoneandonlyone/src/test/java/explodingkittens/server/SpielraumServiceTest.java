package explodingkittens.server;

import explodingkittens.*;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Collections;

/**
 * Testklasse für den SpielraumService
 *
 * @author nico
 */
class SpielraumServiceTest {


    private SpielraumService service;

    @BeforeEach
    void setUp() throws RemoteException {
        service = new SpielraumService();
    }

    /**
     * Standardtest, der alle grundlegenden Methoden testet
     * (Erstellen, getSpielraeume, loeschen)
     *
     * @throws RemoteException           Ausnahme.
     * @throws ExplodingkittensException Ausnahme.
     */
    @Test
    void standard() throws RemoteException, ExplodingkittensException {
        SpielerInterface ersteller = new Spieler("x1");
        SpielraumInterface spielraum = service.erstellen(ersteller, "sp1");
        Collection<SpielraumInterface> raeume = service.getSpielraeume();
        Assert.assertThat(raeume, CoreMatchers.hasItems(spielraum));

        service.loeschen(ersteller, "sp1");
        raeume = service.getSpielraeume();
        Assert.assertTrue(raeume.isEmpty());
    }

    /**
     * Testet die aendern()-Methode.
     * Prüft die die NameSchonVergeben- und SpielraumNichtExistentException
     *
     * @throws RemoteException           Ausnahme.
     * @throws ExplodingkittensException Ausnahmen.
     */
    @Test
    void aendern() throws RemoteException, ExplodingkittensException {
        SpielerInterface ersteller1 = new Spieler("e1");
        SpielerInterface ersteller2 = new Spieler("e2");
        service.erstellen(ersteller1, "sp1");
        Assert.assertThat(service.getSpielarumNamen(), CoreMatchers.hasItem("sp1"));
        service.aendern(ersteller1, "sp1", "newSp1");
        Assert.assertThat(service.getSpielarumNamen(), CoreMatchers.hasItem("newSp1"));
        service.erstellen(ersteller2, "sp2");
        Assert.assertThat(service.getSpielarumNamen(), CoreMatchers.hasItems("newSp1", "sp2"));
        Assertions.assertThrows(NameSchonVergebenException.class, () -> service.aendern(ersteller2, "sp2", "newSp1"));
        Assertions.assertThrows(SpielraumNichtExistentException.class, () -> service.aendern(ersteller1, "sp1", "sp3"));
    }

    /**
     * Testet die erstellen()-Methode darauf, ob die NameSchonVergebenException korrekt geschmissen wird.
     *
     * @throws RemoteException           Ausnahme.
     * @throws ExplodingkittensException Ausnahme.
     */
    @Test
    void erstellenException() throws RemoteException, ExplodingkittensException {
        SpielerInterface ersteller1 = new Spieler("e1");
        service.erstellen(ersteller1, "sp1");
        Assertions.assertThrows(NameSchonVergebenException.class, () -> service.erstellen(ersteller1, "sp1"));
    }

    /**
     * Testet die loeschen()-Methode darauf, ob die SpielraumNichtExistent- und NichtLeerException korrekt geschmissen werden.
     *
     * @throws RemoteException           Ausnahme.
     * @throws ExplodingkittensException Ausnahme.
     * @throws SpielraumVollException    Ausnahme.
     */
    @Test
    void loeschenExceptions() throws RemoteException, ExplodingkittensException, SpielraumVollException {
        SpielerInterface ersteller1 = new Spieler("e1");
        SpielraumInterface spielraum = service.erstellen(ersteller1, "sp1");
        Assertions.assertThrows(SpielraumNichtExistentException.class, () -> service.loeschen(ersteller1, "sp2"));
        SpielerInterface spieler1 = new Spieler("s1");
        spielraum.beitreten(spieler1);
        Assertions.assertThrows(NichtLeerException.class, () -> service.loeschen(ersteller1, "sp1"));
    }

    /**
     * Testet die Getter-Methoden für die Spielräume und Spielraumnamen
     *
     * @throws RemoteException Ausnahme,wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     * @throws ExplodingkittensException Wenn Spielraum nicht gefunden werden kann.
     */
    @Test
    void getSpielraumnamenUndSpielraeume() throws RemoteException, ExplodingkittensException {
        Assert.assertThat(service.getSpielraeume(), CoreMatchers.is((Collections.emptyList())));
        SpielerInterface ersteller1 = new Spieler("e1");
        SpielraumInterface sp = service.erstellen(ersteller1, "sp1");
        Assert.assertEquals(Collections.singletonList(sp), service.getSpielraeume());
        Assert.assertThat(service.getSpielarumNamen(), CoreMatchers.is((Collections.singletonList("sp1"))));
        Assert.assertThat(service.getSpielraum("sp1"), CoreMatchers.is(sp));
    }

    /**
     * Testet, ob die Anzahl erhöht wird, wenn die Bestenliste aktualisiert wird.
     *
     */
    @Test
    void updateList() {
        Assert.assertEquals(0, service.aktualisieren());
        service.signal();
        Assert.assertEquals(1, service.aktualisieren());
    }

}