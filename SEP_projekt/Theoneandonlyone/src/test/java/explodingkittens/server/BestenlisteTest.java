package explodingkittens.server;

import explodingkittens.BenutzernameNichtExistentException;
import explodingkittens.server.datenbankTest.DatenbankServer;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * BestenlisteTest, Tests für die Bestenliste-Klasse.
 * @author Hugo, erstellt am 14.06.2020.
 */
class BestenlisteTest {
    private Bestenliste bestenlisteServer;

    @BeforeEach
    void beforeEach() throws RemoteException{
        ArrayList<ArrayList<String>> list = new ArrayList<>();
        list.add(new ArrayList<>(Arrays.asList("works", "8")));
        DatenbankServer datenbankServer = mock(DatenbankServer.class);
        when(datenbankServer.getLeaderboard()).thenReturn(list);
        bestenlisteServer = new Bestenliste(datenbankServer);

    }

    @AfterEach
    void afterEach(){
        bestenlisteServer = null;
    }

    /**
     * Testet, ob die Anzahl erhöht wird, wenn die Bestenliste aktualisiert wird.
     */
    @Test
    void addSpieler() {
        bestenlisteServer.signal();
        Assert.assertEquals(1, bestenlisteServer.aktualisieren());

        bestenlisteServer.signal();
        bestenlisteServer.signal();

        Assert.assertEquals(3, bestenlisteServer.aktualisieren());
    }


    /**
     * Testen, ob die Anzahl erhöht wird, wenn die Bestenliste aktualisiert wird.
     * @throws BenutzernameNichtExistentException Wenn der Benutzername nicht in der BestenList gespeichert ist.
     */
    @Test
    void updateList() throws BenutzernameNichtExistentException {
        String winner = "winner";

        Assert.assertEquals(0, bestenlisteServer.aktualisieren());

        bestenlisteServer.updateList(winner);
        Assert.assertEquals(1, bestenlisteServer.aktualisieren());

        bestenlisteServer.updateList(winner);
        bestenlisteServer.updateList(winner);

        Assert.assertEquals(3, bestenlisteServer.aktualisieren());
    }

    @Test
    void getList(){
        ArrayList<ArrayList<String>> list = bestenlisteServer.getList();
        Assert.assertTrue(list.contains(new ArrayList<>(Arrays.asList("works", "8"))));
    }
}