package explodingkittens.server;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;

class SpielerTest {

    @Test
    void isEquals() throws RemoteException {
        Teilnehmer teilnehmer = new Teilnehmer("Dieter");
        Spieler spieler = new Spieler("Dieter");
        Bot bot = new Bot("Dieter");

        Assert.assertThat(spieler, CoreMatchers.is(new Spieler("Dieter")));
        Assert.assertThat(new Spieler("Dieter"), CoreMatchers.not(new Spieler("Dieterxx")));
        Assert.assertThat(teilnehmer, CoreMatchers.is(spieler));
        Assert.assertThat(spieler, CoreMatchers.not(teilnehmer));

        Assert.assertThat(teilnehmer, CoreMatchers.is(bot));
        Assert.assertThat(spieler, CoreMatchers.not(bot));
        Assert.assertThat(bot, CoreMatchers.not(spieler));
    }
}