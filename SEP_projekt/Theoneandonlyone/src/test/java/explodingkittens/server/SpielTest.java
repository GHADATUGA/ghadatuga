package explodingkittens.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SpielTest {

    @Test
    void verlassen() {
        //Noch 2 Spieler, 2 Bots nach Verlassen im Spiel
        Spieler[] spieler = new Spieler[3];
        Bot[] bots = new Bot[2];
        Spieler spieler1 = mock(Spieler.class);
        Spieler spieler2 = mock(Spieler.class);
        Spieler spieler3 = mock(Spieler.class);
        spieler[0]=spieler1;
        spieler[1]=spieler2;
        spieler[2]=spieler3;
        Bot bot1 = mock(Bot.class);
        Bot bot2 = mock(Bot.class);
        bots[0]=bot1;
        bots[1]=bot2;
        Spiel spiel = new Spiel(spieler,bots);
        when(spieler1.getName()).thenReturn("spieler1");
        when(spieler2.getName()).thenReturn("spieler2");
        when(spieler3.getName()).thenReturn("spieler3");
        when(bot1.getName()).thenReturn("bot1");
        when(bot2.getName()).thenReturn("bot2");
        ArrayList ergebnis = spiel.verlassen(spieler3);
        ArrayList ergebnis_erwartet = new ArrayList(Arrays.asList(false,null));
        if (!ergebnis.equals(ergebnis_erwartet)){
            Assertions.fail("Es gibt noch keinen Gewinner - Noch 2 Spieler und 2 Bots im Spiel");
        }
        //Noch 2 Spieler, 1 Bot nach Verlassen im Spiel
        ArrayList ergebnis0 = spiel.verlassen(bot1);
        ArrayList ergebnis_erwartet0 = new ArrayList(Arrays.asList(false,null));
        if (!ergebnis0.equals(ergebnis_erwartet0)){
            Assertions.fail("Es gibt noch keinen Gewinner - Noch 2 Spieler und 1 Bots im Spiel");
        }
        //Noch 1 Spieler, 1 Bots nach Verlassen im Spiel

        ArrayList ergebnis1 = spiel.verlassen(spieler1);
        ArrayList ergebnis_erwartet1 = new ArrayList(Arrays.asList(false,null));
        if (!ergebnis1.equals(ergebnis_erwartet1)){
            Assertions.fail("Es gibt noch keinen Gewinner - Noch 1 Spieler und 2 Bots im Spiel");
        }
        //Kein Spieler, 1 Bots nach Verlassen im Spiel
        ArrayList ergebnis2 = spiel.verlassen(spieler2);
        ArrayList ergebnis_erwartet2 = new ArrayList(Arrays.asList(false,null));
        if (!ergebnis2.equals(ergebnis_erwartet2)){
            Assertions.fail("Es gibt keinen Gewinner - Noch 2 Bots im Spiel");
        }
        //Nur ein Spieler im Spiel
        Spieler[] spieler_2 = new Spieler[2];
        Spieler spieler1_2 = mock(Spieler.class);
        Spieler spieler2_2 = mock(Spieler.class);
        spieler_2[0]=spieler1_2;
        spieler_2[1]=spieler2_2;
        when(spieler1_2.getName()).thenReturn("spieler1_2");
        when(spieler2_2.getName()).thenReturn("spieler2_2");
        Bot[] bots_2 = new Bot[0];
        Spiel spiel_2 = new Spiel(spieler_2,bots_2);
        ArrayList ergebnis3 = spiel_2.verlassen(spieler1_2);
        ArrayList ergebnis_erwartet3 = new ArrayList(Arrays.asList(true,"spieler2_2"));
        if (!ergebnis3.equals(ergebnis_erwartet3)){
            Assertions.fail("Es gibt einen Gewinner, nur noch 1 Spieler im Spiel");
        }
        //Nur noch zwei Bots im Spiel
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        ArrayList ergebnis4 = spiel_3.verlassen(spieler1_3);
        ArrayList ergebnis_erwartet4 = new ArrayList(Arrays.asList(false,null));
        if (!ergebnis4.equals(ergebnis_erwartet4)){
            Assertions.fail("Es gibt keinen Gewinner, nur noch 2 Bot im Spiel");
        }
    }

    @Test
    void obersteAblagestapelkarte() {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        Spielkarte mockedSpielkarte = mock(Spielkarte.class);
        spiel_3.getAblagestapel().push(mockedSpielkarte);
        try{
            Spielkarte ergebnis_karte = spiel_3.obersteAblagestapelkarte();
            if (ergebnis_karte!=mockedSpielkarte){
                Assertions.fail("Falsche Karte zurückgegeben");
            }
        }
        catch (AblagestapelLeerException e){
            Assertions.fail("Der Ablagestapel ist nicht leer");
        }
    }


    @Test
    void getAnzahlHandkarten() {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        assert(spiel_3.getHand(bot1_3).size()==spiel_3.getAnzahlHandkarten().get(bot1_3));
    }

    @Test
    void getExplodingKittenDichte() {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        int teilnehmer = spiel_3.getReihenfolge().size();
        assert(spiel_3.getExplodingKittenDichte()==(float)(teilnehmer-1)/(float)((6 - teilnehmer) + (teilnehmer - 1) + (26 - teilnehmer * 4)));
    }



    @Test
    void getSpielzustand() {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        spiel_3.getSpielzustand();
        Spielkarte mockedSpielkarte = mock(Spielkarte.class);
        spiel_3.getAblagestapel().push(mockedSpielkarte);
        spiel_3.getSpielzustand();
    }

    @Test
    void zugBeenden() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        boolean ek_gezogen = false;
        while(!ek_gezogen){
            int spielstapelgroesse = spiel_3.getSpielstapel().getKarten().size();
            Teilnehmer next = spiel_3.getNaechsterTeilnehmer();
            ek_gezogen =spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());

            assert(spiel_3.getSpielstapel().getKarten().size()==spielstapelgroesse-1);
            if (!ek_gezogen){
                assert(spiel_3.getAktuellerTeilnehmer()==next);
            }
        }
    }

    @Test
    void blickInZukunft() {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.blickInZukunft(spiel_3.getNaechsterTeilnehmer());
            Assertions.fail("Teilnehmer ist nicht am Zug -> Exception erwartet");
        }
        catch(NichtAktuellerTeilnehmerException|KarteNichtAufHandException e){
        }
        boolean aufHand=false;
        boolean nichtaufHand =false;
        boolean genoet = false;
        boolean ungenoet = false;
        while(!aufHand || !nichtaufHand||!genoet ||!ungenoet) {
            try {
                Thread t = new NoeThread(spiel_3.getNaechsterTeilnehmer(),spiel_3);
                t.start();
                String[] ergebnis = spiel_3.blickInZukunft(spiel_3.getAktuellerTeilnehmer());
                if (ergebnis==null){
                    genoet = true;
                    assert(spiel_3.obersteAblagestapelkarte().getName()=="noe");
                }
                else{
                    ungenoet=true;
                }
                aufHand = true;

            } catch (NichtAktuellerTeilnehmerException e) {
                Assertions.fail("Teilnehmer ist an der Reihe");
            } catch (KarteNichtAufHandException e) {
                nichtaufHand = true;
            } catch (AblagestapelLeerException e) {
                e.printStackTrace();
            }
            if(spiel_3.getSpielstapel().getKarten().size()!=0) {
                try {
                    spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
                } catch (NichtAktuellerTeilnehmerException e) {
                    e.printStackTrace();
                }
            }
            else{
                break;
            }
        }
    }

    @Test
    void mischen() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.mischen(spiel_3.getNaechsterTeilnehmer());
            Assertions.fail("Teilnehmer ist nicht am Zug -> Exception erwartet");
        }
        catch(NichtAktuellerTeilnehmerException|KarteNichtAufHandException e){
        }
        boolean aufHand=false;
        boolean nichtaufHand =false;
        boolean genoet = false;
        boolean ungenoet = false;
        while(!aufHand || !nichtaufHand ||!genoet ||!ungenoet) {
            try {
                Thread t = new NoeThread(spiel_3.getNaechsterTeilnehmer(),spiel_3);
                t.start();
                try{
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Spielkartenstapel davor = spiel_3.getSpielstapel();
                spiel_3.mischen(spiel_3.getAktuellerTeilnehmer());
                if (davor.equals(spiel_3.getSpielstapel())){
                    genoet = true;
                }
                else{
                    ungenoet=true;
                }
                aufHand = true;
            } catch (NichtAktuellerTeilnehmerException e) {
                Assertions.fail("Teilnehmer ist an der Reihe");
            } catch (KarteNichtAufHandException e) {
                nichtaufHand=true;
            }
            if(spiel_3.getSpielstapel().getKarten().size()!=0) {
                spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
            }
            else{
                break;
            }
        }
    }

    @Test
    void noe() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.noe(spiel_3.getNaechsterTeilnehmer());
            Assertions.fail("Karte nicht ausspielbar");
        }
        catch(KarteNichtAufHandException | KarteNichtAusspielbarException e){
        }

        try {
                spiel_3.noe(spiel_3.getAktuellerTeilnehmer());
                Assertions.fail("Karte ist nicht ausspielbar");
            } catch (KarteNichtAusspielbarException e) {
            } catch (KarteNichtAufHandException ex) {

            }
            if(spiel_3.getSpielstapel().getKarten().size()!=0) {
                spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
            }
        }


    @Test
    void gegnerGibtKarte() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.gegnerGibtKarte(spiel_3.getNaechsterTeilnehmer(),spiel_3.getAktuellerTeilnehmer());
            Assertions.fail("Teilnehmer ist nicht am Zug -> Exception erwartet");
        }
        catch(NichtAktuellerTeilnehmerException|KarteNichtAufHandException e){
        }
        boolean aufHand=false;
        boolean nichtaufHand =false;
        boolean genoet = false;
        boolean ungenoet = false;
        while(!aufHand || !nichtaufHand ||!genoet ||!ungenoet) {
            try {
                Thread t = new NoeThread(spiel_3.getNaechsterTeilnehmer(),spiel_3);
                t.start();
                if (spiel_3.getNaechsterTeilnehmer()!=null){
                    boolean ergebnis = spiel_3.gegnerGibtKarte(spiel_3.getAktuellerTeilnehmer(),spiel_3.getNaechsterTeilnehmer());
                    if (ergebnis==false){
                        genoet = true;
                    }
                    else{

                        ungenoet=true;
                        try{
                            spiel_3.gegnerGibtKarteAb(spiel_3.getAktuellerTeilnehmer(),spiel_3.getAktuellerTeilnehmer(),spiel_3.getHand(spiel_3.getNaechsterTeilnehmer()).get(0).getName());}
                        catch (KarteNichtAufHandException e){}
                    }
                aufHand = true;}
                else{break;}
            } catch (NichtAktuellerTeilnehmerException e) {
                Assertions.fail("Teilnehmer ist an der Reihe");
            } catch (KarteNichtAufHandException e) {
                nichtaufHand=true;
            }
            if(spiel_3.getSpielstapel().getKarten().size()!=0) {
                spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
            }
            else{
                break;
            }
        }

    }

    @Test
    void gegnerGibtKarteAb() {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.gegnerGibtKarteAb(spiel_3.getNaechsterTeilnehmer(),spiel_3.getAktuellerTeilnehmer(),"wunsch");
        }

        catch(KarteNichtAufHandException ex){

        }
        try {
            spiel_3.gegnerGibtKarteAb(spiel_3.getAktuellerTeilnehmer(),spiel_3.getNaechsterTeilnehmer(),spiel_3.getHand(spiel_3.getNaechsterTeilnehmer()).get(0).getName());
            } catch (KarteNichtAufHandException e) {
                Assertions.fail("Teilnehmer hat Karte auf der Hand");
            }

        }


    @Test
    void entschaerfen_explodieren() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        boolean ek_gezogen = false;
        boolean explodiert = false;
        while(!ek_gezogen||!explodiert){
            ek_gezogen =spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
            if (ek_gezogen){
                try{
                    Teilnehmer next = spiel_3.getNaechsterTeilnehmer();
                    spiel_3.entschaerfen(spiel_3.getAktuellerTeilnehmer(),0);
                    assert(spiel_3.getAktuellerTeilnehmer()==next);
                }
                catch(PositionNichtMoeglichException e){
                    Assertions.fail("Position ist immer möglich und der Teilnehmer ist der aktuelle Teilnehmer");
                }
                catch(KarteNichtAufHandException e){
                    explodiert=true;
                    spiel_3.explodieren(spiel_3.getAktuellerTeilnehmer());
                }
            }


        }
    }

    @Test
    void explodieren() {
    }

    @Test
    void angriff() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.angriff(spiel_3.getNaechsterTeilnehmer());
            Assertions.fail("Teilnehmer ist nicht am Zug -> Exception erwartet");
        }
        catch(NichtAktuellerTeilnehmerException|KarteNichtAufHandException e){
        }
        boolean aufHand=false;
        boolean nichtaufHand =false;
        boolean genoet = false;
        boolean ungenoet = false;
        while(!aufHand || !nichtaufHand ||!genoet ||!ungenoet) {
            try {
                Thread t = new NoeThread(spiel_3.getNaechsterTeilnehmer(),spiel_3);
                t.start();
                Teilnehmer danach = spiel_3.getNaechsterTeilnehmer();
                spiel_3.angriff(spiel_3.getAktuellerTeilnehmer());
                aufHand = true;
                Integer integer = 2;
                if (spiel_3.getAktuellerTeilnehmer()==danach && spiel_3.getSpielzustand().get(1).equals(integer)){
                    ungenoet=true;
                }
                else if (spiel_3.getAktuellerTeilnehmer()!=danach){
                    genoet=true;
                }
            } catch (NichtAktuellerTeilnehmerException e) {
                Assertions.fail("Teilnehmer ist an der Reihe");
            } catch (KarteNichtAufHandException e) {
                nichtaufHand=true;
                if(spiel_3.getSpielstapel().getKarten().size()!=0) {
                    spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
                }
                else{
                    break;
                }


            }

        }

    }

    @Test
    void hops() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        try{
            spiel_3.hops(spiel_3.getNaechsterTeilnehmer());
            Assertions.fail("Teilnehmer ist nicht am Zug -> Exception erwartet");
        }
        catch(NichtAktuellerTeilnehmerException|KarteNichtAufHandException e){
        }
        boolean aufHand=false;
        boolean nichtaufHand =false;
        boolean genoet = false;
        boolean ungenoet = false;
        while(!aufHand || !nichtaufHand ||!genoet ||!ungenoet) {
            try {
                Thread t = new NoeThread(spiel_3.getNaechsterTeilnehmer(),spiel_3);
                t.start();
                Teilnehmer danach = spiel_3.getNaechsterTeilnehmer();
                spiel_3.hops(spiel_3.getAktuellerTeilnehmer());
                aufHand = true;
                if (spiel_3.getAktuellerTeilnehmer()==danach){
                    ungenoet=true;
                }
                else{
                    genoet=true;
                }
            } catch (NichtAktuellerTeilnehmerException e) {
                Assertions.fail("Teilnehmer ist an der Reihe");
            } catch (KarteNichtAufHandException e) {
                nichtaufHand=true;
            }
            if(spiel_3.getSpielstapel().getKarten().size()!=0) {
                spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
            }
            else{
                break;
            }

        }
    }

    @Test
    void modified() throws NichtAktuellerTeilnehmerException {
        Spieler[] spieler_3 = new Spieler[1];
        Spieler spieler1_3 = mock(Spieler.class);
        spieler_3[0]=spieler1_3;
        when(spieler1_3.getName()).thenReturn("spieler1_3");
        Bot[] bots_3 = new Bot[2];
        Bot bot1_3 = mock(Bot.class);
        Bot bot2_3 = mock(Bot.class);
        when(bot1_3.getName()).thenReturn("bot1_3");
        when(bot2_3.getName()).thenReturn("bot2_3");

        bots_3[0]=bot1_3;
        bots_3[1]=bot2_3;
        Spiel spiel_3= new Spiel(spieler_3,bots_3);
        assert(spiel_3.modified()>=1);
        spiel_3.zugBeenden(spiel_3.getAktuellerTeilnehmer());
        assert(spiel_3.modified()>=2);
    }

    private class NoeThread extends Thread {
        private Teilnehmer teilnehmer;
        private Spiel spiel;
        public NoeThread(Teilnehmer teilnehmer, Spiel spiel){
            this.teilnehmer = teilnehmer;
            this.spiel=spiel;
        }
        @Override
        public void run() {
            /*try{
                Thread.sleep(2000);
            }
            catch(InterruptedException e){

            }*/
            try {
                spiel.noe(teilnehmer);
            } catch (KarteNichtAusspielbarException ex) {
            } catch (KarteNichtAufHandException e2) {

            }

        }
    }
}