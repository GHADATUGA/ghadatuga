package explodingkittens.server;

import explodingkittens.server.datenbankTest.DatenbankServer;

import java.rmi.RemoteException;

/**
 * diese Klasse wird verwendet um test des Benutzer zu ermaeglichen
 */
//Wenn DB angebunden: Durch Datenbank ersetzen
public class StubDatenbank extends DatenbankServer {
    public StubDatenbank() throws RemoteException {
    }

    @Override
    public void checkUserData(String benutzername_eingegeben, String passwort_eingegeben) {

    }

    @Override
    public void changeUserName(String alter_benutzername, String neuer_benutzername){

    }

    @Override
    public void changePassword(String benutzername_eingegeben, String neues_passwort) {

    }

    @Override
    public void createUser(String benutzername_eingegeben, String passwort_eingegeben) {

    }

    @Override
    public void deleteUser(String benutzername_eingegeben){

    }

}
