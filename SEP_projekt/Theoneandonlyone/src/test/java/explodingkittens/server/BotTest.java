package explodingkittens.server;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BotTest {
    Spiel mockSpiel;
    Spieler testSpieler;
    Bot testBot;

    @BeforeEach
    void beforeEach() throws RemoteException {
        testBot = new Bot("testBot");
        testSpieler = mock(Spieler.class);
        when(testSpieler.getName()).thenReturn("testSpieler");
        mockSpiel = mock(Spiel.class);
    }
    @Test
    void initialisiereDaten() throws RemoteException {
        ArrayList<Spielkarte> mockHand = new ArrayList<>();
        mockHand.add(mock(Spielkarte.class));
        when(mockSpiel.getHand(testBot)).thenReturn(mockHand );

        Spielkartenstapel mockStapel = mock(Spielkartenstapel.class);
        ArrayList<Spielkarte> mockStapelstack = new ArrayList<>();
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        when(mockStapel.getKarten()).thenReturn(mockStapelstack);

        Hashtable<Teilnehmer, Integer> mockHandHash = new Hashtable<>();
        mockHandHash.put(testBot, 3);
        mockHandHash.put(testSpieler,4);

        when(mockSpiel.getAnzahlHandkarten()).thenReturn(mockHandHash);
        when(mockSpiel.getSpielstapel()).thenReturn(mockStapel);

        LinkedList<String> assertStapel = new LinkedList<>();
        assertStapel.add("unbekannt");
        assertStapel.add("unbekannt");
        assertStapel.add("unbekannt");

        testBot.initialisiereDaten(mockSpiel);

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel));
    }

    @Test
    void wunschWahl() throws UngueltigerSpielkartenname {
        ArrayList<Spielkarte> testHandkarten = new ArrayList<>();
        testHandkarten.add(new Spielkarte("angriff"));
        testHandkarten.add(new Spielkarte("mischen"));
        testHandkarten.add(new Spielkarte("hops"));

        testBot.setHandkarten(testHandkarten);

        Assert.assertTrue(testBot.wunschWahl(mockSpiel).getName().equals("mischen"));
    }

    @Test
    void zaehleHankartentyp() throws UngueltigerSpielkartenname {
        ArrayList<Spielkarte> testHandkarten = new ArrayList<>();
        testHandkarten.add(new Spielkarte("angriff"));
        testHandkarten.add(new Spielkarte("mischen"));
        testHandkarten.add(new Spielkarte("hops"));
        testHandkarten.add(new Spielkarte("mischen"));

        testBot.setHandkarten(testHandkarten);

        Assert.assertTrue(testBot.zaehleHandkartentyp("angriff") == 1);
        Assert.assertTrue(testBot.zaehleHandkartentyp("mischen") == 2);
        Assert.assertTrue(testBot.zaehleHandkartentyp("noe") == 0);
    }

    @Test
    void resetteStapel() {
        testBot.setBekannteSpielstapel(new LinkedList<>());

        Spielkartenstapel mockStapel = mock(Spielkartenstapel.class);
        ArrayList<Spielkarte> mockStapelstack = new ArrayList<>();
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        when(mockStapel.getKarten()).thenReturn(mockStapelstack);
        when(mockSpiel.getSpielstapel()).thenReturn(mockStapel);

        LinkedList<String> assertStapel = new LinkedList<>();
        assertStapel.add("unbekannt");
        assertStapel.add("unbekannt");
        assertStapel.add("unbekannt");

        testBot.resetteStapel(mockSpiel);

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel));
    }

    @Test
    void updateSpielstapel() {
        LinkedList<String> altstapel = new LinkedList<>();
        altstapel.add("unbekannt");
        altstapel.add("markiert");
        altstapel.add("angriff");
        testBot.setBekannteSpielstapel(altstapel);

        LinkedList<String> assertStapel = new LinkedList<>();
        assertStapel.add("unbekannt");
        assertStapel.add("markiert");

        testBot.updateSpielstapel();

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel));
    }

    @Test
    void ekZuruecklegen() throws UngueltigerSpielkartenname {
        LinkedList<String> altstapel = new LinkedList<>();
        altstapel.add("unbekannt");
        altstapel.add("markiert");
        altstapel.add("angriff");
        altstapel.add("unbekannt");
        testBot.setBekannteSpielstapel(altstapel);

        ArrayList<Spielkarte> testHandkarten = new ArrayList<>();
        testHandkarten.add(new Spielkarte("angriff"));
        testHandkarten.add(new Spielkarte("mischen"));
        testHandkarten.add(new Spielkarte("hops"));
        testHandkarten.add(new Spielkarte("mischen"));
        testBot.setHandkarten(testHandkarten);

        Spielkartenstapel mockStapel = mock(Spielkartenstapel.class);
        ArrayList<Spielkarte> mockStapelstack = new ArrayList<>();
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        when(mockStapel.getKarten()).thenReturn(mockStapelstack);

        Hashtable<Teilnehmer, Integer> mockHandHash = new Hashtable<>();
        mockHandHash.put(testBot, 4);
        mockHandHash.put(testSpieler,4);

        when(mockSpiel.getAnzahlHandkarten()).thenReturn(mockHandHash);
        when(mockSpiel.getSpielstapel()).thenReturn(mockStapel);

        testBot.updateSpielstapel();
        int platz = testBot.ekZuruecklegen(mockSpiel);

        LinkedList<String> assertStapel = new LinkedList<>();
        assertStapel.add("exploding_kitten");
        assertStapel.add("unbekannt");
        assertStapel.add("markiert");
        assertStapel.add("angriff");

        Assert.assertTrue(platz== 0);
        Assert.assertTrue(testBot.getBekannteSpielstapel().size()==4);
        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel));
    }


    @Test
    void zugAusfuehren() throws UngueltigerSpielkartenname {
        LinkedList<String> altstapel = new LinkedList<>();
        altstapel.add("unbekannt");
        altstapel.add("markiert");
        altstapel.add("angriff");
        altstapel.add("wunsch");
        testBot.setBekannteSpielstapel(altstapel);

        ArrayList<Spielkarte> testHandkarten = new ArrayList<>();
        testHandkarten.add(new Spielkarte("angriff"));
        testHandkarten.add(new Spielkarte("mischen"));
        testHandkarten.add(new Spielkarte("hops"));
        testHandkarten.add(new Spielkarte("mischen"));
        testBot.setHandkarten(testHandkarten);

        Spielkartenstapel mockStapel = mock(Spielkartenstapel.class);
        ArrayList<Spielkarte> mockStapelstack = new ArrayList<>();
        mockStapelstack.add(new Spielkarte("angriff"));
        mockStapelstack.add(new Spielkarte("hops"));
        mockStapelstack.add(new Spielkarte("angriff"));
        mockStapelstack.add(new Spielkarte("wunsch"));
        when(mockStapel.getKarten()).thenReturn(mockStapelstack);

        Hashtable<Teilnehmer, Integer> mockHandHash = new Hashtable<>();
        mockHandHash.put(testBot, 4);
        mockHandHash.put(testSpieler,4);

        when(mockSpiel.getAktuellerTeilnehmer()).thenReturn(testBot);
        when(mockSpiel.getNaechsterTeilnehmer()).thenReturn(testSpieler);
        when(mockSpiel.getAnzahlHandkarten()).thenReturn(mockHandHash);
        when(mockSpiel.getSpielstapel()).thenReturn(mockStapel);
        testBot.zugAusfuehren(mockSpiel);
        testBot.getHandkarten().add(new Spielkarte("wunsch"));

        ArrayList<Spielkarte> assertHand = new ArrayList<>();
        assertHand.add(new Spielkarte("angriff"));
        assertHand.add(new Spielkarte("mischen"));
        assertHand.add(new Spielkarte("hops"));
        assertHand.add(new Spielkarte("mischen"));
        assertHand.add(new Spielkarte("wunsch"));

        Assert.assertTrue(testBot.getHandkarten().get(4).getName().equals(assertHand.get(4).getName()));
    }

    @Test
    void reagieren() throws UngueltigerSpielkartenname {
        LinkedList<String> altstapel = new LinkedList<>();
        altstapel.add("unbekannt");
        altstapel.add("unbekannt");
        altstapel.add("angriff");
        altstapel.add("unbekannt");
        testBot.setBekannteSpielstapel(altstapel);

        Spielkartenstapel mockStapel = mock(Spielkartenstapel.class);
        ArrayList<Spielkarte> mockStapelstack = new ArrayList<>();
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        when(mockStapel.getKarten()).thenReturn(mockStapelstack);
        when(mockSpiel.getSpielstapel()).thenReturn(mockStapel);
        when(mockSpiel.getAktuellerTeilnehmer()).thenReturn(testBot);
        when(mockSpiel.getNaechsterTeilnehmer()).thenReturn(testSpieler);

        ArrayList<Spielkarte> testHandkarten = new ArrayList<>();
        testHandkarten.add(new Spielkarte("angriff"));
        testHandkarten.add(new Spielkarte("mischen"));
        testHandkarten.add(new Spielkarte("hops"));
        testHandkarten.add(new Spielkarte("mischen"));
        testBot.setHandkarten(testHandkarten);

        testBot.reagieren(new Spielkarte("blick_in_die_zukunft"), mockSpiel);

        LinkedList<String> assertStapel = new LinkedList<>();
        assertStapel.add("unbekannt");
        assertStapel.add("markiert");
        assertStapel.add("angriff");
        assertStapel.add("markiert");


        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel));

        testBot.reagieren(new Spielkarte("mischen"), mockSpiel);

        LinkedList<String> assertStapel2 = new LinkedList<>();
        assertStapel2.add("unbekannt");
        assertStapel2.add("unbekannt");
        assertStapel2.add("unbekannt");

        for(String k : testBot.getBekannteSpielstapel()){
            System.out.println(k);
        }

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel2));

        testBot.reagieren(new Spielkarte("hops"), mockSpiel);

        LinkedList<String> assertStapel3 = new LinkedList<>();
        assertStapel3.add("unbekannt");
        assertStapel3.add("unbekannt");
        assertStapel3.add("unbekannt");

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel3));

        testBot.reagieren(new Spielkarte("angriff"), mockSpiel);

        LinkedList<String> assertStapel4 = new LinkedList<>();
        assertStapel4.add("unbekannt");
        assertStapel4.add("unbekannt");
        assertStapel4.add("unbekannt");

        for(String k : testBot.getBekannteSpielstapel()){
            System.out.println(k);
        }

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel4));
    }

    @Test
    void reagieren2() throws UngueltigerSpielkartenname {
        LinkedList<String> altstapel = new LinkedList<>();
        altstapel.add("unbekannt");
        altstapel.add("unbekannt");
        altstapel.add("angriff");
        altstapel.add("unbekannt");
        testBot.setBekannteSpielstapel(altstapel);

        Spielkartenstapel mockStapel = mock(Spielkartenstapel.class);
        ArrayList<Spielkarte> mockStapelstack = new ArrayList<>();
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        mockStapelstack.add(mock(Spielkarte.class));
        when(mockStapel.getKarten()).thenReturn(mockStapelstack);
        when(mockSpiel.getSpielstapel()).thenReturn(mockStapel);

        ArrayList<Spielkarte> testHandkarten = new ArrayList<>();
        testHandkarten.add(new Spielkarte("angriff"));
        testHandkarten.add(new Spielkarte("mischen"));
        testHandkarten.add(new Spielkarte("hops"));
        testHandkarten.add(new Spielkarte("mischen"));
        testBot.setHandkarten(testHandkarten);

        testBot.reagieren(new Spielkarte("entschaerfung"), mockSpiel);

        LinkedList<String> assertStapel = new LinkedList<>();
        assertStapel.add("markiert");
        assertStapel.add("markiert");
        assertStapel.add("markiert");

        Assert.assertTrue(testBot.getBekannteSpielstapel().equals(assertStapel));

    }
}
