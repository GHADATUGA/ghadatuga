package explodingkittens.server;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.net.BindException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

/**
 * MainServerTest, Testet die MainServer Klasse.
 *
 * @author Hugo
 */
class MainServerTest {
    /**
     * testet, ob Remote-Objekte an die Registry gebunden sind.
     * @throws AlreadyBoundException Ausnahme, die ausgelöst wird, wenn die Registry schon in Verbindung ist.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @Test
    void mainTest() throws AlreadyBoundException, RemoteException {
        int count = 0;
        MainServer.main(null);
        Registry registry = LocateRegistry.getRegistry(8014);
        String[] objects = registry.list();

        Assert.assertThat(Arrays.asList(objects), CoreMatchers.hasItems("benutzer", "spielraumService", "chat"));

    }
}