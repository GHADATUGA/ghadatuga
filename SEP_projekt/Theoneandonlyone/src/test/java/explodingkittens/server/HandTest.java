package explodingkittens.server;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

/**
 * Testklasse der Hand_Klasse.
 * @author Hugo, erstellt am 15.06.2020
 */
class HandTest {
    private Hand hand;
    @BeforeEach
    void beforeEach(){
        Teilnehmer teilnehmer = mock(Teilnehmer.class);
        hand = new Hand(teilnehmer);
        assert(hand.getBesitzer()==teilnehmer);
    }
    /**
     * testet, ob der Hand eine Karte hinzugefügt wurde.
     */
    @Test
    void addKarte(){
        Spielkarte spielkarte1 = mock(Spielkarte.class);
        hand.addKarte(spielkarte1);
        Assert.assertFalse(hand.getHand().isEmpty());

    }

    /**
     * testet, ob eine Karte von der Hand abgezogen wird.
     */
    @Test
    void subKarte() {
        Spielkarte spielkarte1 = mock(Spielkarte.class);
        hand.addKarte(spielkarte1);//added
        hand.subKarte(spielkarte1);//substracted
        Assert.assertTrue(hand.getHand().isEmpty());
    }

}