package explodingkittens.server;

import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testet die Klasse SpielerService
 * @author Desiree
 */
class SpielerServiceTest {

    @Test
    void create() {
        try{
            SpielerService spielerService = new SpielerService();
            assert(spielerService.create("test").getName()=="test");
        }
        catch (RemoteException e){}
    }
}