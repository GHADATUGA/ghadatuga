package explodingkittens.server;

import explodingkittens.NachrichtZuLangException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.rmi.RemoteException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

/**
 * chat Test, Tests für die Chat-Klasse.
 * @author Hugo, erstellt am 05.06.2020.
 */
class ChatTest {
    private Chat chat;
    @BeforeEach
    void beforeEach() throws RemoteException{
        chat = new Chat();
    }

    @AfterEach
    void afterEach(){
        chat = null;
    }


    /**
     * Testet, ob Nachrichten von der Lobby als Einstiegspunkt gesendet werden können
     * und die Liste der Nachrichten in der Lobby bei der Initialisierung des Chatservers
     * auf dem Server verfügbar sein muss.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void messageInLobby() throws NachrichtZuLangException {
        String text = "hello world";
        String location = "Lobby";
        chat.verfassen(location,text);
        ArrayList<String> list = chat.getNachrichten(location);
        //message found; Location: Lobby set at initialization
        Assert.assertTrue(list.contains(text));
    }

    /**
     * Testet, ob Ausnahme ausgelöst ist, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void verfassenNachrichtZuLang() {
        String location = "Lobby";
        String text = "Niba wifuza kuvugana natwe, " +
                "kuduha inyunganizi ku makuru tubagezaho " +
                "cyangwa ukaba ufite inkuru idasanzwe ndetse " +
                "n'ubuhamya wifuza gusangiza Abanyarwanda " +
                "waduhamagara kuri ";
        assertThrows(NachrichtZuLangException.class, () -> chat.verfassen(location,text));
    }

    /**
     * testet, dass eine Nachricht nicht an einen nicht vorhandenen Speicherort/Spielraum gesendet wird.
     */
    @Test
    void messageInSpielraumNotAdded() {

        assertThrows(NullPointerException.class, () -> {
            String location = "SpielraumA";
            String text = "Hello Mold";
            chat.verfassen(location, text);
        });
    }

    /**
     * Die Nachricht kann an einen vorhandenen Spielraum gesendet werden.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void messageInSpielraumAdded() throws NachrichtZuLangException {
        String location = "SpielraumA";
        String text = "Hello Mold";

        chat.erstellNachrichtenLocation(location); //create Location
        chat.verfassen(location, text);
        ArrayList<String> list2 = chat.getNachrichten(location);
        //message found; location already set
        Assert.assertTrue(list2.contains(text));
    }

    /**
     * testet, ob ein Benutzer zur Benutzerliste hinzugefügt werden kann.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void messageAddUsername() throws NachrichtZuLangException {
        String location = "spielerList";
        String username = "Nico";
        chat.verfassen(location, username);
        ArrayList<String> list3 = chat.getNachrichten(location);
        //username found; username added successfully; location set at initialization
        Assert.assertTrue(list3.contains(username));
    }

    /**
     * testet, ob ein Benutzer mehrmal zur Benutzerliste nicht hinzugefügt werden kann.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void messageAddUsernameManyTimes() throws NachrichtZuLangException{
        String location = "spielerList";
        String username = "Nico";
        int count = 0;
        chat.verfassen(location, username);
        chat.verfassen(location, username);// adding user twice
        chat.verfassen(location, username);// adding user three times
        ArrayList<String> list3 = chat.getNachrichten(location);
        for (String usernam : list3){
            if(usernam.equalsIgnoreCase(username)){
                count = count + 1;
            }
        }
        //username found; username added successfully; location set at initialization
        Assert.assertEquals(1,count);
    }

    /**
     * testet, ob die Liste der Nachrichten aus der Lobby in der Lobby abgerufen werden kann.
     */
    @Test
    void getNachrichtenInLobby() {
        String location = "Lobby";
        ArrayList<String> list1 = chat.getNachrichten(location);
        Assert.assertNotNull(list1);//list created at initialization
    }

    /**
     * Abrufen der Nachrichtenliste an einem nicht vorhandenen Ort.
     */
    @Test
    void getNachrichtenLocationNotFound() {
        //list not found; location not created
        assertThrows(NullPointerException.class, () -> {
            String location = "SpielraumB";
            chat.getNachrichten(location);
        });

    }

    /**
     * Abrufen der Nachrichtenliste an einem vorhandenen Ort.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void getNachrichtenInSpielraum() throws NachrichtZuLangException {
        String location = "SpielraumB";
        String text = "Hello Test";
        chat.erstellNachrichtenLocation(location);
        chat.verfassen(location,text);
        ArrayList<String> list2 = chat.getNachrichten(location);
        //message found at location; location first created
        Assert.assertTrue(list2.contains(text));
    }

    /**
     * Löschen der Nachrichtenliste an einem vorhandenen Speicherort.
     */
    @Test
    void nachrichtenLoeschenInLobby() {
        String location = "Lobby";
        chat.nachrichtenLoeschen(location, null);
        //list deleted; location created at initialization
        assertThrows(NullPointerException.class, () -> {
            String location1 = "Lobby";
            chat.getNachrichten(location1);
        });
    }

    /**
     * Löschen der Nachrichtenliste an einem nicht vorhandenen Speicherort.
     */
    @Test
    void nachrichtenLoeschenInSpielraumLocationNotFound() {
        //list not deleted; location not found
        assertThrows(NullPointerException.class, () -> {
            String location = "SpielraumA";
            chat.nachrichtenLoeschen(location, null);
        });
    }

    /**
     * Entfernen eines Benutzers aus der Benutzerliste.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void benutzernameLoeschenInSpielerList() throws NachrichtZuLangException {
        String location = "spielerList";
        String username1 = "username1";
        String username2 = "uusername2";
        chat.verfassen(location,username1);
        chat.verfassen(location, username2);
        chat.nachrichtenLoeschen(location,username1);
        ArrayList<String> list3 = chat.getNachrichten(location);
        Assert.assertTrue(list3.contains(username2)); // contains username2
        Assert.assertFalse(list3.contains(username1)); // username2 not found; username2 deleted
    }

    /**
     * Erstellen eines Standorts und einer Liste von Nachrichten an diesem Standort.
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void erstellNachrichtenLocation() throws NachrichtZuLangException {
        String location = "SpielraumA";
        String text = "Hello Rwanda";
        chat.erstellNachrichtenLocation(location);
        chat.verfassen(location, text);
        ArrayList<String > list4 = chat.getNachrichten(location);
        Assert.assertTrue(list4.contains(text));//location created; message found at location
    }

    /**
     * prüft, ob die Daten auf dem Server aktualisiert wurden.
     *
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void aktualisierenLocation() throws NachrichtZuLangException {
        assertThrows(NullPointerException.class, () -> {
            String location = "SpielraumX";
            chat.aktualisieren(location);
        });

        String location = "SpielraumY";
        chat.erstellNachrichtenLocation(location);
        int akual = chat.aktualisieren(location);
        Assert.assertEquals(0, akual);

        location = "SpielraumF";
        String text1 = "Hello World";
        String text2 = "Go Gaga";
        chat.erstellNachrichtenLocation(location);
        chat.verfassen(location,text1);
        chat.verfassen(location, text2);
        akual = chat.aktualisieren(location);
        Assert.assertEquals(2, akual);

    }

    /**
     * prüft, ob die Daten auf dem Server aktualisiert wurden.
     *
     * @throws NachrichtZuLangException Ausnahme ausgelöst, wenn die Nachricht mehr als 80 Zeichen enthält.
     */
    @Test
    void istaAktualisierenSpielerList() throws NachrichtZuLangException {
        String location = "spielerList";
        int akual = chat.aktualisieren(location);
        Assert.assertEquals(0, akual);

        String username1 = "David";
        String username2 = "Beyonce";
        chat.verfassen(location,username1);
        chat.verfassen(location, username2);
        akual = chat.aktualisieren(location);
        Assert.assertEquals(2, akual);

        String username3 = "Dj";
        chat.verfassen(location,username3);
        akual = chat.aktualisieren(location);
        Assert.assertEquals(3, akual);

    }
}