package explodingkittens.server;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SpielkartenstapelTest {
    private static Spielkartenstapel spielstapel;
    private static  Spielkarte mockedSpielkarte1;
    private static Spielkarte mockedSpielkarte2;

    @BeforeAll
    private static void first(){
        mockedSpielkarte1 = mock(Spielkarte.class);
        mockedSpielkarte2 = mock(Spielkarte.class);
        Spielkarte[] spielkarten = {mockedSpielkarte2,mockedSpielkarte1};
        spielstapel= new Spielkartenstapel(spielkarten);
    }

    @Test
    @Order(1)
    void manipulate(){
        Spielkarte mockedSpielkarte3 = mock(Spielkarte.class);
        spielstapel.push(mockedSpielkarte3);
        Spielkarte ergebnis = spielstapel.top();
        Spielkarte ergebnis2 = spielstapel.top();
        //Top-Karte ist immer noch die gleiche
        assertTrue(ergebnis == ergebnis2);
        assertEquals(ergebnis, mockedSpielkarte3);

        Spielkarte ergebnis3 = spielstapel.pop();
        Spielkarte ergebnis4 = spielstapel.pop();
        Spielkarte ergebnis5 = spielstapel.pop();
        assertFalse((ergebnis3==ergebnis4) && (ergebnis4==ergebnis5));

    }

    @Test
    @Order(2)
    void getStapel() {
        spielstapel.push(mockedSpielkarte1);
        spielstapel.push(mockedSpielkarte2);
        ArrayList ergebnis_list2 = spielstapel.getKarten();
        ArrayList compare_list = new ArrayList<>(Arrays.asList(mockedSpielkarte2,mockedSpielkarte1));
        assertTrue(ergebnis_list2.equals(compare_list));
        spielstapel.pop();
        spielstapel.pop();
        ArrayList ergebnis_list = spielstapel.getKarten();
        assert (ergebnis_list.isEmpty());

    }
    @Test
    @Order(3)
    void mischen(){
        spielstapel.push(mockedSpielkarte1);
        spielstapel.push(mockedSpielkarte2);
        Spielkarte mockedSpielkarte3 = mock(Spielkarte.class);
        spielstapel.push(mockedSpielkarte3);
        ArrayList initial = spielstapel.getKarten();
        for (int i=0; i<5; i++){
            spielstapel.mischen();
            if (spielstapel.getKarten().equals(initial)==false){
                break;
            }
            if (i==4){
                Assertions.fail("Fehler: Nach dem 5. Mischen sind die Karten im noch in der initialen Reihenfolge");
            }
        }

    }
    @Order(4)
    @Test
    void insert(){
        spielstapel.push(mockedSpielkarte1);
        spielstapel.push(mockedSpielkarte2);
        //Karte an g�ltige Position(5) einf�gen -> oberste Position
        Spielkarte mockedSpielkarte3 = mock(Spielkarte.class);
        try{
            spielstapel.insert(mockedSpielkarte3, 5);
        }
        catch(PositionNichtMoeglichException e){
            Assertions.fail("Fehler: Die Position ist g�ltig.");
        }
        //Karte an g�ltige Position(1) einf�gen -> mittlere Position
        Spielkarte mockedSpielkarte5 = mock(Spielkarte.class);
        try{
            spielstapel.insert(mockedSpielkarte5, 1);
        }
        catch(PositionNichtMoeglichException e){
            Assertions.fail("Fehler: Die Position ist g�ltig.");
        }

        //Karte an ung�ltiger Position(8) einf�gen -> Kartenstapel = 7 Karten
        Spielkarte mockedSpielkarte4 = mock(Spielkarte.class);
        try{
            spielstapel.insert(mockedSpielkarte4, 8);
            Assertions.fail("Fehler: Die Position ist nicht g�ltig.");
        }
        catch(PositionNichtMoeglichException e){
        }
    }
    @Test
    void otherTesst() throws PositionNichtMoeglichException {
        mockedSpielkarte1 = mock(Spielkarte.class);
        mockedSpielkarte2 = mock(Spielkarte.class);
        Spielkarte mockedSpielkarte3 = mock(Spielkarte.class);
        Spielkarte[] spielkarten = {mockedSpielkarte3,mockedSpielkarte2,mockedSpielkarte1};
        spielstapel= new Spielkartenstapel(spielkarten);
        Spielkarte mockedSpielkarte4 = mock(Spielkarte.class);
        spielstapel.insert(mockedSpielkarte4,2);
        System.out.println(mockedSpielkarte4);
        System.out.println(spielstapel.getKarten());


    }
}


/*
public void insert (Spielkarte karte, int position) throws PositionNichtMoeglichException{
        if (karten.size()>position){
            throw new PositionNichtMoeglichException();
        }
        else{
            Stack<Spielkarte> ablage = new Stack<>();
            for (int i=karten.size();i>position;i++){
                ablage.push(karten.pop());
            }
            karten.push(karte);
            for (int i=0; i<ablage.size();i++){
                karten.push(ablage.pop());
            }
        }
 */



