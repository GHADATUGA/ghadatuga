package explodingkittens.server;


import explodingkittens.*;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DatenbankTest{
    Datenbank db= new Datenbank("test_db");


    @BeforeClass
    public static void test_db_connection(){

        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl =
                    "jdbc:sqlserver://DESKTOP-H3N658L;"
                            + "databaseName=" + "test_db"
                            + ";user=sa;"
                            + "password=sa;";

            Connection connection = DriverManager.getConnection(connectionUrl);

        }
        catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
            Assertions.fail("Fehler: Verbindung zur Datenbank fehlgeschlagen");
        }

    }
    @Test
    public void checkUserName(){

        //existierender Benutzername
        try{
            db.checkUserName("desiree");
            Assertions.fail("Fehler: Benutzer existiert schon");
        }
        catch (BenutzernameSchonVergebenException e){ }

        //nicht-existierender Benutzername (21 Zeichen -> kann wegen maxChar nicht in Datenbank gespeichert werden)
        try{
            db.checkUserName("123456789123456789123");
        }
        catch (BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der Benutzername existiert noch nicht in der Datenbank");
        }
    }
    @Test
    void checkUserData() {
        //Existierender Benutzername mit richtigem Passwort
        try{
            db.checkUserData("desiree","mypassword");
        }
        catch (BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der Benutzername existiert in der Datenbank");
        }
        catch(PasswortFalschException e){
            Assertions.fail("Fehler: Das Passwort ist richtig");
        }

        //Existierender Benutzername mit falschem Passwort
        try{
            db.checkUserData("desiree","notmypassword");
            Assertions.fail("Fehler: Das Passwort ist falsch");
        }
        catch (BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der Benutzername existiert in der Datenbank");
        }
        catch(PasswortFalschException e){ }

        //Nicht-existierender Benutzername
        try{
            db.checkUserData("idontexist","mypassword");
            Assertions.fail("Fehler: Der Benutzername existiert nicht in der Datenbank");
        }
        catch (BenutzernameNichtExistentException e){

        }
        catch(PasswortFalschException e){
            Assertions.fail("Fehler: Der Benutzername existiert nicht in der Datenbank. Daher keine Passwortprüfung");
        }
    }
    @Test
    void getLeaderboard(){
        //teste, ob der Ausgabetyp richtig ist
        assertTrue(db.getLeaderboard() instanceof ArrayList);
    }
    @Test
    void changeUserName(){
        //alter Benutzername existent, neuer Benutzername ist schon vergeben und nicht gleich dem alten Benutzernamen
        try{
            db.changeUserName("desiree", "admin");
            try{
                db.changeUserName("admin","desiree");
            }
            catch(BenutzernameNichtExistentException|BenutzernameSchonVergebenException| EingabeZuLangException | LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht leer");
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der angegebene alte Benutzername existiert in der Datenbank");
        }


        //alter Benutzername existent, neuer Benutzername entspricht altem
        try{
            db.changeUserName("desiree", "desiree");
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der neue Benutzername entspricht dem alten. Das ist ok und erlaubt");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht leer");
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der angegebene alte Benutzername existiert in der Datenbank");
        }

        //alter Benutzername existent, neuer Benutzername erfüllt Vorgaben (einmal Länge = 1, einmal Länge = 20)
        try{
            //neuer Benutzername der Länge 1
            db.changeUserName("desiree", "a");
            try{
                db.changeUserName("a","desiree");
            }
            catch(BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException|BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
            //neuer Benutzername der Länge 20
            db.changeUserName("desiree", "12345678912345678912");
            try{
                db.changeUserName("12345678912345678912","desiree");
            }
            catch(BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException|BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der neue Benutzername ist noch nicht vergeben");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht leer");
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der angegebene alte Benutzername existiert in der Datenbank");
        }

        //alter Benutzername existent, neuer Benutzername leer
        try{
            db.changeUserName("desiree", "");
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist leer. Achtung: Testfälle können dadurch fälschlicherweise fehlschlagen.");
            try{
                db.changeUserName("","desiree");
            }
            catch(BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException|BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der neue Benutzername ist noch nicht vergeben");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht zu lang");
        }
        catch(LeereEingabeException e){

        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der angegebene alte Benutzername existiert in der Datenbank");
        }

        //alter Benutzername existent, neuer Benutzername hat mehr als 20 Zeichen
        try{
            db.changeUserName("desiree", "123456789123456789123");
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist zu lang. Achtung: Testfälle können dadurch fälschlicherweise fehlschlagen.");
            try{
                db.changeUserName("123456789123456789123","desiree");
            }
            catch(BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException|BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der neue Benutzername ist noch nicht vergeben");
        }
        catch(EingabeZuLangException e){

        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht leer");
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der angegebene alte Benutzername existiert in der Datenbank");
        }

        //alter Benutzername nicht existent
        try{
            db.changeUserName("idontexist", "12345678912345678912");
            Assertions.fail("Fehler: Der angegebene alte Benutzername existiert nicht in der Datenbank. Achtung: Testfälle können dadurch fälschlicherweise fehlschlagen.");
            try{
                db.changeUserName("12345678912345678912","iontexist");
            }
            catch(BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException|BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der neue Benutzername ist noch nicht vergeben");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Der gewünschte neue Benutzername ist nicht leer");
        }
        catch(BenutzernameNichtExistentException e){

        }
    }
    @Test
    void deleteUser(){
        //Benutzer existiert in Datenbank
        try{
            db.deleteUser("desiree");
            try{
                db.createUser("desiree","mypassword");
            }
            catch(BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch (BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der angegebene Benutzer existiert in der Datenbank");
        }

        //Benutzer existiert nicht in Datenbank -> sichergestellt, da Benutzername länger als die max. 20 zeichen ist

        try{
            db.deleteUser("123456789123456789123");
            Assertions.fail("Fehler: Benutzer existiert nicht in der Datenbank");
        }
        catch(BenutzernameNichtExistentException e){}

    }

    @Test
    void updateLeaderboard(){
        //Benutzer existiert in Datenbank
        try{
            db.updateLeaderboard("desiree");
        }
        catch (BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der Benutzername existiert in der Datenbank");
        }

        //Benutzer existiert nicht in Datenbank -> sichergestellt, da Benutzername länger als die max. 20 zeichen ist
        try{
            db.updateLeaderboard("123456789123456789123");
            Assertions.fail("Fehler: Benutzer existiert nicht in der Datenbank");
        }
        catch(BenutzernameNichtExistentException e){}
    }

    @Test
    void changePassword(){
        //Benutzername existent, Passwort erfüllt Vorgaben
        try{
            //Passwort der Länge 1
            db.changePassword("desiree", "1");
            try{
                db.changePassword("desiree","mypassword");
            }
            catch(BenutzernameNichtExistentException|EingabeZuLangException|LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
            //Passwort der Länge 20
            db.changePassword("desiree","12345678912345678912");
            try{
                db.changePassword("desiree","mypassword");
            }
            catch(BenutzernameNichtExistentException|EingabeZuLangException|LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der Benutzername existiert in der Datenbank");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist nicht leer");
        }

        //Benutzername existent, Passwort leer
        try{
            db.changePassword("desiree", "");
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist leer");
            try{
                db.changePassword("desiree","mypassword");
            }
            catch(BenutzernameNichtExistentException|EingabeZuLangException|LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der Benutzername existiert in der Datenbank");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist nicht zu lang");
        }
        catch(LeereEingabeException e){ }

        //Benutzername existent, Passwort zu lang (mehr als 20 Zeichen)
        try{
            db.changePassword("desiree", "123456789123456789123");
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist zu lang");
            try{
                db.changePassword("desiree","mypassword");
            }
            catch(BenutzernameNichtExistentException|EingabeZuLangException|LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameNichtExistentException e){
            Assertions.fail("Fehler: Der Benutzername existiert in der Datenbank");
        }
        catch(EingabeZuLangException e){

        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist nicht leer");
        }

        //Benutzername nicht existent -> sichergestellt, da Benutzername länger als die max. 20 zeichen ist
        try{
            db.changePassword("123456789123456789123", "a");
            Assertions.fail("Fehler: Der angegebene Benutzer existiert nicht");
        }
        catch(BenutzernameNichtExistentException e){

        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Das gewünschte neue Passwort ist nicht leer");
        }
    }

    @Test
    void createUser(){
        //Benutzername ist noch nicht vergeben, Passwort und Benutzername erfüllen Vorgaben
        try{
            db.createUser("notexistent","valid_password");
            try{
                db.deleteUser("notexistent");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch (BenutzernameSchonVergebenException|EingabeZuLangException|LeereEingabeException e){
            Assertions.fail("Fehler: Der angegebene Benutzer existiert noch nicht und das Passwort ist valide");
        }
        //Benutzername ist schon vergeben
        try{
            db.createUser("desiree","smth");
            Assertions.fail("Fehler: Der angegebene Benutzername existiert schon");
            try{
                db.changePassword("desiree","mypassword");
            }
            catch(BenutzernameNichtExistentException|EingabeZuLangException|LeereEingabeException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){}
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Das gewünschte Passwort und der gewünschte Benutzername sind nicht zu lang");
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Das gewünschte Passwort und der gewünschte Benutzername sind nicht leer");
        }

        //Benutzername ist noch nicht vergeben, aber der Benutzername und/oder das Passwort sind leer
        try{
            db.createUser("","smth");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind nicht leer");
            try{
                db.deleteUser("");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
            db.createUser("idontexist","");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind nicht leer");
            try{
                db.deleteUser("idontexist");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
            db.createUser("","");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind nicht leer");
            try{
                db.deleteUser("");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der angegebene Benutzername noch nicht");
        }
        catch(EingabeZuLangException e){
            Assertions.fail("Fehler: Das gewünschte Passwort und der gewünschte Benutzername sind nicht zu lang");
        }
        catch(LeereEingabeException e){
        }

        //Benutzername ist noch nicht vergeben, aber der Benutzername und/oder das Passwort sind zu lang
        try{
            db.createUser("idontexist","123456789123456789123");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind nicht leer");
            try{
                db.deleteUser("idontexist");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }


            db.createUser("123456789123456789123","ok");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind nicht leer");
            try{
                db.deleteUser("123456789123456789123");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }


            db.createUser("123456789123456789123","123456789123456789123");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind zu lang");
            try{
                db.deleteUser("123456789123456789123");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }

        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der angegebene Benutzername noch nicht");
        }
        catch(EingabeZuLangException e){
        }
        catch(LeereEingabeException e){
            Assertions.fail("Fehler: Das gewünschte Passwort und der gewünschte Benutzername sind nicht leer");
        }

        //Benutzername ist noch nicht vergeben, aber der Benutzername und das Passwort sind zu lang oder leer
        try{
            db.createUser("","123456789123456789123");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind zu lang oder leer");
            try{
                db.deleteUser("");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
            db.createUser("123456789123456789123","");
            Assertions.fail("Fehler: Das gewünschte Passwort und/oder der gewünschte Benutzername sind zu lang oder leer");
            try{
                db.deleteUser("");
            }
            catch(BenutzernameNichtExistentException e){
                Assertions.fail("Fehler: Achtung Testfälle könnten dadurch fälschlicherweise fehlschlagen");
            }
        }
        catch(BenutzernameSchonVergebenException e){
            Assertions.fail("Fehler: Der angegebene Benutzername noch nicht");
        }
        catch(EingabeZuLangException|LeereEingabeException e){
        }

    }
    @Test
    public void testConstructor2(){
        assert(new Datenbank().datenbankName == "ek_produktiv");
    }



}
