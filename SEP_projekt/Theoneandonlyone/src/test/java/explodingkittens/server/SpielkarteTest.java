package explodingkittens.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SpielkarteTest {
    @Test
    void initialisieren(){
        try{
            Spielkarte spielkarte_ek= new Spielkarte("exploding_kitten");
            Spielkarte spielkarte_entschaerfung = new Spielkarte ("entschaerfung");
            Spielkarte spielkarte_noe = new Spielkarte("noe");
            Spielkarte spielkarte_angriff = new Spielkarte("angriff");
            Spielkarte spielkarte_hops= new Spielkarte("hops");
            Spielkarte spielkarte_wunsch = new Spielkarte("wunsch");
            Spielkarte spielkarte_mischen = new Spielkarte("mischen");
            Spielkarte spielkarte_bidz = new Spielkarte("blick_in_die_zukunft");
        }
        catch (UngueltigerSpielkartenname e){
            Assertions.fail("Spielkartennamen sind gültig");
        }
        try{
            Spielkarte spielkarte_ungueltig = new Spielkarte("something");
            Assertions.fail("Spielkartenname ist ungültig");
        }
        catch (UngueltigerSpielkartenname e){
        }
    }
    @Test
    void getName() throws UngueltigerSpielkartenname {
        Spielkarte spielkarte_ek= new Spielkarte("exploding_kitten");
        if (spielkarte_ek.getName()!="exploding_kitten"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_entschaerfung = new Spielkarte ("entschaerfung");
        if (spielkarte_entschaerfung.getName()!="entschaerfung"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_noe = new Spielkarte("noe");
        if (spielkarte_noe.getName()!="noe"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_angriff = new Spielkarte("angriff");
        if (spielkarte_angriff.getName()!="angriff"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_hops= new Spielkarte("hops");
        if (spielkarte_hops.getName()!="hops"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_wunsch = new Spielkarte("wunsch");
        if (spielkarte_wunsch.getName()!="wunsch"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_mischen = new Spielkarte("mischen");
        if (spielkarte_mischen.getName()!="mischen"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }
        Spielkarte spielkarte_bidz = new Spielkarte("blick_in_die_zukunft");
        if (spielkarte_bidz.getName()!="blick_in_die_zukunft"){
            Assertions.fail("Name der Spielkarte wurde falsch zurückgegeben");
        }

    }
}