package explodingkittens.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Test für den Ablagestapel.
 * @author Desiree
 */
class AblagestapelTest {

    private final Ablagestapel ablagestapel = new Ablagestapel();
    @Test
    @Order(1)
    void manipulate(){
        try{
            ablagestapel.top();
            Assertions.fail("Fehler: Der Ablagestapel ist leer, es soll eine Exception geworfen werden.");
        }
        catch(AblagestapelLeerException e){

        }
        try{
            ablagestapel.pull();
            Assertions.fail("Fehler: Der Ablagestapel ist leer, es soll eine Exception geworfen werden.");
        }
        catch(AblagestapelLeerException e){

        }
        Spielkarte mockedSpielkarte1 = mock(Spielkarte.class);
        Spielkarte mockedSpielkarte2 = mock(Spielkarte.class);

        ablagestapel.push(mockedSpielkarte1);
        ablagestapel.push(mockedSpielkarte2);
        try{
            Spielkarte ergebnis = ablagestapel.top();
            Spielkarte ergebnis2 = ablagestapel.top();
            //Top-Karte ist immer noch die gleiche
            assertTrue(ergebnis == ergebnis2);
        }
        catch(AblagestapelLeerException e){
            Assertions.fail("Fehler: Der Ablagestapel ist nicht leer, es soll keine Exception geworfen werden.");
        }
        try{
            Spielkarte ergebnis = ablagestapel.pull();
            Spielkarte ergebnis2 = ablagestapel.pull();
            assertFalse(ergebnis==ergebnis2);

        }
        catch(AblagestapelLeerException e){
            Assertions.fail("Fehler: Der Ablagestapel ist nicht leer, es soll keine Exception geworfen werden.");
        }
        try{
            ablagestapel.pull();
            Assertions.fail("Fehler: Der Ablagestapel sollte leer sein.");
        }
        catch(AblagestapelLeerException e){

        }
    }

    @Test
    @Order(2)
    void getAblagestapel(){
        ArrayList ergebnis_list = ablagestapel.getAblagestapel();
        assertTrue(ergebnis_list.isEmpty());
        Spielkarte mockedSpielkarte1 = mock(Spielkarte.class);
        Spielkarte mockedSpielkarte2 = mock(Spielkarte.class);
        ablagestapel.push(mockedSpielkarte1);
        ablagestapel.push(mockedSpielkarte2);
        ArrayList ergebnis_list2 = ablagestapel.getAblagestapel();
        ArrayList compare_list = new ArrayList<>(Arrays.asList(mockedSpielkarte1,mockedSpielkarte2));
        assertTrue(ergebnis_list2.equals(compare_list));

    }
}