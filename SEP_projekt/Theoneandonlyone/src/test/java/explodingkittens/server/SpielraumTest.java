package explodingkittens.server;

import explodingkittens.*;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Testklasse für den Spielraum.
 *
 * @author nico
 */
class SpielraumTest {

    private static final String SPIEL = "Spiel1";
    private Spielraum spielraum;
    private final SpielerInterface ersteller = new Spieler("e1");

    SpielraumTest() throws RemoteException {
    }

    @BeforeEach
    void setUp() throws RemoteException {
        spielraum = new Spielraum(ersteller, SPIEL);
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * Testet die Grundlegenden Methoden, wie beitreten, verlassen und ob der Spielraum leer ist.
     *
     * @throws SpielraumVollException Ausnahme, falls Spielraum voll.
     * @throws ErstellerException     Ausnahme, falls der Ersteller den Spielraum verlassen will.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @Test
    void beitretenUndVerlassenUndIstLeer() throws SpielraumVollException, ErstellerException, RemoteException {
        SpielerInterface spieler1 = new Spieler("s1");
        Assert.assertTrue(spielraum.istLeer());
        Assert.assertThat(spielraum.getErsteller(), CoreMatchers.is(ersteller));
        spielraum.beitreten(spieler1);
        Assert.assertFalse(spielraum.istLeer());
        Assert.assertThat(spielraum.getSpieler(), CoreMatchers.is(Collections.singleton(spieler1)));
        spielraum.verlassen(spieler1);
        Assert.assertTrue(spielraum.istLeer());
       // Assertions.assertThrows(ErstellerException.class, () -> spielraum.verlassen(ersteller));
        SpielerInterface spieler2 = new Spieler("s2");
        SpielerInterface spieler3 = new Spieler("s3");
        SpielerInterface spieler4 = new Spieler("s4");
        Assertions.assertThrows(SpielraumVollException.class, () -> {
            spielraum.beitreten(spieler1);
            spielraum.beitreten(spieler2);
            spielraum.beitreten(spieler3);
            spielraum.beitreten(spieler4);
        });
        spielraum.verlassen(spieler1);
        spielraum.verlassen(spieler2);
        spielraum.verlassen(spieler3);


    }

    /**
     * Testet das Hinzufügen und Entfernen von Bots..
     *
     * @throws SpielraumVollException    Ausnahme, falls Spielraum bereits voll.
     * @throws KeinBotVorhandenException Ausnahme, falls kein Bot zum Entfernen vorhanden ist.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @Test
    void botHinzufuegenUndEntfernen() throws SpielraumVollException, KeinBotVorhandenException, RemoteException {
        BotInterface bot1 = new Bot("b1");
        Assert.assertTrue(spielraum.istLeer());
        spielraum.botHinzufuegen(bot1);
        Assert.assertThat(spielraum.getBots(), CoreMatchers.is(Collections.singletonList(bot1)));
        BotInterface bot2 = new Bot("b2");
        BotInterface bot3 = new Bot("b3");
        BotInterface bot4 = new Bot("b4");
        Assertions.assertThrows(SpielraumVollException.class, () -> {
            spielraum.botHinzufuegen(bot2);
            spielraum.botHinzufuegen(bot3);
            spielraum.botHinzufuegen(bot4);
        });
        spielraum.botEntfernen(bot1);
        spielraum.botEntfernen(bot2);
        spielraum.botEntfernen(bot3);
        Assertions.assertThrows(KeinBotVorhandenException.class, () -> spielraum.botEntfernen(bot3));


    }

    /**
     * Testet, ob korrekter Name zurückgeliefert wird.
     */
    @Test
    void getName() {
        Assert.assertThat(spielraum.getName(), CoreMatchers.is("Spiel1"));
    }

    /**
     * Testet die setzeGeloescht()-Methode
     *
     * @throws KeinErstellerException Ausnahme, falls ein Nichtersteller die Flag setzen will.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @Test
    void setzeGeloescht() throws KeinErstellerException, RemoteException {
        SpielerInterface nico = new Spieler("s1");
        Assertions.assertThrows(KeinErstellerException.class, () -> spielraum.setzeGeloescht(nico));
        spielraum.setzeGeloescht(ersteller);
        Assert.assertThat(spielraum.getStatus(), CoreMatchers.is("geloescht"));
    }

    /**
     * Testet die Methode aenderName().
     *
     * @throws KeinErstellerException Ausnahme, falls der Veränderer kein Ersteller ist.
     * @throws RemoteException Ausnahme, die ausgelöst wird, wenn keine Verbindung zum Remote-Objekt hergestellt werden kann.
     */
    @Test
    void aenderName() throws KeinErstellerException, RemoteException {
        SpielerInterface nico = new Spieler("s1");
        spielraum.aenderName(ersteller, "neuerName");
        Assert.assertThat(spielraum.getName(), CoreMatchers.is("neuerName"));
        Assertions.assertThrows(KeinErstellerException.class, () -> spielraum.aenderName(nico, "Test"));
    }

    @Test
    void getMitspieler() throws RemoteException, SpielraumVollException {
        Bot bot1 = new Bot("bot1");
        Spieler spieler1 = new Spieler("sp1");
        spielraum.botHinzufuegen(bot1);
        spielraum.beitreten(spieler1);
        ArrayList list = new ArrayList();
        list.add("sp1");
        list.add("bot1");
        Assert.assertThat(spielraum.getMitSpieler(), CoreMatchers.is(list));
        spielraum.setGestartet();
        Assert.assertThat(spielraum.isGestartet(), CoreMatchers.is(true));

    }
}