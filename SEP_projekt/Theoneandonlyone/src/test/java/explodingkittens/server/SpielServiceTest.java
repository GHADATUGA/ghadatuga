package explodingkittens.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.RemoteException;
import java.util.ArrayList;

class SpielServiceTest {
    private SpielService spielService;


    @BeforeEach
    void initialize() throws RemoteException {
        spielService = new SpielService();
        ArrayList spieler = new ArrayList();
        spieler.add("sp1");
        spieler.add("sp2");
        ArrayList bots = new ArrayList();
        bots.add("bot1");
        bots.add("bot2");
        spielService.erzeugeSpiel("spiel",spieler,bots);
    }
    @Test
    void verlassen() throws RemoteException {
        spielService.verlassen("bot1","spiel");
        ArrayList ergebnis = spielService.getSpielzustand("spiel");
        String[] teilnehmer = (String[]) ergebnis.get(2);
        for (String t:teilnehmer){
            if (t=="bot1"){
                Assertions.fail("Bot hat das Spiel bereits verlassen");
            }

        }

    }

    @Test
    void zugBeenden() throws RemoteException, NichtAktuellerTeilnehmerException {
        SpielService spielService = new SpielService();
        ArrayList spieler = new ArrayList();
        spieler.add("sp1");
        spieler.add("sp2");
        ArrayList bots = new ArrayList();
        spielService.erzeugeSpiel("spiel",spieler,bots);
        boolean ek_gezogen=false;
        boolean not_ek_gezogen=false;
        while (!ek_gezogen||!not_ek_gezogen) {
            String teilnehmer = (String) spielService.getSpielzustand("spiel").get(0);
            String[] teilnehmerliste = (String[])spielService.getSpielzustand("spiel").get(2);
            String next = (String) teilnehmerliste[0];
            Integer verbleibendeZuege = (Integer) spielService.getSpielzustand("spiel").get(1);

            boolean ergebnis =spielService.zugBeenden(teilnehmer, "spiel");
            if (ergebnis==true){
                ek_gezogen=true;
                assert((String) spielService.getSpielzustand("spiel").get(0)==teilnehmer);
                spielService.explodieren("spiel", teilnehmer);

            }
            else if (verbleibendeZuege==1){
                not_ek_gezogen=true;
                assert(((String) spielService.getSpielzustand("spiel").get(0))==(next));
            }
            else{
                not_ek_gezogen=true;
                assert((String) spielService.getSpielzustand("spiel").get(0)==teilnehmer);
                assert((Integer) spielService.getSpielzustand("spiel").get(1)==verbleibendeZuege-1);

            }
        }
    }

    @Test
    void modified() {
        assert(spielService.modified("spiel")>0);
    }



    @Test
    void blickInDieZukunft() throws NichtAktuellerTeilnehmerException {
        String teilnehmer = (String) spielService.getSpielzustand("spiel").get(0);
        try{
            String[] ergebnis=spielService.blickInDieZukunft("spiel",teilnehmer);
        }
        catch (KarteNichtAufHandException e){

        }catch (NichtAktuellerTeilnehmerException ex){
            Assertions.fail("Teilnehmer ist der aktuelle Teilnehmer");
        }
        spielService.zugBeenden(teilnehmer, "spiel");
    }

    @Test
    void mischen() {

            String teilnehmer = (String) spielService.getSpielzustand("spiel").get(0);
            try{
                spielService.mischen("spiel",teilnehmer);

            }
            catch (KarteNichtAufHandException e){


            }catch (NichtAktuellerTeilnehmerException ex){
                Assertions.fail("Teilnehmer ist der aktuelle Teilnehmer");
            }
        try {
            spielService.zugBeenden(teilnehmer, "spiel");
        } catch (NichtAktuellerTeilnehmerException e) {
            e.printStackTrace();
        }
    }

    @Test
    void noe() {
        String teilnehmer = (String) spielService.getSpielzustand("spiel").get(0);
            try{
                spielService.noe("spiel",teilnehmer);

            }
            catch (KarteNichtAufHandException e){


            } catch (KarteNichtAusspielbarException e) {

            }
        try {
            spielService.zugBeenden(teilnehmer, "spiel");
        } catch (NichtAktuellerTeilnehmerException e) {
            e.printStackTrace();
        }
    }

    @Test
    void gegnerGibtKarte(){
        try {
            spielService.gegnerGibtKarte("spiel","sp1","sp2");
        } catch (RemoteException e) {
        } catch (KarteNichtAufHandException e) {

        } catch (NichtAktuellerTeilnehmerException e) {

        }

    }

    @Test
    void gegnerGibtKarteAb() {
        try {
            spielService.gegnerGibtKarteAb("spiel","sp1","sp2","wunsch");
        } catch (KarteNichtAufHandException e) {
            e.printStackTrace();
        }
    }

    @Test
    void entschaerfen() {
        try {
            spielService.entschaerfen("spiel","sp1",0);
        } catch (KarteNichtAufHandException e) {
            e.printStackTrace();
        } catch (PositionNichtMoeglichException e) {
            e.printStackTrace();
        } catch (NichtAktuellerTeilnehmerException e) {
            e.printStackTrace();
        }
    }


    @Test
    void angriff() {
        try {
            spielService.angriff("spiel","bot1");
        } catch (KarteNichtAufHandException e) {
            e.printStackTrace();
        } catch (NichtAktuellerTeilnehmerException e) {
            e.printStackTrace();
        }
    }

    @Test
    void hops() {
        String teilnehmer = (String) spielService.getSpielzustand("spiel").get(0);
        String[] teilnehmerliste = (String[])spielService.getSpielzustand("spiel").get(2);
        String next = (String) teilnehmerliste[0];
        Integer verbleibendeZuege = (Integer) spielService.getSpielzustand("spiel").get(1);

        try {
            spielService.hops("spiel", teilnehmer);
            if (verbleibendeZuege==1){
                assert(((String) spielService.getSpielzustand("spiel").get(0)).equals(next));
            }
            else{
                assert(((String) spielService.getSpielzustand("spiel").get(0)).equals(teilnehmer));
                assert((Integer) spielService.getSpielzustand("spiel").get(1)==verbleibendeZuege-1);

            }
        } catch (KarteNichtAufHandException e) {

        } catch (NichtAktuellerTeilnehmerException e) {
            Assertions.fail("Teilnehmer ist aktueller Teilnehmer");
        }

    }
}