package explodingkittens.client;
/*
import explodingkittens.server.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

class SpielServerClientTest {
    private static Registry registry;
    private SpielServerClient spielClient;
    private String spielName="spiel";

    @BeforeAll
    static void initialize1() throws RemoteException {

    try{
        registry = LocateRegistry.createRegistry(8014);
        SpielService spielService = new SpielService();
        registry.bind("spielService", spielService);}
    catch(Exception e){
        registry=null;
    }

    }
    @AfterAll
    static void shutdown() throws RemoteException, NotBoundException {
       // registry.unbind("spielService");
        if(registry!= null){
        UnicastRemoteObject.unexportObject(registry,true);}
    }
    @BeforeEach
    void intialize() throws RemoteException, AlreadyBoundException {

        spielClient = new SpielServerClient();
        ArrayList spieler= new ArrayList();
        spieler.add("sp1");
        spieler.add("sp2");
        ArrayList bots = new ArrayList();
        spielClient.erzeugeSpiel("spiel",spieler, bots);


    }
    @Test
    void verlassen() throws RemoteException {
        spielClient = new SpielServerClient();
        ArrayList spieler= new ArrayList();
        spieler.add("sp1");
        spieler.add("sp2");
        ArrayList bots = new ArrayList();
        spielClient.erzeugeSpiel("spiel",spieler, bots);
        spielClient.verlassen("sp1",spielName);

    }

    @Test
    void zugBeenden() throws RemoteException{
        try {
            spielClient.zugBeenden("sp1",spielName);
        } catch (NichtAktuellerTeilnehmerException e) {
            e.printStackTrace();
        }
    }

    @Test
    void modified() throws RemoteException {
        spielClient.modified(spielName);
    }

    @Test
    void getSpielzustand() throws RemoteException {
        spielClient.getSpielzustand(spielName);
    }


    @Test
    void blickInDieZukunft() throws RemoteException{
        try{
            spielClient.blickInDieZukunft(spielName,"sp1");
        }
        catch(KarteNichtAufHandException|NichtAktuellerTeilnehmerException e){}
    }

    @Test
    void mischen() throws RemoteException {
        try{
        spielClient.mischen(spielName,"sp1");
    }
        catch(KarteNichtAufHandException|NichtAktuellerTeilnehmerException e){}
    }


    @Test
    void noe() throws RemoteException {
        try{

        spielClient.noe(spielName,"sp1");
    }
        catch(KarteNichtAufHandException|KarteNichtAusspielbarException e){}
    }

    @Test
    void gegnerGibtKarte() throws RemoteException {
        try{
        spielClient.gegnerGibtKarte(spielName,"sp1","sp2");
    }
        catch(KarteNichtAufHandException|NichtAktuellerTeilnehmerException e){}
    }

    @Test
    void gegnerGibtKarteAb() throws RemoteException {
        spielClient = new SpielServerClient();
        ArrayList spieler= new ArrayList();
        spieler.add("sp1");
        spieler.add("sp2");
        ArrayList bots = new ArrayList();
        spielClient.erzeugeSpiel("spiel",spieler, bots);
        try{
            spielClient.gegnerGibtKarteAb(spielName,"sp1","sp2","mischen");
        }
        catch(KarteNichtAufHandException e){}
    }

    @Test
    void entschaerfen() throws RemoteException{
        spielClient = new SpielServerClient();
        ArrayList spieler= new ArrayList();
        spieler.add("sp1");
        spieler.add("sp2");
        ArrayList bots = new ArrayList();
        spielClient.erzeugeSpiel("spiel",spieler, bots);
        try{
        spielClient.entschaerfen(spielName,"sp1",0);
    }
        catch(KarteNichtAufHandException|PositionNichtMoeglichException|NichtAktuellerTeilnehmerException e){}
    }

    @Test
    void explodieren() throws RemoteException {
        spielClient.explodieren(spielName,"sp1");
    }

    @Test
    void angriff() throws RemoteException {
       try{
           spielClient.angriff(spielName,"sp1");
    }
        catch(KarteNichtAufHandException|NichtAktuellerTeilnehmerException e){}
    }

    @Test
    void hops() throws RemoteException {
        try{
        spielClient.hops(spielName,"sp1");
    }
        catch(KarteNichtAufHandException|NichtAktuellerTeilnehmerException e){}
    }
}*/