$("button[name='showOutput']").click(function () {
    var worksheetSel = document.getElementById("sheetSelect");
    var worksheet = worksheetSel.options[worksheetSel.selectedIndex].value;

    var taskSel = document.getElementById("taskSelect");
    var task = taskSel.options[taskSel.selectedIndex].value;

    console.log(worksheet);
    console.log(task);

    $.ajax({
        url: "/getOutput",
        type: "POST",
        data: {
            worksheet: worksheet,
            task: task
        },
        success: function (data) {
            //     parsedData = JSON.parse(data);
            console.log(data);
            $('.container').append($('<div/>').attr('id','results'));
            $('#results').html(data);
        }
    });
});