package com.tukl.DBProject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DBProjectController {

    @RequestMapping("/getOutput")
    @ResponseBody
    public String greeting(@RequestParam(value = "worksheet") String worksheet,
                           @RequestParam(value = "task") String task) {
        String msg = String.format("Received request for %s, task %s.", worksheet, task);
        return msg;
    }

}
