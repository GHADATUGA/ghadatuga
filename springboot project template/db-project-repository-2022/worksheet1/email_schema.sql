CREATE TABLE attachment (
    id integer PRIMARY KEY NOT NULL,
    filename varchar(40),
    subject varchar(80)
);

CREATE TABLE picture (
    width integer,
    height integer,
    colors integer
) INHERITS(attachment);

CREATE TABLE video (
    fps float,
    seconds integer
) INHERITS(picture);

CREATE TABLE text (
    content varchar(80)
) INHERITS(attachment);

CREATE TABLE euser (
    euid integer NOT NULL,
    nickname varchar(20)
);

CREATE TABLE person (
    surname varchar(20),
    first_name varchar(20),
    date_of_birth date
) INHERITS(euser);

CREATE TABLE organization (
    firm varchar(30)
) INHERITS(euser);

CREATE TABLE address (
    adid integer PRIMARY KEY NOT NULL,
    username varchar(20) NOT NULL,
    domain varchar(20) NOT NULL,
    UNIQUE(username, domain)
);

CREATE TABLE use (
    euser integer,
    address integer REFERENCES address(adid)
);

CREATE TABLE email (
    id integer PRIMARY KEY NOT NULL,
    date timestamp NOT NULL,
    _from integer REFERENCES address(adid),
    in_reply_to integer REFERENCES email(id)
) INHERITS (text);

ALTER TABLE attachment ADD contained_in integer REFERENCES email(id);

CREATE TABLE addressee (
    _to integer REFERENCES address(adid),
    email integer REFERENCES email(id)
);