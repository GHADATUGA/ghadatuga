package com.tukl.dbproject.worksheet1.queries;

public class Queries {

    //Exercise2 a)Multiple addresses:
    public final static String query1 ="select  nickname, count(u.address) as counts from euser e,use u \n" +
            "   where e.euid=u.euser and \n" +
            "   (select count(address)from use us where us.euser=e.euid and u.euser=e.euid)>1\n" +
            "   group by (euid,nickname)order by counts DESC, nickname DESC";
    //b)Total numbers:
    public final static String query2 = "select type, count from (\n" +
            "\tselect 'euser' as type, count(*) from euser where euser.euid not in (select euid from person union select euid from organization)\n" +
            "\tunion select 'total', count(*) from euser \n" +
            "\tunion select 'person', count(*) from person\n" +
            "\tunion select 'organization', count(*) from organization\n" +
            ") as total_data group by count, type order by count desc;";

    // c) Most commonly used letter:
    public final static String query3="SELECT SUBSTRING(filename, 1, 1) as letter, count(*) as often FROM attachment \n" +
            "          GROUP BY SUBSTRING(filename, 1, 1)ORDER BY often ASC,letter DESC limit 1";

    //d) Address sharing:
    public final static String query4="WITH youngest_person_address(young_addr) as\n" +
            "    (select  address as youngest_address_id from person per1, use us \n" +
            "    where us.euser=per1.euid and not exists(select * from person per2 \n" +
            "\t\t\t\t where per2.date_of_birth>per1.date_of_birth))\t\t\t \n" +
            "    SELECT distinct (nickname)\n" +
            "    FROM youngest_person_address y,person per3,address ad, addressee adds2, email em, use u\n" +
            "    WHERE u.euser=per3.euid and u.address=ad.adid \n" +
            "\tand (adds2._to=y.young_addr  or em._from=y.young_addr) order by nickname ASC;";

     //e) Birthday greetings:
    public final static String query5="select nickname, date_of_birth as dateOfBirth,count(em.id) as counts \n" +
             "        from person per, use u, address ads,email em,addressee adee\n" +
             "\t\twhere per.euid=u.euser and u.address=ads.adid and ads.adid=adee._to and adee.email=em.id\t  \t  \n" +
             "\t\tand  ( DATE(em.date)=DATE(per.date_of_birth) or \n" +
             "\t\t\t  (DATE_PART('month', DATE(per.date_of_birth)::date)=\n" +
             "\t\t\t   DATE_PART('month', DATE(em.date)::date) and\n" +
             "\t\t\t  DATE_PART('day', DATE(per.date_of_birth)::date) - \n" +
             "\t\t\t   DATE_PART('day', DATE(em.date)::date)<2))\n" +
             "\t\t\t  and not exists(select firm from organization o where o.euid=em._from)\n" +
             "\t\tgroup by (euid,nickname,dateOfBirth)\torder by nickname ASC";

    //f) Top-10
    public final static String query6="with sentreplies (email_id, replies_to,senderaddress,timetaken)as\n" +
            "(select received.id as email_id,sent.id as replied_to,received._from as \n" +
            "receivedfrom,TRUNC(DATE_PART('day', received.date- sent.date)/7)as reply_time \n" +
            "from email sent,email received\n" +
            "where sent.id=received.in_reply_to),\n" +
            "frommessage3(email_id,replies_to,timetaken,euser,nickname,number_of_replies) as(\n" +
            "select distinct (email_id), replies_to,timetaken,euser,nickname,\n" +
            "count(*) over(partition by u.euser) as number_of_replies\n" +
            "\tfrom sentreplies sentrep,use u,person per \n" +
            "where sentrep.senderaddress=u.euser and per.euid=u.euser ),\n" +
            "ranking (email_id,replies_to,timetaken,euser,nickname,number_of_replies)as (select * from frommessage3 where number_of_replies>2),\n" +
            "limitingranks (timetaken,nickname,rank) as (select timetaken,nickname,(select count(distinct timetaken)\n" +
            "        from ranking ra\n" +
            "        where ra.timetaken <= ranking.timetaken) as rank\n" +
            "\t\tfrom ranking ) \n" +
            "\t\tselect nickname,rank from limitingranks where rank<11 order by rank asc";


    //g) Top-10 variation:
    public final static String query7="";

    //TODO.. i stil working on it


    //h) Top-10 using OLAP:
    public final static String query8="with sentreplies (email_id, replies_to,senderaddress,timetaken)as\n" +
            "(select received.id as email_id,sent.id as replied_to,received._from as \n" +
            "receivedfrom,TRUNC(DATE_PART('day', received.date- sent.date)/7)as reply_time \n" +
            "from email sent,email received\n" +
            "where sent.id=received.in_reply_to),\n" +
            "frommessage3(email_id,replies_to,timetaken,euser,nickname,number_of_replies) as(\n" +
            "select distinct (email_id), replies_to,timetaken,euser,nickname,\n" +
            "count(*) over(partition by u.euser) as number_of_replies\n" +
            "from sentreplies sentrep,use u,person per \n" +
            "where sentrep.senderaddress=u.euser and per.euid=u.euser ),\n" +
            "ranking (email_id,replies_to,timetaken,euser,nickname,number_of_replies)as (select * from frommessage3 where number_of_replies>2),\n" +
            "limitingrank(timetaken,nickname,rank)as(select timetaken,nickname,DENSE_RANK() over(  order by timetaken asc )as rank from ranking)\n" +
            "select nickname,rank from limitingrank where rank<11";


     //i) Thread decomposition:
    public final static String query9="";

    //TODO.. still thinking on it



}
