package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "address")
@Data
public class Address {
    @Id
    int adid;
    String username;
    String domain;
}
