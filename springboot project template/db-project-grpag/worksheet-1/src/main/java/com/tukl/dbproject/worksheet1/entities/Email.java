package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "email")
@Data
public class Email extends Text{
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "date")
    private Timestamp date;
    @ManyToOne
    @JoinColumn(name = "_from")
    private Address _from;
    @OneToOne
    @JoinColumn(name = "in_reply_to")
    private Email in_reply_to;
}
