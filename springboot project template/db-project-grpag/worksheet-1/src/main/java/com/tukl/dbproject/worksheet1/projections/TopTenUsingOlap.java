package com.tukl.dbproject.worksheet1.projections;

public interface TopTenUsingOlap {
    String getNickname();
    int getRank();
}
