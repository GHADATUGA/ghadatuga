package com.tukl.dbproject.worksheet1.repository;
import com.tukl.dbproject.worksheet1.projections.*;
import com.tukl.dbproject.worksheet1.entities.Euser;
import com.tukl.dbproject.worksheet1.queries.Queries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<Euser, Integer> {
    @Query(value = Queries.query1,nativeQuery = true)
    List<MultipleAdresses> getMultipleAddressesUsers();
    @Query(value = Queries.query2, nativeQuery = true)
    List<UserTypeCountMapping> getUserTypeCountMapping();
    @Query(value = Queries.query3,nativeQuery = true)
    List<MostCommonlyUsedLetter> getMostCommonlyUsedLetter();
    @Query(value = Queries.query4,nativeQuery = true)
    List<AddressSharing> getAddressSharingUsers();

    @Query(value = Queries.query5, nativeQuery = true)
    List<BirthdayGreetings> getBirthGreetings();

    @Query(value = Queries.query6,nativeQuery = true)
     List<TopTen> getTopTen();
    @Query(value = Queries.query8,nativeQuery = true)
    List<TopTenUsingOlap> getTopTenUsingOlap();

}
