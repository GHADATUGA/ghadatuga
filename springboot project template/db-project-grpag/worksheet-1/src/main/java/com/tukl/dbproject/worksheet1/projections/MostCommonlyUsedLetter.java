package com.tukl.dbproject.worksheet1.projections;

public interface MostCommonlyUsedLetter {
    String getLetter();
    int getOften();
}
