package com.tukl.dbproject.worksheet1.entities;

import com.tukl.dbproject.worksheet1.entities.ids.UseId;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "use")
@Data
@IdClass(UseId.class)
public class Use {
    @Id
    int euser;

    @ManyToOne
    @JoinColumn(name = "address")
    @Id
    Address address;
}
