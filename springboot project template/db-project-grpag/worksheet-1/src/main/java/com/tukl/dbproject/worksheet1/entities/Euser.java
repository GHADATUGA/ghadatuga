package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "euser")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
public class Euser {
    @Id
    int euid;
    String nickname;
}
