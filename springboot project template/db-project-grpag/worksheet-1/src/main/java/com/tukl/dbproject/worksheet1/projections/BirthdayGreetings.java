package com.tukl.dbproject.worksheet1.projections;

import java.util.Date;

public interface BirthdayGreetings {
    String getNickname();
    Date getDateofbirth();
    int getCounts();

}
