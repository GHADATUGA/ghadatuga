package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "video")
@Data
public class Video extends Picture{
    private float fps;
    private int seconds;
}
