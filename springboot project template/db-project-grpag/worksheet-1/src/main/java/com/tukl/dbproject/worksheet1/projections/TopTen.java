package com.tukl.dbproject.worksheet1.projections;

public interface TopTen {
    String getNickname();
    int getRank();
}
