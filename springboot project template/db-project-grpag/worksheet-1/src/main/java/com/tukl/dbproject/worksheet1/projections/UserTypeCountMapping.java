package com.tukl.dbproject.worksheet1.projections;

import lombok.Data;
import lombok.NoArgsConstructor;

public interface UserTypeCountMapping {
    String getType();
    int getCount();
}
