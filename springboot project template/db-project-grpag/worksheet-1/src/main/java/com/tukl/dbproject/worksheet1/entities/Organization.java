package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "organization")
@Data
public class Organization extends Euser{
    String firm;
}
