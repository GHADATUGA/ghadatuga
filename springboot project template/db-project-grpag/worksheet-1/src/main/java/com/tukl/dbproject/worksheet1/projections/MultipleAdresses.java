package com.tukl.dbproject.worksheet1.projections;

public interface MultipleAdresses {
    String getNickname();
    int getCounts();
}
