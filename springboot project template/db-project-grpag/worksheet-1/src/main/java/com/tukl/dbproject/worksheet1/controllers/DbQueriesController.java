package com.tukl.dbproject.worksheet1.controllers;
import com.tukl.dbproject.worksheet1.projections.*;
import com.tukl.dbproject.worksheet1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class DbQueriesController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "get-multiple-addresses-users", method = RequestMethod.GET)
    public List<MultipleAdresses> getMultipleAddressesUsers() {
        return userRepository.getMultipleAddressesUsers();
    }

    @RequestMapping(value = "get-user-type-numbers", method = RequestMethod.GET)
    public List<UserTypeCountMapping> getUserTypeNumbers() {
    	return userRepository.getUserTypeCountMapping();
    }

    @RequestMapping(value = "get-most-commonly-used-letter", method = RequestMethod.GET)
    public List<MostCommonlyUsedLetter> getMostCommonlyUsedLetter() {
        return userRepository.getMostCommonlyUsedLetter();
    }
    
    @RequestMapping(value = "get-address-sharing", method = RequestMethod.GET)
    public List<AddressSharing> getAddressSharing() {
    	return userRepository.getAddressSharingUsers();
    }
    
    @RequestMapping(value = "get-bithday-greetings", method = RequestMethod.GET)
    public List<BirthdayGreetings> getBirthdayGreetings() {
    	return userRepository.getBirthGreetings();
    }
    
    @RequestMapping(value = "get-top-10", method = RequestMethod.GET)
    public List<TopTen> getTop10() {
    	return userRepository.getTopTen();
    }
    
    @RequestMapping(value = "get-top-10-variation", method = RequestMethod.GET)
    public List<Object> getTop10Variation() {
    	return null;
    }
    
    @RequestMapping(value = "get-top-10-using-OLAP", method = RequestMethod.GET)
    public List<TopTenUsingOlap> getTop10UsingOLAP() {
    	return userRepository.getTopTenUsingOlap();
    }
    
    @RequestMapping(value = "get-thread-decomposition", method = RequestMethod.GET)
    public List<Object> getThreadDecomposition() {
    	return null;
    }

}
