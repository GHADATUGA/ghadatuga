package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "picture")
@Data
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Picture extends Attachment{

    @Column(name = "width")
    int width;
    @Column(name = "height")
    int height;
    @Column(name = "colors")
    int colors;
}
