package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "person")
@Data
public class Person extends Euser{
    @Column(name = "surname")
    String surname;
    @Column(name = "first_name")
    String first_name;
    @Column(name = "date_of_birth")
    Date date_of_birth;
}
