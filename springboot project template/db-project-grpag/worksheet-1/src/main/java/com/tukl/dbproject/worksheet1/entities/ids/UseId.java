package com.tukl.dbproject.worksheet1.entities.ids;

import com.tukl.dbproject.worksheet1.entities.Address;

import java.io.Serializable;

public class UseId implements Serializable {
    int euser;
    Address address;

    public UseId(int euser, Address address) {
        this.euser = euser;
        this.address = address;
    }
}
