
-------------------------------------------------------------------

------------------------------ materialized views for the olap function exercises sheet 4-----------------------------

----------------------------------------------------------------
---1)monthlypassanger for query41
create materialized view monthlypassanger as
(select 'Yellow Cab' as taxicab, round (sum(passenger_count)*.5,0) as avgMontlyPassangers from yellow_tripdata 
            union
    select 'Green Cab' as taxicab, round (sum(passenger_count)*.5,0) as avgMontlyPassangers from green_tripdata);
	
	select * from monthlypassanger;

--2)avgtimeonscene query42
create materialized view avgtimeonscene as
(select hvfhs_license_num as hvfhsLicenseNumber , round(cast(avg(extract(epoch from (on_scene_datetime- request_datetime)))AS numeric),2) as seconds from fhvhv_tripdata where on_scene_datetime is not null 
            and request_datetime is not null group by (hvfhs_license_num)   order by seconds asc limit 10);

 select * from avgtimeonscene;

--3)expensivetrip query43
create materialized view expensivetrip as
(select 'High Volume FHV' as carservice, pulocationid, dolocationid, driver_pay as driverPay from fhvhv_tripdata v where v.driver_pay>= all(select driver_pay from fhvhv_tripdata )
            union
            select 'Green Cab' as carservice, pulocationid, dolocationid, total_amount from green_tripdata gr where gr.total_amount>= all(select total_amount from green_tripdata )
            union
            select 'Yellow Cab' as carservice, pulocationid, dolocationid, total_amount from yellow_tripdata gr where gr.total_amount>= all(select total_amount from yellow_tripdata ));

select * from expensivetrip;

---4) paymentmodelrank query44
create materialized view paymentmodelrank as
(SELECT distinct payment_type as paymentType,
             CASE
            WHEN payment_type =1 THEN  'Green Credit card'
            WHEN payment_type =2 THEN 'Green Cash'
           WHEN payment_type =3 THEN 'Green No charge'
            WHEN payment_type =4 THEN 'Green Dispute'
            WHEN payment_type =5 THEN 'Green Unknown'
                ELSE 'Green Voided trip'
             END 
             AS paymentName, DENSE_RANK() over(order by payment_type) as ranking from green_tripdata 
             union
             SELECT distinct payment_type as paymentType,
             CASE
              WHEN payment_type =1 THEN  'yellow Credit card'
             WHEN payment_type =2 THEN 'Yellow Cash'
             WHEN payment_type =3 THEN 'Yellow No charge'
            WHEN payment_type =4 THEN 'Yellow Dispute'
            WHEN payment_type =5 THEN 'Yellow Unknown'
               ELSE ' Voided trip'
              END 
             AS paymentName, DENSE_RANK() over(order by payment_type) as ranking from yellow_tripdata order by ranking ASC); 
			 
select * from paymentmodelrank;

--5)monthlydriverpay query45
create materialized view monthlydriverpay as
(select  dispatching_base_num as dispatchingBaseNumber, sum(driver_pay)*.5 as monthlyPay from fhvhv_tripdata
           group by dispatching_base_num order by monthlyPay DESC);
		   
		   select * from monthlydriverpay;

--6)popularcarservice query46
create materialized view popularcarservice as
(select distinct hvfhs_license_num as highVolumeFHV, count( hvfhs_license_num)/2 as numberOfTrip from fhvhv_tripdata group by highVolumeFHV 
            order by numberOfTrip DESC);
			
			select * from popularcarservice;

--7) carserviceandbaselicencenumber query47
create materialized view carserviceandbaselicencenumber as
(select  hvfhs_license_num as carService, count (distinct dispatching_base_num)as baseLicenseNumber from fhvhv_tripdata
            group by hvfhs_license_num order by baseLicenseNumber DESC);
			
			select * from carserviceandbaselicencenumber;


--8)avgtripdistance query48
create materialized view avgtripdistance as
(select hvfhs_license_num as carservice, round (AVG(trip_miles),2) as averageTripMile from fhvhv_tripdata group by hvfhs_license_num
            union
            select 'yellow_tripdata' as carservice , round(AVG(trip_distance),2) as averagetripMile from  yellow_tripdata group by vendorid
             union
            select 'green_tripdata' as carservice , round(AVG(trip_distance),2) as averagetripMile  from  green_tripdata group by vendorid order by  averagetripMile  desc);

select * from avgtripdistance;

--9)popularpolocation query49
create materialized view popularpolocation as
(with q1(taxservice,pulocationid,numberoftrips)as (select 'Yellow Cab' as taxservice, pulocationid, count (*)/2 as numberoftrips from yellow_tripdata
            group by pulocationid order by numberoftrips DESC limit 2),
            q2(taxservice,pulocationid,numberoftrips)as (select 'Green Cab' as taxservice, pulocationid, count (*)/2 as numberoftrips from green_tripdata 
            group by pulocationid order by numberoftrips DESC limit 2),
            q3(taxservice,pulocationid,numberoftrips)as (select  'High Volume FHV' as taxservice,pulocationid, count (*)/2 as numberoftrips from fhvhv_tripdata  
            group by pulocationid order by numberoftrips DESC  limit 2)
            select * from q1 union select * from q2 union select * from q3 order by numberoftrips DESC);
			
			select * from popularpolocation;
			

--10)hourswithmostpassanger query410
create materialized view hourswithmostpassanger as(
with q1(taxservice,pickuptime,tripnumber) as (select 'Green Cab' as taxservice,  extract(hour from ipep_pickup_datetime) as pickup_time, 
             count(*)/60 as tripnumber from green_tripdata group by (pickup_time)order by tripnumber DESC limit 3),
             
              q2(taxservice,pickuptime,tripnumber)as (select 'Yellow Cab' as taxservice, extract(hour from tpep_pickup_datetime) as pickup_time,
               count(*)/60 as tripnumber from yellow_tripdata group by (pickup_time)order by tripnumber DESC limit 3),
             
              q3(taxservice,pickuptime,tripnumber) as (select 'High Volume FHV' as taxservice, extract(hour from pickup_datetime) as pickup_time, 
                count(*)/60 as tripnumber from fhvhv_tripdata group by (pickup_time)order by tripnumber DESC limit 3) 
             select * from q1 union select * from q2 union select * from q3 order by tripnumber DESC);
			 
			 select * from hourswithmostpassanger;
			 
			 
		











































  