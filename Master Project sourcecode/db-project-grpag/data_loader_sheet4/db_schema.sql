--drop table yellow_tripdata;
--drop table green_tripdata;
--drop table fhvhv_tripdata;

CREATE TABLE yellow_tripdata (

                                  vendorID integer,
                                  tpep_pickup_datetime timestamp ,
								  tpep_dropoff_datetime timestamp ,
                                  passenger_count integer,
                                  trip_distance numeric,
	                              ratecodeID integer,
	                              store_and_fwd_flag varchar,
	                              pULocationID integer,
	                              dOLocationID integer,
	                              payment_type integer,
	                              fare_amount  numeric,
	                              extra numeric ,
	                              mta_tax numeric,
	                              tip_amount numeric,
	                              tolls_amount numeric,
	                              improvement_surcharge numeric,
	                              total_amount numeric,
	                              congestion_surcharge numeric,
	                              airport_fee numeric
);
CREATE TABLE green_tripdata(
                                  vendorID integer,
                                  ipep_pickup_datetime timestamp ,
								  ipep_dropoff_datetime timestamp ,
	                              store_and_fwd_flag varchar,
	                              ratecodeID integer,
	                              pULocationID integer,
	                              dOLocationID integer,
                                  passenger_count integer,
                                  trip_distance numeric,
	                              fare_amount  numeric,
	                              extra numeric ,
	                              mta_tax numeric,
	                              tip_amount numeric,
	                              tolls_amount numeric,
	                              improvement_surcharge numeric,
	                              total_amount numeric,
	                              payment_type integer,
	                              trip_type integer,
	                              congestion_surcharge numeric
);
CREATE TABLE fhvhv_tripdata(

                                  hvfhs_license_num varchar,
                                  dispatching_base_num varchar,
								  originating_base_num varchar,
	                              request_datetime timestamp,
	                              on_scene_datetime timestamp,
	                              pickup_datetime timestamp,
	                              dropoff_datetime timestamp,
                                  pULocationID integer,
                                  dOLocationID integer,
	                              trip_miles numeric,
	                              trip_time integer,
	                              base_passenger_fare numeric,
	                              tolls numeric,
	                              bcf numeric,
	                              sales_tax numeric,
	                              congestion_surcharge numeric,
	                              tips numeric,
	                              driver_pay numeric,
	                              shared_request_flag varchar,
	                              shared_match_flag varchar,
	                              access_a_ride_flag varchar,
	                              wav_request_flag varchar
);
