import numpy as np
import pandas as pd
import pyarrow as pa
import uuid, time, gc, psycopg2.extras
import pyarrow.parquet as pq
from db_populator import Db
from multiprocessing import Process


def load_yellow_data_2():
    data = pq.read_pandas('yellow_tripdata_2019-02.parquet').to_pandas()
    #data = data.replace(r'', np.NaN)
    #data = data.head(500000)
    data = data[500000::]
    # print(data.loc[:])
    data = data.replace(r'', 0.0)
    data['tpep_pickup_datetime'] = pd.to_datetime(data.tpep_pickup_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                  errors='coerce')
    data['tpep_dropoff_datetime'] = pd.to_datetime(data.tpep_dropoff_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                   errors='coerce')
    #data.dropna(subset=['tpep_dropoff_datetime'], inplace=True)
    #data.dropna(subset=['tpep_pickup_datetime'], inplace=True)
    data.replace({pd.NaT: None}, inplace=True)
    data.fillna(0.0, inplace=True)
    #print(data.loc[0, :])
    subset = data[
        ["VendorID", "tpep_pickup_datetime", "tpep_dropoff_datetime", "passenger_count", "trip_distance", "RatecodeID",
         "store_and_fwd_flag", "PULocationID", "DOLocationID", "payment_type", "fare_amount", "extra", "mta_tax",
         "tip_amount", "tolls_amount", "improvement_surcharge", "total_amount", "congestion_surcharge", "airport_fee"]]

    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    cur.executemany(
        "INSERT INTO yellow_tripdata(vendorID,tpep_pickup_datetime,tpep_dropoff_datetime ,passenger_count ,"
        "trip_distance,ratecodeID,store_and_fwd_flag ,pULocationID ,dOLocationID,payment_type,fare_amount,extra,"
        "mta_tax,tip_amount,tolls_amount,improvement_surcharge , total_amount,congestion_surcharge,airport_fee) "
        "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
        tuples)
    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


def load_yellow_data_3():
    data = pq.read_pandas('yellow_tripdata_2019-03.parquet').to_pandas()
    ##data = data.head(1000000)
    data = data[1000000::]
    # print(data.loc[:])
    #data = data.replace(r'', np.NaN)
    data = data.replace(r'', 0.0)
    data['tpep_pickup_datetime'] = pd.to_datetime(data.tpep_pickup_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                  errors='coerce')
    data['tpep_dropoff_datetime'] = pd.to_datetime(data.tpep_dropoff_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                   errors='coerce')
    #data.dropna(subset=['tpep_dropoff_datetime'], inplace=True)
    #data.dropna(subset=['tpep_pickup_datetime'], inplace=True)
    data.replace({pd.NaT: None}, inplace=True)
    data.fillna(0.0, inplace=True)

    #print(data.loc[0, :])
    subset = data[
        ["VendorID", "tpep_pickup_datetime", "tpep_dropoff_datetime", "passenger_count", "trip_distance", "RatecodeID",
         "store_and_fwd_flag", "PULocationID", "DOLocationID", "payment_type", "fare_amount", "extra", "mta_tax",
         "tip_amount", "tolls_amount", "improvement_surcharge", "total_amount", "congestion_surcharge", "airport_fee"]]

    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    cur.executemany(
        "INSERT INTO yellow_tripdata(vendorID,tpep_pickup_datetime,tpep_dropoff_datetime ,passenger_count ,"
        "trip_distance,ratecodeID,store_and_fwd_flag ,pULocationID ,dOLocationID,payment_type,fare_amount,extra,"
        "mta_tax,tip_amount,tolls_amount,improvement_surcharge , total_amount,congestion_surcharge,airport_fee) "
        "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
        tuples)
    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


def load_green_data_2():
    data = pq.read_pandas('green_tripdata_2019-02.parquet').to_pandas()
    data = data.drop('ehail_fee', axis=1)
    #data = data.head(1000000)
    data = data[1000000::]
    # print(data.loc[:])
    # print(data.loc[:])
    # print(data.loc[:, 'VendorID'])
    data = data.replace(r'', 0.0)

    data['lpep_pickup_datetime'] = pd.to_datetime(data.lpep_pickup_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                  errors='coerce')
    data['lpep_dropoff_datetime'] = pd.to_datetime(data.lpep_dropoff_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                   errors='coerce')
    #data.dropna(subset=['lpep_dropoff_datetime'], inplace=True)
    #data.dropna(subset=['lpep_pickup_datetime'], inplace=True)
    data.replace({pd.NaT: None}, inplace=True)
    data.fillna(0.0, inplace=True)
    subset = data[["VendorID", "lpep_pickup_datetime", "lpep_dropoff_datetime", "store_and_fwd_flag", "RatecodeID",
                   "PULocationID", "DOLocationID", "passenger_count", "trip_distance", "fare_amount", "extra",
                   "mta_tax",
                   "tip_amount", "tolls_amount", "improvement_surcharge", "total_amount", "payment_type",
                   "trip_type", "congestion_surcharge"]]

    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    cur.executemany("INSERT INTO green_tripdata(vendorID,ipep_pickup_datetime,ipep_dropoff_datetime,"
                    "store_and_fwd_flag,ratecodeID,pULocationID,dOLocationID,passenger_count,trip_distance,"
                    "fare_amount, extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount,"
                    "payment_type,trip_type,congestion_surcharge) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
                    "%s,%s,%s,%s,%s)", tuples)
    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


def load_green_data_3():
    data = pq.read_pandas('green_tripdata_2019-03.parquet').to_pandas()
    data = data.drop('ehail_fee', axis=1)
    #data = data.head(1000000)
    data = data[1000000::]
    # print(data.loc[:])
    # print(data.loc[:])
    # print(data.loc[:, 'VendorID'])
    data = data.replace(r'', 0.0)

    data['lpep_pickup_datetime'] = pd.to_datetime(data.lpep_pickup_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                  errors='coerce')
    data['lpep_dropoff_datetime'] = pd.to_datetime(data.lpep_dropoff_datetime, format='%Y-%m-%d %H:%M:%S.%f',
                                                   errors='coerce')
    # data.dropna(subset=['lpep_dropoff_datetime'], inplace=True)
    # data.dropna(subset=['lpep_pickup_datetime'], inplace=True)
    data.replace({pd.NaT: None}, inplace=True)
    data.fillna(0.0, inplace = True)

    subset = data[["VendorID", "lpep_pickup_datetime", "lpep_dropoff_datetime", "store_and_fwd_flag", "RatecodeID",
                   "PULocationID", "DOLocationID", "passenger_count", "trip_distance", "fare_amount", "extra",
                   "mta_tax",
                   "tip_amount", "tolls_amount", "improvement_surcharge", "total_amount", "payment_type",
                   "trip_type", "congestion_surcharge"]]

    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()

    '''for tup in tuples:
        print(tup)
        cur.execute("INSERT INTO green_tripdata(vendorID,ipep_pickup_datetime,ipep_dropoff_datetime,"
                    "store_and_fwd_flag,ratecodeID,pULocationID,dOLocationID,passenger_count,trip_distance,"
                    "fare_amount, extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount,"
                    "payment_type,trip_type,congestion_surcharge) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
                    "%s,%s,%s,%s,%s)", tup)'''

    cur.executemany("INSERT INTO green_tripdata(vendorID,ipep_pickup_datetime,ipep_dropoff_datetime,"
                    "store_and_fwd_flag,ratecodeID,pULocationID,dOLocationID,passenger_count,trip_distance,"
                    "fare_amount, extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount,"
                    "payment_type,trip_type,congestion_surcharge) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"
                    "%s,%s,%s,%s,%s)", tuples)
    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


def load_fhvhv_data_2():
    data = pq.read_pandas('fhvhv_tripdata_2019-02.parquet').to_pandas()
    # drop empty column
    #data = data.head(1000000)
    data = data[1000000::]
    data = data.head(7000000)
    # print(data.loc[:])
    data = data.drop('airport_fee', axis=1)
    data = data.drop('wav_match_flag', axis=1)
    data.replace({pd.NaT: None}, inplace=True)
    # print(data.loc[:, 'on_scene_datetime'])
    # print(data.loc[:, 'on_scene_datetime'])

    # data and time format
    data['request_datetime'] = pd.to_datetime(data.request_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')
    data['on_scene_datetime'] = pd.to_datetime(data.on_scene_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')
    data['pickup_datetime'] = pd.to_datetime(data.pickup_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')
    data['dropoff_datetime'] = pd.to_datetime(data.dropoff_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')

    data.replace({pd.NaT: None}, inplace=True)
    #data.fillna(0.0, inplace=True)

    subset = data[["hvfhs_license_num",
                   "dispatching_base_num",
                   "originating_base_num",
                   "request_datetime",
                   "on_scene_datetime",
                   "pickup_datetime",
                   "dropoff_datetime",
                   "PULocationID",
                   "DOLocationID",
                   "trip_miles",
                   "trip_time",
                   "base_passenger_fare",
                   "tolls",
                   "bcf",
                   "sales_tax",
                   "congestion_surcharge",
                   "tips",
                   "driver_pay",
                   "shared_request_flag",
                   "shared_match_flag",
                   "access_a_ride_flag",
                   "wav_request_flag"]]

    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    cur.executemany("INSERT INTO fhvhv_tripdata(hvfhs_license_num,dispatching_base_num,originating_base_num,"
                    "request_datetime,on_scene_datetime,pickup_datetime,dropoff_datetime,pUlocationID,dOLocationID,"
                    "trip_miles,trip_time,base_passenger_fare,tolls,bcf,sales_tax,congestion_surcharge,tips,driver_pay,"
                    "shared_request_flag,shared_match_flag,access_a_ride_flag,wav_request_flag) VALUES (%s,%s,%s,%s,"
                    "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", tuples)
    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


def load_fhvhv_data_3():
    data = pq.read_pandas('fhvhv_tripdata_2019-03.parquet').to_pandas()
    # drop empty column
    #data = data.head(1000000)
    data = data[1000000::]
    data = data.head(7000000)
    # print(data.loc[:])
    data = data.drop('airport_fee', axis=1)
    data = data.drop('wav_match_flag', axis=1)
    data.replace({pd.NaT: None}, inplace=True)
    # print(data.loc[:, 'on_scene_datetime'])
    # print(data.loc[:, 'on_scene_datetime'])

    # data and time format
    data['request_datetime'] = pd.to_datetime(data.request_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')
    data['on_scene_datetime'] = pd.to_datetime(data.on_scene_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')
    data['pickup_datetime'] = pd.to_datetime(data.pickup_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')
    data['dropoff_datetime'] = pd.to_datetime(data.dropoff_datetime, format='%Y-%m-%d %H:%M:%S.%f', errors='coerce')

    data.replace({pd.NaT: None}, inplace=True)
    # print(data.loc[:, 'on_scene_datetime'])

    subset = data[["hvfhs_license_num",
                   "dispatching_base_num",
                   "originating_base_num",
                   "request_datetime",
                   "on_scene_datetime",
                   "pickup_datetime",
                   "dropoff_datetime",
                   "PULocationID",
                   "DOLocationID",
                   "trip_miles",
                   "trip_time",
                   "base_passenger_fare",
                   "tolls",
                   "bcf",
                   "sales_tax",
                   "congestion_surcharge",
                   "tips",
                   "driver_pay",
                   "shared_request_flag",
                   "shared_match_flag",
                   "access_a_ride_flag",
                   "wav_request_flag"]]

    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    cur.executemany("INSERT INTO fhvhv_tripdata(hvfhs_license_num,dispatching_base_num,originating_base_num,"
                    "request_datetime,on_scene_datetime,pickup_datetime,dropoff_datetime,pUlocationID,dOLocationID,"
                    "trip_miles,trip_time,base_passenger_fare,tolls,bcf,sales_tax,congestion_surcharge,tips,driver_pay,"
                    "shared_request_flag,shared_match_flag,access_a_ride_flag,wav_request_flag) VALUES (%s,%s,%s,%s,"
                    "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", tuples)
    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


if __name__ == "__main__":
    #load_yellow_data_2()#500k
    #load_yellow_data_3()
    #load_green_data_2()#????
    #load_green_data_3()  # ???
    load_fhvhv_data_2()
    load_fhvhv_data_3()
