from os import name
from memory_profiler import profile
import pandas as pd
import numpy as np
import uuid, time, gc, psycopg2.extras

from database_populator import Db
from multiprocessing import Process

psycopg2.extras.register_uuid()



@profile
def load_name_basic():
    data = pd.read_csv('name.basics.tsv', sep='\t', header=0, skipinitialspace=True, skip_blank_lines=True, converters={'nconst': str.strip, 'primaryname' : str.strip, 'birthYear' : str.strip, 'deathYear' : str.strip, 'primaryProfession' : str.strip})
    size = len(data.index)
    data = data.replace("\\N", None)
    data = data.replace(np.nan, None)
    data['knownForTitles'].replace('', np.nan, inplace=True)
    data.dropna(subset=['knownForTitles'], inplace=True)
    subset = data[["nconst", "primaryName", "birthYear", "deathYear", "primaryProfession"]]
    tuples = [tuple(x) for x in subset.to_numpy()]
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    cur.executemany("INSERT INTO name_basic(nconst, primary_name, birth_year, death_year, primary_profession) VALUES (%s,%s,%s,%s,%s)", tuples)
    cur.execute("select count(*) from name_basic")
    ignored_tuples = size - cur.fetchone()[0]
    with open("log.txt", "a+") as file:
        file.write("\nName_Basic: Ignored rows = {}\n".format(ignored_tuples))

    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()



@profile
def load_title_basic():
    data = pd.read_csv('title.basics.tsv', sep='\t', header=0, skipinitialspace=True, skip_blank_lines=True, converters={"tconst" : str.strip, "titleType" : str.strip, "primaryTitle" : str.strip, "originalTitle" : str.strip, "isAdult" : str.strip, "startYear" : str.strip, "endYear" : str.strip, "runtimeMinutes" : str.strip, "genres" : str.strip})
    size = len(data.index)
    data = data.replace("\\N", None)
    data = data.replace(np.nan, None)
    data['genres'].replace('', np.nan, inplace=True)
    data.dropna(subset=['genres'], inplace=True)
    subset = data[["tconst", "titleType", "primaryTitle", "originalTitle", "isAdult", "startYear", "endYear", "runtimeMinutes", "genres"]]
    tuples = [tuple(x) for x in subset.to_numpy()]
    print(tuples[:5])
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()

    cur.executemany("INSERT INTO title_basic(tconst, title_type, primary_title, original_title, is_adult, start_year, end_year, runtime_minutes, genres) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)", tuples)
    cur.execute("select count(*) from title_basic")
    ignored_tuples = size - cur.fetchone()[0]
    with open("log.txt", "a+") as file:
        file.write("\nTitle_Basic: Ignored rows = {}\n".format(ignored_tuples))

    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()



@profile
def load_imdb_movie_details():
    data = pd.read_json('IMDB_movie_details.json', lines=True)
    size = len(data.index)
    data = data.replace(np.nan, None)
    data.replace('', None, inplace=True)

    data['movie_id'] = data['movie_id'].str.replace('\W', '')

    data['tconst'] = data['movie_id']
    data['release_date'] = pd.to_datetime(data.release_date, format='%Y/%m/%d', errors='coerce')
    data.dropna(subset=['release_date'], inplace=True)

    subset = data[["movie_id", "plot_summary", "rating", "release_date", "plot_synopsis", "tconst"]]
    tuples = [tuple(x, ) for x in subset.to_numpy()]
    print(tuples[0])
    print("Tuples prepared")

    db = Db()
    cur = db.get_cursor()

    cur.executemany("INSERT INTO imdb_movie_details(movie_id, plot_summary, rating, release_date, plot_synopsis, tconst) VALUES (%s,%s,%s,%s,%s,%s)", tuples)
    cur.execute("select count(*) from imdb_movie_details")
    ignored_tuples = size - cur.fetchone()[0]
    with open("log.txt", "a+") as file:
        file.write("\nImdb_Movie_Details: Ignored rows = {}\n".format(ignored_tuples))

    del data
    del subset
    del tuples
    del db
    del cur
    gc.collect()


@profile
def load_title_basics_name_basics():
    data = pd.read_csv('name.basics.tsv', sep='\t', header=0, skipinitialspace=True, skip_blank_lines=True, converters={'nconst': str.strip, 'primaryname' : str.strip, 'birthYear' : str.strip, 'deathYear' : str.strip, 'primaryProfession' : str.strip})
    data = data.replace("\\N", None)
    data = data.replace(np.nan, None)
    data['knownForTitles'].replace('', np.nan, inplace=True)
    data.dropna(subset=['knownForTitles'], inplace=True)
    data = data[["nconst", "knownForTitles"]]

    tuples = [ (uuid.uuid4(), x[0], tconst) for x in data.to_numpy() for tconst in x[1].split(",")]
    print(len(tuples))
    db = Db()
    cur = db.get_cursor()
    for i in range(0,len(tuples), 1000000):
        j = i + 1000000
        if(j >= len(tuples)):
            j = len(tuples)
        values = ",".join(cur.mogrify("(%s,%s,%s)", x).decode("utf-8") for x in tuples[i: j])

        
        cur.execute("INSERT INTO title_basics_name_basics(id, nconst, tconst) select * from ( values {} ) as x(id, nconst, tconst) where exists (select 1 from title_basic where tconst = x.tconst)".format(values))

    del data
    del tuples
    del db
    del cur
    gc.collect()


@profile
def load_title_principal(end=None, init=0):
    if end is not None:
        data = pd.read_csv('title.principals.tsv', sep='\t', header=0, skipinitialspace=True, skip_blank_lines=True, converters={"tconst" : str.strip, "ordering" : str.strip, "nconst" : str.strip, "category" : str.strip, "job" : str.strip, "characters" : str.strip}, skiprows=init, nrows=end)
    else:
        data = pd.read_csv('title.principals.tsv', sep='\t', header=None, skipinitialspace=True, skip_blank_lines=True, converters={"tconst" : str.strip, "ordering" : str.strip, "nconst" : str.strip, "category" : str.strip, "job" : str.strip, "characters" : str.strip}, skiprows=init)
        data.columns = ["tconst", "ordering", "nconst", "category", "job", "characters"]

    data = data.replace("\\N", None)
    data = data.replace(np.nan, None)
    data['characters'] = data['characters'].str.replace('[', '').str.replace(']', '').replace('"')

    data['tconst'] = data['tconst'].str.replace('\W', '')
    data['nconst'] = data['nconst'].str.replace('\W', '')

    subset = data[["tconst", "ordering", "nconst", "category", "job", "characters"]]
    tuples = [(uuid.uuid4(), x[0], int(x[1]), x[2], x[3], x[4], x[5]) for x in subset.to_numpy()]
    print(tuples[0])
    print("Tuples prepared")

    db = Db()
    cur = db.get_cursor()
    for i in range(0,len(tuples), 1000000):
        j = i + 1000000
        if(j >= len(tuples)):
            j = len(tuples)
        values = ",".join(cur.mogrify("(%s,%s,%s,%s,%s,%s,%s)", x).decode("utf-8") for x in tuples[i: j])
        cur.execute("INSERT INTO title_principal(id, tconst, ordering_, nconst, category, job, character_) select * from ( values {} ) as x(id, tconst, ordering_, nconst, category, job, character_) where exists (select 1 from title_basic t, name_basic n where t.tconst = x.tconst and n.nconst = x.nconst)".format(values))

    del data
    del subset
    del tuples
    del db
    del cur
    del values
    gc.collect()


@profile
def load_title_rating():
    data = pd.read_csv('title.ratings.tsv', sep='\t', header=0, skipinitialspace=True, skip_blank_lines=True, converters={"tconst" : str.strip, "averagerating" : str.strip, "numVotes" : str.strip})
    size = len(data.index)
    data = data.replace("\\N", None)
    data = data.replace(np.nan, None)
    data['tconst'] = data['tconst'].str.replace('\W', '')
    subset = data[["tconst", "averageRating", "numVotes"]]
    tuples = [(uuid.uuid1(), x[0], x[1], int(x[2])) for x in subset.to_numpy()]
    print(tuples[:5])
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    for i in range(0,len(tuples), 1000000):
        j = i + 1000000
        if(j >= len(tuples)):
            j = len(tuples)
        print(i, j)
        values = ",".join(cur.mogrify("(%s,%s,%s,%s)", x).decode("utf-8") for x in tuples[i: j])
        cur.execute("INSERT INTO title_rating(id_, tconst, average_rating, num_votes) select * from ( values {} ) as x(id_, tconst, average_rating, num_votes) where exists (select 1 from title_basic t where t.tconst = x.tconst)".format(values))
    cur.execute("select count(*) from title_rating")
    ignored_tuples = size - cur.fetchone()[0]
    with open("log.txt", "a+") as file:
        file.write("\nTitle_Rating: Ignored rows = {}\n".format(ignored_tuples))

    del data
    del subset
    del tuples
    del db
    del cur
    del values
    gc.collect()


@profile
def load_title_akas():
    data = pd.read_csv('title.akas.tsv', sep='\t', header=0, skipinitialspace=True, skip_blank_lines=True, converters={"titleId" : str.strip, "ordering" : str.strip, "title" : str.strip, "region" : str.strip, "language" : str.strip, "types" : str.strip, "attributes" : str.strip, "isOriginalTitle" : str.strip})
    size = len(data.index)
    data = data.replace("\\N", None)
    data = data.replace(np.nan, None)
    data["isOriginalTitle"].fillna(value=0, inplace=True)
    data['titleId'] = data['titleId'].str.replace('\W', '')
    subset = data[["titleId", "ordering", "title", "region", "language", "types", "attributes", "isOriginalTitle"]]
    tuples = [(uuid.uuid4(), x[0], int(x[1]), x[2], x[3], x[4], x[5], x[6], int(x[7])) for x in subset.to_numpy()]
    print(tuples[:5])
    print("Tuples prepared")
    db = Db()
    cur = db.get_cursor()
    for i in range(0,len(tuples), 1000000):
        j = i + 1000000
        if(j >= len(tuples)):
            j = len(tuples)
        print(i, j)
        values = ",".join(cur.mogrify("(%s,%s,%s,%s,%s,%s,%s,%s,%s)", x).decode("utf-8") for x in tuples[i: j])
        cur.execute("INSERT INTO title_akas(id_, title_id, ordering_, title, region, language_, types_, attributes_, is_original_title) select * from ( values {} ) as x(id_, title_id, ordering_, title, region, language_, types_, attributes_, is_original_title) where exists (select 1 from title_basic t where t.tconst = x.title_id)".format(values))
    cur.execute("select count(*) from title_akas")
    ignored_tuples = size - cur.fetchone()[0]
    with open("log.txt", "a+") as file:
        file.write("\nTitle_Akas: Ignored rows = {}\n".format(ignored_tuples))

    del data
    del subset
    del tuples
    del db
    del cur
    del values
    gc.collect()

@profile
def load_all_tables():
    st = time.time()
    db = Db()
    cur = db.get_cursor()
    try:
        cur.execute(open("db_schema_sheet3.sql", "r").read())
    except psycopg2.errors.DuplicateTable:
        print("Tables already exist. Skipping table creation.")

    name_basic_process = Process(target=load_name_basic)
    title_basic_process = Process(target=load_title_basic)

    name_basic_process.start()
    title_basic_process.start()
    name_basic_process.join()
    title_basic_process.join()

    del title_basic_process
    del name_basic_process
    gc.collect()

    imdb_process = Process(target=load_imdb_movie_details)
    known_for_process = Process(target=load_title_basics_name_basics)
    principal1_process = Process(target=load_title_principal, args=(18000000,))
    principal2_process = Process(target=load_title_principal, args=(None,18000000,))
    rating_process = Process(target=load_title_rating)
    akas_process = Process(target=load_title_akas)

    imdb_process.start()
    rating_process.start()

    imdb_process.join()
    rating_process.join()

    del imdb_process
    del rating_process
    gc.collect()

    principal1_process.start()
    principal1_process.join()

    del principal1_process
    gc.collect()

    principal2_process.start()
    principal2_process.join()

    
    del principal2_process
    gc.collect()

    akas_process.start()
    akas_process.join()

    del akas_process
    gc.collect()

    known_for_process.start()
    known_for_process.join()

    del known_for_process
    gc.collect()

    try:
        cur.execute(open("data_staging.sql", "r").read())
    except:
        print("Error in transforming tables. Run data_staging.sql manually.")

    # Codeblock goes here.
    et = time.time()
    elapsed_time = et - st
    print('Execution time:', elapsed_time, 'seconds')
    with open("log.txt", "a+") as file:
        file.write("\n'Execution time: {} seconds'\n".format(elapsed_time))


if __name__ == "__main__":
    load_all_tables()