memory-profiler==0.60.0
numpy==1.22.4
pandas==1.4.2
psutil==5.9.1
psycopg2==2.9.3
python-dateutil==2.8.2
pytz==2022.1
six==1.16.0
