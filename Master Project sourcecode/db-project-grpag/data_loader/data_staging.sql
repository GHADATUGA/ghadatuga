CREATE TABLE title as
select t.tconst, t.title_type, t.primary_title, t.original_title, t.is_adult, t.start_year, t.end_year, t.runtime_minutes, t.genres, 
                i.plot_summary, i.release_date, i.plot_synopsis, i.rating, r.average_rating, r.num_votes  from title_basic t left join 
                imdb_movie_details i on i.movie_id=t.tconst left join title_rating r on t.tconst = r.tconst;

ALTER TABLE title
    ADD PRIMARY KEY (tconst);

ALTER TABLE name_basic
  RENAME TO name;

ALTER TABLE title_principal DROP CONSTRAINT title_principal_tconst_fkey;
ALTER TABLE title_principal ADD CONSTRAINT title_principal_tconst_fkey FOREIGN KEY (tconst) REFERENCES title(tconst);

ALTER TABLE title_akas DROP CONSTRAINT title_akas_title_id_fkey;
ALTER TABLE title_akas ADD CONSTRAINT title_akas_title_id_fkey FOREIGN KEY (title_id) REFERENCES title(tconst);

ALTER TABLE title_basics_name_basics
  RENAME TO titles_names;

ALTER TABLE titles_names DROP CONSTRAINT title_basics_name_basics_tconst_fkey;
ALTER TABLE titles_names ADD CONSTRAINT title_basics_name_basics_tconst_fkey FOREIGN KEY (tconst) REFERENCES title(tconst);

DROP TABLE title_rating;
DROP TABLE imdb_movie_details;
DROP TABLE title_basic;

