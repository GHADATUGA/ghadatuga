import psycopg2
from psycopg2 import Error

class Db:

    cursor = []
    connection = None

    def __init__(self):
        try:
            # Connect to an existing database
            connection = psycopg2.connect(user="postgres",
                                        password="Temp2022",
                                        host="127.0.0.1",
                                        port="5432",
                                        database="postgres")
            connection.autocommit = True

            self.connection = connection

        except (Exception, Error) as error:
            raise error

    def get_connection(self):
        return self.connection

    def get_cursor(self):
        cursor = self.connection.cursor()
        self.cursor.append(cursor)
        return cursor

    def close_connection(self):
        if (self.connection):
            for cur in self.cursor:
                cur.close()
            self.connection.close()
            print("PostgreSQL connection is closed")

