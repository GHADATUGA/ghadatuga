# DB Project GrpAG

DB Project Repo for Group AG

## Requirements

Maven

Docker

Make sure you don't have any service running at port 8080.

## Steps to Run the Application


1. In the project, go to worksheet-1 in a terminal. 
    ```cd ./worksheet-1```
2. Run:
```
    mvn clean install
    docker-compose up
```
The App and the database should be up and running.

Now the app is ready to be used. Go to http://localhost:8080 on a browser (If not using Docker Toolkit) to access the app.

All the Exercises can be accessed directly from "/".

If you want to access an execise in particular, navigate to:

/dbQueries : Exercise 2

/dbFunctions : Exercise 3

/dbSearch : Exercise 4

*Docker Toolkit is for older OSs which dont have full support of OS and hence the toolkit provides its own Linux based VM which is not bridged to the System. Hence, in that case the app would be accessible at the address where the Docker Toolkit is Running.

(To delete the containers run ``` docker-compose down``` in ./worksheet-1)

