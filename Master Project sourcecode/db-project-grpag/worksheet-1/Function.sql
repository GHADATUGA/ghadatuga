-- 3 a) Format Address
-- Given an address id, return its string representation in the format “username@domain”
create function FormatAddress(identifier int)
    RETURNS varchar(40) AS $$
select CONCAT(username,'@',domain) from address where adid=identifier;
$$ LANGUAGE sql ;

-- 3 b)Format User: Given an euser id, return the name of the user
-- depending on which type the euser belongs to
-- if euser is a person, it returns both names and nickname in brackets
-- else if euser is an organization it returns firm name and nickname in brackets
-- else it returns nickname without brackets

CREATE FUNCTION FormatUser( identifier int ) RETURNS varchar(40) AS $$
DECLARE qty int ; results varchar(40);
BEGIN SELECT euid INTO qty FROM person h WHERE  h.euid=identifier ;
if qty is not null then  select CONCAT(per.first_name,' ',per.surname,'(',per.nickname,')')
                         into results  from person per where per.euid=identifier ;
return results;
end if;
if qty is null then SELECT euid INTO qty FROM organization o WHERE  o.euid=identifier;
end if;
 if qty is not null then  select CONCAT(o.firm,' ','(',nickname,')')into results
                          from organization o where o.euid=identifier;

else  select nickname into results from euser eu where eu.euid=identifier;
end if;
RETURN results;
END $$ LANGUAGE plpgsql ;


-- 3c) a table function which computes an address book for a specific user.
    -- The function takes a euser id as input parameter and
    -- return the computed address book as a table with usernames and email addresses as columns
    create or replace function AddressBook(euserid int)
 RETURNS TABLE(username varchar ,addresse text )LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (select ads.username,CONCAT(ads.username,'@',ads.domain)
                             from use u,address ads where u.euser=euserid and u.address=ads.adid

	    union
	select ads5.username,CONCAT(ads5.username,'@',ads5.domain)
                             from address ads4,email em3, use u3, address ads5, addressee a1
	   where u3.euser= euserid and u3.address=ads4.adid and
	   em3._from=ads4.adid  and a1.email= em3.id and ads5.adid=a1._to
	union
		select ads3.username,CONCAT(ads3.username,'@',ads3.domain)
                             from use u2,address ads2,addressee addse1,email em1,address ads3
		where u2.euser=euserid and u2.address=ads2.adid and ads2.adid=addse1._to
		and addse1.email= em1.id and em1._from=ads3.adid
    );
END $func$ ;


-- 4: Email Attachment Search
-- exercise 4: Parameter are nickname, startdate , EndDate and keyword
-- this function will be called by TableToSearchIn function
-- As you use you have to call SearchKeyWord function with
-- Nickname startdate (like 2012-01-10), EndDate and Keyword
--Example: select * from EmailAttachment('fool875','2009-05-17','2012-01-10','or')
create or replace function NicknameInInterval(Nickn varchar, StartDate timestamp,EndDate timestamp)
 RETURNS TABLE(id int,
			   nickname varchar,
			   filename varchar,
			   subject varchar ,
			   content varchar,
			   addresse text )LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (select  em.id,eus.nickname,em.filename,em.subject,em.content, CONCAT(ads.username,'@',ads.domain) as addresse
	from email em, address ads, euser eus, use u where
		em.date>=StartDate and em.date<=EndDate
		and em._from=ads.adid and ads.adid=u.address
	and u.euser=eus.euid and eus.nickname ilike Nickn

	 union

	  select em2.id,eus2.nickname,em2.filename,em2.subject,em2.content,CONCAT(add2.username,'@',add2.domain) as addresse
	   from euser eus2, use u2,address add2,addressee adsse,email em2   where eus2.nickname ilike Nickn and eus2.euid=u2.euser
	           and add2.adid=u2.address and
	           adsse._to=add2.adid and adsse.email=em2.id and em2.date>=StartDate and em2.date<=EndDate );

END $func$ ;

-- This function will be called by SearchKeyWord
create or replace function TableToSearchIn(Nickn varchar, StartDate timestamp,
										   EndDate timestamp )
 RETURNS TABLE(id int,
			   nickname varchar,
			   addresse text,
			   Efilename varchar,
			   Afilename varchar,
			   Esubject varchar ,
			   Asubject varchar,
			   content varchar )LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (-- emails and picture
    select nick.id,nick.nickname,nick.addresse,nick.filename,pic.filename ,
                   nick.subject,pic.subject,nick.content
                       from picture pic right join NicknameInInterval(Nickn,StartDate,EndDate) nick
 on nick.id=pic.contained_in
 union
-- join with video and email
select nickInt.id,nickInt.nickname,nickInt.addresse,nickInt.filename,vid.filename,
                   nickInt.subject,vid.subject,nickInt.content
                       from video vid right join NicknameInInterval(Nickn,StartDate,EndDate) nickInt
 on nickInt.id=vid.contained_in
 union
-- join with text
select nickIn.id,nickIn.nickname,nickIn.addresse,nickIn.filename,tex.filename,
                   nickIn.subject,tex.subject,nickIn.content
                       from text tex right join  NicknameInInterval(Nickn,StartDate,EndDate) nickIn
 on nickIn.id=tex.contained_in
 union
-- join with attachment
select nickI.id,nickI.nickname,nickI.addresse,nickI.filename,att.filename,
                   nickI.subject,att.subject,nickI.content
                       from attachment att right join NicknameInInterval(Nickn,StartDate,EndDate) nickI
 on nickI.id=att.contained_in) ;
END $func$ ;

-- this is the overall function which calls all other functions
-- then it returns the tuples, which contain the sepecified keyword

create or replace function EmailAttachment(Nickn varchar, StartDate timestamp,
										 EndDate timestamp,KeyWord varchar)
 RETURNS TABLE(id int,
			   addresse text,
			   efilename varchar,
			   esubject varchar)LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (select res.id,res.addresse,res.efilename,res.esubject from TableToSearchIn(Nickn,StartDate,EndDate)res  where
       res.esubject ilike concat('%', KeyWord, '%') or
		res.content ilike concat('%', KeyWord, '%') or
		res.efilename ilike concat('%', KeyWord, '%') or
		res.afilename ilike concat('%', KeyWord, '%') or
		res.asubject ilike concat('%', KeyWord, '%')
    );
END $func$ ;
