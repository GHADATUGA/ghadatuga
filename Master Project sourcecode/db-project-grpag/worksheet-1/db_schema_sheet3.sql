CREATE TABLE name_basic(
                           nconst varchar(40) PRIMARY KEY NOT NULL,
                           primary_name varchar,
                           birth_year integer,
                           death_year integer,
                           primary_profession varchar
);
CREATE TABLE title_basic(
                           tconst varchar(40) PRIMARY KEY NOT NULL,
                           title_type varchar(40),
                           primary_title varchar,
                           original_title varchar,
                           is_adult integer,
                           start_year integer,
                           end_year integer,
                           runtime_minutes integer,
                           genres varchar
);
CREATE TABLE imdb_movie_details (
                                  movie_id varchar(40) PRIMARY KEY NOT NULL,
                                  plot_summary varchar,
                                  rating NUMERIC(5,2),
                                  release_date date NOT NULL,
                                  plot_synopsis varchar,
                                  tconst varchar (40) REFERENCES title_basic(tconst)
);
CREATE TABLE title_akas(
                          id_ uuid PRIMARY KEY NOT NULL,
                          title_id varchar(20) REFERENCES title_basic(tconst),
                          ordering_ integer,
                          title varchar,
                          region varchar,
                          language_ varchar,
                          types_ varchar,
                          attributes_ varchar,
                          is_original_title integer
);
CREATE TABLE title_rating(
                            id_ uuid PRIMARY KEY NOT NULL,
                            tconst varchar(20)REFERENCES title_basic(tconst),
                            average_rating NUMERIC(5,2),
                            num_votes integer
);
CREATE TABLE title_principal(
                               id uuid PRIMARY KEY NOT NULL,
                               tconst varchar(20) REFERENCES title_basic(tconst),
                               ordering_ integer,
                               nconst varchar(40) REFERENCES name_basic(nconst),
                               category varchar(40),
                               job varchar,
                               character_ varchar
);
CREATE TABLE title_basics_name_basics(
                             id uuid PRIMARY KEY NOT NULL,
                             nconst varchar(40) REFERENCES name_basic(nconst),
                             tconst varchar(20)REFERENCES title_basic(tconst)
);
