package com.tukl.dbproject.worksheet1.repository.worksheet3;

import com.tukl.dbproject.worksheet1.entities.worksheet3.LogData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LogDataRepository extends JpaRepository<LogData, UUID> {
    Optional<LogData> findByKeyAndSearchType(String key, String searchType);

    List<LogData> findAllBySearchTypeEqualsAndKeyContainsIgnoreCase(String searchType, String key);
}