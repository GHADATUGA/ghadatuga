package com.tukl.dbproject.worksheet1.dto;

import com.tukl.dbproject.worksheet1.projections.EmailAttachment;

import lombok.Data;

import java.util.List;
/**
 * Class to map generic repository for EmailAttachment interface to data
 */
@Data
public class EmailAttachmentDTO {
    public EmailAttachmentDTO(List<EmailAttachment> emailAttachment, String query){
        super();
        this.data=emailAttachment;
        this.query=query;
    }
    List<EmailAttachment> data;
    String query;

}
