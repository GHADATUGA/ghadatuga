package com.tukl.dbproject.worksheet1.projections;

/**
* Interface for Top Ten of the Eusers who have the shortest response time
* without using olap functions
 */
public interface TopTen {
    String getNickname();
    int getRank();
}
