package com.tukl.dbproject.worksheet1.projections;
import java.util.Date;
/**
 * Interface for Thread decomposition. For finding the largest thread
 */
public interface ThreadDecomposition {
    int getThreadid();
    int getId();
    String getUsername();
    Integer getInreplyto();
    int getDepth();
    Date getDate();
}
