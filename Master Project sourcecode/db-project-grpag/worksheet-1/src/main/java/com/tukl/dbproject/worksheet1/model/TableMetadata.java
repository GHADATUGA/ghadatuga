package com.tukl.dbproject.worksheet1.model;

import lombok.Data;

@Data
public class TableMetadata {

    String name;
    String type;
    String size;
    String isNullable;
    String isAutoIncrement;

    public TableMetadata(String name, String type, String size, String isNullable, String isAutoIncrement) {
        this.name = name;
        this.size = size;
        this.type = type;
        this.isNullable = isNullable;
        this.isAutoIncrement = isAutoIncrement;
    }
}
