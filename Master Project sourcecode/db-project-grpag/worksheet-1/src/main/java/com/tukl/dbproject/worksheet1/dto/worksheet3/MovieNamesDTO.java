package com.tukl.dbproject.worksheet1.dto.worksheet3;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieNamesDTO {
    private String titleType;

    private String primaryTitle;

    private String originalTitle;

    private boolean isAdult;

    private Integer startYear;

    private Integer endYear;

    private Integer runtimeMinutes;

    private String genres;

    private String plotSummary;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date releaseDate;

    private String plotSynopsis;

    private BigDecimal rating;

    private BigDecimal averageRating;

    private Integer numVotes;

    List<String> casts;

//    public MovieNamesDTO(String titleType, String primaryTitle, String originalTitle, boolean isAdult, Integer startYear, Integer endYear, Integer runtimeMinutes, String genres, String plotSummary, LocalDate releaseDate, String plotSynopsis, BigDecimal rating, BigDecimal averageRating, Integer numVotes, List<String> casts) {
//        this.titleType = titleType;
//        this.primaryTitle = primaryTitle;
//        this.originalTitle = originalTitle;
//        this.isAdult = isAdult;
//        this.startYear = startYear;
//        this.endYear = endYear;
//        this.runtimeMinutes = runtimeMinutes;
//        this.genres = genres;
//        this.plotSummary = plotSummary;
//        this.releaseDate = releaseDate;
//        this.plotSynopsis = plotSynopsis;
//        this.rating = rating;
//        this.averageRating = averageRating;
//        this.numVotes = numVotes;
//        this.casts = casts;
//    }
}
