package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.UserTypeCountMapping;

import lombok.Data;

/**
 * Class to map generic repository for User types to data
 */
@Data
public class UserTypeCountMappingDTO {

	
	
	public UserTypeCountMappingDTO(List<UserTypeCountMapping> userTypeCountMapping, String query) {
		super();
		this.data = userTypeCountMapping;
		this.query = query;
	}
	List<UserTypeCountMapping> data;
	String query;
}
