package com.tukl.dbproject.worksheet1.services;

import com.tukl.dbproject.worksheet1.entities.worksheet3.LogData;

import java.util.List;

public interface LogDataService {

    public LogData addLog(String key, String searchType);

    public List<LogData> getAlikeLogData(LogData logData);
}
