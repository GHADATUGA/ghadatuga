package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.TopTenUsingOlap;

import lombok.Data;
/**
 * Class to map generic repository for TopTenUsingOlap Interface to data
 */
@Data
public class TopTenUsingOlapDTO {

	List<TopTenUsingOlap> data;
	String query;
	public TopTenUsingOlapDTO(List<TopTenUsingOlap> topTenUsingOlap, String query) {
		super();
		this.data = topTenUsingOlap;
		this.query = query;
	}
}
