package com.tukl.dbproject.worksheet1.config;

import com.tukl.dbproject.worksheet1.entities.AuthGrantedAuthority;
import com.tukl.dbproject.worksheet1.entities.UserAuthDetails;
import com.tukl.dbproject.worksheet1.repository.AuthGrantedAuthorityRepository;
import com.tukl.dbproject.worksheet1.repository.UserAuthDetailsRepository;
import com.tukl.dbproject.worksheet1.services.JpaUserDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class UserPopulatorConfig {
    @Autowired
    private UserAuthDetailsRepository userAuthDetailsRepository;

    @Autowired
    private JpaUserDetailsManager jpaUserDetailsManager;

    @Autowired
    private AuthGrantedAuthorityRepository authGrantedAuthorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    public CommandLineRunner initializeJpaData() {
        return (args)->{

            if (jpaUserDetailsManager.userExists("amartya") || jpaUserDetailsManager.userExists("ghad")) {
                return;
            }

            AuthGrantedAuthority grantedAuthority = new AuthGrantedAuthority();
            grantedAuthority.setAuthority("USER");
            grantedAuthority = authGrantedAuthorityRepository.save(grantedAuthority);
            System.out.println(grantedAuthority.getAuthority());


            UserAuthDetails user1 = new UserAuthDetails();
            user1.setUsername("amartya");
            user1.setPassword(passwordEncoder.encode("test"));
            user1.setEnabled(true);
            user1.setCredentialsNonExpired(true);
            user1.setAccountNonExpired(true);
            user1.setAccountNonLocked(true);

            UserAuthDetails user2 = new UserAuthDetails();
            user2.setUsername("ghad");
            user2.setPassword(passwordEncoder.encode("test"));
            user2.setEnabled(true);
            user2.setCredentialsNonExpired(true);
            user2.setAccountNonExpired(true);
            user2.setAccountNonLocked(true);

            AuthGrantedAuthority auth = authGrantedAuthorityRepository.getByAuthority("USER");


            userAuthDetailsRepository.save(user2);
            userAuthDetailsRepository.save(user1);

            user2.getAuthorities().add(auth);
            user1.getAuthorities().add(auth);

            userAuthDetailsRepository.save(user2);
            userAuthDetailsRepository.save(user1);

        };

    }
}