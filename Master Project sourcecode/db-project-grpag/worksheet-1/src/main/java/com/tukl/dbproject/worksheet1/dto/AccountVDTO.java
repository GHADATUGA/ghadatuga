package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.AccountV;

import lombok.Data;

@Data
public class AccountVDTO {
	public AccountVDTO(List<AccountV> accountV) {
		this.data = accountV;
	}

	List<AccountV> data;
}
