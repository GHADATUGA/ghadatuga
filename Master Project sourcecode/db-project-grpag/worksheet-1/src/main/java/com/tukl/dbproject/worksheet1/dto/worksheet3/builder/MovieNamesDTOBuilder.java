package com.tukl.dbproject.worksheet1.dto.worksheet3.builder;

import com.tukl.dbproject.worksheet1.dto.worksheet3.MovieNamesDTO;
import com.tukl.dbproject.worksheet1.entities.worksheet3.Title;

import java.util.List;

public class MovieNamesDTOBuilder {
    public static MovieNamesDTO build(Title title, List<String> casts) {
        return new MovieNamesDTO(
                title.getTitleType(),
                title.getPrimaryTitle(),
                title.getOriginalTitle(),
                title.isAdult(),
                title.getStartYear(),
                title.getEndYear(),
                title.getRuntimeMinutes(),
                title.getGenres(),
                title.getPlotSummary(),
                title.getReleaseDate(),
                title.getPlotSynopsis(),
                title.getRating(),
                title.getAverageRating(),
                title.getNumVotes(),
                casts
        );
    }
}
