package com.tukl.dbproject.worksheet1.projections;

import java.util.Date;
/**
* Interface for who received emails a day before or on their birthday
 */
public interface BirthdayGreetings {
    String getNickname();
    Date getDateofbirth();
    int getCounts();

}
