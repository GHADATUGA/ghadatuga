package com.tukl.dbproject.worksheet1.repository;
import com.tukl.dbproject.worksheet1.projections.*;
import com.tukl.dbproject.worksheet1.entities.Attachment;
import com.tukl.dbproject.worksheet1.projections.sheet4.*;
import com.tukl.dbproject.worksheet1.queries.Queries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * The Repository to execute native queries.
 * Use this repository for executing native query. The type and primary key type
 * Specification is not significant here.
 */
@Repository
public interface GenericRepository extends JpaRepository<Attachment, Integer> {
    /**
     * Define query for Multiple Addresse
     */
    @Query(value = Queries.query1,nativeQuery = true)
    List<MultipleAdresses> getMultipleAddressesUsers();
    /**
     * Define query for UsertypeCounting
     */
    @Query(value = Queries.query2, nativeQuery = true)
    List<UserTypeCountMapping> getUserTypeCountMapping();

    /**
     * query definition for executing letter occurs least frequently
     */
    @Query(value = Queries.query3,nativeQuery = true)
    List<MostCommonlyUsedLetter> getMostCommonlyUsedLetter();

    /**
     * query definition for executing Address sharing
     */
    @Query(value = Queries.query4,nativeQuery = true)
    List<AddressSharing> getAddressSharingUsers();

    /**
     * query definition for executing Birthday Greetings
     */
    @Query(value = Queries.query5, nativeQuery = true)
    List<BirthdayGreetings> getBirthGreetings();
    /**
     * query definition for executing Topten
     */
    @Query(value = Queries.query6,nativeQuery = true)
     List<TopTen> getTopTen();
    /**
     * query definition for executing Topten variation
     */
    @Query(value = Queries.query7,nativeQuery = true)
    List<TopTenVariation> getTopTenVariation();
    /**
     * query definition for executing Top ten using Olap
     */
    @Query(value = Queries.query8,nativeQuery = true)
    List<TopTenUsingOlap> getTopTenUsingOlap();
    /**
     * query definition for executing Thread decomposition
     */
    @Query(value = Queries.query9,nativeQuery = true)
    List<ThreadDecomposition> getThreadDecompostion();


    /**
     * Defining procedure for Formatted Address (Email adress in human readable format)
     * @param adid, addresse id
     */
    @Procedure(value = "FormatAddress")
    String getFormattedAddress(int adid);
    /**
     * Defining procedure for FormatUser: person names and nickname or organization (firm)
     *  and nickname or simply nickname of eusers who are neither person nor organization
     * @param euid, Email user id
     */
    @Procedure(value ="FormatUser" )
    String getFormatUser(int euid);
    /**
     * Defining procedure for computing an address book for a specific user
     * @param euid, Email user id
     */
    @Procedure(value = "AddressBook")
    List<AddressBook> getAddressBook(int euid);
    /**
     * Defining procedure for computing Email Attachment Search
     * @param nickname, nickname of the euser
     * @param startDate, starting date
     * @param endDate, end date
     * @param keyword, search keyword
     * Startdate and Enddate are for defining the interval
     */
    @Procedure(value = "EmailAttachment")
	List<EmailAttachment> getEmailAttachment(String nickname, Timestamp startDate, Timestamp endDate, String keyword);

    //monthly passenger
    @Query(value = Queries.query41,nativeQuery = true)
    List<MonthlyPassengers> getMonthlyPassengers();

    //OnscenceArrivalTime
    @Query(value = Queries.query42,nativeQuery = true)
    List<OnscenceArrivalTime> getOnscenceArrivalTime();
    //Most expensive trip
    @Query(value = Queries.query43,nativeQuery = true)
    List<MostExpensiveTrip> getMostExpensiveTrip();

    //PaymentModelRank
    @Query(value = Queries.query44,nativeQuery = true)
    List<PaymentModelRank> getPaymentModelRank();

    //MonthlyDriverPay
    @Query(value = Queries.query45,nativeQuery = true)
    List<MonthlyDriverPay> getMonthlyDriverPay();

    //PopularCarService
    @Query(value = Queries.query46,nativeQuery = true)
    List<PopularCarService> getPopularCarService();

    //BaseLicenseNumberPerCarService (car service and the number of drivers)
    @Query(value = Queries.query47,nativeQuery = true)
    List<BaseLicenseNumberPerCarService> getBaseLicenseNumberPerCarService();

    //DistancePerTrip
    @Query(value = Queries.query48,nativeQuery = true)
    List<DistancePerTrip> getDistancePerTrip();

    //PopularPOLocation

    @Query(value = Queries.query49,nativeQuery = true)
    List<PopularPOLocation> getPopularPOLocation();

    //HoursWithMostPassengers
    @Query(value = Queries.query410,nativeQuery = true)
    List<HoursWithMostPassengers> getHoursWithMostPassengers();

}
