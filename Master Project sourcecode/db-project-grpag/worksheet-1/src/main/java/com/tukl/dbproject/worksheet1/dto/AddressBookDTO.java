package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.AddressBook;

import lombok.Data;

/**
 *Class to map Generic repository for
 */
@Data
public class AddressBookDTO {

	List<AddressBook> data;

	public AddressBookDTO(List<AddressBook> addressBook) {
		this.data = addressBook;
	}
}
