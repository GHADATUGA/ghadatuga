package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.OnscenceArrivalTime;
import lombok.Data;

import java.util.List;

/**
 * Average on Scene arrival time dto
 */
@Data
public class OnscenceArrivalTimeDTO {
    List<OnscenceArrivalTime> data;
    String query;
    public OnscenceArrivalTimeDTO(List<OnscenceArrivalTime> onscenceArrivalTimes, String query) {
        super();
        this.data = onscenceArrivalTimes;
        this.query = query;
    }
}
