package com.tukl.dbproject.worksheet1.projections.sheet4;

public interface MostExpensiveTrip {
    String getCarservice();
    int getPulocationid();
    int getDolocationid();
    float getDriverPay();
}
