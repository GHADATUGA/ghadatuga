package com.tukl.dbproject.worksheet1.projections;

/**
* Interface for the Addressbook to get username and corresponding address: exercise 3c)
 */
public interface AddressBook {
    String getUsername();
    String getAddresse();
}
