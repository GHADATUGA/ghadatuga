package com.tukl.dbproject.worksheet1.dto.worksheet3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TitleAkasDTO {
    private UUID id;

    private TitleDTO title;

    private Integer ordering;

    private String title1;

    private String region;

    private String language;

    private String types;

    private String attributes;

    private boolean isOriginalTitle;
}
