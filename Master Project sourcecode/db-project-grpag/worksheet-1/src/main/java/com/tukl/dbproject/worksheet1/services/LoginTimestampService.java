package com.tukl.dbproject.worksheet1.services;

import com.tukl.dbproject.worksheet1.entities.UserAuthDetails;
import com.tukl.dbproject.worksheet1.repository.UserAuthDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;

@Component
public class LoginTimestampService implements AuthenticationSuccessHandler, AuthenticationFailureHandler {

    @Autowired
    private JpaUserDetailsManager jpaUserDetailsManager;

    @Autowired
    UserAuthDetailsRepository userAuthDetailsRepository;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            UserAuthDetails user = (UserAuthDetails) jpaUserDetailsManager.loadUserByUsername(authentication.getName());
            user.setLastLogin(new Timestamp(System.currentTimeMillis()));
            userAuthDetailsRepository.save(user);
        }
        redirectStrategy.sendRedirect(request, response, "/imdbSearch");
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String username = request.getParameter("username");
        String error = exception.getMessage();
        request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, String.valueOf(HttpStatus.UNAUTHORIZED.value()));
        request.getRequestDispatcher("/error").forward(request, response);
    }
}
