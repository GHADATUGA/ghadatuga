package com.tukl.dbproject.worksheet1.queries;

public class Queries {


    /**
     * Exercise2 a)Multiple addresses:
     * This query represent Which users have multiple addresses
     */
    public final static String query1 ="select  nickname, count(u.address) as counts from euser e,use u \n" +
            "   where e.euid=u.euser and \n" +
            "   (select count(address)from use us where us.euser=e.euid and u.euser=e.euid)>1\n" +
            "   group by (euid,nickname)order by counts DESC, nickname DESC";

    /**
     * b)Total numbers:
     * This query retrieves How many email users of each type are registered in the system
     */
    public final static String query2 = "select type, count from (\n" +
            "\tselect 'euser' as type, count(*) from euser where euser.euid not in (select euid from person union select euid from organization)\n" +
            "\tunion select 'total', count(*) from euser \n" +
            "\tunion select 'person', count(*) from person\n" +
            "\tunion select 'organization', count(*) from organization\n" +
            ") as total_data group by count, type order by count desc;";


    /**
     * c) Most commonly used letter:
     * This query retrieves Which letter occurs least frequently
     * as the first character of attached files (filename)  and how often
     */
    public final static String query3="SELECT SUBSTRING(filename, 1, 1) as letter, count(*) as often FROM attachment \n" +
            "          GROUP BY SUBSTRING(filename, 1, 1)ORDER BY often ASC,letter DESC limit 1";

    /**
     * d) Address sharing:
     * This query retrieves Which users share an email address with the youngest person from database
     * Results contains the nickname of the persons in ascending order
     */
    public final static String query4="WITH youngest_person_address(young_addr) as\n" +
            "    (select  address as youngest_address_id from person per1, use us \n" +
            "    where us.euser=per1.euid and not exists(select * from person per2 \n" +
            "\t\t\t\t where per2.date_of_birth>per1.date_of_birth))\t\t\t \n" +
            "    SELECT distinct (nickname)\n" +
            "    FROM youngest_person_address y,person per3,address ad, addressee adds2, email em, use u\n" +
            "    WHERE u.euser=per3.euid and u.address=ad.adid \n" +
            "\tand (adds2._to=y.young_addr  or em._from=y.young_addr) order by nickname ASC;";

    /**
     * e) Birthday greetings:
     * This query retrieves Who received emails a day before or on their birthday.
     * Result  contains the user’s nickname, the date of birth, and the number of emails they received.
     *  We exclude the emails received from organisations.
     */
    public final static String query5="select nickname, date_of_birth as dateOfBirth,count(em.id) as counts \n" +
             "        from person per, use u, address ads,email em,addressee adee\n" +
             "\t\twhere per.euid=u.euser and u.address=ads.adid and ads.adid=adee._to and adee.email=em.id\t  \t  \n" +
             "\t\tand  ( DATE(em.date)=DATE(per.date_of_birth) or \n" +
             "\t\t\t  (DATE_PART('month', DATE(per.date_of_birth)\\:\\:date)=\n" +
             "\t\t\t   DATE_PART('month', DATE(em.date)\\:\\:date) and\n" +
             "\t\t\t  DATE_PART('day', DATE(per.date_of_birth)\\:\\:date) - \n" +
             "\t\t\t   DATE_PART('day', DATE(em.date)\\:\\:date)<2))\n" +
             "\t\t\t  and not exists(select firm from organization o where o.euid=em._from)\n" +
             "\t\tgroup by (euid,nickname,dateOfBirth)\torder by nickname ASC";

    /**
     * f) Top-10
     * We retrieve Top ten eusers who responds to their emails in shorted time
     * We assign the with rank
     * The query don t use Olap functions
     */
    public final static String query6="with sentreplies (email_id, replies_to,senderaddress,timetaken)as\n" +
            "(select received.id as email_id,sent.id as replied_to,received._from as \n" +
            "receivedfrom,TRUNC(DATE_PART('day', received.date- sent.date)/7)as reply_time \n" +
            "from email sent,email received\n" +
            "where sent.id=received.in_reply_to),\n" +
            "frommessage3(email_id,replies_to,timetaken,euser,nickname,number_of_replies) as(\n" +
            "select distinct (email_id), replies_to,timetaken,euser,nickname,\n" +
            "count(*) over(partition by u.euser) as number_of_replies\n" +
            "\tfrom sentreplies sentrep,use u,person per \n" +
            "where sentrep.senderaddress=u.euser and per.euid=u.euser ),\n" +
            "ranking (email_id,replies_to,timetaken,euser,nickname,number_of_replies)as (select * from frommessage3 where number_of_replies>2),\n" +
            "limitingranks (timetaken,nickname,rank) as (select timetaken,nickname,(select count(distinct timetaken)\n" +
            "        from ranking ra\n" +
            "        where ra.timetaken <= ranking.timetaken) as rank\n" +
            "\t\tfrom ranking ) \n" +
            "\t\tselect nickname,rank from limitingranks where rank<11 order by rank asc";



    /**
     * g) Top-10 variation:
     * This query implement the idea of ranking without using olap function
     * we print out the first ten ranks
     */
    public final static String query7="with sentreplies (email_id, replies_to,senderaddress,timetaken)as\n" +
            "(select received.id as email_id,sent.id as replied_to,received._from as \n" +
            "receivedfrom,TRUNC(DATE_PART('day', received.date- sent.date)/7)as reply_time \n" +
            "from email sent,email received\n" +
            "where sent.id=received.in_reply_to),\n" +
            "--partition of numberof replies\n" +
            "frommessage3(email_id,replies_to,timetaken,euser,nickname,number_of_replies) as(\n" +
            "select distinct (email_id), replies_to,timetaken,euser,nickname,\n" +
            "count(*) over(partition by u.euser) as number_of_replies\n" +
            "from sentreplies sentrep,use u,person per \n" +
            "where sentrep.senderaddress=u.euser and per.euid=u.euser ),\n" +
            "--select reply>2\n" +
            "ranking (email_id,replies_to,timetaken,euser,nickname,number_of_replies)as \n" +
            "(select * from frommessage3 where number_of_replies>2),\n" +
            "-- start ranking them\n" +
            "topten (nickname,ranks) as (select nickname, (select 1+ count(*) from ranking ra where ra.timetaken<ranking.timetaken) as ranks\n" +
            " from ranking  order by ranks asc)\n" +
            " select * from topten where ranks<11";



    /**
     * h) Top-10 using OLAP:
     * We retrieve Top ten eusers who responds to their emails in shorted time
     * We assign the with rank
     * The query  use Olap functions dense_rank()
     */
    public final static String query8="with sentreplies (email_id, replies_to,senderaddress,timetaken)as\n" +
            "(select received.id as email_id,sent.id as replied_to,received._from as \n" +
            "receivedfrom,TRUNC(DATE_PART('day', received.date- sent.date)/7)as reply_time \n" +
            "from email sent,email received\n" +
            "where sent.id=received.in_reply_to),\n" +
            "frommessage3(email_id,replies_to,timetaken,euser,nickname,number_of_replies) as(\n" +
            "select distinct (email_id), replies_to,timetaken,euser,nickname,\n" +
            "count(*) over(partition by u.euser) as number_of_replies\n" +
            "from sentreplies sentrep,use u,person per \n" +
            "where sentrep.senderaddress=u.euser and per.euid=u.euser ),\n" +
            "ranking (email_id,replies_to,timetaken,euser,nickname,number_of_replies)as (select * from frommessage3 where number_of_replies>2),\n" +
            "limitingrank(timetaken,nickname,rank)as(select timetaken,nickname,DENSE_RANK() over(  order by timetaken asc )as rank from ranking)\n" +
            "select nickname,rank from limitingrank where rank<11";


     //
    /**
     * i) Thread decomposition:
     * this query retrieve largest thread representation
     */
    public final static String query9="with recursive email_hierarchy as (\n" +
            "SELECT    id,_from,in_reply_to, CAST (em.id AS text) as path, date FROM email em\n" +
            "  WHERE in_reply_to IS NULL\n" +
            "\t\n" +
            "\t UNION ALL \n" +
            "SELECT e.id,e._from,e.in_reply_to, email_hierarchy.path ||'-'|| e.id,e.date\n" +
            "  FROM email e, email_hierarchy\n" +
            "  WHERE e.in_reply_to = email_hierarchy.id\n" +
            "),\n" +
            "generateThreads(Threadid,id,username, in_reply_to,depth,date)as(\n" +
            "select cast(split_part(path,'-',1) as int)as ThreadID, id,ad.username, in_reply_to,array_length(string_to_array(path, '-'), 1) as depth, date \n" +
            "from email_hierarchy,address ad where ad.adid=email_hierarchy._from \n" +
            "),\n" +
            "QualifiedThreads(ThreadID,id,username,inreplyto,depth,date)as(\n" +
            "select thri.ThreadID,thri.id,thri.username,thri.in_reply_to,thri.depth,thri.date\n" +
            "from generateThreads thri where\n" +
            "exists (select th.id from generateThreads th \n" +
            "\t\twhere th.ThreadID=thri.ThreadID and exists\n" +
            "\t\t(select pic.contained_in from picture pic where pic.contained_in=th.id and pic.width=pic.height)\n" +
            "\t\t)order by ThreadID asc, depth asc),\t\t\n" +
            "--here we select one largest thread\n" +
            "LargestThread(threadID,counts)as(\n" +
            "select threadID, count(*) as counts from QualifiedThreads group by threadID \n" +
            " order by counts DESC LIMIT 1)\n" +
            " select * from QualifiedThreads qua where qua.threadID  in\n" +
            " (select threadID from LargestThread)\n";

    /**
     * Sheet 4 Business analysis
     */
    // average number of passange per month
    public final static String query41="select * from monthlypassanger;";
  //  average arrives time on scence
    public final static String query42="select * from avgtimeonscene;";
    //most expensive trip
    public final static String query43="select * from expensivetrip;";
    // ranking of payment model
   public final static String query44="select * from paymentmodelrank;";

   //Sum: TLC Base License Number and their  monthly driver_pay
    public final static String query45="select * from monthlydriverpay;";
    //Max and Group by: Car service with monthly more trips.
    public final static String query46="select * from popularcarservice;";
    //--1) count: car services and the number of  TLC Base License Number.
    public final static String query47="select * from carserviceandbaselicencenumber;";
    //--4)Average: the average distance per trip
    public final static String query48="select * from avgtripdistance;";

    //7) location from which more trips start (TLC Taxi Zone in which the taximeter was engaged)
    public final static String query49="select * from popularpolocation;";
     //-- 6) time on which there are more pickups
    public final static String query410="select * from hourswithmostpassanger;";

}
