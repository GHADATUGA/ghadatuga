package com.tukl.dbproject.worksheet1.projections.sheet4;

public interface MonthlyPassengers {
    String getTaxicab();
    int getAvgMontlyPassangers();
}
