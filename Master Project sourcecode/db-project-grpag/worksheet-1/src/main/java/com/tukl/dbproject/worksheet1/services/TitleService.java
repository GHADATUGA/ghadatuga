package com.tukl.dbproject.worksheet1.services;

import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleGenreDTO;

import java.util.List;

public interface TitleService {
    public List<TitleGenreDTO> findByGenre(String genre);

    public List<TitleDTO> getFullTextSearchResult(String keyword);
}
