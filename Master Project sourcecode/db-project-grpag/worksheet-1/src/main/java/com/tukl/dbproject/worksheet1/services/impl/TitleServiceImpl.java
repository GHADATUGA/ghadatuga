package com.tukl.dbproject.worksheet1.services.impl;

import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleGenreDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.builder.TitleDTOBuilder;
import com.tukl.dbproject.worksheet1.repository.worksheet3.TitleRepository;
import com.tukl.dbproject.worksheet1.services.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * implementation of the service for the gerne and search by keywork query.
 */
@Service
@CacheConfig(cacheNames = "titleCache")
public class TitleServiceImpl implements TitleService {

    @Autowired
    private TitleRepository titleRepository;

    @Cacheable(cacheNames = "titleGenre", key = "#genre")
    @Override
    public List<TitleGenreDTO> findByGenre(String genre) {
        return titleRepository.findTop10ByGenresContainingIgnoreCaseAndNumVotesIsNotNullOrderByNumVotesDesc(genre)
                .orElse(new ArrayList<>()).stream().map(title -> new TitleGenreDTO(title.getOriginalTitle(),
                        title.getNumVotes(), title.getAverageRating())).collect(Collectors.toList());
    }

    @Cacheable(cacheNames = "titleDTO", key = "#keyword")
    @Override
    public List<TitleDTO> getFullTextSearchResult(String keyword) {
        return titleRepository.searchFullText(keyword).orElse(new ArrayList<>())
                .stream().limit(3)
                .map(title -> TitleDTOBuilder.build(title))
                .collect(Collectors.toList());
    }
}
