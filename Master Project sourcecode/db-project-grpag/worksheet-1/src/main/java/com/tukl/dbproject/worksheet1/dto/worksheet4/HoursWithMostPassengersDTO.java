package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.HoursWithMostPassengers;
import lombok.Data;

import java.util.List;

/**
 * Hour with most passengers
 */
@Data
public class HoursWithMostPassengersDTO {
    List<HoursWithMostPassengers> data;
    String query;
    public HoursWithMostPassengersDTO(List<HoursWithMostPassengers> hoursWithMostPassengers, String query) {
        super();
        this.data = hoursWithMostPassengers;
        this.query = query;
    }
}
