package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.BirthdayGreetings;

import lombok.Data;
/**
 * Class to map generic repository for BirthdayGreetings interface to data
 */
@Data
public class BirthdayGreetingsDTO {

	List<BirthdayGreetings> data;
	String query;
	public BirthdayGreetingsDTO(List<BirthdayGreetings> birthdayGreetings, String query) {
		super();
		this.data = birthdayGreetings;
		this.query = query;
	}
}
