package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.MostCommonlyUsedLetter;

import lombok.Data;
/**
 * Class to map generic repository for MostCommonlyUsedLetter interface to data
 */
@Data
public class MostCommonlyUsedLetterDTO {

	public MostCommonlyUsedLetterDTO(List<MostCommonlyUsedLetter> mostCommonlyUsedLetter, String query) {
		super();
		this.data = mostCommonlyUsedLetter;
		this.query = query;
	}
	List<MostCommonlyUsedLetter> data;
	String query;
}
