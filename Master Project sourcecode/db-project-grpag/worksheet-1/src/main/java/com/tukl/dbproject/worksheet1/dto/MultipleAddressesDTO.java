package com.tukl.dbproject.worksheet1.dto;

import com.tukl.dbproject.worksheet1.projections.MultipleAdresses;

import lombok.Data;

import java.util.List;

/**
 * Class to map generic repository for Multiple addresse Interface to data
 */
@Data
public class MultipleAddressesDTO {

	public MultipleAddressesDTO(List<MultipleAdresses> multipleAdresses, String query) {
		this.data = multipleAdresses;
		this.query = query;
	}

	List<MultipleAdresses> data;
	String query;

}
