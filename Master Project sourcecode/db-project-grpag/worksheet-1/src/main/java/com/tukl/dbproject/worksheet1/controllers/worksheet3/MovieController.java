package com.tukl.dbproject.worksheet1.controllers.worksheet3;

import com.tukl.dbproject.worksheet1.dto.UserDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.*;
import com.tukl.dbproject.worksheet1.dto.worksheet3.builder.MovieNamesDTOBuilder;
import com.tukl.dbproject.worksheet1.dto.worksheet3.builder.NameMovieDTOBuilder;
import com.tukl.dbproject.worksheet1.entities.UserAuthDetails;
import com.tukl.dbproject.worksheet1.entities.worksheet3.LogData;
import com.tukl.dbproject.worksheet1.entities.worksheet3.Title;
import com.tukl.dbproject.worksheet1.entities.worksheet3.TitleAkas;
import com.tukl.dbproject.worksheet1.repository.UserAuthDetailsRepository;
import com.tukl.dbproject.worksheet1.repository.worksheet3.NameRepository;
import com.tukl.dbproject.worksheet1.repository.worksheet3.TitleAkasRepository;
import com.tukl.dbproject.worksheet1.repository.worksheet3.TitleRepository;
import com.tukl.dbproject.worksheet1.services.LogDataService;
import com.tukl.dbproject.worksheet1.services.NameService;
import com.tukl.dbproject.worksheet1.services.TitleAkasService;
import com.tukl.dbproject.worksheet1.services.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The controller of movies searchs for worksheet 3 and 4.
 */
@Transactional
@RestController
@RequestMapping(value = "/api")
public class MovieController {

    @Autowired
    private LogDataService logDataService;

    @Autowired
    private TitleService titleService;

    @Autowired
    private TitleAkasService titleAkasService;

    @Autowired
    private NameService nameService;

    @Autowired
    private UserAuthDetailsRepository userAuthDetailsRepository;

    @GetMapping("/movie/akas/{name}")
    public GenericResponseBodyDTO<List<TitleAkasDTO>> getMovieByTitle(@PathVariable("name") String name) {
        LogData logData = logDataService.addLog(name, "TitleAkas");
        return new GenericResponseBodyDTO<>(
                titleAkasService.findByTitleAndIsOriginal(name),
                logDataService.getAlikeLogData(logData)
        );
    }
    // search by Actor
    @GetMapping("/movie/name/{name}")
    public GenericResponseBodyDTO<List<NameMovieDTO>> getMovieByName(@PathVariable("name") String name) {
        LogData logData = logDataService.addLog(name, "Name");
        return new GenericResponseBodyDTO<>(
                nameService.getMovieByName(name),
                logDataService.getAlikeLogData(logData)
        );
    }
    // search by genre
    @GetMapping("/movie/genre/{genre}")
    public GenericResponseBodyDTO<List<TitleGenreDTO>> getMovieByGenre(@PathVariable("genre") String genre) {
        LogData logData = logDataService.addLog(genre, "Genre");
        return new GenericResponseBodyDTO<>(
                titleService.findByGenre(genre),
                logDataService.getAlikeLogData(logData)
        );
    }
    //search keyword
    @GetMapping("/movie/keyword/{keyword}")
    public GenericResponseBodyDTO<List<TitleDTO>> getMovieByKeyword(@PathVariable("keyword") String keyword) {
        LogData logData = logDataService.addLog(keyword, "Keyword");
        return new GenericResponseBodyDTO<>(
                titleService.getFullTextSearchResult(keyword),
                logDataService.getAlikeLogData(logData)
        );
    }

    @GetMapping("/username")
    public UserDTO currentUserName(Principal principal) {
        UserAuthDetails user = userAuthDetailsRepository.findByUsername(principal.getName()).orElse(new UserAuthDetails());
        return new UserDTO(user.getUsername(), user.getLastLogin());
    }
}
