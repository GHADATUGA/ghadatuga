package com.tukl.dbproject.worksheet1.projections.sheet4;

public interface PopularCarService {
    String getHighVolumeFHV();
    int getNumberOfTrip();
}
