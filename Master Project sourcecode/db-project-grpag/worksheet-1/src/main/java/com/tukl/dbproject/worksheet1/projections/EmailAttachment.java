package com.tukl.dbproject.worksheet1.projections;
/**
* Interface for Email Attachements Search
* Given Nickname, Startdate, Enddate and a keyword
* we need to return the ID of the user, corresponding addresse,
* filename and the subject.
 */
public interface EmailAttachment {
    int getId();
    String getAddresse();
    String getEfilename();
    String getEsubject();

}
