package com.tukl.dbproject.worksheet1.projections.sheet4;

/**
 * BaseLicenseNumberPerCarService interface
 */
public interface BaseLicenseNumberPerCarService {
    String getCarService();
    int getBaseLicenseNumber();
}
