package com.tukl.dbproject.worksheet1.dto;

import com.tukl.dbproject.worksheet1.projections.ThreadDecomposition;
import com.tukl.dbproject.worksheet1.projections.TopTenVariation;

import lombok.Data;

import java.util.List;

/**
 * Class to map generic repository for Thread decompostion Interface to data
 */
@Data
public class ThreadDecompositionDTO {


    List<ThreadDecomposition> data;
    String query;
    public ThreadDecompositionDTO(List<ThreadDecomposition> threadDecompositions, String query) {
        super();
        this.data = threadDecompositions;
        this.query = query;
    }

}
