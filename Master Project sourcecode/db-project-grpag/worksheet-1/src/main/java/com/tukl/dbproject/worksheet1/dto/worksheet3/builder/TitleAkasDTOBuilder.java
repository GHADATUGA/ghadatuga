package com.tukl.dbproject.worksheet1.dto.worksheet3.builder;

import com.tukl.dbproject.worksheet1.dto.worksheet3.MovieNamesDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleAkasDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleDTO;
import com.tukl.dbproject.worksheet1.entities.worksheet3.Title;
import com.tukl.dbproject.worksheet1.entities.worksheet3.TitleAkas;

import java.util.List;

public class TitleAkasDTOBuilder {

    public static TitleAkasDTO build(TitleAkas title) {
        return new TitleAkasDTO(
                title.getId(),
                TitleDTOBuilder.build(title.getTitle()),
                title.getOrdering(),
                title.getTitle1(),
                title.getRegion(),
                title.getLanguage(),
                title.getTypes(),
                title.getAttributes(),
                title.isOriginalTitle()
        );
    }
}
