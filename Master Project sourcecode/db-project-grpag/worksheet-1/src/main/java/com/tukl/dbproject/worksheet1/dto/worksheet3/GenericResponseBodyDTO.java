package com.tukl.dbproject.worksheet1.dto.worksheet3;

import com.tukl.dbproject.worksheet1.entities.worksheet3.LogData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponseBodyDTO<T> {
    T data;
    List<LogData> logData;
}
