package com.tukl.dbproject.worksheet1.controllers;
import com.tukl.dbproject.worksheet1.dto.*;
import com.tukl.dbproject.worksheet1.dto.worksheet4.*;
import com.tukl.dbproject.worksheet1.queries.Queries;
import com.tukl.dbproject.worksheet1.repository.GenericRepository;
import com.tukl.dbproject.worksheet1.repository.StockMarketRepository;
import com.tukl.dbproject.worksheet1.requestbody.EmailAttachmentRequest;

import com.tukl.dbproject.worksheet1.services.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

/**
* This class provides the REST controller for the native queries
 */
@RestController
@RequestMapping(value = "/api")
@Transactional(readOnly = true)
public class DbQueriesController {


    @Autowired
    private GenericRepository genericRepository;

    @Autowired
    private StockMarketRepository stockMarketRepository;

    /**
     * Controller for Multiple Addresses Users: users who have multiple addresses
     * @return a table with nickname and corresponding number of addressess.
     */
    @RequestMapping(value = "get-multiple-addresses-users", method = RequestMethod.GET)
    public MultipleAddressesDTO getMultipleAddressesUsers() {
        return new MultipleAddressesDTO(genericRepository.getMultipleAddressesUsers(), Queries.query1);
    }

    /**
     * This is the controller for User Type Count:
     * The number ofemail users of each type registered
     * @return how many person, organization, euser how are neither, and total
     */

    @RequestMapping(value = "get-user-type-numbers", method = RequestMethod.GET)
    public UserTypeCountMappingDTO getUserTypeNumbers() {
    	return new UserTypeCountMappingDTO(genericRepository.getUserTypeCountMapping(), Queries.query2);
    }

    /**
     * Controller for the query to retrieve the letter
     * occurs least frequently as the first character of attached files for filename and How often
     * @return letter and how often
     */

    @RequestMapping(value = "get-most-commonly-used-letter", method = RequestMethod.GET)
    public MostCommonlyUsedLetterDTO getMostCommonlyUsedLetter() {
        return new MostCommonlyUsedLetterDTO(genericRepository.getMostCommonlyUsedLetter(), Queries.query3);
    }

    /**
     * Controller for the query, which retrieves the Address sharing:
     * users who share an email address with the youngest person
     * @return their nicknames in ASC
     */
    @RequestMapping(value = "get-address-sharing", method = RequestMethod.GET)
    public AddressSharingDTO getAddressSharing() {
    	return new AddressSharingDTO(genericRepository.getAddressSharingUsers(), Queries.query4);
    }

    /**
     * Controller of the query for BirthdayGreetings:
     * Who received emails a day before or on their birthday
     * @return Nickname, date of birth and number of the emails they received
     */
    @RequestMapping(value = "get-bithday-greetings", method = RequestMethod.GET)
    public BirthdayGreetingsDTO getBirthdayGreetings() {
    	return new BirthdayGreetingsDTO(genericRepository.getBirthGreetings(), Queries.query5.replace("\\", ""));
    }

    /**
     * Controller for Topten query to retrieve top ten rank of the euser who reply fast
     * Without using olap functions: ranking without skipping a rank
     * here we implement dense rank
     * @return Nickname and the rank
     */
    @RequestMapping(value = "get-top-10", method = RequestMethod.GET)
    public TopTenDTO getTop10() {
    	return new TopTenDTO(genericRepository.getTopTen(), Queries.query6);
    }
    /**
     * Controller for Topten variation query to retrieve top ten rank of the euser who reply fast
     * Without using olap functions: ranking without skipping a rank
     * here we implement rank
     * @return Nickname and the rank
     */
    @RequestMapping(value = "get-top-10-variation",method = RequestMethod.GET)
    public TopTenVariationDTO getTopTenVariation(){
        return new TopTenVariationDTO(genericRepository.getTopTenVariation(),Queries.query7);
    }


    /**
     * Controller for Topten query to retrieve top ten rank of the euser who reply fast
     * With using olap functions. ranking without skipping a rank
     * @return Nickname and the rank
     */
    @RequestMapping(value = "get-top-10-using-OLAP", method = RequestMethod.GET)
    public TopTenUsingOlapDTO getTop10UsingOLAP() {
    	return new TopTenUsingOlapDTO(genericRepository.getTopTenUsingOlap(), Queries.query8);
    }
    /**
     * Controller for Thread decomposition query
     * @return the largest set of emails that are connected by their in reply to relationships
     */
    @RequestMapping(value = "get-thread-decomposition", method = RequestMethod.GET)
    public ThreadDecompositionDTO getThreadDecomposition() {
    	return new ThreadDecompositionDTO(genericRepository.getThreadDecompostion(),Queries.query9);
    }

    /**
     * Controller for FormattedAddress query
     * @param adid, address id (takes part of URL and makes it a parameter)
     * @return email address in human readable form
     */
    
    @RequestMapping(value = "get-format-address/{adid}", method = RequestMethod.GET)
    public FormatAddressDTO function(@PathVariable("adid")int adid) {
    	return new FormatAddressDTO(genericRepository.getFormattedAddress(adid));

    }

    /**
     * controller for FormatUser
     * @param euid, Euser id (takes part of URL and makes it a parameter)
     * @return names or firm with corresponding nickname in bracket or simply Nickname when the euser
     * is neither person nor organization
     */
    @RequestMapping(value = "get-format-user/{euid}",method =RequestMethod.GET )
    public FormatUserDTO function2(@PathVariable("euid")int euid){
        return new FormatUserDTO(genericRepository.getFormatUser(euid));
    }

    /**
     * Controller for AddressBook query
     * @param euid, euser id
     * @return table  an address book: usernames and email addresses
     */
    @RequestMapping(value = "get-address-book/{euid}",method=RequestMethod.GET)
    public AddressBookDTO function3(@PathVariable("euid")int euid){
        return new AddressBookDTO(genericRepository.getAddressBook(euid));
    }

    /**
     * controller for Email Attachment Search
     * @param emailAttachmentRequest, which consists of Nickname, Startdate, Enddate and Keyword
     * @return results consists of email id, from address, filename and the subject of the attachment
     */
    @RequestMapping(value = "get-email-attachment",method = RequestMethod.POST)
    public EmailAttachmentDTO function4(@RequestBody EmailAttachmentRequest emailAttachmentRequest){
        return new EmailAttachmentDTO(genericRepository.getEmailAttachment(emailAttachmentRequest.getNickname(), 
        		emailAttachmentRequest.getStartDate(), emailAttachmentRequest.getEndDate(), emailAttachmentRequest.getKeyword()), null);
    }
    
    
    //From here APIs for Worksheet 2.
    
    @RequestMapping(value = "get-account-v/{stock}",method = RequestMethod.GET)
    public AccountVDTO getAccountV(@PathVariable("stock") String stock){
        return new AccountVDTO(stockMarketRepository.getAccountVByStock(stock));
    }
    
    @Transactional(readOnly = false)
    @RequestMapping(value = "get-order-v/{stock}",method = RequestMethod.GET)
    public OrderVDTO getOrderV(@PathVariable("stock") String stock){
        return new OrderVDTO(stockMarketRepository.getOrderVByStock(stock));
    }

    //-----------------------------------------
    //-                   sheet4
    //----------------------------------------
     //retrieving monthly passangers
    @RequestMapping(value = "get-monthly-passengers",method = RequestMethod.GET)
    public MonthlyPassengersDTO getMonthlyPassengers(){
        return new MonthlyPassengersDTO(genericRepository.getMonthlyPassengers(),Queries.query41);
    }
    //retrieving average on scene arrival time
    @RequestMapping(value = "get-onscence-arrival-time",method = RequestMethod.GET)
    public OnscenceArrivalTimeDTO getOnscenceArrivalTime(){
        return new OnscenceArrivalTimeDTO(genericRepository.getOnscenceArrivalTime(),Queries.query42);
    }
    //retrieving most expensive trip

    @RequestMapping(value = "get-most-expensive-trip",method = RequestMethod.GET)
    public MostExpensiveTripDTO getMostExpensiveTrip(){
        return new MostExpensiveTripDTO(genericRepository.getMostExpensiveTrip(),Queries.query43);
    }
    //retrieving payment model rank

    @RequestMapping(value = "get-payment-model-rank",method = RequestMethod.GET)
    public PaymentModelRankDTO getPaymentModelRank(){
        return new PaymentModelRankDTO(genericRepository.getPaymentModelRank(),Queries.query44);
    }
    //retrieving montly driver-pay

    @RequestMapping(value = "get-monthly-driver-pay",method = RequestMethod.GET)
    public MonthlyDriverPayDTO getMonthlyDriverPay(){
        return new MonthlyDriverPayDTO(genericRepository.getMonthlyDriverPay(),Queries.query45);
    }
    //retrieving popular car services

    @RequestMapping(value = "get-popular-car-service",method = RequestMethod.GET)
    public PopularCarServiceDTO getPopularCarService(){
        return new PopularCarServiceDTO(genericRepository.getPopularCarService(),Queries.query46);
    }
    //retrieving license number per car services

    @RequestMapping(value = "get-base-license-number-per-car-service",method = RequestMethod.GET)
    public BaseLicenseNumberPerCarServiceDTO getBaseLicenseNumberPerCarService(){
        return new BaseLicenseNumberPerCarServiceDTO(genericRepository.getBaseLicenseNumberPerCarService(),Queries.query47);
    }
    //retrieving average distance per trip

    @RequestMapping(value = "get-distance-per-trip",method = RequestMethod.GET)
    public DistancePerTripDTO getDistancePerTrip(){
        return new  DistancePerTripDTO(genericRepository.getDistancePerTrip(),Queries.query48);
    }
    //retrieving popular start location

    @RequestMapping(value = "get-popular-polocation",method = RequestMethod.GET)
    public PopularPOLocationDTO getPopularPOLocation(){
        return new  PopularPOLocationDTO(genericRepository.getPopularPOLocation(),Queries.query49);
    }
    //retrieving popular hours for trip

    @RequestMapping(value = "get-hours-with-most-passengers",method = RequestMethod.GET)
    public HoursWithMostPassengersDTO getHoursWithMostPassengers(){
        return new  HoursWithMostPassengersDTO(genericRepository.getHoursWithMostPassengers(),Queries.query410);
    }




}
