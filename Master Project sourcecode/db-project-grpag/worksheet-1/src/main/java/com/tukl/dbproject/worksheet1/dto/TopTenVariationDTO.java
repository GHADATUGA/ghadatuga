package com.tukl.dbproject.worksheet1.dto;

import com.tukl.dbproject.worksheet1.projections.TopTenUsingOlap;
import com.tukl.dbproject.worksheet1.projections.TopTenVariation;

import lombok.Data;

import java.util.List;
/**
 * Class to map generic repository for top ten variation Interface to data
 */
@Data
public class TopTenVariationDTO {
    List<TopTenVariation> data;
    String query;
    public TopTenVariationDTO(List<TopTenVariation> topTenVariations, String query) {
        super();
        this.data = topTenVariations;
        this.query = query;
    }
}
