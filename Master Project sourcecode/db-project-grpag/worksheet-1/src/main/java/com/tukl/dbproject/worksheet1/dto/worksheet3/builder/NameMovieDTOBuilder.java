package com.tukl.dbproject.worksheet1.dto.worksheet3.builder;


import com.tukl.dbproject.worksheet1.dto.worksheet3.MovieNamesDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.NameMovieDTO;
import com.tukl.dbproject.worksheet1.entities.worksheet3.Name;

import java.util.List;

public class NameMovieDTOBuilder {

    public static NameMovieDTO build(Name name, List<MovieNamesDTO> movieNamesDTO) {
        return new NameMovieDTO(
                name.getPrimaryName(),
                name.getBirthYear(),
                name.getDeathYear(),
                name.getPrimaryProfession(),
                movieNamesDTO
        );
    }
}
