package com.tukl.dbproject.worksheet1.repository.worksheet3;

import com.tukl.dbproject.worksheet1.entities.worksheet3.TitleAkas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TitleAkasRepository extends JpaRepository<TitleAkas, UUID> {

    Optional<TitleAkas> findByTitle1(String title);

    Optional<List<TitleAkas>> findAllByTitle1IgnoreCaseAndIsOriginalTitle(String name, boolean isOriginal);
}