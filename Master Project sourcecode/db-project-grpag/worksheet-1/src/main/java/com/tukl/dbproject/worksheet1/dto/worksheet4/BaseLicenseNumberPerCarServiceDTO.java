package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.BaseLicenseNumberPerCarService;
import lombok.Data;

import java.util.List;

/**
 * Base licence number per car
 */
@Data
public class BaseLicenseNumberPerCarServiceDTO {
    List<BaseLicenseNumberPerCarService> data;
    String query;
    public BaseLicenseNumberPerCarServiceDTO(List<BaseLicenseNumberPerCarService> baseLicenseNumberPerCarServices,String query){
        super();
        this.data=baseLicenseNumberPerCarServices;
        this.query=query;
    }
}


