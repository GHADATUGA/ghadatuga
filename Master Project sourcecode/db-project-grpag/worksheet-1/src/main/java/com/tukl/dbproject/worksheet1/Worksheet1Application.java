package com.tukl.dbproject.worksheet1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Worksheet1Application {

	public static void main(String[] args) {
		SpringApplication.run(Worksheet1Application.class, args);
	}

}
