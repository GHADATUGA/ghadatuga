package com.tukl.dbproject.worksheet1.services.impl;

import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleAkasDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.builder.TitleAkasDTOBuilder;
import com.tukl.dbproject.worksheet1.entities.worksheet3.TitleAkas;
import com.tukl.dbproject.worksheet1.repository.worksheet3.TitleAkasRepository;
import com.tukl.dbproject.worksheet1.services.TitleAkasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@CacheConfig(cacheNames = "titleAkasCache")
public class TitleAkasServiceImpl implements TitleAkasService {

    @Autowired
    private TitleAkasRepository titleAkasRepository;

    @Cacheable(cacheNames = "titleAkas", key = "#name")
    @Override
    public List<TitleAkasDTO> findByTitleAndIsOriginal(String name) {
        return titleAkasRepository.findAllByTitle1IgnoreCaseAndIsOriginalTitle(name, true)
                .orElse(new ArrayList<>()).stream().map(titleAkas -> TitleAkasDTOBuilder.build(titleAkas)).collect(Collectors.toList());
    }
}
