package com.tukl.dbproject.worksheet1.entities.worksheet3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "title_akas")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TitleAkas {
    @Id
    @Column(name = "id_", nullable = false)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "title_id")
    private Title title;

    @Column(name = "ordering_")
    private Integer ordering;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "title")
    private String title1;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "region")
    private String region;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "language_")
    private String language;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "types_")
    private String types;

    @Lob
    @Column(name = "attributes_")
    private String attributes;

    @Column(name = "is_original_title")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isOriginalTitle;

}