package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.TopTen;

import lombok.Data;
/**
 * Class to map generic repository for TopTen Interface to data
 */
@Data
public class TopTenDTO {

	List<TopTen> data;
	String query;
	public TopTenDTO(List<TopTen> topTen, String query) {
		super();
		this.data = topTen;
		this.query = query;
	}
}
