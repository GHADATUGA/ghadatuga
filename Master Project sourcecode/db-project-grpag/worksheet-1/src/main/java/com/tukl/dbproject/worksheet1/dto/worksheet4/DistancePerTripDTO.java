package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.DistancePerTrip;
import lombok.Data;

import java.util.List;

/**
 * distance per trip
 */
@Data
public class DistancePerTripDTO {
    List<DistancePerTrip> data;
    String query;
    public DistancePerTripDTO(List<DistancePerTrip> distancePerTrips, String query) {
        super();
        this.data = distancePerTrips;
        this.query = query;
    }
}
