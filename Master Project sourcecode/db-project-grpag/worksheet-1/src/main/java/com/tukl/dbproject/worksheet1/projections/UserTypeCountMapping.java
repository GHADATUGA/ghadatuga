package com.tukl.dbproject.worksheet1.projections;

import lombok.Data;
import lombok.NoArgsConstructor;
/**
* Interface for Total numbers: How many email users of each type are registered in the system
 */
public interface UserTypeCountMapping {
    String getType();
    int getCount();
}
