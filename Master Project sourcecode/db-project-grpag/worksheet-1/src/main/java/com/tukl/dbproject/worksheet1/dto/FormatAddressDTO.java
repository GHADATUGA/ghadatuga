package com.tukl.dbproject.worksheet1.dto;

import lombok.Data;
/**
 * Class to map Url parameter  (adresse id ) to corresponding data for FormatAddress query
 */
@Data
public class FormatAddressDTO {

	String data;

	public FormatAddressDTO(String data) {
		super();
		this.data = data;
	}
}
