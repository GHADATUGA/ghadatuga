package com.tukl.dbproject.worksheet1.services.impl;

import com.tukl.dbproject.worksheet1.dto.worksheet3.MovieNamesDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.NameMovieDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.builder.MovieNamesDTOBuilder;
import com.tukl.dbproject.worksheet1.dto.worksheet3.builder.NameMovieDTOBuilder;
import com.tukl.dbproject.worksheet1.entities.worksheet3.LogData;
import com.tukl.dbproject.worksheet1.repository.worksheet3.NameRepository;
import com.tukl.dbproject.worksheet1.services.LogDataService;
import com.tukl.dbproject.worksheet1.services.NameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementaion of service for the movie name query
 */
@Service
@CacheConfig(cacheNames = "nameCache")
public class NameServiceImpl implements NameService {

    @Autowired
    private NameRepository nameRepository;

    @Cacheable(cacheNames = "nameMovieDTOs", key = "#name")
    @Override
    public List<NameMovieDTO> getMovieByName(String name) {
        List<NameMovieDTO> nameMovieDTOS = nameRepository.findAllByPrimaryNameIgnoreCase(name).orElse(new ArrayList<>())
                .stream().map(nameObj -> {
                    List<MovieNamesDTO> movieNames = nameObj.getTitlesNames().stream()
                            .sorted(Comparator.comparing(o -> {
                                BigDecimal rating = o.getTconst().getAverageRating();
                                if (rating == null) {
                                    rating = BigDecimal.valueOf(0);
                                }
                                return rating;
                            }, Comparator.reverseOrder()))
                            .limit(10)
                            .map(titlesName -> {
                                List<String> casts = titlesName.getTconst().getTitlePrincipals()
                                        .stream().map(principal -> principal.getNconst().getPrimaryName()).collect(Collectors.toList());
                                return MovieNamesDTOBuilder.build(titlesName.getTconst(), casts);
                            }).collect(Collectors.toList());
                    return NameMovieDTOBuilder.build(nameObj, movieNames);
                }).collect(Collectors.toList());
        return nameMovieDTOS;
    }
}
