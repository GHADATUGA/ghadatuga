package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.MonthlyPassengers;
import lombok.Data;

import java.util.List;

/**
 * Monthly passenger dto
 */
@Data
public class MonthlyPassengersDTO {
    List<MonthlyPassengers> data;
    String query;
    public MonthlyPassengersDTO(List<MonthlyPassengers> monthlyPassengers, String query) {
        super();
        this.data = monthlyPassengers;
        this.query = query;
    }
}
