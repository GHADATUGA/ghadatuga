package com.tukl.dbproject.worksheet1.entities.worksheet3;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "title_principal", indexes = @Index(columnList = "tconst"))
public class TitlePrincipal {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tconst")
    private Title tconst;

    @Column(name = "ordering_")
    private Integer ordering;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nconst")
    private Name nconst;

    @Column(name = "category", length = 40)
    private String category;


    @Column(name = "job")
    private String job;


    @Column(name = "character_")
    private String character;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Title getTconst() {
        return tconst;
    }

    public void setTconst(Title tconst) {
        this.tconst = tconst;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public Name getNconst() {
        return nconst;
    }

    public void setNconst(Name nconst) {
        this.nconst = nconst;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

}