package com.tukl.dbproject.worksheet1.repository.worksheet3;

import com.tukl.dbproject.worksheet1.entities.worksheet3.Title;
import com.tukl.dbproject.worksheet1.entities.worksheet3.TitleAkas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TitleRepository extends JpaRepository<Title, String> {

    Optional<List<Title>> findTop10ByGenresContainingIgnoreCaseAndNumVotesIsNotNullOrderByNumVotesDesc(String genre);

    @Query("SELECT t FROM Title t WHERE fts(:query) = true")
    Optional<List<Title>> searchFullText(@Param("query") String query);


}