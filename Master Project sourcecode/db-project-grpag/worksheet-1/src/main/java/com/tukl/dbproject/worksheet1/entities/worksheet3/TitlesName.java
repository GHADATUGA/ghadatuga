package com.tukl.dbproject.worksheet1.entities.worksheet3;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "titles_names", indexes = @Index(columnList = "nconst"))
public class TitlesName {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nconst")
    private Name nconst;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tconst")
    private Title tconst;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Name getNconst() {
        return nconst;
    }

    public void setNconst(Name nconst) {
        this.nconst = nconst;
    }

    public Title getTconst() {
        return tconst;
    }

    public void setTconst(Title tconst) {
        this.tconst = tconst;
    }

}