package com.tukl.dbproject.worksheet1.services;

import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleAkasDTO;

import java.util.List;

public interface TitleAkasService {
    public List<TitleAkasDTO> findByTitleAndIsOriginal(String name);
}
