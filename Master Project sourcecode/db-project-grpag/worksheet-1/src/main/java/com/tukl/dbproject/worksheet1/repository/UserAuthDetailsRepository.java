package com.tukl.dbproject.worksheet1.repository;

import com.tukl.dbproject.worksheet1.entities.UserAuthDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;

import java.util.Optional;

public interface UserAuthDetailsRepository extends JpaRepository<UserAuthDetails, Long> {
    Optional<UserAuthDetails> findByUsername(String username);
}