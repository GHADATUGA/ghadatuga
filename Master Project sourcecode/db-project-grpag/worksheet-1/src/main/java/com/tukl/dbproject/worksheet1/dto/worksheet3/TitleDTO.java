package com.tukl.dbproject.worksheet1.dto.worksheet3;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TitleDTO {
    private String titleType;

    private String primaryTitle;

    private String originalTitle;

    private boolean isAdult;

    private Integer startYear;

    private Integer endYear;

    private Integer runtimeMinutes;

    private String genres;

    private String plotSummary;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date releaseDate;

    private String plotSynopsis;

    private BigDecimal rating;

    private BigDecimal averageRating;

    private Integer numVotes;
}
