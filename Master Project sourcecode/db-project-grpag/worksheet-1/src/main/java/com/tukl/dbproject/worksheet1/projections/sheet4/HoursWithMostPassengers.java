package com.tukl.dbproject.worksheet1.projections.sheet4;
/**
 * Hours with most passengers interface
 */
public interface HoursWithMostPassengers {
    String getTaxservice();
    double getPickuptime();
    int getTripnumber();
}
