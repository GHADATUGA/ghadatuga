package com.tukl.dbproject.worksheet1.dto.worksheet3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TitleGenreDTO {
    String title;
    Integer numberOfVotes;
    BigDecimal averageRating;
}
