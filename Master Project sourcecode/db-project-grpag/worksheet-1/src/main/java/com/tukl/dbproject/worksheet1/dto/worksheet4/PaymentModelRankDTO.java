package com.tukl.dbproject.worksheet1.dto.worksheet4;

import com.tukl.dbproject.worksheet1.projections.sheet4.PaymentModelRank;
import lombok.Data;

import java.util.List;

/**
 * Payment model rank dto
 */
@Data
public class PaymentModelRankDTO {
    List<PaymentModelRank> data;
    String query;
    public PaymentModelRankDTO(List<PaymentModelRank> paymentModelRanks, String query) {
        super();
        this.data = paymentModelRanks;
        this.query = query;
    }
}
