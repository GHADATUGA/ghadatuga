package com.tukl.dbproject.worksheet1.projections;

public interface AccountV {
	Integer getTrader();
	String getStock();
	Integer getAmountInDepots();
	Integer getCall_Amount();
	Integer getCall_Limit();
	Integer getPut_Amount();
	Integer getPut_Limit();
	Integer getM_Price();
	Integer getMarketValue();
}
