package com.tukl.dbproject.worksheet1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tukl.dbproject.worksheet1.entities.Attachment;
import com.tukl.dbproject.worksheet1.projections.AccountV;
import com.tukl.dbproject.worksheet1.projections.OrderV;
// Repostory for stock market sheet 2
@Repository
public interface StockMarketRepository extends JpaRepository<Attachment, Integer> {
	@Query(value = "Select * from account_v where stock = ?1",nativeQuery = true)
    List<AccountV> getAccountVByStock(String stock);
	
	@Query(value = "Select * from order_v where stock = ?1",nativeQuery = true)
	List<OrderV> getOrderVByStock(String stock);
}
