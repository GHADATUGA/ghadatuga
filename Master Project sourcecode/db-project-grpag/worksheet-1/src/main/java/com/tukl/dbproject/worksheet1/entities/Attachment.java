package com.tukl.dbproject.worksheet1.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
/**
 * Class for Attachment Entity
 */
@Entity
@Table(name = "attachment")
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Attachment implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @Column(name = "id")
    private int id;
    @Column(name = "filename")
    private String filename;
    @Column(name = "subject")
    private String subject;

}
