package com.tukl.dbproject.worksheet1.dto;

import com.tukl.dbproject.worksheet1.dto.worksheet3.ColumnMetadata;
import com.tukl.dbproject.worksheet1.model.TableMetadata;
import lombok.Data;

import java.util.List;

@Data
public class MetadataDTO {

    List<ColumnMetadata> data;

    public MetadataDTO(List<ColumnMetadata> data) {
        this.data = data;
    }
}
