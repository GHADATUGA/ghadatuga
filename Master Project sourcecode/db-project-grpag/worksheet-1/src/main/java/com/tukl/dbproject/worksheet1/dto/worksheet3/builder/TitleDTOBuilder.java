package com.tukl.dbproject.worksheet1.dto.worksheet3.builder;

import com.tukl.dbproject.worksheet1.dto.worksheet3.TitleDTO;
import com.tukl.dbproject.worksheet1.entities.worksheet3.Title;

public class TitleDTOBuilder {
    public static TitleDTO build(Title title) {
        return new TitleDTO(
                title.getTitleType(),
                title.getPrimaryTitle(),
                title.getOriginalTitle(),
                title.isAdult(),
                title.getStartYear(),
                title.getEndYear(),
                title.getRuntimeMinutes(),
                title.getGenres(),
                title.getPlotSummary(),
                title.getReleaseDate(),
                title.getPlotSynopsis(),
                title.getRating(),
                title.getAverageRating(),
                title.getNumVotes()
        );
    }
}
