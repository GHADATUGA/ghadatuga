package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.OrderV;

import lombok.Data;

@Data
public class OrderVDTO {
	public OrderVDTO(List<OrderV> orderV) {
		// TODO Auto-generated constructor stub
		this.data = orderV;
	}

	List<OrderV> data;
}
