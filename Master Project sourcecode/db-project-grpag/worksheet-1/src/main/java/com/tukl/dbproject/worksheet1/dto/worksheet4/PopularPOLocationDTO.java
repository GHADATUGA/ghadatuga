package com.tukl.dbproject.worksheet1.dto.worksheet4;

import com.tukl.dbproject.worksheet1.projections.sheet4.PopularPOLocation;
import lombok.Data;

import java.util.List;

/**
 * Populatar place the trip started dto
 */
@Data
public class PopularPOLocationDTO {
    List<PopularPOLocation> data;
    String query;
    public PopularPOLocationDTO(List<PopularPOLocation> popularPOLocations, String query) {
        super();
        this.data = popularPOLocations;
        this.query = query;
    }
}
