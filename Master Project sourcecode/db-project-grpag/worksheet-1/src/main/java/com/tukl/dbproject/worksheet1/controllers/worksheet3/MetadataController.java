package com.tukl.dbproject.worksheet1.controllers.worksheet3;

import com.tukl.dbproject.worksheet1.dto.TableNameDTO;
import com.tukl.dbproject.worksheet1.dto.worksheet3.ColumnMetadataDTO;
import com.tukl.dbproject.worksheet1.services.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

/**
 * The controllers of retrieving database tables metadata
 */
@RestController
@RequestMapping(value = "/api")
public class MetadataController {
    @Autowired
    private MetadataService metadataService;

    @Transactional(readOnly = false)
    @RequestMapping(value = "get-metadata/{tableName}",method = RequestMethod.GET)
    public ColumnMetadataDTO getMetaData(@PathVariable("tableName") String tableName) throws SQLException {

        return new ColumnMetadataDTO(metadataService.connect_to_db(tableName));
    }
    @GetMapping("get-tablename/{prefixname}")
    @ResponseBody
    public List<TableNameDTO> getTablename(@PathVariable("prefixname") String prefixname) throws SQLException {
        return metadataService.tablenames(prefixname);
    }

    @GetMapping("get-tablenamestring/{prefixname}")
    @ResponseBody
    public List<String>getTablenamestring(@PathVariable("prefixname") String prefixname) throws SQLException {
        return metadataService.tablenamesString(prefixname);
    }





}

