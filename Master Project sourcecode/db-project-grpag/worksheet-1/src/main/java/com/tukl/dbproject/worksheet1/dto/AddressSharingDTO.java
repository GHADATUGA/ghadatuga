package com.tukl.dbproject.worksheet1.dto;

import java.util.List;

import com.tukl.dbproject.worksheet1.projections.AddressSharing;

import lombok.Data;
/**
 * Class to map generic repository for AddressSharing interface to data
 */
@Data
public class AddressSharingDTO {

	List<AddressSharing> data;
	public AddressSharingDTO(List<AddressSharing> addressSharing, String query) {
		super();
		this.data = addressSharing;
		this.query = query;
	}
	String query;
}
