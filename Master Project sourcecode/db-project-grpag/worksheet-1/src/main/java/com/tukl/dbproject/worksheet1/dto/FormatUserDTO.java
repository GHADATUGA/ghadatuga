package com.tukl.dbproject.worksheet1.dto;


import lombok.Data;

/**
 * Class to map Url parameter  (euid ) to corresponding data for FormatUser query
 */
@Data
public class FormatUserDTO {

	String data;

	public FormatUserDTO(String data) {
		super();
		this.data = data;
	}
	
}
