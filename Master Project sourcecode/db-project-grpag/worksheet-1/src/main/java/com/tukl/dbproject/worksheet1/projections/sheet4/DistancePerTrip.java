package com.tukl.dbproject.worksheet1.projections.sheet4;
/**
 * distance per trip interface
 */

public interface DistancePerTrip {
    String getCarservice();
    float getAveragetripMile();
}
