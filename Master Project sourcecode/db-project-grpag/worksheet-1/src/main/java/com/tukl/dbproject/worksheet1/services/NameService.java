package com.tukl.dbproject.worksheet1.services;

import com.tukl.dbproject.worksheet1.dto.worksheet3.NameMovieDTO;
import com.tukl.dbproject.worksheet1.entities.worksheet3.TitleAkas;
import com.tukl.dbproject.worksheet1.repository.worksheet3.NameRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface NameService {

    public List<NameMovieDTO> getMovieByName(String name);

}
