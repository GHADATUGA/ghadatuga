package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.MostExpensiveTrip;
import lombok.Data;

import java.util.List;

/**
 * Most expensive trip dto
 */
@Data
public class MostExpensiveTripDTO {
    List<MostExpensiveTrip> data;
    String query;
    public MostExpensiveTripDTO(List<MostExpensiveTrip> mostExpensiveTrips, String query) {
        super();
        this.data = mostExpensiveTrips;
        this.query = query;
    }
}
