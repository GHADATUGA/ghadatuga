package com.tukl.dbproject.worksheet1.entities.worksheet3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "log_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    private String key;

    private String searchType;

    private long count;

    public LogData(String key, String searchType, long count) {
        this.key = key;
        this.searchType = searchType;
        this.count = count;
    }
}