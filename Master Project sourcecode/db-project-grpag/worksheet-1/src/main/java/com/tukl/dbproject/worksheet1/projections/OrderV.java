package com.tukl.dbproject.worksheet1.projections;

public interface OrderV {
	String getStock();
	Integer getCallVolume();
	Integer getCallBacklog();
	Integer getPutBacklog();
	Integer getPutVolume();
}
