package com.tukl.dbproject.worksheet1.repository;

import com.tukl.dbproject.worksheet1.entities.AuthGrantedAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthGrantedAuthorityRepository extends JpaRepository<AuthGrantedAuthority, Long> {

    public AuthGrantedAuthority getByAuthority(String authority);
}