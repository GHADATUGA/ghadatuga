package com.tukl.dbproject.worksheet1.dto.worksheet4;

import com.tukl.dbproject.worksheet1.projections.sheet4.MonthlyDriverPay;
import lombok.Data;

import java.util.List;

/**
 * Monthly driver pay
 */
@Data
public class MonthlyDriverPayDTO {
    List<MonthlyDriverPay> data;
    String query;
    public MonthlyDriverPayDTO(List<MonthlyDriverPay> monthlyDriverPays, String query) {
        super();
        this.data = monthlyDriverPays;
        this.query = query;
    }
}
