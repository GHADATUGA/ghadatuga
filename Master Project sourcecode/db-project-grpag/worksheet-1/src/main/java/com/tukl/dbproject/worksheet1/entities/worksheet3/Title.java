package com.tukl.dbproject.worksheet1.entities.worksheet3;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Table(name = "title", indexes = @Index(columnList = "genres, num_votes"))
public class Title {
    @Id
    @Column(name = "tconst", nullable = false, length = 40)
    private String id;

    @Column(name = "title_type", length = 40)
    private String titleType;


    @Column(name = "primary_title")
    private String primaryTitle;


    @Column(name = "original_title")
    private String originalTitle;

    @Column(name = "is_adult")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isAdult;

    @Column(name = "start_year")
    private Integer startYear;

    @Column(name = "end_year")
    private Integer endYear;

    @Column(name = "runtime_minutes")
    private Integer runtimeMinutes;


    @Column(name = "genres")
    private String genres;


    @Column(name = "plot_summary")
    private String plotSummary;

    @Column(name = "release_date")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date releaseDate;


    @Column(name = "plot_synopsis")
    private String plotSynopsis;

    @Column(name = "rating", precision = 5, scale = 2)
    private BigDecimal rating;

    @Column(name = "average_rating", precision = 5, scale = 2)
    private BigDecimal averageRating;

    @Column(name = "num_votes")
    private Integer numVotes;

    @OneToMany(mappedBy = "title", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<TitleAkas> titleAkas = new ArrayList<>();

    @OneToMany(mappedBy = "tconst", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<TitlePrincipal> titlePrincipals = new ArrayList<>();

    @OneToMany(mappedBy = "tconst", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<TitlesName> titlesNames = new ArrayList<>();

}