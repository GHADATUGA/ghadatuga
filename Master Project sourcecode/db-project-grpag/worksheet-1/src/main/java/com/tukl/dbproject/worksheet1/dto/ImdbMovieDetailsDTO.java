package com.tukl.dbproject.worksheet1.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class ImdbMovieDetailsDTO {
   private String movie_id;
   private String plot_summary;
  private float rating;
  private Date release_date ;
   private String plot_synopsis;
  private String tconst;
  public ImdbMovieDetailsDTO(){
      super();
      this.movie_id=movie_id;
  }


}
