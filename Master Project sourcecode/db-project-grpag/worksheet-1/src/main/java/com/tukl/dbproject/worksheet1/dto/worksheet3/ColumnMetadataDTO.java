package com.tukl.dbproject.worksheet1.dto.worksheet3;

import lombok.Data;

import java.util.List;

@Data
public class ColumnMetadataDTO {
    List<ColumnMetadata> data;

    public ColumnMetadataDTO(List<ColumnMetadata> data) {
        this.data = data;
    }
}
