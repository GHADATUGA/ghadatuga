package com.tukl.dbproject.worksheet1.projections;

/**
* Interface for the Address Sharing: Users share an email address with the youngest person
 */
public interface AddressSharing {
    String getNickname();
}
