package com.tukl.dbproject.worksheet1.services.impl;

import com.tukl.dbproject.worksheet1.entities.worksheet3.LogData;
import com.tukl.dbproject.worksheet1.repository.worksheet3.LogDataRepository;
import com.tukl.dbproject.worksheet1.services.LogDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogDataServiceImpl implements LogDataService {

    @Autowired
    private LogDataRepository logDataRepository;

    @Override
    public LogData addLog(String key, String searchType) {

        LogData logData = logDataRepository.findByKeyAndSearchType(key, searchType)
                .orElse(new LogData(key, searchType, 0));
        logData.setCount(logData.getCount() + 1);
        logDataRepository.save(logData);
        return logData;
    }

    @Override
    public List<LogData> getAlikeLogData(LogData logData) {
        return logDataRepository
                .findAllBySearchTypeEqualsAndKeyContainsIgnoreCase(logData.getSearchType(), logData.getKey())
                .stream().filter(log -> log != logData).collect(Collectors.toList());
    }

}
