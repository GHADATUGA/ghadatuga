package com.tukl.dbproject.worksheet1.services;

import com.tukl.dbproject.worksheet1.entities.UserAuthDetails;
import com.tukl.dbproject.worksheet1.repository.UserAuthDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

@Service
public class JpaUserDetailsManager implements UserDetailsManager {

    @Autowired
    private UserAuthDetailsRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) {
//        return new UserAuthDetails(Integer.toUnsignedLong(1),"a", "$2a$12$IQaWcV2JkClKYCFQxRcG.uaimwyGiJX8UMCOF/Jd4Jhgmdm8z0ZiO", true, true, true, true, new HashSet<>());
        return repository.findByUsername(username).orElseThrow(
                () -> {
                    throw new UsernameNotFoundException("No user found with username = " + username);
                }
        );
    }

    @Override
    public void createUser(UserDetails user) {
        repository.save((UserAuthDetails) user);
    }

    @Override
    public void updateUser(UserDetails user) {
        repository.save((UserAuthDetails) user);
    }

    @Override
    public void deleteUser(String username) {
        UserAuthDetails userDetails = repository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("No User found for username -> " + username));
        repository.delete(userDetails);
    }

    /**
     * This method assumes that both oldPassword and the newPassword params
     * are encoded with configured passwordEncoder
     *
     * @param oldPassword the old password of the user
     * @param newPassword the new password of the user
     */
    @Override
    @Transactional
    public void changePassword(String oldPassword, String newPassword) {
        //
    }

    @Override
    public boolean userExists(String username) {
        return repository.findByUsername(username).isPresent();
    }
}
