package com.tukl.dbproject.worksheet1.services;


import com.tukl.dbproject.worksheet1.dto.worksheet3.ColumnMetadata;
import com.tukl.dbproject.worksheet1.model.TableMetadata;
import com.tukl.dbproject.worksheet1.dto.TableNameDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class MetadataService {

    @Autowired
    DataSource dataSource;

    /**
     * get connection from data base.
     * @return list of metadata
     */

    public List<ColumnMetadata> connect_to_db(String tableName) throws SQLException {

        Connection connection=dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        List<ColumnMetadata> columnMetadata = new ArrayList<>();


                try(ResultSet columns = databaseMetaData.getColumns(null,null, tableName, null)){

                    while(columns.next()) {
                        String columnName = columns.getString("COLUMN_NAME");
                        String columnSize = columns.getString("COLUMN_SIZE");
                        if (Integer.parseInt(columnSize)>40)columnSize="MaxLength";
                        String datatype = valueTypeName(columns.getString("DATA_TYPE"));
                        String isNullable = columns.getString("IS_NULLABLE");
                        String isAutoIncrement = columns.getString("IS_AUTOINCREMENT");


                        columnMetadata.add(
                                new ColumnMetadata(columnName, datatype,
                                        columnSize, isNullable, isAutoIncrement)
                        );

                    }
                }

        return columnMetadata;
    }

    /**
     * Method to return table names in DB
     * @return
     */
    public List<TableNameDTO> tablenames(String prefix) throws SQLException {
        Connection connection=dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        List<TableNameDTO> tableNameDTOS=new ArrayList<>();
        try(ResultSet resultSet = databaseMetaData.getTables(null, null, null, new String[]{"TABLE"})){
            while(resultSet.next()) {
                String tableName = resultSet.getString("TABLE_NAME");
                    if (tableName.contains(prefix))tableNameDTOS.add(new TableNameDTO(tableName));
            }
        }

        return tableNameDTOS;
    }
    public List<String> tablenamesString(String prefix) throws SQLException {
        Connection connection=dataSource.getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData();
        List<String> tableNameDTOS=new ArrayList<>();
        try(ResultSet resultSet = databaseMetaData.getTables(null, null, null, new String[]{"TABLE"})){
            while(resultSet.next()) {
                String tableName = resultSet.getString("TABLE_NAME");
                if (tableName.contains(prefix))tableNameDTOS.add(tableName);
            }
        }

        return tableNameDTOS;
    }
    /**
     * Convert the returned data type value to human readable format
     * @return datatype
     */
    public String valueTypeName(String typeinString){
        int value=Integer.parseInt(typeinString);
        String datatype;
        switch(value) {

            case 12:
                datatype="varchar";
                break;
            case 2:
                datatype="Short Integer";
                break;
            case -7:
                datatype="Boolean";
                break;
            case 1111:
                datatype="Universal Unique Identifier";
                break;
            case 4:
                datatype="Integer";
                break;
            case 91:
                datatype="Date";
                break;
            default:
                datatype="Data type not seen";
        }
        return datatype;
    }

}


