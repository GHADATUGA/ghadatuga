package com.tukl.dbproject.worksheet1.repository.worksheet3;

import com.tukl.dbproject.worksheet1.entities.worksheet3.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NameRepository extends JpaRepository<Name, String> {
    Optional<List<Name>> findAllByPrimaryNameIgnoreCase(String name);
}