package com.tukl.dbproject.worksheet1.controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class is the controller for the Web.
 */
@Controller
public class WebController {

   @RequestMapping(value = "/businessAnalysis")
   public String businessAnalysis() {
      return "businessAnalysis";
   }

   @RequestMapping(value = "/login")
   public String login(HttpServletResponse response) throws IOException {

      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      if (!(auth instanceof AnonymousAuthenticationToken)) {
         response.sendRedirect("/imdbSearch");
      }
      return "login";
   }

   @RequestMapping(value = "/logout")
   public String logout() {
      return "login";
   }

   @RequestMapping(value = "/imdbSearch")
   public String imdbSearch() {
      return "imdbSearch";
   }
   //imdb metadata web controller sheet 3
   @RequestMapping(value = "/imdbMetadata")
   public String metadata() {
      return "imdbMetadata";
   }
   //home page for login worksheet 4
   @RequestMapping(value = "/")
   public String index() {
      return "index";
   }
   // db queries web controller sheet1
   @RequestMapping(value = "/dbQueries")
   public String dbQueries() {
      return "dbQueries";
   }
   // db function web controller sheet 1
   @RequestMapping(value = "/dbFunctions")
   public String dbFunctions() {
      return "dbFunctions";
   }
   //search sheet 1
   @RequestMapping(value = "/dbSearch")
   public String dbSearch() {
      return "dbSearch";
   }
   
   //From here Web Controllers for Worksheet 2
   
   @RequestMapping(value = "/dbViews")
   public String dbViews() {
      return "dbViews";
   }
}
