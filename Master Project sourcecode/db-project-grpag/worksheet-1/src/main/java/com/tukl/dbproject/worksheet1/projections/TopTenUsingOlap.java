package com.tukl.dbproject.worksheet1.projections;

/**
 * Interface for Top Ten of the Eusers who have the shortest response time
 * using olap function
 */
public interface TopTenUsingOlap {
    String getNickname();
    int getRank();
}
