package com.tukl.dbproject.worksheet1.dto.worksheet3;

import lombok.Data;

@Data
public class ColumnMetadata {

    String name;
    String type;
    String size;
    String isNullable;
    String isAutoIncrement;

    public ColumnMetadata(String name, String type, String size, String isNullable, String isAutoIncrement) {
        this.name = name;
        this.size = size;
        this.type = type;
        this.isNullable = isNullable;
        this.isAutoIncrement = isAutoIncrement;
    }
}
