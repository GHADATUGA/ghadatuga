package com.tukl.dbproject.worksheet1.dto.worksheet3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NameMovieDTO {

    private String primaryName;

    private Integer birthYear;

    private Integer deathYear;

    private String primaryProfession;

    private List<MovieNamesDTO> movies;

}
