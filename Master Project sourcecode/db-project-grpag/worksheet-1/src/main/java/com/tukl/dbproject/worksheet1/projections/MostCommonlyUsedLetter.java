package com.tukl.dbproject.worksheet1.projections;

/**
* Interface for Which letter occurs least frequently as the first character of attached files
 */
public interface MostCommonlyUsedLetter {
    String getLetter();
    int getOften();
}
