package com.tukl.dbproject.worksheet1.requestbody;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class LoginRequest {
    String username;
    String password;
}
