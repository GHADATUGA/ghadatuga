package com.tukl.dbproject.worksheet1.dto.worksheet3;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginDTO {
    String name;
    String password;

    public LoginDTO(LoginDTO userNameAndPassword) {
        this.name=userNameAndPassword.name;
        this.password=userNameAndPassword.password;
    }
}
