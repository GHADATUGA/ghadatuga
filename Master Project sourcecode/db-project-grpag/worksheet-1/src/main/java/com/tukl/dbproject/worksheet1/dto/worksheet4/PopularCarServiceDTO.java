package com.tukl.dbproject.worksheet1.dto.worksheet4;
import com.tukl.dbproject.worksheet1.projections.sheet4.PopularCarService;
import lombok.Data;

import java.util.List;

/**
 * Popular car service dto
 */
@Data
public class PopularCarServiceDTO {
    List<PopularCarService> data;
    String query;
    public PopularCarServiceDTO(List<PopularCarService> popularCarServices, String query) {
        super();
        this.data =popularCarServices;
        this.query = query;
    }
}
