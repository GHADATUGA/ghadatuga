package com.tukl.dbproject.worksheet1.projections.sheet4;

public interface PaymentModelRank {
    int getPaymentType();
    String getPaymentName();
    int getRanking();
}
