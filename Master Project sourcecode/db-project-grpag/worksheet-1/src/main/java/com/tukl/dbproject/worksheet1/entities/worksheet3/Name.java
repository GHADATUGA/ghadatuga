package com.tukl.dbproject.worksheet1.entities.worksheet3;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * table in database: name
 */
@Entity
@Table(name = "name", indexes = @Index(columnList = "primary_name"))
public class Name {
    @Id
    @Column(name = "nconst", nullable = false, length = 40)
    private String id;


    @Column(name = "primary_name")
    private String primaryName;

    @Column(name = "birth_year")
    private Integer birthYear;

    @Column(name = "death_year")
    private Integer deathYear;


    @Column(name = "primary_profession")
    private String primaryProfession;

    @OneToMany(mappedBy = "nconst", fetch = FetchType.LAZY)
    private Set<TitlePrincipal> titlePrincipals = new LinkedHashSet<>();

    @OneToMany(mappedBy = "nconst", fetch = FetchType.LAZY)
    private Set<TitlesName> titlesNames = new LinkedHashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public Integer getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(Integer deathYear) {
        this.deathYear = deathYear;
    }

    public String getPrimaryProfession() {
        return primaryProfession;
    }

    public void setPrimaryProfession(String primaryProfession) {
        this.primaryProfession = primaryProfession;
    }

    public Set<TitlePrincipal> getTitlePrincipals() {
        return titlePrincipals;
    }

    public void setTitlePrincipals(Set<TitlePrincipal> titlePrincipals) {
        this.titlePrincipals = titlePrincipals;
    }

    public Set<TitlesName> getTitlesNames() {
        return titlesNames;
    }

    public void setTitlesNames(Set<TitlesName> titlesNames) {
        this.titlesNames = titlesNames;
    }

}