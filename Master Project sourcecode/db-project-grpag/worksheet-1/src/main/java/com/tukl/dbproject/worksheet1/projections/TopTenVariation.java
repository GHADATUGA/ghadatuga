package com.tukl.dbproject.worksheet1.projections;
/**
 * Interface for Top Ten variation of the Eusers who have the shortest response time
 * using olap function
 */
public interface TopTenVariation {
    String getNickname();
    int getRanks();
}
