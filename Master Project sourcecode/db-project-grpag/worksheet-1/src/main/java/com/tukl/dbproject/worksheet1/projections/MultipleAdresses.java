package com.tukl.dbproject.worksheet1.projections;

/**
* Interface for Which users have multiple addresses
* we require the nickname and how many addresses
 */
public interface MultipleAdresses {
    String getNickname();
    int getCounts();
}
