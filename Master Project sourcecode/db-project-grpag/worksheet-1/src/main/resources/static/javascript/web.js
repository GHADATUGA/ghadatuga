
//Creates Table according to the JSON data
function createTable(data) {
	console.log(data)
	var tableInfo = document.querySelector("#table-info");
	var table = document.querySelector("table");
	table.innerHTML = ``;
	if (data.data == null || data.data.length == 0) {
		tableInfo.textContent = "No data to display here....";
		return;
	}
	
	if(data.query) {
		tableInfo.innerHTML = `<b>Query:</b> ${data.query}`;
	} else {
		tableInfo.innerHTML = "";
	}
	data = data.data;
	var row = document.createElement("tr");
	for (let key of Object.keys(data[0])) {
		let header = document.createElement("th");
		header.textContent = key;
		row.appendChild(header);
	}
	table.appendChild(row);
	
	//Add data rows
	
	for (let tuple of data) {
		let dataRow = document.createElement("tr");
		for (let key of Object.keys(data[0])) {
			let td = document.createElement("td");
			td.textContent = tuple[key];
			dataRow.appendChild(td);
		}
		table.appendChild(dataRow);
	}
}

//Shows result of the function calls in exercise 3, except the last one.
function showFunctionResult(data) {
	var tableInfo = document.querySelector("#table-info");
	var table = document.querySelector("table");
	table.innerHTML = ``;
	if (data.data == null || data.data == "") {
		tableInfo.textContent = "No data to display here";
		return;
	}
	tableInfo.textContent = data.data;
}

//REST API call function for get method
function getRequest(endpoint, callback, resolveBaseUrl = false) {
	let url = window.location.href;
	if(resolveBaseUrl) {
		url = url.split("/");
		url.pop();
		url = url.join("/") + "/";
	}
	fetch(`${url}${endpoint}`)
  		.then(response => response.json())
  		.then(data => callback(data));
}

//REST API call function for post method
function postRequest(endpoint, payload, callback, resolveBaseUrl = false) {
	let url = window.location.href;
	if(resolveBaseUrl) {
		url = url.split("/");
		url.pop();
		url = url.join("/") + "/";
	}
	fetch(`${url}${endpoint}`,
	{
	    method: 'POST', // *GET, POST, PUT, DELETE, etc.
	    mode: 'cors', // no-cors, *cors, same-origin
	    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
	    credentials: 'same-origin', // include, *same-origin, omit
	    headers: {
	      'Content-Type': 'application/json'
	      // 'Content-Type': 'application/x-www-form-urlencoded',
	    },
	    redirect: 'follow', // manual, *follow, error
	    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
	    body: JSON.stringify(payload) // body data type must match "Content-Type" header
  	})
  	.then(response => response.json())
  	.then(data => callback(data));
}

//Wrapper function for getRequest to provide callback and endpoint.
function wrapRequestWithPathValue(endpoint, callback, pathValueContainerId, resolveBaseUrl = false){
	let pathValue = document.querySelector(`#${pathValueContainerId}`).value;
	if(pathValue == '') {
		document.querySelector("#table-info").textContent = "Input Required!";
		document.querySelector("table").innerHTML = ``;
		return;
	}
	getRequest(`${endpoint}/${pathValue}`, callback, resolveBaseUrl)
}

//Prepares data and call postRequest for POST API request and callback.
function getSearchResult(endpoint, callback, resolveBaseUrl = false) {
	data = {
		"nickname" : document.querySelector("#nickname").value,
		"startDate" : `${document.querySelector("#startDate").value} 00:00:00`,
		"endDate" : `${document.querySelector("#endDate").value} 00:00:00`,
		"keyword" : document.querySelector("#keyword").value
	}
	if(data.nickname == '' || data.startDate.split(" ")[0] == '' || data.endDate.split(" ")[0] == '' || data.keyword == '') {
		document.querySelector("#table-info").textContent = "All the fields are mandatory. Please try again.";
		document.querySelector("table").innerHTML = ``;
		return;
	}
	postRequest(endpoint, data, callback, resolveBaseUrl);
}

//Component for Exercise 2
var queriesComponent = `<div class="container">
			<div class="row">
	    		<div class="col-sm">
	      			<div class = "col-md-8 mx-auto">
	      			
	      				<div class="flex flex-column">
	      					<button class="button m-1" onclick="getRequest('api/get-multiple-addresses-users', createTable)">Multiple Addresses</button>
	      					<button class="button m-1" onclick="getRequest('api/get-user-type-numbers', createTable)">Get User Type Numbers</button>
	      					<button class="button m-1" onclick="getRequest('api/get-most-commonly-used-letter', createTable)">Most Commonly Used Letter</button>
	      					<button class="button m-1" onclick="getRequest('api/get-address-sharing', createTable)">Address Sharing</button>
	      					<button class="button m-1" onclick="getRequest('api/get-bithday-greetings', createTable)">Birthday Greetings</button>
	      					<button class="button m-1" onclick="getRequest('api/get-top-10', createTable)">Top 10</button>
	      					<button class="button m-1" onclick="getRequest('api/get-top-10-variation', createTable)">Top 10 Variation</button>
	      					<button class="button m-1" onclick="getRequest('api/get-top-10-using-OLAP', createTable)">Top 10 Using OLAP</button>
	      					<button class="button m-1" onclick="getRequest('api/get-thread-decomposition', createTable)">Thread Decomposition</button>	
	      				</div>
	   				</div>
	    		</div>
	    		<div class="col-sm">
	      			<div class = "col-md-8 mx-auto">
	      			<p id = "table-info">No data to display here.</p>
	   					<table class = "table">
	   						
	   					</table>
	   				</div>
	    		</div>
	    	</div>
	  </div>`;
	  
	  
// Component for Exercise 3
var funtionComponent = `<div class="container">
			<div class="row">
	    		<div class="col-sm">
	      			<div class = "col-md-8 mx-auto">
	      			
	      				<div class="flex flex-column">
		      				<div class="preference">
							    <label for="format-address">Address Id (adid):</label>
							    <input type="number" name="format-address" id="format-address">
							</div>
	      					<button class="button m-1" onclick="wrapRequestWithPathValue('api/get-format-address', showFunctionResult, 'format-address')">Format Address</button>
	      					
	      					<div class="preference">
							    <label for="format-user">Euser Id (euid):</label>
							    <input type="number" name="format-user" id="format-user">
							</div>
	      					<button class="button m-1" onclick="wrapRequestWithPathValue('api/get-format-user', showFunctionResult, 'format-user')">Format User</button>
	      					
	      					<div class="preference">
							    <label for="address-book">Euser Id (euid):</label>
							    <input type="number" name="address-book" id="address-book">
							</div>
	      					<button class="button m-1" onclick="wrapRequestWithPathValue('api/get-address-book', createTable, 'address-book')">Address Book</button>
	      				</div>
	   				</div>
	    		</div>
	    		<div class="col-sm">
	      			<div class = "col-md-8 mx-auto">
	      			<p id = "table-info">No data to display here.</p>
	   					<table class = "table">
	   						
	   					</table>
	   				</div>
	    		</div>
	    	</div>
	  </div>`;
	
// Component for Exercise 4  
var searchComponent = `<div class="container">
			<div class="row">
	    		<div class="col-sm">
	      			<div class = "col-md-8 mx-auto">
	      			
	      				<div class="flex flex-column">
		      				<div class="preference">
							    <label for="nickname">Nickname:</label>
							    <input type="text" name="nickname" id="nickname">
							</div>
							
							<div class="preference">
							    <label for="startDate">Start Date:</label>
							    <input type="date" name="startDate" id="startDate">
							</div>
	      					
	      					<div class="preference">
							    <label for="endDate">End Date:</label>
							    <input type="date" name="endDate" id="endDate">
							</div>
	      					
	      					<div class="preference">
							    <label for="keyword">Keyword:</label>
							    <input type="text" name="keyword" id="keyword">
							</div>
	      					<button class="button m-1" onclick="getSearchResult('api/get-email-attachment', createTable)">Address Book</button>
	      				</div>
	   				</div>
	    		</div>
	    		<div class="col-sm">
	      			<div class = "col-md-8 mx-auto">
	      			<p id = "table-info">No data to display here.</p>
	   					<table class = "table">
	   						
	   					</table>
	   				</div>
	    		</div>
	    	</div>
	  </div>`;
	  
	  
// Global variable for accessing the components according to task and worksheet
var taskMapping = {
	"worksheet1" : {
		"Task 1" : "This task is not available in this application. Please refer to the pdf.",
		"Task 2" : queriesComponent,
		"Task 3" : funtionComponent,
		"Task 4" : searchComponent
	}
}



//Selects the workseet and shows the task.
function selectWorksheet(){
	let selectedSheet = document.querySelector("#sheetSelect").value;
	let taskSelector = document.querySelector("#taskSelect");
	taskSelector.innerHTML = ``;
	for (let taskname of Object.keys(taskMapping[selectedSheet])) {
		let option = document.createElement("option");
		option.textContent = taskname;
		option.value = taskMapping[selectedSheet][taskname];
		taskSelector.appendChild(option);
	}
}

// Gets the component of selected task
function submitRequestForTask(){
	let taskSelector = document.querySelector("#taskSelect");
	if (taskSelector.value == null) {}
	else{
		document.querySelector("#task-layout").innerHTML = taskSelector.value;
	}
}

function executeQuery() {
	getRequest(document.querySelector("#sheetSelect").value, createTable, true);
}
