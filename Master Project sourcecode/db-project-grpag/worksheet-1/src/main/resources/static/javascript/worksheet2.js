function createTable(targetClass, data) {
	var tableInfo = document.querySelector("#table-info");
	var table = document.querySelector(`.${targetClass}`);
	table.innerHTML = ``;
	if (data.data == null || data.data.length == 0) {
		tableInfo.textContent = "No data to display here";
		return;
	}
	
	if(data.query) {
		tableInfo.innerHTML = `<b>Query:</b> ${data.query}`;
	} else {
		tableInfo.innerHTML = "";
	}
	data = data.data;
	var row = document.createElement("tr");
	for (let key of Object.keys(data[0])) {
		let header = document.createElement("th");
		header.textContent = key;
		row.appendChild(header);
	}
	table.appendChild(row);
	
	//Add data rows
	
	for (let tuple of data) {
		let dataRow = document.createElement("tr");
		for (let key of Object.keys(data[0])) {
			let td = document.createElement("td");
			td.textContent = tuple[key];
			dataRow.appendChild(td);
		}
		table.appendChild(dataRow);
	}
}

//REST API call function for get method
function getRequest(endpoint, callback, parameter, resolveBaseUrl = false) {
	let url = window.location.href;
	if(resolveBaseUrl) {
		url = url.split("/");
		url.pop();
		url = url.join("/") + "/";
	}
	fetch(`${url}${endpoint}`)
  		.then(response => response.json())
  		.then(data => callback(parameter, data));
}

//Wrapper function for getRequest to provide callback and endpoint.
function wrapRequestWithPathValue(endpoint, callback, pathValueContainerId, resolveBaseUrl = false){
	let pathValue = document.querySelector(`#${pathValueContainerId}`).value;
	if(pathValue == '') {
		document.querySelector("#table-info").textContent = "Input Required!";
		document.querySelector(".table-1").innerHTML = ``;
		document.querySelector(".table-2").innerHTML = ``;
		return;
	}
	getRequest(`${endpoint[0]}/${pathValue}`, callback, "table-1", resolveBaseUrl)
	getRequest(`${endpoint[1]}/${pathValue}`, callback, "table-2", resolveBaseUrl)
}