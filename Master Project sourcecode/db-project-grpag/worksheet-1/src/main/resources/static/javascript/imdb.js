function fireQuery(callback) {
    let endpoint = document.querySelector('input[name="imdbSearch"]:checked').value;
    let getUrl = window.location;
    let baseUrl = getUrl.protocol + "//" + getUrl.host;
    let query = document.querySelector("#query").value;
    let url = `${baseUrl}/${endpoint}${query}`;
    getRequest(url, callback);
}

function getCardData(data) {
    data = [].concat(data)
    var mainDivContainer = document.createElement("div");
    for (let tuple of data) {
            if(typeof tuple == "string") {
                if(mainDivContainer.innerHTML == "") {
                    mainDivContainer.innerHTML += tuple;
                }
                mainDivContainer.innerHTML += `, ${tuple}`;
                continue;
            }
    	    var divContainer = document.createElement("div");
            divContainer.classList = ['card'];
            let divBody = document.createElement("div");
            divBody.classList = ["card-body"];
            for (let key of Object.keys(tuple)) {
                let htmlData;
                if (typeof tuple[key] == 'object' && tuple[key] != null) {
                    htmlData = `
                          <h5 class="card-title">${key}</h5>
                          <p class="card-text">${getCardData(tuple[key]).outerHTML}</p>
                                                `;
                } else {
                    htmlData = `
                           <h5 class="card-title">${key}</h5>
                           <p class="card-text">${tuple[key]}</p>`;
                }

                divBody.innerHTML += htmlData;
            }
            divContainer.appendChild(divBody);
            mainDivContainer.appendChild(divContainer);
    }
    return mainDivContainer;
}

function populateLog(log) {
    let logList = document.querySelector("#log-list");
    let htmlData = log.map(logContent => `<li>${logContent.key} : ${logContent.count}</li>`).join("");
    logList.innerHTML = "";
    logList.innerHTML = htmlData;
}

function createTable(data) {
    let log = data.logData
    data = data.data
	var tableInfo = document.querySelector("#table-info");
	var table = document.querySelector(".table");
	table.innerHTML = ``;
	if (data.length == 0) {
		tableInfo.textContent = "No data to display here";
		let logList = document.querySelector("#log-list");
        logList.innerHTML = "";
		return;
	}

	tableInfo.innerHTML = "";
    populateLog(log);

	for (let tuple of data) {
	    var divContainer = document.createElement("div");
        divContainer.classList = ['card'];
        divBody = document.createElement("div");
        divBody.classList = ["card-body"];
        for (let key of Object.keys(tuple)) {

            var htmlData;
            if (typeof tuple[key] == 'object' && tuple[key] != null) {
                htmlData = `
                      <h5 class="card-title">${key}</h5>
                      <p class="card-text">${getCardData(tuple[key]).outerHTML}</p>
                 `;

            } else {

                htmlData = `
                       <h5 class="card-title">${key}</h5>
                       <p class="card-text">${tuple[key]}</p>
                `;

            }
            divBody.innerHTML = divBody.innerHTML + htmlData;
        }
        divContainer.appendChild(divBody);
        table.append(divContainer);
    }
}

//REST API call function for get method
function getRequest(url, callback) {
	fetch(url)
  		.then(response => response.json())
  		.then(data => callback(data));
}

function getUsername(data) {
    let username = data.username;
    let timestamp = data.lastLogin;
    let msg = `Welcome ${username}! Last Login : ${timestamp}`;
    document.querySelector("#username").textContent = msg;
}

$(document).ready( function() {
    let getUrl = window.location;
    let baseUrl = getUrl.protocol + "//" + getUrl.host;
    let url = `${baseUrl}/api/username`;
    fetch(url)
        .then(data => data.json())
        .then(data => getUsername(data));
});