package redisTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;

import java.io.Serializable;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(SpringRunner.class)
//@SpringBootTest
public class RedisTest {

    @MockBean
    RedisTemplate<String, Serializable> redisCacheTemplate;

    @Mock
    private ValueOperations<String, Serializable> valueOperations;

    @Mock
    private Jedis jedis;

    @Before
    public void setUp() {
        Mockito.when(redisCacheTemplate.opsForValue()).thenReturn(valueOperations);
        Mockito.doNothing().when(valueOperations).set(anyString(), anyString());
        Mockito.when(valueOperations.get("testKey")).thenReturn("testValue");
    }

    @Test
    public void testJedisConnection() {
        Jedis jedis = new Jedis("127.0.0.1", 6379, 1000);
        jedis.set("test", "test");
        assert("test".equals(jedis.get("test")));
        jedis.flushAll();
        jedis.close();
    }

    @Test
    public void testRedisTemplate() {
        redisCacheTemplate.opsForValue().set("testKey", "testValue");
        Assert.assertEquals("testValue", redisCacheTemplate.opsForValue().get("testKey"));
    }
}
