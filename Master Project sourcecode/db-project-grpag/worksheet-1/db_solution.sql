
---------------------------------
---- ACCOUNT VIEW
---------------------
create view account_v as select o.o_trader as trader,
                    o.o_stock as stock, (select distinct d.d_amount from depots d where
        concat(d.d_trader,d.d_stock)= concat(o.o_trader,o.o_stock)) as AmountInDepots,
                    o.o_amount as call_amount,
                    o.o_limit as call_limit,
                    null as put_amount,
                    null as put_limit,m.m_price,
                    m.m_price*o.o_amount as marketvalue
                    from orders o,market m where o.o_type='CALL' and o.o_stock=m.m_stock

        union
                    select o.o_trader as trader,
                           o.o_stock as stock,(select distinct d.d_amount from depots d where
                                 concat(d.d_trader,d.d_stock)= concat(o.o_trader,o.o_stock)) as AmountInDepots,
                          null as call_amount,
                          null as call_limit,
                          o.o_amount as put_amount,
                          o.o_limit as put_limit,m.m_price,
                          m.m_price*o.o_amount as marketvalue
                         from orders o, market m where o.o_type='PUT' and o.o_stock=m.m_stock;


-------------------------------------------------
-- Function which helps to create orders view
--------------------------------------------

CREATE or replace FUNCTION ordervfinal()
RETURNS TABLE (stock varchar,callVolume int,
			   callBacklog int,limits int,putBacklog int,putVolume int)
    LANGUAGE plpgsql
AS
$$
DECLARE
--TABLE_RECORD RECORD;
alimit int;
	stockname varchar;
	callvolumei int:=0 ;
	callBacklogi int:=0;
	putBacklogi int :=0;
	putvolumei int :=0;
BEGIN
create table templateorder(stock varchar,callVolume int,
                           callBacklog int,limits int,putBacklog int,putVolume int);
FOR stockname in select distinct o_stock from orders
                                                  LOOP
    FOR alimit IN SELECT distinct(tr.o_limit) FROM orders tr where tr.o_stock ilike stockname order by tr.o_limit asc
                     LOOP
select sum(tcall.o_amount) into callvolumei from orders tcall where tcall.o_type='CALL'
                                                                and tcall.o_limit>=alimit and o_stock ilike stockname;
if callvolumei is null then callvolumei=0; end if;
select sum(tput.o_amount) into putvolumei from orders tput where tput.o_type='PUT'
                                                             and tput.o_limit<=alimit and o_stock ilike stockname;
if putvolumei is null then putvolumei=0; end if;
if callvolumei-putvolumei>=0 then callBacklogi=callvolumei-putvolumei;putBacklogi=0;
		elseif putvolumei-callvolumei>=0then putBacklogi=putvolumei-callvolumei; callBacklogi=0;
end if;
insert into templateorder(stock, callVolume,
                          callBacklog ,limits ,putBacklog,putVolume)
values(stockname,callvolumei,callBacklogi,alimit,putBacklogi,putvolumei);
END LOOP;
END LOOP;

RETURN QUERY select * from templateorder; drop table templateorder;
END
$$;
-----------------------------------------
    --- ORDERS VIEW
-----------------------------------------
create view order_v as(select * from ordervfinal());

-------------------------------------------





/*CREATE or replace FUNCTION order_v(stockname varchar)
RETURNS TABLE (stock varchar,callVolume int,
			   callBacklog int,limit_ int,putBacklog int,putVolume int)
    LANGUAGE plpgsql
AS
$$
DECLARE
TABLE_RECORD RECORD;
    alimit int;
	callvolumei int ;
	callBacklogi int:=0;
	putBacklogi int :=0;
	putvolumei int;
BEGIN
create table templateTable(stock varchar,callVolume int,
                           callBacklog int,limit_ int,putBacklog int,putVolume int);

FOR alimit IN SELECT distinct(tr.o_limit) FROM orders tr where tr.o_stock ilike stockname
        LOOP
select sum(tcall.o_amount) into callvolumei from orders tcall where tcall.o_type='CALL'
                                                                and tcall.o_limit>=alimit;
select sum(tput.o_amount) into putvolumei from orders tput where tput.o_type='PUT'
                                                             and tput.o_limit<=alimit;
if callvolumei-putvolumei>=0 then callBacklogi=callvolumei-putvolumei;putBacklogi=0;
		elseif putvolumei-callvolumei>=0then putBacklogi=putvolumei-callvolumei; callBacklogi=0;
end if;
insert into templateTable(stock, callVolume,
                          callBacklog ,limit_ ,putBacklog,putVolume)
values(stockname,callvolumei,callBacklogi,alimit,putBacklogi,putvolumei);
END LOOP;

RETURN QUERY select * from templateTable; drop table templateTable;
END
$$;

 */




--Exerise 2
--Insert Trigger--

------------------------------------------
CREATE OR REPLACE FUNCTION fn_account_v_insert() RETURNS trigger AS $trigger_bound$
DECLARE
depot_val INT;
acc_bal INT;
pending_order INT;
order_count INT;
BEGIN

    -- Check that Seller has enough stock amount in his depot
SELECT d_amount into depot_val from depots
where d_trader = NEW.trader and d_stock = NEW.stock;

IF NEW.put_amount > depot_val or (NEW.put_amount > 0 and depot_val IS NULL)  THEN
        RAISE EXCEPTION SQLSTATE '70001';
END IF;

	-- Check that Buyer has adequate account balance
SELECT t_balance into acc_bal from traders where t_id = NEW.trader;
SELECT sum(o_amount * o_limit) into pending_order from orders
where o_trader = NEW.trader and o_type = 'CALL';
IF pending_order is NULL THEN
    pending_order := 0;
END IF;
IF (NEW.call_limit * NEW.call_amount) > (acc_bal - pending_order) THEN
        RAISE SQLSTATE '70002';
END IF;

    -- Check if there is any order for the same stock
SELECT count(*) into order_count from orders
where o_trader = NEW.trader and o_stock = NEW.stock;

IF order_count > 0 THEN
        RAISE SQLSTATE '70003';
END IF;

    -- Check if it is not an empty order
    IF (NEW.call_amount < 0 or NEW.call_limit < 0
	or NEW.put_amount < 0 or NEW.put_limit < 0)
	or
    ((NEW.call_amount = 0 or NEW.call_amount IS NULL) and
    (NEW.call_limit = 0 or NEW.call_limit IS NULL) and
    (NEW.put_amount = 0 or NEW.put_amount IS NULL) and
    (NEW.put_limit = 0 or NEW.put_limit IS NULL))
	THEN
        RAISE SQLSTATE '70004';
END IF;

	-- Check if it is both call and put in one order
	IF NEW.put_amount + NEW.put_limit > 0 and
	NEW.call_amount + New.call_limit > 0 THEN
		RAISE SQLSTATE '70005';
END IF;

	-- Check if amountindepots, m_price, marketvalue are null or omited
	IF NEW.amountindepots IS NOT NULL OR
	NEW.m_price IS NOT NULL or NEW.marketvalue IS NOT NULL THEN
		RAISE SQLSTATE '70006';
END IF;

	--Insert Order
 	IF NEW.put_amount > 0 THEN
 		INSERT INTO orders (o_type, o_trader, o_amount, o_stock, o_limit)
 		VALUES('PUT', NEW."trader", NEW."put_amount", NEW."stock", NEW."put_limit");
 	ELSEIF NEW.call_amount > 0 THEN
 		INSERT INTO orders (o_type, o_trader, o_amount, o_stock, o_limit)
 		VALUES('CALL', NEW."trader", NEW."call_amount", NEW."stock", NEW."call_limit");
END IF;
RETURN NEW;
END;


$trigger_bound$
LANGUAGE plpgsql;

CREATE TRIGGER account_v_insert
    INSTEAD OF INSERT ON account_v FOR EACH ROW
    EXECUTE PROCEDURE fn_account_v_insert();

------------------------------------------

-- Delete Trigger --
------------------------------------------
CREATE OR REPLACE FUNCTION fn_account_v_delete() RETURNS trigger AS $trigger_bound$
DECLARE
order_type char(4);
del_amount int;
del_limit int;
BEGIN
	-- Check if the order is put or call
	IF OLD.put_amount > 0 and OLD.put_limit > 0 THEN
		order_type := 'PUT';
		del_amount := OLD.put_amount;
		del_limit := OLD.put_limit;
	ELSEIF OLD.call_amount > 0 and OLD.call_limit > 0 THEN
		order_type := 'CALL';
		del_amount := OLD.call_amount;
		del_limit := OLD.call_limit;
END IF;

	-- Delete the Row
DELETE FROM orders where o_type = order_type and o_trader = OLD.trader
                     and o_amount = del_amount and o_stock = OLD.stock and o_limit = del_limit;

RETURN NEW;
END;


$trigger_bound$
LANGUAGE plpgsql;

CREATE TRIGGER account_v_delete
    INSTEAD OF DELETE ON account_v FOR EACH ROW
    EXECUTE PROCEDURE fn_account_v_delete();

---------------------------------------------
--------------------------------------------------------------------------------------
-----EXERCISE 3 : PRICE CALCULATION
---- use this procedure like this: call calculate_prices('foo'), where foo is stockname
-----------------------------------------------------------------------------

create or replace function calculate_prices()
RETURNS TABLE (Stock varchar, Price int)
language plpgsql
as $$
declare
-- variable declaration
goodlimit int;
currentmarketprice int;
countslimit int;
lowestlimit int;
stockname varchar;
highestlimit int;
timer int;
begin
For stockname in select m_stock from market
                                         LOOP
drop table if exists templateTable;
drop table if exists records;
select	m_price into currentmarketprice from market where m_stock ilike stockname;
select m_time into timer from market where m_stock ilike stockname;
-- In this table we keep the orders of the current stock
create  table templateTable as select * from orders_v ordv where ordv.stock ilike stockname;
-- limits of highest trading volume: it creates the records for that
create table records as select limits,callbacklog,putbacklog,callvolume,putvolume from templateTable tmp where
            tmp.callvolume+tmp.putvolume >= all (select tmp2.callvolume+tmp2.putvolume from templateTable tmp2);

-- Here we implement P1 rule
select count(*) into countslimit from records;
if countslimit = 1 and exists (select * from records where putvolume >0 and callvolume >0)then
           select limits into goodlimit from records limit 1;
           update market set m_price=goodlimit,m_time=timer+1 where m_stock ilike stockname;
           drop table records;
           drop table templateTable;
-- Here we  implement p4 and p5 rules
elseif	countslimit >1 and (not exists (select * from records where callbacklog >0 or putbacklog >0) or
							(exists (select * from records where callbacklog >0) and exists (select * from records where putbacklog >0)))
							and exists (select * from records where putvolume >0 and callvolume >0)then

          select MIN(limits) into lowestlimit from records;
          select  Max(limits) into highestlimit from records;
          if currentmarketprice <= lowestlimit then
                         update market set m_price=lowestlimit,m_time=timer+1 where m_stock ilike stockname;
                         drop table records;
                         drop table templateTable;
          elseif currentmarketprice>=highestlimit then
                            update market set m_price=highestlimit,m_time= timer+1 where m_stock ilike stockname;
                            drop table records;
                            drop table templateTable;
          else
                  update market set m_time=timer+1 where m_stock ilike stockname;
                  drop table records;
                  drop table templateTable;
                  end if;
	--Here We implement p2 rules
 elseif countslimit>1 and exists (select * from records where callbacklog>0) and not exists (select * from records where putbacklog>0)
                               and exists (select * from records where putvolume >0 and callvolume >0)then
                    select MAX(limits) into highestlimit from records;
                    update market set m_price=highestlimit,m_time=timer+1 where m_stock ilike stockname;
                    drop table records;
                    drop table templateTable;
--Here We implement  p3 rule
elseif	countslimit>1 and exists (select * from records where putbacklog>0) and not exists (select * from records where callbacklog>0)
                           and exists (select * from records where putvolume >0 and callvolume >0)then
                       select MIN(limits) into lowestlimit from records;

                      update  market set m_price=lowestlimit,m_time= timer+1 where m_stock ilike stockname;
                      drop table records;drop table templateTable;-- end if;


--else p7: and p6 the part contains other parts (like the case of SAP)
elseif countslimit = 1 and not exists (select * from records where putvolume >0 and callvolume >0) then
               if  exists (select * from templateTable where callvolume >0 ) and exists (select * from templateTable where putvolume >0 )then
                         select MIN(limits) into lowestlimit from records;
                         select  Max(limits) into highestlimit from records;
                  if currentmarketprice>=highestlimit then
                       update market set m_price=highestlimit,m_time= timer+1 where m_stock ilike stockname;
                       drop table records;
                       drop table templateTable;
                  elseif currentmarketprice<=lowestlimit then
                        update market set m_price=lowestlimit,m_time= timer+1 where m_stock ilike stockname;
                        drop table records;
                        drop table templateTable;
                  else
                      update market set m_time= timer+1 where m_stock ilike stockname;
                      drop table records;
                    drop table templateTable; end if;
              --else p7
              else
                   update market set m_time= timer where m_stock ilike stockname;
                   drop table records;
                   drop table templateTable; end if;


--p6 normal parts like the case of Yahoo!
elseif countslimit >1 and not exists (select * from records where putvolume >0 and callvolume >0) then

                 select MIN(limits) into lowestlimit from records;
                 select  Max(limits) into highestlimit from records;
                 if currentmarketprice>=highestlimit then
                          update market set m_price=highestlimit,m_time= timer+1 where m_stock ilike stockname;
                          drop table records;
                          drop table templateTable;
                 elseif currentmarketprice<= lowestlimit then
                         update market set m_price=lowestlimit, m_time=timer+1 where m_stock ilike stockname;
                         drop table records;
                         drop table templateTable;
                else update market set m_time=timer+1 where m_stock ilike stockname;
                     drop table records;
                     drop table templateTable; end if;

--- unreachable part
else
       update market set m_time= timer where m_stock ilike stockname;
       drop table records;drop table templateTable;
       end if;
END LOOP;
RETURN QUERY select m_stock,m_price  from market order by m_stock collate "C";

end; $$;

---------------------------------------------------------------------------------
------------------------------EXERCISE 4-----------------------------------------
---------------------------------------------------------------------------------
create or replace function trade()
returns table (T_name varchar,T_Balance int)
    LANGUAGE plpgsql
AS
$$
DECLARE
TABLE_RECORD RECORD;
	backlogrecord RECORD;
	precords RECORD;
	callsrecords RECORD;
	putsrecords RECORD;
    stockname varchar;
	moneygot int:=0;
	moneygave int:=0;
	price int:=0;
	putremains int:=0;
	callremains int:=0;
	currentbalance int:=0;
	putvolume int:=0 ;
	callvolume int:=0 ;
BEGIN

FOR stockname in select m_stock from market
    LOOP

select m_price into price from market where m_stock ilike stockname;
-- to hold traders who can buy or sell on current market price
create table currentstockbuysell as select * from orders where o_stock ilike stockname and
                                                        ((o_type='PUT' and o_limit<=price)or
													   (o_type='CALL' and o_limit>=price))order by o_id ASC;
select sum(o_amount) into callvolume from currentstockbuysell where o_type='CALL';
select sum(o_amount) into putvolume from currentstockbuysell where o_type='PUT';
-- all puts can be sold and all calls can bought
if putvolume=callvolume then

     for TABLE_RECORD in select * from currentstockbuysell
         LOOP


    if TABLE_RECORD.o_type='CALL' then
          select tr1.t_balance into currentbalance from traders tr1 where tr1.t_id=TABLE_RECORD.o_trader;
          moneygave= price * TABLE_RECORD.o_amount;
         update traders tr set t_balance=currentbalance-moneygave where tr.t_id=TABLE_RECORD.o_trader;
          --UPDATE OF ORDERS BOOK
          delete from orders ord where ord.o_id=TABLE_RECORD.o_id;
    elseif TABLE_RECORD.o_type='PUT' then
          select tr1.t_balance into currentbalance from traders tr1 where tr1.t_id=TABLE_RECORD.o_trader;
          moneygot=price * TABLE_RECORD.o_amount;
          update traders tr set t_balance=currentbalance+moneygot  where tr.t_id=TABLE_RECORD.o_trader ;
          --UPDATE OF ORDERS BOOK
         delete from orders ord where ord.o_id=TABLE_RECORD.o_id;
    end if;
    END LOOP;
-- if call> put then all puts are served.
elseif	putvolume<callvolume	then

       create table onlyputs as select * from currentstockbuysell curs where curs.o_type='PUT';
       create table onlycalls as select * from currentstockbuysell curb where curb.o_type='CALL' order by o_id ASC;
       for precords in select * from onlyputs
               LOOP
               select tr.t_balance into currentbalance from traders tr where tr.t_id=precords.o_trader;
               moneygot=price * precords.o_amount;
               update traders set t_balance=currentbalance+moneygot  where t_id=precords.o_trader;
               --UPDATE OF ORDERS BOOK.
               delete from orders ors where  ors.o_id=precords.o_id;
               end loop;drop table if exists onlyputs;
       for backlogrecord in select * from onlycalls order by o_id asc
             loop
		   if backlogrecord.o_amount<=putvolume then
		         putremains=putvolume-backlogrecord.o_amount;
                 putvolume=putremains;
                 select tr.t_balance into currentbalance from traders tr where tr.t_id=backlogrecord.o_trader;
                 moneygave=price * backlogrecord.o_amount;
                update traders set t_balance=currentbalance-moneygave  where t_id=backlogrecord.o_trader;
                --UPDATE OF ORDERS BOOK
                delete from orders ord where ord.o_id=backlogrecord.o_id;

           elseif backlogrecord.o_amount> putvolume and putvolume >0 then
                 update orders ord set o_amount=backlogrecord.o_amount-putvolume where ord.o_id=backlogrecord.o_id;
                 select tr.t_balance into currentbalance from traders tr where tr.t_id=backlogrecord.o_trader;
                 moneygave=price * putvolume;
                 update traders set t_balance=currentbalance- moneygave where t_id=backlogrecord.o_trader;
                 putvolume=0;
           end if;
           end loop;
           drop table if exists onlycalls;
-- if put>call then all calls are served
elseif putvolume>callvolume then

               create table onlyputs2 as select * from currentstockbuysell curs where curs.o_type='PUT' order by o_id ASC;
               create table onlycalls2 as select * from currentstockbuysell curb where curb.o_type='CALL' ;
               for callsrecords in select * from onlycalls2
                          LOOP
                     select tr.t_balance into currentbalance from traders tr where tr.t_id=callsrecords.o_trader;
                     moneygave=price * callsrecords.o_amount;
                     update traders set t_balance=currentbalance-moneygave where t_id=callsrecords.o_trader;
                     --UPDATE OF ORDERS BOOK.
                     delete from orders ors where  ors.o_id=callsrecords.o_id;
                     end loop;drop table if exists onlycalls2;
               for putsrecords in select * from onlyputs2 order by o_id asc
                   loop
		        if putsrecords.o_amount<=callvolume then
		           callremains=callvolume-putsrecords.o_amount;
                   callvolume=callremains;
                   select tr.t_balance into currentbalance from traders tr where tr.t_id=putsrecords.o_trader;
                   moneygot=price * putsrecords.o_amount;
                   update traders set t_balance=currentbalance+moneygot where t_id=putsrecords.o_trader;
                   --UPDATE OF ORDERS BOOK
                   delete from orders ord where ord.o_id=putsrecords.o_id;

                elseif putsrecords.o_amount> callvolume and callvolume >0 then
                   update orders ord set o_amount=putsrecords.o_amount-callvolume where ord.o_id=putsrecords.o_id;
                   select tr.t_balance into currentbalance from traders tr where tr.t_id=putsrecords.o_trader;
                   moneygot=price * callvolume;
                   update traders set t_balance=currentbalance+moneygot  where t_id=putsrecords.o_trader;
                   callvolume=0;
                end if;
                end loop;
                drop table if exists onlyputs2;
end if;
drop table if exists currentstockbuysell;
END LOOP;

RETURN QUERY select ta.t_name,ta.t_balance from traders ta order by ta.t_name ASC;
END
$$;
