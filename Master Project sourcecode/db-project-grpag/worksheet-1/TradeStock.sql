create or replace function trade()
returns table (T_name varchar,T_Balance int)
    LANGUAGE plpgsql
AS
$$
DECLARE
TABLE_RECORD RECORD;
	backlogrecord RECORD;
	precords RECORD;
	callsrecords RECORD;
	putsrecords RECORD;
    stockname varchar;
	moneygot int:=0;
	moneygave int:=0;
	price int:=0;
	putremains int:=0;
	callremains int:=0;
	currentbalance int:=0;
	putvolume int:=0 ;
	callvolume int:=0 ;
BEGIN

FOR stockname in select m_stock from market
    LOOP

select m_price into price from market where m_stock ilike stockname;
-- to hold traders who can buy or sell on current market price
create table currentstockbuysell as select * from orders where o_stock ilike stockname and
                                                        ((o_type='PUT' and o_limit<=price)or
													   (o_type='CALL' and o_limit>=price))order by o_id ASC;
select sum(o_amount) into callvolume from currentstockbuysell where o_type='CALL';
select sum(o_amount) into putvolume from currentstockbuysell where o_type='PUT';
-- all puts can be sold and all calls can bought
if putvolume=callvolume then

     for TABLE_RECORD in select * from currentstockbuysell
         LOOP


    if TABLE_RECORD.o_type='CALL' then
          select tr1.t_balance into currentbalance from traders tr1 where tr1.t_id=TABLE_RECORD.o_trader;
          moneygave= price * TABLE_RECORD.o_amount;
         update traders tr set t_balance=currentbalance-moneygave where tr.t_id=TABLE_RECORD.o_trader;
          --UPDATE OF ORDERS BOOK
          delete from orders ord where ord.o_id=TABLE_RECORD.o_id;
    elseif TABLE_RECORD.o_type='PUT' then
          select tr1.t_balance into currentbalance from traders tr1 where tr1.t_id=TABLE_RECORD.o_trader;
          moneygot=price * TABLE_RECORD.o_amount;
          update traders tr set t_balance=currentbalance+moneygot  where tr.t_id=TABLE_RECORD.o_trader ;
          --UPDATE OF ORDERS BOOK
         delete from orders ord where ord.o_id=TABLE_RECORD.o_id;
    end if;
    END LOOP;
-- if call> put then all puts are served.
elseif	putvolume<callvolume	then

       create table onlyputs as select * from currentstockbuysell curs where curs.o_type='PUT';
       create table onlycalls as select * from currentstockbuysell curb where curb.o_type='CALL' order by o_id ASC;
       for precords in select * from onlyputs
               LOOP
               select tr.t_balance into currentbalance from traders tr where tr.t_id=precords.o_trader;
               moneygot=price * precords.o_amount;
               update traders set t_balance=currentbalance+moneygot  where t_id=precords.o_trader;
               --UPDATE OF ORDERS BOOK.
               delete from orders ors where  ors.o_id=precords.o_id;
               end loop;drop table if exists onlyputs;
       for backlogrecord in select * from onlycalls order by o_id asc
             loop
		   if backlogrecord.o_amount<=putvolume then
		         putremains=putvolume-backlogrecord.o_amount;
                 putvolume=putremains;
                 select tr.t_balance into currentbalance from traders tr where tr.t_id=backlogrecord.o_trader;
                 moneygave=price * backlogrecord.o_amount;
                update traders set t_balance=currentbalance-moneygave  where t_id=backlogrecord.o_trader;
                --UPDATE OF ORDERS BOOK
                delete from orders ord where ord.o_id=backlogrecord.o_id;

           elseif backlogrecord.o_amount> putvolume and putvolume >0 then
                 update orders ord set o_amount=backlogrecord.o_amount-putvolume where ord.o_id=backlogrecord.o_id;
                 select tr.t_balance into currentbalance from traders tr where tr.t_id=backlogrecord.o_trader;
                 moneygave=price * putvolume;
                 update traders set t_balance=currentbalance- moneygave where t_id=backlogrecord.o_trader;
                 putvolume=0;
           end if;
           end loop;
           drop table if exists onlycalls;
-- if put>call then all calls are served
elseif putvolume>callvolume then

               create table onlyputs2 as select * from currentstockbuysell curs where curs.o_type='PUT' order by o_id ASC;
               create table onlycalls2 as select * from currentstockbuysell curb where curb.o_type='CALL' ;
               for callsrecords in select * from onlycalls2
                          LOOP
                     select tr.t_balance into currentbalance from traders tr where tr.t_id=callsrecords.o_trader;
                     moneygave=price * callsrecords.o_amount;
                     update traders set t_balance=currentbalance-moneygave where t_id=callsrecords.o_trader;
                     --UPDATE OF ORDERS BOOK.
                     delete from orders ors where  ors.o_id=callsrecords.o_id;
                     end loop;drop table if exists onlycalls2;
               for putsrecords in select * from onlyputs2 order by o_id asc
                   loop
		        if putsrecords.o_amount<=callvolume then
		           callremains=callvolume-putsrecords.o_amount;
                   callvolume=callremains;
                   select tr.t_balance into currentbalance from traders tr where tr.t_id=putsrecords.o_trader;
                   moneygot=price * putsrecords.o_amount;
                   update traders set t_balance=currentbalance+moneygot where t_id=putsrecords.o_trader;
                   --UPDATE OF ORDERS BOOK
                   delete from orders ord where ord.o_id=putsrecords.o_id;

                elseif putsrecords.o_amount> callvolume and callvolume >0 then
                   update orders ord set o_amount=putsrecords.o_amount-callvolume where ord.o_id=putsrecords.o_id;
                   select tr.t_balance into currentbalance from traders tr where tr.t_id=putsrecords.o_trader;
                   moneygot=price * callvolume;
                   update traders set t_balance=currentbalance+moneygot  where t_id=putsrecords.o_trader;
                   callvolume=0;
                end if;
                end loop;
                drop table if exists onlyputs2;
end if;
drop table if exists currentstockbuysell;
END LOOP;

RETURN QUERY select ta.t_name,ta.t_balance from traders ta order by ta.t_name ASC;
END
$$;
