

CREATE TABLE attachment (
    id integer PRIMARY KEY NOT NULL,
    filename varchar(40),
    subject varchar(80)
);

CREATE TABLE picture (
    width integer,
    height integer,
    colors integer
) INHERITS(attachment);

CREATE TABLE video (
    fps float,
    seconds integer
) INHERITS(picture);

CREATE TABLE text (
    content varchar(80)
) INHERITS(attachment);

CREATE TABLE euser (
    euid integer NOT NULL,
    nickname varchar(20)
);

CREATE TABLE person (
    surname varchar(20),
    first_name varchar(20),
    date_of_birth date
) INHERITS(euser);

CREATE TABLE organization (
    firm varchar(30)
) INHERITS(euser);

CREATE TABLE address (
    adid integer PRIMARY KEY NOT NULL,
    username varchar(20) NOT NULL,
    domain varchar(20) NOT NULL,
    UNIQUE(username, domain)
);

CREATE TABLE use (
    euser integer,
    address integer REFERENCES address(adid)
);

CREATE TABLE email (
    id integer PRIMARY KEY NOT NULL,
    date timestamp NOT NULL,
    _from integer REFERENCES address(adid),
    in_reply_to integer REFERENCES email(id)
) INHERITS (text);

ALTER TABLE attachment ADD contained_in integer REFERENCES email(id);

CREATE TABLE addressee (
    _to integer REFERENCES address(adid),
    email integer REFERENCES email(id)
);






--------------------------------------------------------







insert into euser (euid, nickname) values (111, 'neo986'			);
insert into euser (euid, nickname) values (112, 'neo943'			);
insert into euser (euid, nickname) values (113, 'dau955'			);
insert into euser (euid, nickname) values (114, 'zeta869'			);
insert into euser (euid, nickname) values (115, 'redneck513'		);
insert into euser (euid, nickname) values (116, 'ArmyOfLamers141'	);
insert into euser (euid, nickname) values (117, 'fool328'			);
insert into euser (euid, nickname) values (118, 'zeta735'			);
insert into euser (euid, nickname) values (119, 'drago198'			);
insert into euser (euid, nickname) values (120, 'drago783'			);
insert into euser (euid, nickname) values (121, 'ArmyOfLamers669'	);
insert into euser (euid, nickname) values (122, 'neo751'			);
insert into euser (euid, nickname) values (123, 'ArmyOfLamers109'	);
insert into euser (euid, nickname) values (124, 'morpheus272'		);
insert into euser (euid, nickname) values (125, 'cyber426'			);
insert into euser (euid, nickname) values (126, 'ArmyOfLamers542'	);
insert into euser (euid, nickname) values (127, 'redneck625'		);
insert into euser (euid, nickname) values (128, 'morpheus76'		);
insert into euser (euid, nickname) values (129, 'redneck742'		);
insert into euser (euid, nickname) values (130, 'drago585'			);

insert into person (euid, nickname, surname, first_name, date_of_birth) values (1,  'cypher362'			, 'Maier', 'Markus', '1966-02-16');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (2,  'trinity216'		, 'Einstein', 'Sascha', '1903-10-21');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (3,  'cyber977'			, 'Niemand', 'Marianne', '1955-04-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (4,  'wiesel763'			, 'Schaefer', 'Markus', '1948-10-06');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (5,  'redneck819'		, 'Schreiner', 'Sascha', '1976-06-26');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (6,  'cypher793'			, 'Mayer', 'Ute', '1990-01-19');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (7,  'wiesel865'			, 'Schmeuid', 'Christian', '1909-08-21');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (8,  'cypher212'			, 'Schmeuid', 'Julia', '1943-12-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (9,  'ArmyOfLamers871'	, 'Schmitt', 'Christian', '1983-03-02');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (10, 'wiesel413'			, 'Schmitt', 'Sarah', '1922-03-08');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (11, 'drago861'			, 'Maier', 'Andrea', '1926-03-05');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (12, 'wiesel650'			, 'Schmeuidt', 'Alois', '1951-03-03');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (13, 'ArmyOfLamers410'	, 'Schmeuid', 'Sarah', '1985-01-27');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (14, 'fool366'			, 'Neumann', 'Andreas', '1915-06-02');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (15, 'zeta429'			, 'Metzger', 'Christian', '1959-03-14');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (16, 'neo382'			, 'Armstrong', 'Andrea', '1979-12-27');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (17, 'redneck250'		, 'Maier', 'Christian', '1969-06-12');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (18, 'ArmyOfLamers674'	, 'Metzger', 'Albert', '1902-08-09');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (19, 'morpheus176'		, 'Mayer', 'Martin', '1986-05-11');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (20, 'ArmyOfLamers824'	, 'Armstrong', 'Markus', '1919-08-07');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (21, 'hacker511'			, 'Schmeuid', 'Marianne', '1978-11-19');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (22, 'wiesel611'			, 'Bechtel', 'Julia', '1991-05-03');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (23, 'neo927'			, 'Wichtel', 'Julia', '1952-12-20');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (24, 'cyber974'			, 'Newton', 'Sarah', '1959-03-05');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (25, 'zeta984'			, 'Neumann', 'Fritz', '1963-12-26');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (26, 'zeta527'			, 'Schmeuid', 'Jane', '1937-05-04');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (27, 'drago96'			, 'Niemand', 'Albert', '1925-11-09');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (28, 'fool28'			, 'Mueller', 'Joe', '1953-03-04');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (29, 'neo17'				, 'Schmeuid', 'Julia', '1966-01-10');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (30, 'drago247'			, 'Wichtel', 'Fritz', '1940-09-15');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (31, 'hacker219'			, 'Mayer', 'Marianne', '1922-08-09');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (32, 'redneck832'		, 'Mayer', 'Martin', '1979-07-18');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (33, 'morpheus766'		, 'Niemand', 'Albert', '1955-10-30');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (34, 'hacker944'			, 'Schreiner', 'Fritz', '1914-07-08');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (35, 'fool812'			, 'Schmeuidt', 'Lilli', '1980-12-01');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (36, 'zeta253'			, 'Bechtel', 'Marianne', '1991-01-12');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (37, 'cyber614'			, 'Schmitt', 'Markus', '1909-09-21');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (38, 'hacker847'			, 'Mayer', 'Sarah', '1980-01-27');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (39, 'cypher920'			, 'Schreiner', 'Fritz', '1902-04-03');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (40, 'wiesel597'			, 'Newton', 'Julia', '1975-11-07');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (41, 'cypher15'			, 'Einstein', 'Sarah', '1966-04-24');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (42, 'cyber701'			, 'Neumann', 'Andreas', '1934-06-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (43, 'neo555'			, 'Mueller', 'Jane', '1968-07-30');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (44, 'ArmyOfLamers318'	, 'Wichtel', 'Ute', '1934-07-02');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (45, 'drago831'			, 'Mayer', 'Jane', '1963-05-05');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (46, 'cyber585'			, 'Newton', 'Markus', '1954-06-02');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (47, 'fool380'			, 'Einstein', 'Ute', '1956-08-19');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (48, 'cyber44'			, 'Schmeuid', 'Markus', '1984-02-25');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (49, 'morpheus451'		, 'Maier', 'Andrea', '1962-09-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (50, 'hacker285'			, 'Schaefer', 'Marta', '1939-11-18');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (51, 'drago42'			, 'Schmitt', 'Lilli', '1919-08-15');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (52, 'morpheus41'		, 'Wichtel', 'Markus', '1985-10-20');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (53, 'trinity675'		, 'Maier', 'Andrea', '1983-05-22');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (54, 'cypher941'			, 'Einstein', 'John', '1948-02-12');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (55, 'morpheus313'		, 'Schreiner', 'Andreas', '1957-08-25');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (56, 'ArmyOfLamers423'	, 'Niemand', 'Joe', '1907-08-12');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (57, 'fool875'			, 'Becker', 'Martin', '1951-02-20');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (58, 'wiesel715'			, 'Schmeuidt', 'Alois', '1931-07-22');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (59, 'hacker964'			, 'Schaefer', 'Andreas', '1974-12-05');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (60, 'drago179'			, 'Neumann', 'Sarah', '1944-12-24');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (61, 'dau814'			, 'Becker', 'Sascha', '1962-07-01');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (62, 'ArmyOfLamers459'	, 'Newton', 'Sascha', '1908-04-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (63, 'neo517'			, 'Neumann', 'Sascha', '1973-11-06');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (64, 'fool583'			, 'Mueller', 'Alois', '1959-08-08');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (65, 'wiesel509'			, 'Maier', 'Marta', '1954-10-06');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (66, 'cypher687'			, 'Schmeuid', 'Julia', '1980-09-17');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (67, 'neo245'			, 'Schmeuid', 'Albert', '1993-10-29');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (68, 'redneck396'		, 'Bechtel', 'Martin', '1935-05-05');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (69, 'dau316'			, 'Metzger', 'Sascha', '1913-04-11');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (70, 'wiesel432'			, 'Armstrong', 'Sarah', '1925-10-31');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (71, 'drago862'			, 'Schmitt', 'Julia', '1975-10-14');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (72, 'neo636'			, 'Wichtel', 'Sascha', '1943-10-20');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (73, 'cypher642'			, 'Schaefer', 'Sascha', '1988-03-09');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (74, 'zeta712'			, 'Mueller', 'Marianne', '1993-10-29');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (75, 'redneck893'		, 'Armstrong', 'Albert', '1916-11-13');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (76, 'dau749'			, 'Niemand', 'Marianne', '1921-08-10');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (77, 'cypher227'			, 'Niemand', 'Andreas', '1942-08-11');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (78, 'redneck46'			, 'Schmeuidt', 'Alois', '1992-06-12');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (79, 'neo259'			, 'Wichtel', 'Andreas', '1958-12-21');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (80, 'morpheus462'		, 'Schaefer', 'Lilli', '1924-11-02');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (81, 'wiesel600'			, 'Newton', 'Alois', '1971-05-01');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (82, 'ArmyOfLamers334'	, 'Becker', 'Sascha', '1990-07-26');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (83, 'cypher53'			, 'Maier', 'Martin', '1954-02-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (84, 'wiesel529'			, 'Bechtel', 'Ute', '1968-08-07');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (85, 'cyber633'			, 'Mayer', 'Andreas', '1949-09-27');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (86, 'hacker432'			, 'Becker', 'Albert', '1976-12-09');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (87, 'dau424'			, 'Schmeuidt', 'John', '1900-01-28');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (88, 'redneck386'		, 'Mueller', 'Marta', '1926-09-05');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (89, 'neo83'				, 'Schmitt', 'Martin', '1930-12-03');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (90, 'trinity541'		, 'Niemand', 'Markus', '1945-01-10');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (91, 'hacker346'			, 'Schmeuid', 'Jane', '1979-02-15');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (92, 'morpheus438'		, 'Einstein', 'Andrea', '1982-01-30');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (93, 'zeta963'			, 'Mueller', 'Christian', '1925-03-24');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (94, 'dau603'			, 'Schaefer', 'Fritz', '1921-06-12');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (95, 'wiesel703'			, 'Einstein', 'Sarah', '1935-07-10');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (96, 'dau719'			, 'Wichtel', 'Marianne', '1929-10-09');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (97, 'trinity407'		, 'Mueller', 'Andreas', '1910-11-26');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (98, 'cyber938'			, 'Schaefer', 'Marianne', '1927-02-20');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (99, 'drago646'			, 'Einstein', 'Joe', '1906-07-23');
insert into person (euid, nickname, surname, first_name, date_of_birth) values (100, 'wiesel467'		, 'Schmeuid', 'Ute', '1977-02-22');

insert into organization (euid, nickname, firm) values (101, 'drago600', 'Yellow Pages');
insert into organization (euid, nickname, firm) values (102, 'zeta548', 'NoMoney AG.');
insert into organization (euid, nickname, firm) values (103, 'cyber1000', 'Homeshopping PayTV');
insert into organization (euid, nickname, firm) values (104, 'cyber609', 'NoMoney AG.');
insert into organization (euid, nickname, firm) values (105, 'drago891', 'Yellow Pages');
insert into organization (euid, nickname, firm) values (106, 'wiesel73', 'DB2-Deutsch _to Deutsch GmbH');
insert into organization (euid, nickname, firm) values (107, 'dau445', 'Homeshopping PayTV');
insert into organization (euid, nickname, firm) values (108, 'neo484', 'NoMoney AG.');
insert into organization (euid, nickname, firm) values (109, 'zeta342', 'My Firm Inc.');
insert into organization (euid, nickname, firm) values (110, 'ArmyOfLamers79', 'Homeshopping PayTV');

insert into address (adid, username, domain) values (1, 'fool41', 'mailme.net');
insert into address (adid, username, domain) values (2, 'ArmyOfLamers465', 'orprak.com');
insert into address (adid, username, domain) values (3, 'trinity652', 'mailme.com');
insert into address (adid, username, domain) values (4, 'redneck643', 'mailme.com');
insert into address (adid, username, domain) values (5, 'hacker482', 'prak.net');
insert into address (adid, username, domain) values (6, 'morpheus572', 'orprak.net');
insert into address (adid, username, domain) values (7, 'wiesel271', 'orprak.de');
insert into address (adid, username, domain) values (8, 'zeta446', 'prak.net');
insert into address (adid, username, domain) values (9, 'zeta362', 'orprak.net');
insert into address (adid, username, domain) values (10, 'hacker270', 'mailme.com');
insert into address (adid, username, domain) values (11, 'neo366', 'orprak.com');
insert into address (adid, username, domain) values (12, 'cyber550', 'mailme.com');
insert into address (adid, username, domain) values (13, 'drago317', 'orprak.net');
insert into address (adid, username, domain) values (14, 'cyber303', 'prak.com');
insert into address (adid, username, domain) values (15, 'trinity287', 'orprak.de');
insert into address (adid, username, domain) values (16, 'dau41', 'mailme.de');
insert into address (adid, username, domain) values (17, 'cypher555', 'orprak.de');
insert into address (adid, username, domain) values (18, 'cyber31', 'orprak.de');
insert into address (adid, username, domain) values (19, 'cypher751', 'orprak.de');
insert into address (adid, username, domain) values (20, 'ArmyOfLamers993', 'prak.de');
insert into address (adid, username, domain) values (21, 'cyber156', 'prak.net');
insert into address (adid, username, domain) values (22, 'wiesel336', 'mailme.com');
insert into address (adid, username, domain) values (23, 'hacker19', 'prak.de');
insert into address (adid, username, domain) values (24, 'redneck729', 'orprak.net');
insert into address (adid, username, domain) values (25, 'cypher66', 'orprak.net');
insert into address (adid, username, domain) values (26, 'fool22', 'mailme.de');
insert into address (adid, username, domain) values (27, 'neo972', 'prak.com');
insert into address (adid, username, domain) values (28, 'redneck745', 'mailme.com');
insert into address (adid, username, domain) values (29, 'ArmyOfLamers43', 'prak.de');
insert into address (adid, username, domain) values (30, 'redneck355', 'prak.com');
insert into address (adid, username, domain) values (31, 'dau540', 'orprak.de');
insert into address (adid, username, domain) values (32, 'wiesel413', 'mailme.com');
insert into address (adid, username, domain) values (33, 'drago415', 'mailme.de');
insert into address (adid, username, domain) values (34, 'redneck574', 'mailme.com');
insert into address (adid, username, domain) values (35, 'ArmyOfLamers152', 'mailme.de');
insert into address (adid, username, domain) values (36, 'drago171', 'orprak.de');
insert into address (adid, username, domain) values (37, 'morpheus843', 'prak.net');
insert into address (adid, username, domain) values (38, 'ArmyOfLamers482', 'prak.de');
insert into address (adid, username, domain) values (39, 'wiesel410', 'mailme.com');
insert into address (adid, username, domain) values (40, 'fool600', 'orprak.net');
insert into address (adid, username, domain) values (41, 'morpheus833', 'mailme.com');
insert into address (adid, username, domain) values (42, 'wiesel784', 'prak.net');
insert into address (adid, username, domain) values (43, 'zeta410', 'mailme.de');
insert into address (adid, username, domain) values (44, 'zeta170', 'mailme.de');
insert into address (adid, username, domain) values (45, 'drago977', 'orprak.de');
insert into address (adid, username, domain) values (46, 'trinity791', 'orprak.net');
insert into address (adid, username, domain) values (47, 'cypher96', 'orprak.net');
insert into address (adid, username, domain) values (48, 'cypher186', 'prak.net');
insert into address (adid, username, domain) values (49, 'morpheus476', 'prak.net');
insert into address (adid, username, domain) values (50, 'drago804', 'mailme.de');
insert into address (adid, username, domain) values (51, 'redneck787', 'orprak.com');
insert into address (adid, username, domain) values (52, 'hacker652', 'orprak.net');
insert into address (adid, username, domain) values (53, 'dau496', 'orprak.com');
insert into address (adid, username, domain) values (54, 'drago91', 'mailme.net');
insert into address (adid, username, domain) values (55, 'trinity666', 'mailme.net');
insert into address (adid, username, domain) values (56, 'trinity718', 'orprak.com');
insert into address (adid, username, domain) values (57, 'fool635', 'orprak.com');
insert into address (adid, username, domain) values (58, 'trinity415', 'prak.com');
insert into address (adid, username, domain) values (59, 'zeta528', 'mailme.net');
insert into address (adid, username, domain) values (60, 'wiesel960', 'mailme.de');
insert into address (adid, username, domain) values (61, 'morpheus565', 'mailme.de');
insert into address (adid, username, domain) values (62, 'trinity192', 'orprak.de');
insert into address (adid, username, domain) values (63, 'drago992', 'orprak.com');
insert into address (adid, username, domain) values (64, 'neo409', 'mailme.net');
insert into address (adid, username, domain) values (65, 'redneck565', 'prak.net');
insert into address (adid, username, domain) values (66, 'hacker670', 'mailme.net');
insert into address (adid, username, domain) values (67, 'zeta739', 'prak.com');
insert into address (adid, username, domain) values (68, 'zeta939', 'prak.net');
insert into address (adid, username, domain) values (69, 'morpheus561', 'mailme.com');
insert into address (adid, username, domain) values (70, 'cyber262', 'mailme.de');
insert into address (adid, username, domain) values (71, 'hacker922', 'orprak.com');
insert into address (adid, username, domain) values (72, 'neo10', 'orprak.net');
insert into address (adid, username, domain) values (73, 'hacker669', 'mailme.com');
insert into address (adid, username, domain) values (74, 'cyber821', 'prak.com');
insert into address (adid, username, domain) values (75, 'ArmyOfLamers601', 'prak.net');
insert into address (adid, username, domain) values (76, 'cyber593', 'mailme.de');
insert into address (adid, username, domain) values (77, 'fool181', 'mailme.net');
insert into address (adid, username, domain) values (78, 'fool863', 'orprak.de');
insert into address (adid, username, domain) values (79, 'drago295', 'orprak.de');
insert into address (adid, username, domain) values (80, 'dau272', 'prak.de');
insert into address (adid, username, domain) values (81, 'hacker231', 'orprak.de');
insert into address (adid, username, domain) values (82, 'hacker791', 'orprak.de');
insert into address (adid, username, domain) values (83, 'fool426', 'prak.de');
insert into address (adid, username, domain) values (84, 'ArmyOfLamers507', 'orprak.net');
insert into address (adid, username, domain) values (85, 'cyber314', 'mailme.net');
insert into address (adid, username, domain) values (86, 'redneck811', 'orprak.net');
insert into address (adid, username, domain) values (87, 'ArmyOfLamers753', 'orprak.de');
insert into address (adid, username, domain) values (88, 'hacker719', 'orprak.net');
insert into address (adid, username, domain) values (89, 'neo940', 'mailme.de');
insert into address (adid, username, domain) values (90, 'hacker900', 'mailme.de');
insert into address (adid, username, domain) values (91, 'cyber312', 'mailme.com');
insert into address (adid, username, domain) values (92, 'cyber479', 'mailme.net');
insert into address (adid, username, domain) values (93, 'redneck691', 'mailme.de');
insert into address (adid, username, domain) values (94, 'hacker968', 'mailme.de');
insert into address (adid, username, domain) values (95, 'morpheus726', 'prak.com');
insert into address (adid, username, domain) values (96, 'ArmyOfLamers790', 'mailme.com');
insert into address (adid, username, domain) values (97, 'neo934', 'mailme.de');
insert into address (adid, username, domain) values (98, 'fool690', 'prak.com');
insert into address (adid, username, domain) values (99, 'neo259', 'mailme.de');
insert into address (adid, username, domain) values (100, 'cypher21', 'mailme.com');
insert into address (adid, username, domain) values (101, 'neo446', 'mailme.de');
insert into address (adid, username, domain) values (102, 'cypher610', 'mailme.com');
insert into address (adid, username, domain) values (103, 'morpheus947', 'mailme.com');
insert into address (adid, username, domain) values (104, 'fool675', 'mailme.de');
insert into address (adid, username, domain) values (105, 'wiesel15', 'prak.de');
insert into address (adid, username, domain) values (106, 'wiesel937', 'orprak.net');
insert into address (adid, username, domain) values (107, 'neo881', 'mailme.net');
insert into address (adid, username, domain) values (108, 'cypher660', 'prak.net');
insert into address (adid, username, domain) values (109, 'hacker73', 'mailme.com');
insert into address (adid, username, domain) values (110, 'morpheus967', 'prak.net');
insert into address (adid, username, domain) values (111, 'neo475', 'orprak.com');
insert into address (adid, username, domain) values (112, 'neo415', 'prak.com');
insert into address (adid, username, domain) values (113, 'trinity265', 'prak.com');
insert into address (adid, username, domain) values (114, 'ArmyOfLamers303', 'mailme.net');
insert into address (adid, username, domain) values (115, 'redneck183', 'prak.net');
insert into address (adid, username, domain) values (116, 'zeta310', 'prak.net');
insert into address (adid, username, domain) values (117, 'zeta543', 'mailme.com');
insert into address (adid, username, domain) values (118, 'cyber86', 'orprak.net');
insert into address (adid, username, domain) values (119, 'neo79', 'orprak.net');
insert into address (adid, username, domain) values (120, 'fool58', 'orprak.com');
insert into address (adid, username, domain) values (121, 'redneck428', 'mailme.de');
insert into address (adid, username, domain) values (122, 'zeta114', 'orprak.de');
insert into address (adid, username, domain) values (123, 'wiesel902', 'prak.de');
insert into address (adid, username, domain) values (124, 'dau951', 'prak.de');
insert into address (adid, username, domain) values (125, 'hacker527', 'prak.net');
insert into address (adid, username, domain) values (126, 'cyber70', 'prak.de');
insert into address (adid, username, domain) values (127, 'redneck390', 'mailme.com');
insert into address (adid, username, domain) values (128, 'dau919', 'orprak.de');
insert into address (adid, username, domain) values (129, 'hacker487', 'orprak.net');
insert into address (adid, username, domain) values (130, 'redneck303', 'orprak.de');
insert into address (adid, username, domain) values (131, 'cyber589', 'orprak.net');
insert into address (adid, username, domain) values (132, 'redneck581', 'mailme.de');
insert into address (adid, username, domain) values (133, 'trinity854', 'orprak.net');
insert into address (adid, username, domain) values (134, 'hacker423', 'mailme.com');
insert into address (adid, username, domain) values (135, 'fool289', 'orprak.de');
insert into address (adid, username, domain) values (136, 'hacker333', 'mailme.net');
insert into address (adid, username, domain) values (137, 'redneck424', 'prak.com');
insert into address (adid, username, domain) values (138, 'neo506', 'mailme.com');
insert into address (adid, username, domain) values (139, 'cypher239', 'mailme.net');
insert into address (adid, username, domain) values (140, 'dau71', 'prak.de');
insert into address (adid, username, domain) values (141, 'zeta819', 'prak.de');
insert into address (adid, username, domain) values (142, 'dau47', 'orprak.net');
insert into address (adid, username, domain) values (143, 'cypher278', 'mailme.de');
insert into address (adid, username, domain) values (144, 'drago261', 'prak.net');
insert into address (adid, username, domain) values (145, 'redneck197', 'mailme.de');
insert into address (adid, username, domain) values (146, 'neo730', 'mailme.com');
insert into address (adid, username, domain) values (147, 'cypher163', 'mailme.de');
insert into address (adid, username, domain) values (148, 'fool346', 'mailme.com');
insert into address (adid, username, domain) values (149, 'morpheus917', 'mailme.net');
insert into address (adid, username, domain) values (150, 'fool952', 'prak.com');
insert into address (adid, username, domain) values (151, 'trinity930', 'mailme.de');
insert into address (adid, username, domain) values (152, 'cypher833', 'prak.de');
insert into address (adid, username, domain) values (153, 'zeta270', 'orprak.com');
insert into address (adid, username, domain) values (154, 'wiesel232', 'orprak.net');
insert into address (adid, username, domain) values (155, 'drago604', 'orprak.com');
insert into address (adid, username, domain) values (156, 'wiesel229', 'mailme.com');
insert into address (adid, username, domain) values (157, 'hacker79', 'prak.com');
insert into address (adid, username, domain) values (158, 'cyber43', 'mailme.com');
insert into address (adid, username, domain) values (159, 'dau681', 'orprak.de');
insert into address (adid, username, domain) values (160, 'dau678', 'prak.com');
insert into address (adid, username, domain) values (161, 'redneck308', 'prak.net');
insert into address (adid, username, domain) values (162, 'ArmyOfLamers424', 'mailme.com');
insert into address (adid, username, domain) values (163, 'fool53', 'mailme.de');
insert into address (adid, username, domain) values (164, 'hacker580', 'prak.net');
insert into address (adid, username, domain) values (165, 'hacker502', 'mailme.com');
insert into address (adid, username, domain) values (166, 'dau195', 'mailme.de');
insert into address (adid, username, domain) values (167, 'trinity408', 'mailme.com');
insert into address (adid, username, domain) values (168, 'redneck395', 'prak.com');
insert into address (adid, username, domain) values (169, 'dau847', 'mailme.com');
insert into address (adid, username, domain) values (170, 'cypher33', 'orprak.de');
insert into address (adid, username, domain) values (171, 'hacker332', 'mailme.de');
insert into address (adid, username, domain) values (172, 'zeta785', 'orprak.net');
insert into address (adid, username, domain) values (173, 'cyber713', 'mailme.com');
insert into address (adid, username, domain) values (174, 'zeta741', 'prak.net');
insert into address (adid, username, domain) values (175, 'cyber700', 'prak.de');
insert into address (adid, username, domain) values (176, 'fool783', 'mailme.net');
insert into address (adid, username, domain) values (177, 'cyber77', 'prak.de');
insert into address (adid, username, domain) values (178, 'dau718', 'prak.com');
insert into address (adid, username, domain) values (179, 'neo26', 'mailme.net');
insert into address (adid, username, domain) values (180, 'redneck670', 'orprak.de');
insert into address (adid, username, domain) values (181, 'wiesel383', 'mailme.de');
insert into address (adid, username, domain) values (182, 'fool44', 'prak.com');
insert into address (adid, username, domain) values (183, 'neo776', 'prak.com');
insert into address (adid, username, domain) values (184, 'zeta609', 'prak.com');
insert into address (adid, username, domain) values (185, 'neo961', 'prak.com');
insert into address (adid, username, domain) values (186, 'fool979', 'prak.de');
insert into address (adid, username, domain) values (187, 'cypher689', 'mailme.de');
insert into address (adid, username, domain) values (188, 'neo332', 'mailme.de');
insert into address (adid, username, domain) values (189, 'cypher973', 'mailme.de');
insert into address (adid, username, domain) values (190, 'zeta519', 'orprak.net');
insert into address (adid, username, domain) values (191, 'dau304', 'orprak.com');
insert into address (adid, username, domain) values (192, 'trinity909', 'mailme.com');
insert into address (adid, username, domain) values (193, 'drago526', 'mailme.de');
insert into address (adid, username, domain) values (194, 'morpheus11', 'prak.net');
insert into address (adid, username, domain) values (195, 'cyber942', 'orprak.com');
insert into address (adid, username, domain) values (196, 'morpheus549', 'orprak.com');
insert into address (adid, username, domain) values (197, 'zeta471', 'prak.net');
insert into address (adid, username, domain) values (198, 'cypher708', 'orprak.com');
insert into address (adid, username, domain) values (199, 'zeta695', 'prak.net');
insert into address (adid, username, domain) values (200, 'hacker252', 'prak.net');

insert into use (euser, address) values (66, 1);
insert into use (euser, address) values (105, 2);
insert into use (euser, address) values (129, 3);
insert into use (euser, address) values (66, 4);
insert into use (euser, address) values (97, 5);
insert into use (euser, address) values (68, 6);
insert into use (euser, address) values (50, 7);
insert into use (euser, address) values (57, 8);
insert into use (euser, address) values (34, 9);
insert into use (euser, address) values (112, 10);
insert into use (euser, address) values (96, 11);
insert into use (euser, address) values (83, 12);
insert into use (euser, address) values (111, 13);
insert into use (euser, address) values (65, 14);
insert into use (euser, address) values (61, 15);
insert into use (euser, address) values (80, 16);
insert into use (euser, address) values (103, 17);
insert into use (euser, address) values (112, 18);
insert into use (euser, address) values (57, 19);
insert into use (euser, address) values (88, 20);
insert into use (euser, address) values (4, 21);
insert into use (euser, address) values (34, 22);
insert into use (euser, address) values (50, 23);
insert into use (euser, address) values (2, 24);
insert into use (euser, address) values (7, 25);
insert into use (euser, address) values (116, 26);
insert into use (euser, address) values (91, 27);
insert into use (euser, address) values (64, 28);
insert into use (euser, address) values (69, 29);
insert into use (euser, address) values (78, 30);
insert into use (euser, address) values (115, 31);
insert into use (euser, address) values (25, 32);
insert into use (euser, address) values (118, 33);
insert into use (euser, address) values (121, 34);
insert into use (euser, address) values (76, 35);
insert into use (euser, address) values (64, 36);
insert into use (euser, address) values (13, 37);
insert into use (euser, address) values (97, 38);
insert into use (euser, address) values (107, 39);
insert into use (euser, address) values (71, 40);
insert into use (euser, address) values (102, 41);
insert into use (euser, address) values (82, 42);
insert into use (euser, address) values (54, 43);
insert into use (euser, address) values (8, 44);
insert into use (euser, address) values (27, 45);
insert into use (euser, address) values (70, 46);
insert into use (euser, address) values (30, 47);
insert into use (euser, address) values (97, 48);
insert into use (euser, address) values (64, 49);
insert into use (euser, address) values (22, 50);
insert into use (euser, address) values (123, 51);
insert into use (euser, address) values (12, 52);
insert into use (euser, address) values (109, 53);
insert into use (euser, address) values (30, 54);
insert into use (euser, address) values (73, 55);
insert into use (euser, address) values (125, 56);
insert into use (euser, address) values (92, 57);
insert into use (euser, address) values (18, 58);
insert into use (euser, address) values (103, 59);
insert into use (euser, address) values (123, 60);
insert into use (euser, address) values (57, 61);
insert into use (euser, address) values (64, 62);
insert into use (euser, address) values (84, 63);
insert into use (euser, address) values (108, 64);
insert into use (euser, address) values (41, 65);
insert into use (euser, address) values (76, 66);
insert into use (euser, address) values (50, 67);
insert into use (euser, address) values (15, 68);
insert into use (euser, address) values (11, 69);
insert into use (euser, address) values (32, 70);
insert into use (euser, address) values (18, 71);
insert into use (euser, address) values (15, 72);
insert into use (euser, address) values (61, 73);
insert into use (euser, address) values (58, 74);
insert into use (euser, address) values (93, 75);
insert into use (euser, address) values (62, 76);
insert into use (euser, address) values (7, 77);
insert into use (euser, address) values (69, 78);
insert into use (euser, address) values (76, 79);
insert into use (euser, address) values (57, 80);
insert into use (euser, address) values (8, 81);
insert into use (euser, address) values (47, 82);
insert into use (euser, address) values (94, 83);
insert into use (euser, address) values (126, 84);
insert into use (euser, address) values (47, 85);
insert into use (euser, address) values (70, 86);
insert into use (euser, address) values (42, 87);
insert into use (euser, address) values (97, 88);
insert into use (euser, address) values (127, 89);
insert into use (euser, address) values (45, 90);
insert into use (euser, address) values (14, 91);
insert into use (euser, address) values (125, 92);
insert into use (euser, address) values (123, 93);
insert into use (euser, address) values (52, 94);
insert into use (euser, address) values (98, 95);
insert into use (euser, address) values (23, 96);
insert into use (euser, address) values (28, 97);
insert into use (euser, address) values (109, 98);
insert into use (euser, address) values (108, 99);
insert into use (euser, address) values (92, 100);
insert into use (euser, address) values (61, 101);
insert into use (euser, address) values (57, 102);
insert into use (euser, address) values (121, 103);
insert into use (euser, address) values (76, 104);
insert into use (euser, address) values (89, 105);
insert into use (euser, address) values (30, 106);
insert into use (euser, address) values (92, 107);
insert into use (euser, address) values (95, 108);
insert into use (euser, address) values (5, 109);
insert into use (euser, address) values (130, 110);
insert into use (euser, address) values (41, 111);
insert into use (euser, address) values (119, 112);
insert into use (euser, address) values (65, 113);
insert into use (euser, address) values (96, 114);
insert into use (euser, address) values (97, 115);
insert into use (euser, address) values (113, 116);
insert into use (euser, address) values (41, 117);
insert into use (euser, address) values (2, 118);
insert into use (euser, address) values (24, 119);
insert into use (euser, address) values (16, 120);
insert into use (euser, address) values (105, 121);
insert into use (euser, address) values (47, 122);
insert into use (euser, address) values (23, 123);
insert into use (euser, address) values (61, 124);
insert into use (euser, address) values (94, 125);
insert into use (euser, address) values (120, 126);
insert into use (euser, address) values (10, 127);
insert into use (euser, address) values (45, 128);
insert into use (euser, address) values (76, 129);
insert into use (euser, address) values (19, 130);
insert into use (euser, address) values (45, 131);
insert into use (euser, address) values (118, 132);
insert into use (euser, address) values (95, 133);
insert into use (euser, address) values (82, 134);
insert into use (euser, address) values (31, 135);
insert into use (euser, address) values (74, 136);
insert into use (euser, address) values (7, 137);
insert into use (euser, address) values (21, 138);
insert into use (euser, address) values (119, 139);
insert into use (euser, address) values (49, 140);
insert into use (euser, address) values (67, 141);
insert into use (euser, address) values (82, 142);
insert into use (euser, address) values (1, 143);
insert into use (euser, address) values (124, 144);
insert into use (euser, address) values (54, 145);
insert into use (euser, address) values (96, 146);
insert into use (euser, address) values (129, 147);
insert into use (euser, address) values (92, 148);
insert into use (euser, address) values (58, 149);
insert into use (euser, address) values (50, 150);
insert into use (euser, address) values (54, 151);
insert into use (euser, address) values (68, 152);
insert into use (euser, address) values (35, 153);
insert into use (euser, address) values (130, 154);
insert into use (euser, address) values (24, 155);
insert into use (euser, address) values (79, 156);
insert into use (euser, address) values (19, 157);
insert into use (euser, address) values (95, 158);
insert into use (euser, address) values (94, 159);
insert into use (euser, address) values (125, 160);
insert into use (euser, address) values (104, 161);
insert into use (euser, address) values (93, 162);
insert into use (euser, address) values (92, 163);
insert into use (euser, address) values (77, 164);
insert into use (euser, address) values (8, 165);
insert into use (euser, address) values (109, 166);
insert into use (euser, address) values (61, 167);
insert into use (euser, address) values (95, 168);
insert into use (euser, address) values (75, 169);
insert into use (euser, address) values (122, 170);
insert into use (euser, address) values (127, 171);
insert into use (euser, address) values (65, 172);
insert into use (euser, address) values (114, 173);
insert into use (euser, address) values (116, 174);
insert into use (euser, address) values (38, 175);
insert into use (euser, address) values (83, 176);
insert into use (euser, address) values (89, 177);
insert into use (euser, address) values (2, 178);
insert into use (euser, address) values (51, 179);
insert into use (euser, address) values (53, 180);
insert into use (euser, address) values (27, 181);
insert into use (euser, address) values (93, 182);
insert into use (euser, address) values (81, 183);
insert into use (euser, address) values (82, 184);
insert into use (euser, address) values (53, 185);
insert into use (euser, address) values (126, 186);
insert into use (euser, address) values (23, 187);
insert into use (euser, address) values (22, 188);
insert into use (euser, address) values (35, 189);
insert into use (euser, address) values (110, 190);
insert into use (euser, address) values (130, 191);
insert into use (euser, address) values (72, 192);
insert into use (euser, address) values (88, 193);
insert into use (euser, address) values (29, 194);
insert into use (euser, address) values (70, 195);
insert into use (euser, address) values (21, 196);
insert into use (euser, address) values (82, 197);
insert into use (euser, address) values (112, 198);
insert into use (euser, address) values (83, 199);
insert into use (euser, address) values (124, 200);
insert into use (euser, address) values (11, 52);
insert into use (euser, address) values (62, 157);
insert into use (euser, address) values (18, 141);
insert into use (euser, address) values (56, 60);
insert into use (euser, address) values (24, 146);
insert into use (euser, address) values (128, 147);
insert into use (euser, address) values (79, 27);
insert into use (euser, address) values (78, 27);
insert into use (euser, address) values (34, 181);
insert into use (euser, address) values (74, 59);
insert into use (euser, address) values (2, 28);
insert into use (euser, address) values (71, 18);
insert into use (euser, address) values (114, 138);
insert into use (euser, address) values (22, 13);
insert into use (euser, address) values (14, 127);
insert into use (euser, address) values (106, 152);
insert into use (euser, address) values (116, 116);
insert into use (euser, address) values (12, 163);
insert into use (euser, address) values (96, 183);
insert into use (euser, address) values (6, 158);
insert into use (euser, address) values (98, 20);
insert into use (euser, address) values (52, 96);
insert into use (euser, address) values (51, 158);
insert into use (euser, address) values (49, 160);
insert into use (euser, address) values (19, 159);
insert into use (euser, address) values (115, 84);
insert into use (euser, address) values (81, 28);
insert into use (euser, address) values (94, 179);
insert into use (euser, address) values (63, 146);
insert into use (euser, address) values (87, 102);
insert into use (euser, address) values (52, 162);
insert into use (euser, address) values (110, 191);
insert into use (euser, address) values (94, 12);
insert into use (euser, address) values (124, 132);
insert into use (euser, address) values (85, 191);
insert into use (euser, address) values (20, 183);
insert into use (euser, address) values (57, 41);
insert into use (euser, address) values (63, 159);
insert into use (euser, address) values (54, 6);
insert into use (euser, address) values (80, 142);

insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (1, 'Spass', 'Wie gehts', '2003-02-02 08:52:49', 113, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (2, 'Spass', 'Hallo World', '2009-05-17 02:09:23', 138, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  1, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (3, 'Spass', 'Wollte nur mal Hallo sagen', '2011-05-21 11:31:40', 70, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  2, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (4, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-01-10 17:12:56', 195, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  2, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (5, 'Film', 'Hallo World', '2010-12-30 00:58:22', 190, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (6, 'Urlaub', 'Wie gehts', '2011-11-22 18:34:20', 197, 'Die ist ein Test.',  5, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (7, 'MyLife', 'Wichtig!', '2012-02-26 12:41:37', 177, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  6, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (8, 'MyLife', 'Wichtig!', '2012-03-08 23:22:52', 149, 'Ich hasse Spam-Mails.',  7, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (9, 'MyLife', 'Hallo World', '2012-01-11 11:34:08', 168, 'Die ist ein Test.',  6, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (10, 'Spass', 'Hallo World', '2012-04-13 13:16:54', 49, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  9, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (11, 'Spass', 'Wollte nur mal Hallo sagen', '2012-04-05 07:58:56', 88, 'Die ist ein Test.',  6, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (12, 'MyLife', 'Wichtig!', '2012-04-14 11:52:56', 56, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  11, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (13, 'Film', 'Wie gehts', '2006-04-16 08:13:57', 6, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (14, 'MyLife', 'Wollte nur mal Hallo sagen', '2008-04-21 21:05:21', 177, 'Ich hasse Spam-Mails.',  13, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (15, 'Film', 'Hallo World', '2008-09-07 22:33:19', 124, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  14, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (16, 'Film', 'Wollte nur mal Hallo sagen', '2010-08-18 19:28:31', 18, 'Die ist ein Test.',  15, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (17, 'Film', 'Wichtig!', '2011-11-18 23:58:15', 137, 'Ich hasse Spam-Mails.',  16, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (18, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-03-21 23:11:15', 86, 'No content.',  17, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (19, 'Spass', 'Wie gehts', '2010-11-17 09:29:28', 182, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  16, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (20, 'MyLife', 'Hallo World', '2012-03-12 12:07:00', 25, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  19, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (21, 'Spass', 'Wichtig!', '2011-08-30 13:52:42', 66, 'No content.',  19, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (22, 'MyLife', 'Wie gehts', '2011-09-10 23:30:37', 107, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  15, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (23, 'Film', 'Wie gehts', '2012-02-02 08:36:36', 149, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  22, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (24, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-03-17 02:09:26', 12, 'Ich hasse Spam-Mails.',  23, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (25, 'Spass', 'Wollte nur mal Hallo sagen', '2012-03-09 17:12:52', 55, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  23, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (26, 'MyLife', 'Wie gehts', '2012-02-14 16:15:17', 68, 'Ich hasse Spam-Mails.',  23, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (27, 'Film', 'Wichtig!', '2009-05-31 01:44:07', 20, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  14, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (28, 'Urlaub', 'Wichtig!', '2010-09-29 18:41:12', 76, 'No content.',  27, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (29, 'Film', 'Hallo World', '2010-11-15 14:52:07', 52, 'Die ist ein Test.',  28, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (30, 'Film', 'Wie gehts', '2011-09-02 03:16:44', 59, 'Die ist ein Test.',  29, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (31, 'Urlaub', 'Wie gehts', '2011-07-07 02:19:37', 174, 'No content.',  29, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (32, 'Urlaub', 'Wollte nur mal Hallo sagen', '2010-02-05 09:43:57', 4, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  27, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (33, 'Film', 'Hallo World', '2010-03-16 23:27:15', 123, 'No content.',  32, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (34, 'Film', 'Hallo World', '2011-10-01 05:48:07', 91, 'Die ist ein Test.',  33, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (35, 'Spass', 'Wichtig!', '2011-04-21 09:53:13', 106, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  33, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (36, 'Spass', 'Wichtig!', '2010-10-04 14:36:19', 192, 'Ich hasse Spam-Mails.',  33, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (37, 'MyLife', 'Wichtig!', '2011-04-09 01:46:07', 116, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  32, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (38, 'Urlaub', 'Wichtig!', '2011-12-17 06:15:35', 139, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  37, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (39, 'MyLife', 'Wie gehts', '2011-09-28 06:12:56', 128, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  37, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (40, 'MyLife', 'Wichtig!', '2011-07-06 09:26:35', 65, 'Die ist ein Test.',  37, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (41, 'MyLife', 'Hallo World', '2010-12-01 23:14:51', 149, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  32, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (42, 'Spass', 'Wichtig!', '2011-03-26 05:32:52', 133, 'Ich hasse Spam-Mails.',  41, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (43, 'MyLife', 'Wichtig!', '2009-10-07 05:41:09', 183, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  27, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (44, 'Spass', 'Wichtig!', '2010-08-31 14:39:50', 79, 'No content.',  43, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (45, 'Film', 'Wollte nur mal Hallo sagen', '2010-12-08 14:28:00', 185, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  44, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (46, 'MyLife', 'Hallo World', '2012-01-26 02:13:53', 171, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  43, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (47, 'Urlaub', 'Wie gehts', '2012-04-16 05:20:24', 16, 'Ich hasse Spam-Mails.',  46, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (48, 'Film', 'Wollte nur mal Hallo sagen', '2012-03-02 23:16:04', 26, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  46, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (49, 'Spass', 'Wollte nur mal Hallo sagen', '2012-01-30 09:12:08', 167, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  46, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (50, 'Spass', 'Wichtig!', '2010-07-23 21:27:00', 61, 'Ich hasse Spam-Mails.',  14, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (51, 'MyLife', 'Wollte nur mal Hallo sagen', '2010-10-15 14:56:54', 25, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  50, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (52, 'Spass', 'Wichtig!', '2010-12-05 13:44:34', 185, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  51, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (53, 'MyLife', 'Wollte nur mal Hallo sagen', '2011-08-30 10:00:08', 124, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  52, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (54, 'Film', 'Wie gehts', '2011-09-19 04:46:49', 152, 'Die ist ein Test.',  52, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (55, 'Spass', 'Wie gehts', '2011-10-21 00:50:22', 136, 'Ich hasse Spam-Mails.',  52, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (56, 'MyLife', 'Wichtig!', '2011-03-23 23:29:18', 146, 'Ich hasse Spam-Mails.',  51, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (57, 'Film', 'Wie gehts', '2012-04-02 10:13:02', 99, 'Ich hasse Spam-Mails.',  56, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (58, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-12-20 01:16:06', 124, 'No content.',  56, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (59, 'MyLife', 'Wichtig!', '2009-12-21 15:05:04', 94, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  13, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (60, 'Film', 'Wollte nur mal Hallo sagen', '2010-08-24 23:41:14', 108, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  59, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (61, 'Spass', 'Wie gehts', '2010-10-15 03:08:04', 107, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  60, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (62, 'Spass', 'Wichtig!', '2012-03-03 23:39:39', 19, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  61, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (63, 'Spass', 'Wie gehts', '2012-04-16 01:54:36', 88, 'Ich hasse Spam-Mails.',  62, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (64, 'Urlaub', 'Wichtig!', '2012-03-23 14:37:17', 86, 'No content.',  62, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (65, 'Film', 'Wollte nur mal Hallo sagen', '2012-03-28 22:14:43', 104, 'Ich hasse Spam-Mails.',  62, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (66, 'Film', 'Wollte nur mal Hallo sagen', '2011-04-26 01:19:30', 129, 'Ich hasse Spam-Mails.',  61, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (67, 'Urlaub', 'Wichtig!', '2011-05-18 01:36:33', 160, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  66, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (68, 'Spass', 'Wichtig!', '2012-04-17 15:51:09', 59, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  60, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (69, 'Film', 'Wichtig!', '2012-04-24 22:11:00', 32, 'Die ist ein Test.',  68, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (70, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-04-25 13:37:39', 46, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  69, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (71, 'Film', 'Wichtig!', '2012-04-25 16:54:36', 92, 'Ich hasse Spam-Mails.',  69, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (72, 'Film', 'Wichtig!', '2012-04-26 03:56:49', 120, 'Die ist ein Test.',  69, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (73, 'Spass', 'Wollte nur mal Hallo sagen', '2012-04-24 21:51:05', 27, 'Ich hasse Spam-Mails.',  68, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (74, 'MyLife', 'Wie gehts', '2012-04-26 15:32:39', 59, 'Die ist ein Test.',  73, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (75, 'Spass', 'Wichtig!', '2012-04-25 02:39:52', 196, 'No content.',  73, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (76, 'Film', 'Hallo World', '2011-03-16 19:43:50', 80, 'Die ist ein Test.',  60, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (77, 'MyLife', 'Hallo World', '2011-11-23 14:05:59', 93, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  76, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (78, 'Film', 'Hallo World', '2012-03-04 03:53:58', 86, 'Ich hasse Spam-Mails.',  77, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (79, 'Urlaub', 'Hallo World', '2012-03-12 15:35:06', 154, 'Ich hasse Spam-Mails.',  77, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (80, 'Film', 'Wie gehts', '2011-12-29 06:40:15', 106, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  77, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (81, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-06-22 04:50:23', 99, 'Die ist ein Test.',  76, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (82, 'Spass', 'Wie gehts', '2012-01-14 07:08:48', 10, 'Die ist ein Test.',  81, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (83, 'MyLife', 'Hallo World', '2010-02-16 07:00:55', 162, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  13, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (84, 'Urlaub', 'Hallo World', '2011-02-15 14:10:36', 52, 'No content.',  83, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (85, 'Spass', 'Hallo World', '2012-04-15 10:39:08', 118, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  84, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (86, 'Spass', 'Wie gehts', '2012-04-21 09:31:10', 100, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  85, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (87, 'Film', 'Wie gehts', '2012-04-26 14:26:20', 126, 'Ich hasse Spam-Mails.',  86, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (88, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-04-20 11:03:41', 113, 'Ich hasse Spam-Mails.',  85, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (89, 'Film', 'Wie gehts', '2012-04-23 16:13:02', 79, 'Die ist ein Test.',  88, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (90, 'Spass', 'Wollte nur mal Hallo sagen', '2012-04-21 16:41:27', 42, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  88, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (91, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-04-21 19:09:51', 145, 'Ich hasse Spam-Mails.',  88, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (92, 'Spass', 'Wichtig!', '2012-04-22 10:01:30', 96, 'Ich hasse Spam-Mails.',  85, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (93, 'Spass', 'Wie gehts', '2012-04-26 12:51:38', 62, 'No content.',  92, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (94, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-10-04 09:59:54', 25, 'No content.',  84, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (95, 'Urlaub', 'Wichtig!', '2012-01-06 12:35:26', 155, 'No content.',  94, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (96, 'Urlaub', 'Wie gehts', '2012-02-20 13:39:52', 17, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  95, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (97, 'MyLife', 'Wie gehts', '2012-04-11 18:51:54', 104, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  95, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (98, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-12-29 18:54:09', 188, 'No content.',  94, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (99, 'MyLife', 'Hallo World', '2012-04-05 15:58:52', 149, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  98, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (100, 'Spass', 'Wichtig!', '2012-03-13 08:16:34', 70, 'Ich hasse Spam-Mails.',  98, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (101, 'Urlaub', 'Wie gehts', '2012-04-16 04:03:23', 79, 'Die ist ein Test.',  98, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (102, 'Spass', 'Hallo World', '2011-07-24 07:07:55', 168, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  83, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (103, 'Film', 'Wollte nur mal Hallo sagen', '2012-03-16 10:11:44', 4, 'Die ist ein Test.',  102, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (104, 'Film', 'Hallo World', '2012-04-01 16:55:23', 65, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  103, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (105, 'Urlaub', 'Hallo World', '2012-04-10 10:27:18', 103, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  104, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (106, 'MyLife', 'Wie gehts', '2012-03-26 06:14:46', 177, 'No content.',  103, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (107, 'Spass', 'Hallo World', '2012-04-12 13:29:02', 96, 'No content.',  106, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (108, 'Film', 'Hallo World', '2012-04-01 20:36:54', 90, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  106, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (109, 'Spass', 'Wollte nur mal Hallo sagen', '2012-04-12 14:58:05', 97, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  106, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (110, 'MyLife', 'Wichtig!', '2012-04-22 19:52:58', 196, 'Die ist ein Test.',  103, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (111, 'Film', 'Wollte nur mal Hallo sagen', '2012-04-25 20:52:02', 98, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  110, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (112, 'MyLife', 'Wie gehts', '2012-04-23 15:45:37', 33, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  102, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (113, 'Film', 'Hallo World', '2012-04-25 20:29:54', 168, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  112, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (114, 'Urlaub', 'Wichtig!', '2012-04-26 03:24:07', 151, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  113, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (115, 'Urlaub', 'Wichtig!', '2012-04-26 12:50:42', 76, 'No content.',  113, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (116, 'MyLife', 'Wie gehts', '2011-03-29 09:43:47', 62, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  83, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (117, 'Film', 'Wichtig!', '2011-08-23 22:30:43', 84, 'No content.',  116, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (118, 'MyLife', 'Wichtig!', '2011-12-16 07:50:16', 20, 'Ich hasse Spam-Mails.',  117, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (119, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-03-24 10:46:32', 17, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (120, 'Urlaub', 'Wichtig!', '2012-02-05 15:58:26', 88, 'Ich hasse Spam-Mails.',  118, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (121, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-01-02 21:53:17', 172, 'Die ist ein Test.',  116, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (122, 'Urlaub', 'Wichtig!', '2012-04-02 02:08:59', 97, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  121, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (123, 'Spass', 'Wichtig!', '2012-04-15 11:58:50', 169, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  122, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (124, 'Film', 'Wichtig!', '2012-04-14 03:32:37', 75, 'Die ist ein Test.',  122, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (125, 'Spass', 'Hallo World', '2012-04-15 09:42:43', 157, 'No content.',  122, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (126, 'Spass', 'Hallo World', '2012-01-30 10:45:58', 102, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  121, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (127, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-03-09 14:02:37', 114, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  126, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (128, 'MyLife', 'Wichtig!', '2012-02-09 14:34:18', 115, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  121, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (129, 'Film', 'Wichtig!', '2012-02-22 19:25:16', 51, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  128, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (130, 'Urlaub', 'Hallo World', '2012-01-01 23:20:37', 181, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  116, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (131, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-03-22 14:34:58', 86, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  130, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (132, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-03-22 16:01:25', 62, 'Ich hasse Spam-Mails.',  131, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (133, 'Urlaub', 'Hallo World', '2012-03-21 13:39:23', 2, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  130, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (134, 'Spass', 'Hallo World', '2012-03-25 06:19:16', 101, 'Ich hasse Spam-Mails.',  133, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (135, 'Urlaub', 'Hallo World', '2012-04-13 01:06:11', 184, 'Die ist ein Test.',  133, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (136, 'Spass', 'Wichtig!', '2012-04-17 20:05:35', 175, 'No content.',  133, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (137, 'MyLife', 'Wollte nur mal Hallo sagen', '2004-09-01 09:55:56', 181, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (138, 'Urlaub', 'Wichtig!', '2011-06-18 09:23:08', 26, 'No content.',  137, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (139, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-04-07 16:15:15', 54, 'Ich hasse Spam-Mails.',  138, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (140, 'Spass', 'Wichtig!', '2011-08-09 18:09:36', 83, 'Ich hasse Spam-Mails.',  137, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (141, 'Spass', 'Wichtig!', '2011-09-15 17:16:30', 69, 'Ich hasse Spam-Mails.',  140, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (142, 'MyLife', 'Wichtig!', '2007-01-21 20:55:48', 41, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (143, 'Urlaub', 'Wie gehts', '2011-02-24 10:40:45', 24, 'Ich hasse Spam-Mails.',  142, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (144, 'MyLife', 'Wollte nur mal Hallo sagen', '2011-06-23 07:00:19', 99, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  143, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (145, 'Urlaub', 'Wichtig!', '2012-03-09 06:37:39', 140, 'No content.',  143, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (146, 'Urlaub', 'Hallo World', '2011-04-29 03:45:47', 73, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (147, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-01-11 02:44:25', 163, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  146, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (148, 'MyLife', 'Hallo World', '2012-01-27 22:13:06', 147, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  147, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (149, 'Spass', 'Hallo World', '2012-03-05 02:10:45', 69, 'Ich hasse Spam-Mails.',  148, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (150, 'Film', 'Wie gehts', '2012-03-21 05:47:51', 147, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  149, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (151, 'Film', 'Wichtig!', '2012-03-27 21:53:18', 117, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  150, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (152, 'Urlaub', 'Hallo World', '2012-04-17 21:24:00', 36, 'Die ist ein Test.',  149, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (153, 'Film', 'Hallo World', '2012-04-24 17:25:25', 54, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  152, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (154, 'Urlaub', 'Hallo World', '2012-01-25 10:17:39', 155, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  147, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (155, 'Urlaub', 'Hallo World', '2012-03-06 16:37:08', 170, 'Die ist ein Test.',  154, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (156, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-03-20 00:45:10', 34, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  155, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (157, 'Urlaub', 'Wie gehts', '2012-03-22 08:50:55', 141, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  156, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (158, 'Film', 'Wichtig!', '2012-04-12 05:36:42', 151, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  156, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (159, 'Spass', 'Hallo World', '2012-03-21 12:59:42', 2, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  156, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (160, 'MyLife', 'Hallo World', '2012-03-31 13:12:13', 66, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  155, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (161, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-04-13 07:13:19', 127, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  160, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (162, 'MyLife', 'Wie gehts', '2012-04-06 04:07:59', 93, 'No content.',  160, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (163, 'Film', 'Wichtig!', '2012-04-07 05:04:53', 7, 'No content.',  160, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (164, 'Spass', 'Wichtig!', '2012-03-10 19:05:58', 114, 'Ich hasse Spam-Mails.',  155, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (165, 'MyLife', 'Wichtig!', '2012-03-30 17:00:52', 40, 'No content.',  164, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (166, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-03-17 11:43:37', 8, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  164, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (167, 'Film', 'Wie gehts', '2012-03-17 02:10:26', 36, 'Ich hasse Spam-Mails.',  164, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (168, 'Film', 'Wichtig!', '2012-04-12 16:31:34', 169, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  154, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (169, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-04-20 10:36:45', 119, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  168, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (170, 'MyLife', 'Wichtig!', '2012-04-23 15:30:03', 171, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  169, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (171, 'Film', 'Hallo World', '2012-04-25 02:06:40', 70, 'Ich hasse Spam-Mails.',  169, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (172, 'MyLife', 'Wie gehts', '2012-04-23 09:13:50', 90, 'Ich hasse Spam-Mails.',  169, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (173, 'Urlaub', 'Wie gehts', '2011-08-06 18:08:10', 12, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  146, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (174, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-01-22 19:46:08', 30, 'Die ist ein Test.',  173, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (175, 'Film', 'Hallo World', '2012-02-15 03:35:03', 172, 'Die ist ein Test.',  174, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (176, 'Spass', 'Hallo World', '2012-04-12 03:31:59', 152, 'Die ist ein Test.',  175, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (177, 'MyLife', 'Wichtig!', '2012-04-23 21:49:01', 172, 'No content.',  176, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (178, 'MyLife', 'Wie gehts', '2012-04-20 19:51:38', 56, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  176, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (179, 'Film', 'Wollte nur mal Hallo sagen', '2012-02-16 21:29:54', 187, 'Die ist ein Test.',  175, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (180, 'Film', 'Wollte nur mal Hallo sagen', '2012-03-28 07:05:36', 33, 'Ich hasse Spam-Mails.',  179, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (181, 'Film', 'Hallo World', '2012-04-10 09:50:00', 82, 'Die ist ein Test.',  179, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (182, 'Film', 'Wie gehts', '2012-04-02 00:54:17', 24, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  174, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (183, 'Urlaub', 'Wichtig!', '2012-04-07 13:21:48', 157, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  182, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (184, 'Film', 'Wie gehts', '2012-04-18 03:47:45', 54, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  183, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (185, 'Spass', 'Wichtig!', '2012-04-25 12:54:38', 37, 'Ich hasse Spam-Mails.',  183, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (186, 'Urlaub', 'Wichtig!', '2012-04-21 11:46:17', 99, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  182, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (187, 'Spass', 'Wichtig!', '2012-04-25 12:35:02', 7, 'Ich hasse Spam-Mails.',  186, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (188, 'MyLife', 'Hallo World', '2012-04-26 08:12:39', 12, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  186, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (189, 'MyLife', 'Wie gehts', '2012-04-21 13:50:02', 97, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  186, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (190, 'Spass', 'Wie gehts', '2012-04-03 14:04:17', 19, 'No content.',  182, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (191, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-04-24 08:34:32', 82, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  190, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (192, 'Film', 'Hallo World', '2012-04-19 16:29:23', 109, 'Ich hasse Spam-Mails.',  190, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (193, 'Film', 'Hallo World', '2012-04-11 14:54:10', 52, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  190, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (194, 'Spass', 'Wie gehts', '2012-01-28 06:54:30', 174, 'Ich hasse Spam-Mails.',  173, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (195, 'MyLife', 'Wie gehts', '2012-04-20 05:48:17', 76, 'Ich hasse Spam-Mails.',  194, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (196, 'MyLife', 'Wichtig!', '2012-04-25 16:43:52', 89, 'No content.',  195, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (197, 'Film', 'Wollte nur mal Hallo sagen', '2012-04-26 01:35:07', 49, 'No content.',  196, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (198, 'Film', 'Hallo World', '2012-04-26 12:13:02', 94, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  196, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (199, 'MyLife', 'Wichtig!', '2012-04-25 23:57:02', 10, 'Ich hasse Spam-Mails.',  196, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (200, 'MyLife', 'Wie gehts', '2012-04-20 10:12:06', 89, 'Ich hasse Spam-Mails.',  195, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (201, 'MyLife', 'Wie gehts', '2012-04-23 18:36:10', 124, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  200, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (202, 'Film', 'Wollte nur mal Hallo sagen', '2012-04-23 12:44:12', 55, 'Ich hasse Spam-Mails.',  200, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (203, 'Urlaub', 'Wie gehts', '2007-03-09 12:57:29', 42, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (204, 'Spass', 'Hallo World', '2008-08-09 10:01:28', 168, 'No content.',  203, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (205, 'Film', 'Wichtig!', '2010-07-03 20:41:16', 64, 'Ich hasse Spam-Mails.',  204, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (206, 'Urlaub', 'Hallo World', '2010-10-06 11:07:28', 133, 'No content.',  205, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (207, 'Spass', 'Hallo World', '2011-11-29 00:21:26', 12, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  205, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (208, 'MyLife', 'Wichtig!', '2012-01-25 00:55:25', 57, 'No content.',  205, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (209, 'MyLife', 'Wollte nur mal Hallo sagen', '2009-02-24 02:17:09', 60, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  204, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (210, 'MyLife', 'Wollte nur mal Hallo sagen', '2010-05-24 07:32:38', 4, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  209, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (211, 'Film', 'Wichtig!', '2009-12-28 10:00:47', 78, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  209, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (212, 'Urlaub', 'Wollte nur mal Hallo sagen', '2010-09-25 01:52:31', 149, 'No content.',  209, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (213, 'Spass', 'Wichtig!', '2008-10-10 15:54:00', 42, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  203, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (214, 'Urlaub', 'Hallo World', '2008-12-23 15:35:17', 104, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  213, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (215, 'MyLife', 'Hallo World', '2010-12-02 18:38:55', 138, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  214, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (216, 'MyLife', 'Wollte nur mal Hallo sagen', '2010-02-12 09:52:29', 12, 'Die ist ein Test.',  214, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (217, 'Urlaub', 'Hallo World', '2010-09-11 03:14:30', 12, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  213, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (218, 'MyLife', 'Wie gehts', '2010-11-18 16:16:59', 61, 'Die ist ein Test.',  217, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (219, 'Film', 'Wollte nur mal Hallo sagen', '2011-05-18 06:17:55', 19, 'Ich hasse Spam-Mails.',  217, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (220, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-10-28 10:49:54', 62, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  217, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (221, 'Film', 'Hallo World', '2009-02-13 16:38:30', 151, 'Ich hasse Spam-Mails.',  213, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (222, 'Film', 'Wichtig!', '2009-04-21 14:03:43', 135, 'No content.',  221, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (223, 'Spass', 'Wichtig!', '2009-10-02 18:47:22', 140, 'Ich hasse Spam-Mails.',  221, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (224, 'MyLife', 'Wie gehts', '2010-11-05 07:03:32', 85, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (225, 'Urlaub', 'Hallo World', '2011-02-24 22:30:20', 63, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  224, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (226, 'Spass', 'Wollte nur mal Hallo sagen', '2012-01-21 18:32:05', 30, 'No content.',  225, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (227, 'Spass', 'Wollte nur mal Hallo sagen', '2011-06-17 06:43:03', 188, 'No content.',  225, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (228, 'Film', 'Wie gehts', '2011-01-10 06:04:14', 106, 'Ich hasse Spam-Mails.',  224, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (229, 'MyLife', 'Wie gehts', '2011-05-19 20:06:04', 98, 'Ich hasse Spam-Mails.',  228, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (230, 'Spass', 'Wie gehts', '2009-12-17 20:42:50', 200, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (231, 'Urlaub', 'Wie gehts', '2011-07-23 19:31:37', 173, 'Die ist ein Test.',  230, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (232, 'Urlaub', 'Wichtig!', '2012-04-24 19:34:54', 186, 'Die ist ein Test.',  231, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (233, 'MyLife', 'Hallo World', '2012-04-24 19:56:38', 142, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  232, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (234, 'Urlaub', 'Wichtig!', '2012-04-26 09:47:39', 80, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  233, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (235, 'MyLife', 'Hallo World', '2012-04-26 11:18:29', 115, 'Ich hasse Spam-Mails.',  233, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (236, 'Urlaub', 'Wie gehts', '2012-04-26 05:56:55', 119, 'Die ist ein Test.',  233, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (237, 'Film', 'Wichtig!', '2012-04-24 23:02:14', 82, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  232, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (238, 'MyLife', 'Wie gehts', '2012-04-25 01:13:57', 156, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  237, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (239, 'Film', 'Wollte nur mal Hallo sagen', '2012-04-25 07:30:57', 125, 'Die ist ein Test.',  237, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (240, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-07-20 14:50:56', 55, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  230, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (241, 'Spass', 'Wie gehts', '2011-07-25 12:15:50', 172, 'Die ist ein Test.',  240, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (242, 'Urlaub', 'Hallo World', '2012-01-07 20:23:00', 76, 'Ich hasse Spam-Mails.',  241, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (243, 'Spass', 'Wie gehts', '2012-04-23 10:56:35', 133, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  242, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (244, 'Urlaub', 'Wie gehts', '2012-04-19 20:18:55', 164, 'Die ist ein Test.',  241, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (245, 'Urlaub', 'Wie gehts', '2012-04-20 18:09:26', 11, 'Ich hasse Spam-Mails.',  244, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (246, 'MyLife', 'Wie gehts', '2012-04-24 14:28:32', 161, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  244, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (247, 'Urlaub', 'Hallo World', '2012-04-25 22:33:40', 44, 'Die ist ein Test.',  244, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (248, 'Film', 'Hallo World', '2012-01-01 07:42:49', 128, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  240, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (249, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-03-21 19:09:58', 44, 'Die ist ein Test.',  248, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (250, 'Spass', 'Wollte nur mal Hallo sagen', '2012-03-27 13:58:48', 144, 'No content.',  249, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (251, 'Urlaub', 'Wie gehts', '2012-04-20 02:07:17', 22, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  249, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (252, 'Spass', 'Hallo World', '2012-04-24 05:57:45', 186, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  248, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (253, 'Spass', 'Wichtig!', '2012-04-25 16:34:38', 198, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  252, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (254, 'Spass', 'Wollte nur mal Hallo sagen', '2012-03-02 08:18:28', 73, 'Die ist ein Test.',  248, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (255, 'Urlaub', 'Hallo World', '2012-04-21 05:24:34', 150, 'No content.',  254, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (256, 'MyLife', 'Wollte nur mal Hallo sagen', '2010-02-28 13:39:52', 27, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  230, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (257, 'Spass', 'Wichtig!', '2010-08-07 19:39:27', 19, 'Ich hasse Spam-Mails.',  256, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (258, 'Film', 'Wie gehts', '2010-12-14 18:43:38', 7, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  257, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (259, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-04-08 05:56:32', 2, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  258, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (260, 'Spass', 'Wichtig!', '2011-01-28 21:24:58', 122, 'Ich hasse Spam-Mails.',  257, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (261, 'Urlaub', 'Hallo World', '2012-02-04 12:22:14', 43, 'No content.',  260, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (262, 'Spass', 'Wie gehts', '2011-05-05 07:49:16', 149, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  260, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (263, 'MyLife', 'Hallo World', '2011-10-25 18:27:48', 110, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  260, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (264, 'MyLife', 'Wie gehts', '2010-11-11 04:58:06', 28, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  257, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (265, 'Urlaub', 'Hallo World', '2011-08-08 15:12:33', 161, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  264, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (266, 'Film', 'Wichtig!', '2011-11-27 20:37:20', 157, 'No content.',  264, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (267, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-04-06 08:01:42', 159, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  264, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (268, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-04-15 16:07:28', 89, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  256, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (269, 'Urlaub', 'Wichtig!', '2012-04-20 21:34:09', 73, 'Die ist ein Test.',  268, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (270, 'Urlaub', 'Wie gehts', '2012-04-26 06:04:27', 159, 'Die ist ein Test.',  269, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (271, 'MyLife', 'Wie gehts', '2012-04-22 09:17:23', 101, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  269, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (272, 'Urlaub', 'Wollte nur mal Hallo sagen', '2012-04-24 07:22:02', 167, 'Ich hasse Spam-Mails.',  268, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (273, 'Urlaub', 'Hallo World', '2012-04-26 07:54:50', 146, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  272, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (274, 'Film', 'Wichtig!', '2010-12-01 17:46:18', 70, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (275, 'MyLife', 'Wie gehts', '2011-04-25 12:53:02', 16, 'Ich hasse Spam-Mails.',  274, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (276, 'MyLife', 'Hallo World', '2011-07-29 02:13:42', 118, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  275, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (277, 'MyLife', 'Wie gehts', '2012-02-26 12:53:19', 87, 'Die ist ein Test.',  276, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (278, 'Spass', 'Wie gehts', '2012-04-10 14:13:50', 75, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  276, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (279, 'Spass', 'Wichtig!', '2011-11-26 08:57:33', 36, 'No content.',  276, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (280, 'MyLife', 'Hallo World', '2011-12-05 15:25:32', 57, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.',  274, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (281, 'Urlaub', 'Hallo World', '2012-02-12 19:21:17', 176, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  280, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (282, 'Film', 'Wichtig!', '2012-04-19 23:04:21', 97, 'Ich hasse Spam-Mails.',  281, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (283, 'Spass', 'Wie gehts', '2011-12-31 17:29:54', 152, 'Die ist ein Test.',  280, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (284, 'MyLife', 'Wichtig!', '2012-03-10 17:58:58', 51, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  283, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (285, 'Urlaub', 'Wollte nur mal Hallo sagen', '2011-12-25 15:13:41', 105, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.',  280, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (286, 'MyLife', 'Wichtig!', '2012-02-03 22:04:20', 163, 'No content.',  285, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (287, 'MyLife', 'Hallo World', '2003-07-14 05:43:16', 7, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (288, 'MyLife', 'Wichtig!', '2007-10-31 05:00:30', 45, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (289, 'Spass', 'Wichtig!', '2008-02-02 10:29:30', 118, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (290, 'Film', 'Wie gehts', '2005-01-24 03:31:13', 66, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (291, 'MyLife', 'Wollte nur mal Hallo sagen', '2003-01-17 13:58:51', 198, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (292, 'MyLife', 'Wie gehts', '2011-03-05 13:48:10', 169, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (293, 'Urlaub', 'Wie gehts', '2008-09-26 22:30:25', 103, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (294, 'Urlaub', 'Wollte nur mal Hallo sagen', '2004-04-29 13:21:12', 121, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (295, 'MyLife', 'Wie gehts', '2004-05-10 00:24:05', 113, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (296, 'Film', 'Wollte nur mal Hallo sagen', '2008-08-16 07:34:57', 161, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (297, 'MyLife', 'Hallo World', '2008-05-07 12:15:04', 54, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (298, 'MyLife', 'Wichtig!', '2008-06-24 20:47:33', 10, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (299, 'Urlaub', 'Hallo World', '2010-09-05 13:33:30', 7, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (300, 'Spass', 'Wollte nur mal Hallo sagen', '2008-05-01 16:30:24', 59, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (301, 'Urlaub', 'Wollte nur mal Hallo sagen', '2008-07-20 10:20:43', 47, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (302, 'Film', 'Wichtig!', '2007-04-20 23:50:20', 125, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (303, 'Spass', 'Wie gehts', '2005-05-05 06:39:26', 72, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (304, 'Film', 'Wollte nur mal Hallo sagen', '2010-05-13 11:52:34', 62, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (305, 'Spass', 'Wichtig!', '2009-09-02 19:21:42', 139, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (306, 'Spass', 'Wollte nur mal Hallo sagen', '2008-03-03 09:21:46', 111, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (307, 'Film', 'Wollte nur mal Hallo sagen', '2008-08-28 07:38:39', 189, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (308, 'MyLife', 'Wie gehts', '2009-03-06 22:03:30', 138, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (309, 'Film', 'Wollte nur mal Hallo sagen', '2007-01-07 07:12:38', 156, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (310, 'MyLife', 'Wie gehts', '2011-07-17 21:18:35', 7, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (311, 'Film', 'Wollte nur mal Hallo sagen', '2005-06-11 19:17:37', 100, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (312, 'Film', 'Wichtig!', '2006-05-29 20:40:25', 7, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (313, 'Spass', 'Wollte nur mal Hallo sagen', '2009-02-12 12:21:38', 139, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (314, 'Film', 'Wie gehts', '2006-06-23 12:20:57', 116, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (315, 'Urlaub', 'Hallo World', '2008-03-12 11:36:49', 50, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (316, 'MyLife', 'Wichtig!', '2004-10-21 16:18:00', 84, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (317, 'MyLife', 'Wie gehts', '2009-05-24 19:17:32', 91, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (318, 'MyLife', 'Wollte nur mal Hallo sagen', '2007-09-06 15:12:06', 61, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (319, 'Film', 'Wichtig!', '2007-09-07 03:58:30', 23, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (320, 'MyLife', 'Wie gehts', '2010-11-24 13:51:40', 197, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (321, 'Urlaub', 'Wie gehts', '2009-11-27 21:32:27', 190, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (322, 'MyLife', 'Wichtig!', '2011-11-18 10:37:43', 59, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (323, 'Film', 'Wie gehts', '2011-12-20 21:49:13', 89, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (324, 'Film', 'Wichtig!', '2011-07-30 23:55:52', 3, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (325, 'Spass', 'Wichtig!', '2010-06-02 21:07:47', 156, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null,  213);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (326, 'Film', 'Wichtig!', '2005-04-29 09:42:58', 189, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (327, 'Spass', 'Wollte nur mal Hallo sagen', '2008-07-31 09:55:03', 19, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (328, 'Urlaub', 'Wichtig!', '2008-01-06 14:02:58', 46, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (329, 'Film', 'Hallo World', '2006-10-18 19:56:47', 168, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (330, 'MyLife', 'Wollte nur mal Hallo sagen', '2009-12-14 11:26:04', 9, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (331, 'Film', 'Wie gehts', '2011-12-16 03:11:33', 164, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (332, 'Spass', 'Wichtig!', '2004-11-29 07:22:18', 41, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (333, 'Film', 'Wichtig!', '2010-01-21 18:14:24', 29, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (334, 'Urlaub', 'Wollte nur mal Hallo sagen', '2005-08-29 16:17:26', 168, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (335, 'MyLife', 'Wichtig!', '2009-08-05 20:05:25', 84, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (336, 'MyLife', 'Hallo World', '2009-02-27 12:56:52', 93, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (337, 'Urlaub', 'Hallo World', '2004-08-16 10:18:17', 107, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (338, 'Spass', 'Hallo World', '2005-12-18 14:51:37', 49, 'Die ist ein Test.', null,  90);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (339, 'Film', 'Hallo World', '2003-05-27 09:12:53', 96, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (340, 'MyLife', 'Hallo World', '2009-09-09 17:34:50', 178, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (341, 'Urlaub', 'Wollte nur mal Hallo sagen', '2009-06-18 04:50:12', 57, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (342, 'Film', 'Wollte nur mal Hallo sagen', '2004-11-22 21:25:17', 24, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (343, 'Film', 'Hallo World', '2003-08-26 18:01:06', 186, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (344, 'Film', 'Hallo World', '2007-03-20 14:41:46', 53, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (345, 'MyLife', 'Wichtig!', '2011-02-15 08:34:06', 64, 'No content.', null,  283);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (346, 'MyLife', 'Wollte nur mal Hallo sagen', '2005-07-04 06:09:17', 140, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (347, 'MyLife', 'Wie gehts', '2004-10-17 06:10:00', 135, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (348, 'Spass', 'Wie gehts', '2010-06-29 13:15:24', 188, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (349, 'Spass', 'Wie gehts', '2006-10-15 20:24:46', 93, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (350, 'MyLife', 'Wichtig!', '2004-01-07 15:35:24', 150, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (351, 'Urlaub', 'Wollte nur mal Hallo sagen', '2006-10-05 23:46:43', 70, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (352, 'Spass', 'Wichtig!', '2004-08-27 02:08:46', 109, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (353, 'MyLife', 'Wollte nur mal Hallo sagen', '2007-10-21 02:17:09', 8, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (354, 'Urlaub', 'Wie gehts', '2011-01-08 23:40:34', 83, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (355, 'MyLife', 'Wie gehts', '2011-11-05 01:04:06', 4, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (356, 'Film', 'Hallo World', '2005-03-18 02:06:20', 71, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (357, 'Spass', 'Wollte nur mal Hallo sagen', '2007-08-25 11:32:59', 154, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (358, 'Spass', 'Hallo World', '2007-03-06 21:07:16', 68, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (359, 'Film', 'Wichtig!', '2007-06-05 02:59:10', 15, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (360, 'Spass', 'Wollte nur mal Hallo sagen', '2011-01-30 08:07:56', 157, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (361, 'Spass', 'Wollte nur mal Hallo sagen', '2010-11-12 17:24:55', 48, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (362, 'Film', 'Wichtig!', '2011-01-17 19:27:16', 103, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (363, 'Film', 'Wie gehts', '2010-12-23 13:41:49', 29, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (364, 'MyLife', 'Wollte nur mal Hallo sagen', '2008-01-14 08:12:41', 35, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null,  334);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (365, 'Spass', 'Hallo World', '2011-11-23 09:03:59', 71, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (366, 'MyLife', 'Hallo World', '2003-08-15 10:51:56', 33, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (367, 'Film', 'Hallo World', '2004-12-17 10:42:15', 77, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (368, 'Urlaub', 'Hallo World', '2009-04-21 23:19:45', 76, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (369, 'MyLife', 'Wie gehts', '2011-08-03 09:05:49', 32, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (370, 'Film', 'Wollte nur mal Hallo sagen', '2008-11-24 02:22:23', 126, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (371, 'Film', 'Wie gehts', '2011-11-27 15:52:51', 14, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (372, 'Film', 'Hallo World', '2010-09-08 22:40:34', 47, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (373, 'MyLife', 'Wollte nur mal Hallo sagen', '2008-12-10 23:55:34', 1, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (374, 'Urlaub', 'Wie gehts', '2004-06-03 10:44:34', 167, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (375, 'MyLife', 'Wollte nur mal Hallo sagen', '2004-03-25 00:51:40', 66, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (376, 'MyLife', 'Wollte nur mal Hallo sagen', '2010-04-14 17:37:53', 160, 'No content.', null,  98);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (377, 'Spass', 'Wollte nur mal Hallo sagen', '2005-05-09 12:25:54', 185, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (378, 'MyLife', 'Hallo World', '2009-05-01 11:56:43', 129, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (379, 'Urlaub', 'Hallo World', '2012-03-30 07:39:01', 144, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (380, 'Spass', 'Wie gehts', '2011-10-12 01:01:48', 154, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (381, 'Spass', 'Wichtig!', '2008-02-22 02:42:21', 154, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (382, 'Spass', 'Wie gehts', '2004-11-08 04:17:13', 4, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (383, 'MyLife', 'Wie gehts', '2005-04-23 03:44:17', 113, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (384, 'MyLife', 'Hallo World', '2006-02-16 12:55:47', 184, 'Ich hasse Spam-Mails.', null,  156);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (385, 'Film', 'Wichtig!', '2003-09-08 03:07:20', 42, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null,  174);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (386, 'Spass', 'Wie gehts', '2005-07-18 08:42:56', 139, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (387, 'Spass', 'Wollte nur mal Hallo sagen', '2006-07-17 07:45:53', 21, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (388, 'MyLife', 'Wollte nur mal Hallo sagen', '2007-03-11 09:25:41', 161, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (389, 'MyLife', 'Wichtig!', '2007-03-11 06:26:38', 190, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (390, 'MyLife', 'Wichtig!', '2009-11-30 15:56:11', 169, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (391, 'MyLife', 'Wie gehts', '2011-02-15 04:36:32', 166, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (392, 'Film', 'Hallo World', '2006-11-10 23:44:47', 177, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (393, 'Urlaub', 'Hallo World', '2006-09-26 21:33:28', 154, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (394, 'Spass', 'Wie gehts', '2003-10-13 20:30:06', 189, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (395, 'Film', 'Hallo World', '2005-05-22 01:53:44', 101, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (396, 'Film', 'Wichtig!', '2003-09-18 05:06:17', 34, 'No content.', null,  384);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (397, 'Urlaub', 'Wichtig!', '2005-11-25 15:48:50', 4, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (398, 'MyLife', 'Wie gehts', '2006-02-26 14:42:51', 104, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (399, 'MyLife', 'Wichtig!', '2003-09-21 07:24:03', 194, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (400, 'Spass', 'Wollte nur mal Hallo sagen', '2007-04-08 19:28:06', 12, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (401, 'MyLife', 'Wollte nur mal Hallo sagen', '2004-08-05 22:44:18', 23, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (402, 'Film', 'Hallo World', '2006-02-22 11:51:14', 53, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (403, 'Film', 'Wie gehts', '2011-11-04 01:54:42', 35, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (404, 'Film', 'Hallo World', '2008-06-05 15:00:34', 2, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (405, 'Film', 'Hallo World', '2011-02-04 04:53:20', 62, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (406, 'Spass', 'Wie gehts', '2006-08-23 16:12:00', 95, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (407, 'MyLife', 'Wie gehts', '2010-12-08 20:16:07', 25, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (408, 'Spass', 'Wichtig!', '2011-04-08 15:41:17', 30, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (409, 'Urlaub', 'Hallo World', '2007-08-12 22:50:24', 69, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (410, 'MyLife', 'Wie gehts', '2011-03-22 03:45:56', 142, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (411, 'Urlaub', 'Wollte nur mal Hallo sagen', '2003-10-23 06:21:43', 73, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (412, 'Urlaub', 'Wie gehts', '2012-02-14 00:57:16', 182, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (413, 'Spass', 'Wichtig!', '2007-05-09 13:45:45', 27, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (414, 'Spass', 'Wollte nur mal Hallo sagen', '2007-06-26 07:19:04', 186, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (415, 'Spass', 'Hallo World', '2009-11-10 10:47:25', 182, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (416, 'Spass', 'Wie gehts', '2009-08-15 13:56:13', 136, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (417, 'MyLife', 'Wie gehts', '2003-10-14 13:26:04', 76, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (418, 'MyLife', 'Wie gehts', '2008-04-01 11:07:20', 1, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (419, 'Spass', 'Hallo World', '2005-12-06 22:49:06', 179, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (420, 'Urlaub', 'Hallo World', '2009-06-13 02:56:52', 66, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (421, 'MyLife', 'Wie gehts', '2011-06-02 02:58:05', 195, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (422, 'Urlaub', 'Wichtig!', '2011-09-07 10:39:08', 27, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (423, 'Urlaub', 'Wie gehts', '2007-12-29 02:01:05', 23, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (424, 'Spass', 'Wollte nur mal Hallo sagen', '2010-04-27 06:56:46', 114, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (425, 'Spass', 'Hallo World', '2006-06-22 02:19:57', 36, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (426, 'MyLife', 'Wie gehts', '2011-08-06 22:40:23', 118, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (427, 'Spass', 'Wollte nur mal Hallo sagen', '2006-07-06 03:27:23', 116, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (428, 'MyLife', 'Wie gehts', '2011-09-01 05:23:43', 79, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (429, 'Spass', 'Hallo World', '2004-11-16 22:02:41', 92, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (430, 'Film', 'Hallo World', '2008-02-13 01:05:21', 105, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (431, 'Film', 'Wollte nur mal Hallo sagen', '2010-10-14 15:23:49', 97, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (432, 'Film', 'Wichtig!', '2004-04-01 03:39:05', 93, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null,  18);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (433, 'MyLife', 'Wie gehts', '2012-01-17 14:31:54', 108, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (434, 'Spass', 'Wie gehts', '2007-07-25 10:01:48', 115, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (435, 'Film', 'Wichtig!', '2007-03-13 19:23:40', 50, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (436, 'MyLife', 'Wie gehts', '2007-01-30 06:19:40', 8, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (437, 'Film', 'Wichtig!', '2009-10-26 06:40:49', 69, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (438, 'MyLife', 'Wichtig!', '2011-06-22 14:57:43', 72, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (439, 'Spass', 'Hallo World', '2006-11-22 21:18:41', 85, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (440, 'Urlaub', 'Wie gehts', '2003-09-13 00:27:59', 46, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (441, 'MyLife', 'Wie gehts', '2011-11-23 14:14:30', 197, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (442, 'Film', 'Wie gehts', '2011-09-04 22:41:34', 23, 'Die ist ein Test.', null,  233);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (443, 'Spass', 'Wichtig!', '2003-11-06 23:20:55', 62, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (444, 'Urlaub', 'Wichtig!', '2007-01-30 00:15:56', 136, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (445, 'Spass', 'Wollte nur mal Hallo sagen', '2010-01-14 20:42:35', 130, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (446, 'Film', 'Wie gehts', '2007-12-03 02:41:46', 164, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (447, 'Urlaub', 'Wie gehts', '2004-10-21 08:43:27', 108, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null,  60);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (448, 'MyLife', 'Wie gehts', '2006-12-12 14:58:18', 163, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (449, 'Spass', 'Wichtig!', '2009-02-20 20:15:56', 51, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (450, 'Urlaub', 'Wichtig!', '2005-07-16 09:58:42', 38, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (451, 'Urlaub', 'Wichtig!', '2007-07-09 18:27:57', 4, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (452, 'MyLife', 'Hallo World', '2004-03-03 05:38:03', 63, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (453, 'MyLife', 'Wollte nur mal Hallo sagen', '2009-11-13 04:44:46', 58, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (454, 'Film', 'Wichtig!', '2003-06-11 07:54:04', 184, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (455, 'Film', 'Wichtig!', '2003-06-06 01:47:20', 90, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (456, 'Spass', 'Wichtig!', '2005-04-23 07:05:33', 48, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (457, 'Film', 'Wichtig!', '2006-07-07 09:37:25', 12, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (458, 'Urlaub', 'Hallo World', '2010-10-06 15:59:16', 61, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (459, 'MyLife', 'Wollte nur mal Hallo sagen', '2007-10-30 15:48:46', 46, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (460, 'Urlaub', 'Hallo World', '2006-05-19 07:49:34', 44, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (461, 'Urlaub', 'Wollte nur mal Hallo sagen', '2008-12-28 19:06:13', 61, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (462, 'Spass', 'Wichtig!', '2009-10-22 11:16:08', 176, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (463, 'Spass', 'Wichtig!', '2004-11-09 08:46:32', 66, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null,  185);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (464, 'MyLife', 'Wichtig!', '2005-12-12 22:06:33', 110, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (465, 'Urlaub', 'Wie gehts', '2003-06-11 18:41:33', 32, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (466, 'MyLife', 'Wollte nur mal Hallo sagen', '2010-01-16 21:08:48', 148, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (467, 'Film', 'Hallo World', '2009-02-23 06:01:41', 122, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (468, 'Film', 'Hallo World', '2011-12-11 16:06:51', 37, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (469, 'Film', 'Wie gehts', '2007-11-21 23:25:28', 43, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (470, 'MyLife', 'Wie gehts', '2004-07-05 01:58:08', 21, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (471, 'Urlaub', 'Wichtig!', '2003-06-24 02:03:30', 173, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (472, 'Film', 'Wichtig!', '2009-02-14 07:40:51', 176, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null,  389);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (473, 'Urlaub', 'Hallo World', '2009-06-16 17:24:48', 164, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (474, 'Film', 'Wichtig!', '2004-03-21 14:08:45', 53, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null,  210);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (475, 'Spass', 'Wie gehts', '2003-12-14 14:24:41', 146, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (476, 'Film', 'Wie gehts', '2008-11-28 08:07:14', 138, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (477, 'Film', 'Hallo World', '2011-04-20 02:03:56', 197, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (478, 'Film', 'Wichtig!', '2004-01-20 03:41:07', 88, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (479, 'Spass', 'Wollte nur mal Hallo sagen', '2003-05-30 04:39:53', 22, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (480, 'MyLife', 'Wollte nur mal Hallo sagen', '2012-02-26 01:04:30', 151, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (481, 'Film', 'Wie gehts', '2004-09-10 08:15:45', 132, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (482, 'Spass', 'Hallo World', '2007-02-04 22:50:11', 162, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (483, 'Film', 'Hallo World', '2009-02-10 07:30:56', 51, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (484, 'Spass', 'Wollte nur mal Hallo sagen', '2004-01-08 23:44:50', 52, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (485, 'Spass', 'Hallo World', '2004-08-05 16:11:50', 105, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (486, 'Urlaub', 'Wichtig!', '2010-09-28 08:06:22', 196, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (487, 'Urlaub', 'Wichtig!', '2009-04-10 15:38:55', 30, 'No content.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (488, 'Film', 'Wollte nur mal Hallo sagen', '2009-12-30 01:54:07', 127, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (489, 'Urlaub', 'Wollte nur mal Hallo sagen', '2003-03-02 18:25:26', 44, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (490, 'Film', 'Wie gehts', '2005-08-12 02:09:50', 54, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (491, 'MyLife', 'Hallo World', '2004-07-13 18:16:54', 81, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (492, 'MyLife', 'Wie gehts', '2008-11-05 03:05:48', 84, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (493, 'Film', 'Hallo World', '2003-06-28 09:36:24', 183, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (494, 'Urlaub', 'Wollte nur mal Hallo sagen', '2004-10-23 10:36:58', 30, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (495, 'Film', 'Wie gehts', '2006-10-13 18:02:09', 21, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (496, 'Film', 'Hallo World', '2005-03-24 08:34:39', 169, 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (497, 'Urlaub', 'Wollte nur mal Hallo sagen', '2006-10-15 17:52:06', 2, 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (498, 'Film', 'Hallo World', '2012-03-26 10:26:21', 80, 'Ich hasse Spam-Mails.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (499, 'Spass', 'Wollte nur mal Hallo sagen', '2006-09-02 04:00:10', 21, 'Die ist ein Test.', null, null);
insert into email (id, filename, subject, date, _from, content, in_reply_to, contained_in) values (500, 'Urlaub', 'Wollte nur mal Hallo sagen', '2006-01-29 04:50:29', 27, 'Die ist ein Test.', null, null);

insert into text (id, filename, subject, content, contained_in) values (501, 'Urlaub.txt', 'Wie gehts', 'Die ist ein Test.', 476);
insert into text (id, filename, subject, content, contained_in) values (502, 'Spass.txt', 'Wichtig!', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 48);
insert into text (id, filename, subject, content, contained_in) values (503, 'Urlaub.txt', 'Hallo World', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 320);
insert into text (id, filename, subject, content, contained_in) values (504, 'Urlaub.txt', 'Wichtig!', 'Die ist ein Test.', 49);
insert into text (id, filename, subject, content, contained_in) values (505, 'Film.txt', 'Wichtig!', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 360);
insert into text (id, filename, subject, content, contained_in) values (506, 'Spass.txt', 'Wichtig!', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 24);
insert into text (id, filename, subject, content, contained_in) values (507, 'Film.txt', 'Wollte nur mal Hallo sagen', 'No content.', 74);
insert into text (id, filename, subject, content, contained_in) values (508, 'MyLife.txt', 'Wollte nur mal Hallo sagen', 'Ich hasse Spam-Mails.', 401);
insert into text (id, filename, subject, content, contained_in) values (509, 'Urlaub.txt', 'Hallo World', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 370);
insert into text (id, filename, subject, content, contained_in) values (510, 'Spass.txt', 'Wollte nur mal Hallo sagen', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 410);
insert into text (id, filename, subject, content, contained_in) values (511, 'MyLife.txt', 'Wollte nur mal Hallo sagen', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 373);
insert into text (id, filename, subject, content, contained_in) values (512, 'Urlaub.txt', 'Hallo World', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 260);
insert into text (id, filename, subject, content, contained_in) values (513, 'Film.txt', 'Wie gehts', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 167);
insert into text (id, filename, subject, content, contained_in) values (514, 'Spass.txt', 'Wollte nur mal Hallo sagen', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 485);
insert into text (id, filename, subject, content, contained_in) values (515, 'MyLife.txt', 'Wollte nur mal Hallo sagen', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 94);
insert into text (id, filename, subject, content, contained_in) values (516, 'Urlaub.txt', 'Wichtig!', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 349);
insert into text (id, filename, subject, content, contained_in) values (517, 'Film.txt', 'Wichtig!', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 262);
insert into text (id, filename, subject, content, contained_in) values (518, 'Urlaub.txt', 'Wie gehts', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 287);
insert into text (id, filename, subject, content, contained_in) values (519, 'MyLife.txt', 'Wollte nur mal Hallo sagen', 'Ich hasse Spam-Mails.', 75);
insert into text (id, filename, subject, content, contained_in) values (520, 'Urlaub.txt', 'Wichtig!', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 16);
insert into text (id, filename, subject, content, contained_in) values (521, 'MyLife.txt', 'Hallo World', 'Ich hasse Spam-Mails.', 231);
insert into text (id, filename, subject, content, contained_in) values (522, 'MyLife.txt', 'Hallo World', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 46);
insert into text (id, filename, subject, content, contained_in) values (523, 'Film.txt', 'Wie gehts', 'Ich hasse Spam-Mails.', 220);
insert into text (id, filename, subject, content, contained_in) values (524, 'MyLife.txt', 'Wollte nur mal Hallo sagen', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 473);
insert into text (id, filename, subject, content, contained_in) values (525, 'Urlaub.txt', 'Wie gehts', 'Ich hasse Spam-Mails.', 28);
insert into text (id, filename, subject, content, contained_in) values (526, 'Film.txt', 'Hallo World', 'Warum schreiben manche Leute einfach nur Unsinn in ihre Emails.', 36);
insert into text (id, filename, subject, content, contained_in) values (527, 'MyLife.txt', 'Wollte nur mal Hallo sagen', 'Mir kommt es vor als ob diese Mail automatisch generiert wurde.', 336);
insert into text (id, filename, subject, content, contained_in) values (528, 'Film.txt', 'Wollte nur mal Hallo sagen', 'Die ist ein Test.', 70);
insert into text (id, filename, subject, content, contained_in) values (529, 'Urlaub.txt', 'Wie gehts', 'Ich hasse Spam-Mails.', 154);
insert into text (id, filename, subject, content, contained_in) values (530, 'Urlaub.txt', 'Hallo World', 'Die ist ein Test.', 86);

insert into picture (id, filename, subject, width, height, colors, contained_in) values (531, 'MyLife.jpg', 'Hallo World', 658, 1316, 9447, 448);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (532, 'Urlaub.jpg', 'Hallo World', 552, 1656, 48950, 367);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (533, 'MyLife.jpg', 'Wollte nur mal Hallo sagen', 411, 411, 20470, 14);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (534, 'Film.jpg', 'Wie gehts', 827, 1654, 31916, 23);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (535, 'Urlaub.jpg', 'Wie gehts', 451, 451, 32533, 122);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (536, 'MyLife.jpg', 'Wichtig!', 877, 877, 16837, 22);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (537, 'Urlaub.jpg', 'Wie gehts', 771, 771, 2970, 199);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (538, 'Urlaub.jpg', 'Wichtig!', 758, 1516, 39640, 213);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (539, 'Film.jpg', 'Hallo World', 283, 283, 15732, 258);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (540, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 708, 1416, 54642, 157);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (541, 'Urlaub.jpg', 'Wichtig!', 660, 660, 43935, 241);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (542, 'Film.jpg', 'Wollte nur mal Hallo sagen', 840, 2520, 1456, 1);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (543, 'Spass.jpg', 'Wichtig!', 650, 1300, 17130, 52);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (544, 'Film.jpg', 'Wichtig!', 278, 278, 36656, 299);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (545, 'Urlaub.jpg', 'Hallo World', 395, 790, 22104, 429);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (546, 'Film.jpg', 'Wichtig!', 578, 1156, 36418, 22);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (547, 'Urlaub.jpg', 'Wie gehts', 223, 669, 44070, 12);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (548, 'Film.jpg', 'Hallo World', 431, 862, 59102, 39);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (549, 'Film.jpg', 'Wie gehts', 560, 560, 111, 462);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (550, 'Film.jpg', 'Hallo World', 337, 1011, 42291, 488);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (551, 'Film.jpg', 'Wichtig!', 604, 1208, 63058, 411);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (552, 'MyLife.jpg', 'Wie gehts', 970, 1940, 46140, 239);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (553, 'Film.jpg', 'Wie gehts', 192, 576, 59637, 244);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (554, 'Spass.jpg', 'Wichtig!', 647, 647, 50184, 5);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (555, 'Urlaub.jpg', 'Wie gehts', 257, 514, 31196, 368);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (556, 'Film.jpg', 'Wichtig!', 136, 408, 33553, 139);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (557, 'Spass.jpg', 'Wichtig!', 381, 381, 9495, 36);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (558, 'MyLife.jpg', 'Wollte nur mal Hallo sagen', 542, 1626, 4691, 240);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (559, 'Film.jpg', 'Hallo World', 888, 1776, 32822, 186);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (560, 'Spass.jpg', 'Hallo World', 768, 1536, 54917, 413);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (561, 'MyLife.jpg', 'Wichtig!', 159, 159, 8606, 405);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (562, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 146, 146, 31931, 278);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (563, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 224, 224, 31755, 181);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (564, 'MyLife.jpg', 'Hallo World', 982, 1964, 12882, 324);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (565, 'Film.jpg', 'Wichtig!', 670, 2010, 17164, 37);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (566, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 309, 927, 46580, 138);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (567, 'Film.jpg', 'Wichtig!', 343, 1029, 10783, 387);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (568, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 580, 1740, 97, 251);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (569, 'Spass.jpg', 'Hallo World', 440, 440, 2467, 85);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (570, 'MyLife.jpg', 'Wie gehts', 457, 1371, 55148, 39);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (571, 'Film.jpg', 'Hallo World', 533, 1599, 41576, 137);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (572, 'Urlaub.jpg', 'Wichtig!', 591, 1773, 43713, 494);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (573, 'MyLife.jpg', 'Wollte nur mal Hallo sagen', 884, 2652, 24851, 377);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (574, 'Urlaub.jpg', 'Hallo World', 531, 531, 39336, 133);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (575, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 678, 678, 41071, 126);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (576, 'MyLife.jpg', 'Wichtig!', 579, 1158, 32834, 395);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (577, 'Urlaub.jpg', 'Wichtig!', 838, 1676, 787, 408);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (578, 'Spass.jpg', 'Hallo World', 509, 1018, 23248, 46);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (579, 'Spass.jpg', 'Hallo World', 834, 834, 35167, 227);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (580, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 381, 381, 15003, 417);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (581, 'Spass.jpg', 'Hallo World', 571, 571, 4874, 191);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (582, 'Film.jpg', 'Wichtig!', 234, 468, 27299, 191);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (583, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 899, 2697, 63345, 415);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (584, 'Spass.jpg', 'Wichtig!', 572, 1144, 15811, 468);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (585, 'Film.jpg', 'Hallo World', 852, 2556, 23241, 132);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (586, 'Film.jpg', 'Hallo World', 708, 708, 21192, 458);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (587, 'Spass.jpg', 'Hallo World', 909, 1818, 53625, 306);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (588, 'Urlaub.jpg', 'Wichtig!', 647, 1941, 59134, 377);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (589, 'Spass.jpg', 'Wie gehts', 848, 1696, 52878, 262);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (590, 'Film.jpg', 'Wie gehts', 113, 339, 4683, 474);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (591, 'MyLife.jpg', 'Wie gehts', 933, 933, 13627, 430);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (592, 'Urlaub.jpg', 'Wichtig!', 487, 1461, 33493, 35);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (593, 'Urlaub.jpg', 'Wie gehts', 689, 2067, 16322, 240);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (594, 'Spass.jpg', 'Wichtig!', 574, 1722, 33668, 302);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (595, 'MyLife.jpg', 'Hallo World', 612, 1224, 48128, 492);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (596, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 944, 2832, 49275, 382);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (597, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 331, 993, 25858, 211);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (598, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 468, 468, 25470, 22);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (599, 'Film.jpg', 'Wichtig!', 488, 488, 57471, 484);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (600, 'Urlaub.jpg', 'Hallo World', 546, 1092, 47786, 489);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (601, 'MyLife.jpg', 'Hallo World', 995, 2985, 40759, 177);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (602, 'MyLife.jpg', 'Hallo World', 453, 453, 45384, 303);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (603, 'Urlaub.jpg', 'Wichtig!', 227, 681, 35931, 154);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (604, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 835, 1670, 60358, 170);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (605, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 363, 363, 8680, 219);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (606, 'MyLife.jpg', 'Wie gehts', 271, 271, 23833, 175);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (607, 'MyLife.jpg', 'Wie gehts', 798, 2394, 5379, 459);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (608, 'Film.jpg', 'Wollte nur mal Hallo sagen', 665, 1995, 42288, 123);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (609, 'Spass.jpg', 'Hallo World', 866, 1732, 26125, 485);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (610, 'Spass.jpg', 'Hallo World', 540, 540, 32077, 338);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (611, 'Spass.jpg', 'Wollte nur mal Hallo sagen', 792, 2376, 11931, 232);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (612, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 537, 1611, 57376, 281);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (613, 'Urlaub.jpg', 'Wollte nur mal Hallo sagen', 188, 188, 3815, 18);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (614, 'Spass.jpg', 'Wichtig!', 541, 1082, 33061, 492);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (615, 'MyLife.jpg', 'Wollte nur mal Hallo sagen', 695, 1390, 39940, 460);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (616, 'Urlaub.jpg', 'Wie gehts', 438, 876, 32689, 132);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (617, 'MyLife.jpg', 'Wie gehts', 878, 2634, 42711, 95);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (618, 'Spass.jpg', 'Hallo World', 376, 1128, 6035, 254);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (619, 'Film.jpg', 'Wie gehts', 320, 320, 41706, 151);
insert into picture (id, filename, subject, width, height, colors, contained_in) values (620, 'Urlaub.jpg', 'Hallo World', 122, 122, 23124, 427);

insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (621, 'Spass.avi', 'Wichtig!', 529, 1587, 56422, 17.74, 87, 412);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (622, 'Urlaub.avi', 'Wichtig!', 780, 3120, 22157, 29.74, 104, 284);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (623, 'Film.avi', 'Wollte nur mal Hallo sagen', 681, 2043, 3365, 23.42, 38, 270);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (624, 'MyLife.avi', 'Hallo World', 790, 2370, 9564, 26.78, 78, 15);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (625, 'Film.avi', 'Wichtig!', 345, 345, 16018, 25.99, 26, 465);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (626, 'Spass.avi', 'Hallo World', 557, 2785, 24123, 21.33, 80, 238);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (627, 'Spass.avi', 'Wie gehts', 705, 1410, 61309, 13.12, 65, 75);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (628, 'Film.avi', 'Wollte nur mal Hallo sagen', 551, 1653, 11179, 19.08, 110, 485);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (629, 'Film.avi', 'Hallo World', 315, 315, 32750, 13.36, 112, 319);
insert into video (id, filename, subject, width, height, colors, fps, seconds, contained_in) values (630, 'Spass.avi', 'Wichtig!', 480, 960, 50808, 23.66, 28, 167);

insert into attachment (id, filename, subject, contained_in) values (631, 'Film.dat', 'Hallo World',  41);
insert into attachment (id, filename, subject, contained_in) values (632, 'Urlaub.dat', 'Hallo World',  201);
insert into attachment (id, filename, subject, contained_in) values (633, 'Urlaub.dat', 'Wollte nur mal Hallo sagen',  29);
insert into attachment (id, filename, subject, contained_in) values (634, 'Spass.dat', 'Wichtig!',  262);
insert into attachment (id, filename, subject, contained_in) values (635, 'Spass.dat', 'Wichtig!',  251);
insert into attachment (id, filename, subject, contained_in) values (636, 'MyLife.dat', 'Wichtig!',  393);
insert into attachment (id, filename, subject, contained_in) values (637, 'Film.dat', 'Wie gehts',  351);
insert into attachment (id, filename, subject, contained_in) values (638, 'Spass.dat', 'Wollte nur mal Hallo sagen',  343);
insert into attachment (id, filename, subject, contained_in) values (639, 'Film.dat', 'Hallo World',  158);
insert into attachment (id, filename, subject, contained_in) values (640, 'Film.dat', 'Hallo World',  451);
insert into attachment (id, filename, subject, contained_in) values (641, 'Spass.dat', 'Wichtig!',  460);
insert into attachment (id, filename, subject, contained_in) values (642, 'Film.dat', 'Hallo World',  218);
insert into attachment (id, filename, subject, contained_in) values (643, 'MyLife.dat', 'Wie gehts',  17);
insert into attachment (id, filename, subject, contained_in) values (644, 'Urlaub.dat', 'Wollte nur mal Hallo sagen',  484);
insert into attachment (id, filename, subject, contained_in) values (645, 'Urlaub.dat', 'Hallo World',  136);
insert into attachment (id, filename, subject, contained_in) values (646, 'Spass.dat', 'Wollte nur mal Hallo sagen',  192);
insert into attachment (id, filename, subject, contained_in) values (647, 'Spass.dat', 'Wichtig!',  370);
insert into attachment (id, filename, subject, contained_in) values (648, 'Film.dat', 'Hallo World',  227);
insert into attachment (id, filename, subject, contained_in) values (649, 'Urlaub.dat', 'Wie gehts',  229);
insert into attachment (id, filename, subject, contained_in) values (650, 'MyLife.dat', 'Wichtig!',  496);

insert into addressee (email, _to) values (1, 138);
insert into addressee (email, _to) values (2, 70);
insert into addressee (email, _to) values (3, 51);
insert into addressee (email, _to) values (3, 96);
insert into addressee (email, _to) values (3, 32);
insert into addressee (email, _to) values (2, 195);
insert into addressee (email, _to) values (4, 169);
insert into addressee (email, _to) values (5, 197);
insert into addressee (email, _to) values (6, 177);
insert into addressee (email, _to) values (7, 149);
insert into addressee (email, _to) values (8, 23);
insert into addressee (email, _to) values (8, 166);
insert into addressee (email, _to) values (8, 186);
insert into addressee (email, _to) values (6, 168);
insert into addressee (email, _to) values (9, 49);
insert into addressee (email, _to) values (10, 18);
insert into addressee (email, _to) values (6, 88);
insert into addressee (email, _to) values (11, 56);
insert into addressee (email, _to) values (12, 199);
insert into addressee (email, _to) values (12, 118);
insert into addressee (email, _to) values (13, 177);
insert into addressee (email, _to) values (14, 124);
insert into addressee (email, _to) values (15, 18);
insert into addressee (email, _to) values (16, 137);
insert into addressee (email, _to) values (17, 86);
insert into addressee (email, _to) values (18, 29);
insert into addressee (email, _to) values (18, 163);
insert into addressee (email, _to) values (18, 24);
insert into addressee (email, _to) values (16, 182);
insert into addressee (email, _to) values (19, 25);
insert into addressee (email, _to) values (20, 67);
insert into addressee (email, _to) values (20, 12);
insert into addressee (email, _to) values (19, 66);
insert into addressee (email, _to) values (21, 58);
insert into addressee (email, _to) values (15, 107);
insert into addressee (email, _to) values (22, 149);
insert into addressee (email, _to) values (23, 12);
insert into addressee (email, _to) values (24, 125);
insert into addressee (email, _to) values (24, 154);
insert into addressee (email, _to) values (23, 55);
insert into addressee (email, _to) values (25, 167);
insert into addressee (email, _to) values (25, 10);
insert into addressee (email, _to) values (23, 68);
insert into addressee (email, _to) values (26, 38);
insert into addressee (email, _to) values (26, 46);
insert into addressee (email, _to) values (26, 168);
insert into addressee (email, _to) values (14, 20);
insert into addressee (email, _to) values (27, 76);
insert into addressee (email, _to) values (28, 52);
insert into addressee (email, _to) values (29, 59);
insert into addressee (email, _to) values (30, 148);
insert into addressee (email, _to) values (30, 131);
insert into addressee (email, _to) values (29, 174);
insert into addressee (email, _to) values (31, 138);
insert into addressee (email, _to) values (31, 74);
insert into addressee (email, _to) values (27, 4);
insert into addressee (email, _to) values (32, 123);
insert into addressee (email, _to) values (33, 91);
insert into addressee (email, _to) values (34, 70);
insert into addressee (email, _to) values (34, 174);
insert into addressee (email, _to) values (33, 106);
insert into addressee (email, _to) values (35, 62);
insert into addressee (email, _to) values (35, 51);
insert into addressee (email, _to) values (33, 192);
insert into addressee (email, _to) values (36, 87);
insert into addressee (email, _to) values (36, 76);
insert into addressee (email, _to) values (36, 49);
insert into addressee (email, _to) values (32, 116);
insert into addressee (email, _to) values (37, 139);
insert into addressee (email, _to) values (38, 49);
insert into addressee (email, _to) values (38, 71);
insert into addressee (email, _to) values (38, 71);
insert into addressee (email, _to) values (37, 128);
insert into addressee (email, _to) values (39, 54);
insert into addressee (email, _to) values (39, 173);
insert into addressee (email, _to) values (39, 143);
insert into addressee (email, _to) values (37, 65);
insert into addressee (email, _to) values (40, 79);
insert into addressee (email, _to) values (32, 149);
insert into addressee (email, _to) values (41, 133);
insert into addressee (email, _to) values (42, 170);
insert into addressee (email, _to) values (42, 87);
insert into addressee (email, _to) values (42, 117);
insert into addressee (email, _to) values (27, 183);
insert into addressee (email, _to) values (43, 79);
insert into addressee (email, _to) values (44, 185);
insert into addressee (email, _to) values (45, 26);
insert into addressee (email, _to) values (45, 200);
insert into addressee (email, _to) values (45, 124);
insert into addressee (email, _to) values (43, 171);
insert into addressee (email, _to) values (46, 16);
insert into addressee (email, _to) values (47, 51);
insert into addressee (email, _to) values (47, 84);
insert into addressee (email, _to) values (46, 26);
insert into addressee (email, _to) values (48, 181);
insert into addressee (email, _to) values (48, 18);
insert into addressee (email, _to) values (48, 139);
insert into addressee (email, _to) values (46, 167);
insert into addressee (email, _to) values (49, 32);
insert into addressee (email, _to) values (49, 108);
insert into addressee (email, _to) values (49, 175);
insert into addressee (email, _to) values (14, 61);
insert into addressee (email, _to) values (50, 25);
insert into addressee (email, _to) values (51, 185);
insert into addressee (email, _to) values (52, 124);
insert into addressee (email, _to) values (53, 147);
insert into addressee (email, _to) values (53, 176);
insert into addressee (email, _to) values (52, 152);
insert into addressee (email, _to) values (54, 118);
insert into addressee (email, _to) values (52, 136);
insert into addressee (email, _to) values (55, 92);
insert into addressee (email, _to) values (51, 146);
insert into addressee (email, _to) values (56, 99);
insert into addressee (email, _to) values (57, 149);
insert into addressee (email, _to) values (56, 124);
insert into addressee (email, _to) values (58, 124);
insert into addressee (email, _to) values (58, 150);
insert into addressee (email, _to) values (58, 199);
insert into addressee (email, _to) values (13, 94);
insert into addressee (email, _to) values (59, 108);
insert into addressee (email, _to) values (60, 107);
insert into addressee (email, _to) values (61, 19);
insert into addressee (email, _to) values (62, 88);
insert into addressee (email, _to) values (63, 11);
insert into addressee (email, _to) values (62, 86);
insert into addressee (email, _to) values (64, 61);
insert into addressee (email, _to) values (64, 136);
insert into addressee (email, _to) values (64, 176);
insert into addressee (email, _to) values (62, 104);
insert into addressee (email, _to) values (65, 186);
insert into addressee (email, _to) values (61, 129);
insert into addressee (email, _to) values (66, 160);
insert into addressee (email, _to) values (67, 107);
insert into addressee (email, _to) values (67, 67);
insert into addressee (email, _to) values (60, 59);
insert into addressee (email, _to) values (68, 32);
insert into addressee (email, _to) values (69, 46);
insert into addressee (email, _to) values (70, 197);
insert into addressee (email, _to) values (70, 180);
insert into addressee (email, _to) values (69, 92);
insert into addressee (email, _to) values (71, 67);
insert into addressee (email, _to) values (71, 178);
insert into addressee (email, _to) values (69, 120);
insert into addressee (email, _to) values (72, 156);
insert into addressee (email, _to) values (68, 27);
insert into addressee (email, _to) values (73, 59);
insert into addressee (email, _to) values (74, 128);
insert into addressee (email, _to) values (74, 107);
insert into addressee (email, _to) values (73, 196);
insert into addressee (email, _to) values (75, 95);
insert into addressee (email, _to) values (60, 80);
insert into addressee (email, _to) values (76, 93);
insert into addressee (email, _to) values (77, 86);
insert into addressee (email, _to) values (78, 88);
insert into addressee (email, _to) values (77, 154);
insert into addressee (email, _to) values (79, 62);
insert into addressee (email, _to) values (77, 106);
insert into addressee (email, _to) values (80, 186);
insert into addressee (email, _to) values (80, 15);
insert into addressee (email, _to) values (76, 99);
insert into addressee (email, _to) values (81, 10);
insert into addressee (email, _to) values (82, 70);
insert into addressee (email, _to) values (82, 16);
insert into addressee (email, _to) values (82, 128);
insert into addressee (email, _to) values (13, 162);
insert into addressee (email, _to) values (83, 52);
insert into addressee (email, _to) values (84, 118);
insert into addressee (email, _to) values (85, 100);
insert into addressee (email, _to) values (86, 126);
insert into addressee (email, _to) values (87, 189);
insert into addressee (email, _to) values (87, 77);
insert into addressee (email, _to) values (87, 48);
insert into addressee (email, _to) values (85, 113);
insert into addressee (email, _to) values (88, 79);
insert into addressee (email, _to) values (89, 18);
insert into addressee (email, _to) values (89, 86);
insert into addressee (email, _to) values (89, 157);
insert into addressee (email, _to) values (88, 42);
insert into addressee (email, _to) values (90, 182);
insert into addressee (email, _to) values (90, 56);
insert into addressee (email, _to) values (90, 141);
insert into addressee (email, _to) values (88, 145);
insert into addressee (email, _to) values (91, 178);
insert into addressee (email, _to) values (91, 179);
insert into addressee (email, _to) values (91, 8);
insert into addressee (email, _to) values (85, 96);
insert into addressee (email, _to) values (92, 62);
insert into addressee (email, _to) values (93, 146);
insert into addressee (email, _to) values (93, 150);
insert into addressee (email, _to) values (84, 25);
insert into addressee (email, _to) values (94, 155);
insert into addressee (email, _to) values (95, 17);
insert into addressee (email, _to) values (96, 163);
insert into addressee (email, _to) values (96, 183);
insert into addressee (email, _to) values (96, 153);
insert into addressee (email, _to) values (95, 104);
insert into addressee (email, _to) values (97, 56);
insert into addressee (email, _to) values (97, 34);
insert into addressee (email, _to) values (94, 188);
insert into addressee (email, _to) values (98, 149);
insert into addressee (email, _to) values (99, 93);
insert into addressee (email, _to) values (98, 70);
insert into addressee (email, _to) values (100, 74);
insert into addressee (email, _to) values (98, 79);
insert into addressee (email, _to) values (101, 49);
insert into addressee (email, _to) values (101, 17);
insert into addressee (email, _to) values (83, 168);
insert into addressee (email, _to) values (102, 4);
insert into addressee (email, _to) values (103, 65);
insert into addressee (email, _to) values (104, 103);
insert into addressee (email, _to) values (105, 148);
insert into addressee (email, _to) values (105, 182);
insert into addressee (email, _to) values (105, 73);
insert into addressee (email, _to) values (103, 177);
insert into addressee (email, _to) values (106, 96);
insert into addressee (email, _to) values (107, 119);
insert into addressee (email, _to) values (106, 90);
insert into addressee (email, _to) values (108, 181);
insert into addressee (email, _to) values (106, 97);
insert into addressee (email, _to) values (109, 151);
insert into addressee (email, _to) values (109, 177);
insert into addressee (email, _to) values (109, 180);
insert into addressee (email, _to) values (103, 196);
insert into addressee (email, _to) values (110, 98);
insert into addressee (email, _to) values (111, 171);
insert into addressee (email, _to) values (111, 110);
insert into addressee (email, _to) values (111, 140);
insert into addressee (email, _to) values (102, 33);
insert into addressee (email, _to) values (112, 76);
insert into addressee (email, _to) values (112, 157);
insert into addressee (email, _to) values (113, 151);
insert into addressee (email, _to) values (114, 122);
insert into addressee (email, _to) values (113, 76);
insert into addressee (email, _to) values (115, 108);
insert into addressee (email, _to) values (115, 104);
insert into addressee (email, _to) values (83, 62);
insert into addressee (email, _to) values (116, 84);
insert into addressee (email, _to) values (117, 20);
insert into addressee (email, _to) values (118, 17);
insert into addressee (email, _to) values (119, 93);
insert into addressee (email, _to) values (119, 75);
insert into addressee (email, _to) values (119, 47);
insert into addressee (email, _to) values (118, 88);
insert into addressee (email, _to) values (120, 157);
insert into addressee (email, _to) values (120, 127);
insert into addressee (email, _to) values (116, 172);
insert into addressee (email, _to) values (121, 97);
insert into addressee (email, _to) values (122, 169);
insert into addressee (email, _to) values (123, 70);
insert into addressee (email, _to) values (123, 65);
insert into addressee (email, _to) values (123, 167);
insert into addressee (email, _to) values (122, 75);
insert into addressee (email, _to) values (124, 108);
insert into addressee (email, _to) values (122, 157);
insert into addressee (email, _to) values (125, 50);
insert into addressee (email, _to) values (125, 65);
insert into addressee (email, _to) values (121, 102);
insert into addressee (email, _to) values (126, 114);
insert into addressee (email, _to) values (127, 148);
insert into addressee (email, _to) values (121, 115);
insert into addressee (email, _to) values (128, 51);
insert into addressee (email, _to) values (129, 189);
insert into addressee (email, _to) values (129, 62);
insert into addressee (email, _to) values (129, 94);
insert into addressee (email, _to) values (116, 181);
insert into addressee (email, _to) values (130, 86);
insert into addressee (email, _to) values (131, 62);
insert into addressee (email, _to) values (132, 4);
insert into addressee (email, _to) values (132, 17);
insert into addressee (email, _to) values (132, 59);
insert into addressee (email, _to) values (130, 2);
insert into addressee (email, _to) values (133, 101);
insert into addressee (email, _to) values (134, 162);
insert into addressee (email, _to) values (134, 197);
insert into addressee (email, _to) values (134, 194);
insert into addressee (email, _to) values (133, 184);
insert into addressee (email, _to) values (135, 6);
insert into addressee (email, _to) values (135, 119);
insert into addressee (email, _to) values (135, 113);
insert into addressee (email, _to) values (133, 175);
insert into addressee (email, _to) values (136, 88);
insert into addressee (email, _to) values (136, 69);
insert into addressee (email, _to) values (136, 52);
insert into addressee (email, _to) values (137, 26);
insert into addressee (email, _to) values (138, 54);
insert into addressee (email, _to) values (139, 122);
insert into addressee (email, _to) values (137, 83);
insert into addressee (email, _to) values (140, 69);
insert into addressee (email, _to) values (141, 89);
insert into addressee (email, _to) values (141, 147);
insert into addressee (email, _to) values (141, 129);
insert into addressee (email, _to) values (142, 24);
insert into addressee (email, _to) values (143, 99);
insert into addressee (email, _to) values (144, 67);
insert into addressee (email, _to) values (144, 78);
insert into addressee (email, _to) values (144, 147);
insert into addressee (email, _to) values (143, 140);
insert into addressee (email, _to) values (145, 152);
insert into addressee (email, _to) values (146, 163);
insert into addressee (email, _to) values (147, 147);
insert into addressee (email, _to) values (148, 69);
insert into addressee (email, _to) values (149, 147);
insert into addressee (email, _to) values (150, 117);
insert into addressee (email, _to) values (151, 66);
insert into addressee (email, _to) values (149, 36);
insert into addressee (email, _to) values (152, 54);
insert into addressee (email, _to) values (153, 172);
insert into addressee (email, _to) values (153, 154);
insert into addressee (email, _to) values (153, 58);
insert into addressee (email, _to) values (147, 155);
insert into addressee (email, _to) values (154, 170);
insert into addressee (email, _to) values (155, 34);
insert into addressee (email, _to) values (156, 141);
insert into addressee (email, _to) values (157, 106);
insert into addressee (email, _to) values (156, 151);
insert into addressee (email, _to) values (158, 85);
insert into addressee (email, _to) values (156, 2);
insert into addressee (email, _to) values (159, 47);
insert into addressee (email, _to) values (159, 123);
insert into addressee (email, _to) values (159, 146);
insert into addressee (email, _to) values (155, 66);
insert into addressee (email, _to) values (160, 127);
insert into addressee (email, _to) values (161, 69);
insert into addressee (email, _to) values (161, 153);
insert into addressee (email, _to) values (160, 93);
insert into addressee (email, _to) values (162, 34);
insert into addressee (email, _to) values (162, 174);
insert into addressee (email, _to) values (162, 22);
insert into addressee (email, _to) values (160, 7);
insert into addressee (email, _to) values (163, 192);
insert into addressee (email, _to) values (163, 2);
insert into addressee (email, _to) values (163, 114);
insert into addressee (email, _to) values (155, 114);
insert into addressee (email, _to) values (164, 40);
insert into addressee (email, _to) values (165, 29);
insert into addressee (email, _to) values (164, 8);
insert into addressee (email, _to) values (166, 164);
insert into addressee (email, _to) values (164, 36);
insert into addressee (email, _to) values (167, 190);
insert into addressee (email, _to) values (167, 70);
insert into addressee (email, _to) values (167, 33);
insert into addressee (email, _to) values (154, 169);
insert into addressee (email, _to) values (168, 119);
insert into addressee (email, _to) values (169, 171);
insert into addressee (email, _to) values (170, 140);
insert into addressee (email, _to) values (169, 70);
insert into addressee (email, _to) values (171, 133);
insert into addressee (email, _to) values (171, 138);
insert into addressee (email, _to) values (169, 90);
insert into addressee (email, _to) values (172, 177);
insert into addressee (email, _to) values (172, 169);
insert into addressee (email, _to) values (146, 12);
insert into addressee (email, _to) values (173, 30);
insert into addressee (email, _to) values (174, 172);
insert into addressee (email, _to) values (175, 152);
insert into addressee (email, _to) values (176, 172);
insert into addressee (email, _to) values (177, 135);
insert into addressee (email, _to) values (177, 115);
insert into addressee (email, _to) values (176, 56);
insert into addressee (email, _to) values (178, 125);
insert into addressee (email, _to) values (178, 65);
insert into addressee (email, _to) values (175, 187);
insert into addressee (email, _to) values (179, 33);
insert into addressee (email, _to) values (180, 2);
insert into addressee (email, _to) values (180, 68);
insert into addressee (email, _to) values (180, 57);
insert into addressee (email, _to) values (179, 82);
insert into addressee (email, _to) values (181, 57);
insert into addressee (email, _to) values (174, 24);
insert into addressee (email, _to) values (182, 157);
insert into addressee (email, _to) values (183, 54);
insert into addressee (email, _to) values (184, 96);
insert into addressee (email, _to) values (184, 101);
insert into addressee (email, _to) values (183, 37);
insert into addressee (email, _to) values (185, 175);
insert into addressee (email, _to) values (185, 48);
insert into addressee (email, _to) values (182, 99);
insert into addressee (email, _to) values (186, 7);
insert into addressee (email, _to) values (187, 55);
insert into addressee (email, _to) values (187, 187);
insert into addressee (email, _to) values (187, 47);
insert into addressee (email, _to) values (186, 12);
insert into addressee (email, _to) values (188, 76);
insert into addressee (email, _to) values (188, 200);
insert into addressee (email, _to) values (186, 97);
insert into addressee (email, _to) values (189, 199);
insert into addressee (email, _to) values (189, 40);
insert into addressee (email, _to) values (189, 170);
insert into addressee (email, _to) values (182, 19);
insert into addressee (email, _to) values (190, 82);
insert into addressee (email, _to) values (191, 125);
insert into addressee (email, _to) values (190, 109);
insert into addressee (email, _to) values (192, 157);
insert into addressee (email, _to) values (190, 52);
insert into addressee (email, _to) values (193, 44);
insert into addressee (email, _to) values (173, 174);
insert into addressee (email, _to) values (194, 76);
insert into addressee (email, _to) values (195, 89);
insert into addressee (email, _to) values (196, 49);
insert into addressee (email, _to) values (197, 59);
insert into addressee (email, _to) values (197, 19);
insert into addressee (email, _to) values (196, 94);
insert into addressee (email, _to) values (198, 58);
insert into addressee (email, _to) values (198, 192);
insert into addressee (email, _to) values (198, 159);
insert into addressee (email, _to) values (196, 10);
insert into addressee (email, _to) values (199, 164);
insert into addressee (email, _to) values (199, 164);
insert into addressee (email, _to) values (195, 89);
insert into addressee (email, _to) values (200, 124);
insert into addressee (email, _to) values (201, 175);
insert into addressee (email, _to) values (200, 55);
insert into addressee (email, _to) values (202, 21);
insert into addressee (email, _to) values (202, 134);
insert into addressee (email, _to) values (202, 181);
insert into addressee (email, _to) values (203, 168);
insert into addressee (email, _to) values (204, 64);
insert into addressee (email, _to) values (205, 133);
insert into addressee (email, _to) values (206, 2);
insert into addressee (email, _to) values (206, 161);
insert into addressee (email, _to) values (205, 12);
insert into addressee (email, _to) values (207, 86);
insert into addressee (email, _to) values (205, 57);
insert into addressee (email, _to) values (208, 92);
insert into addressee (email, _to) values (204, 60);
insert into addressee (email, _to) values (209, 4);
insert into addressee (email, _to) values (210, 165);
insert into addressee (email, _to) values (209, 78);
insert into addressee (email, _to) values (211, 189);
insert into addressee (email, _to) values (211, 159);
insert into addressee (email, _to) values (211, 130);
insert into addressee (email, _to) values (209, 149);
insert into addressee (email, _to) values (212, 112);
insert into addressee (email, _to) values (212, 174);
insert into addressee (email, _to) values (212, 108);
insert into addressee (email, _to) values (203, 42);
insert into addressee (email, _to) values (213, 104);
insert into addressee (email, _to) values (214, 138);
insert into addressee (email, _to) values (215, 126);
insert into addressee (email, _to) values (215, 37);
insert into addressee (email, _to) values (214, 12);
insert into addressee (email, _to) values (216, 6);
insert into addressee (email, _to) values (213, 12);
insert into addressee (email, _to) values (217, 61);
insert into addressee (email, _to) values (218, 51);
insert into addressee (email, _to) values (218, 93);
insert into addressee (email, _to) values (218, 196);
insert into addressee (email, _to) values (217, 19);
insert into addressee (email, _to) values (219, 52);
insert into addressee (email, _to) values (219, 4);
insert into addressee (email, _to) values (217, 62);
insert into addressee (email, _to) values (220, 94);
insert into addressee (email, _to) values (213, 151);
insert into addressee (email, _to) values (221, 135);
insert into addressee (email, _to) values (222, 91);
insert into addressee (email, _to) values (221, 140);
insert into addressee (email, _to) values (223, 9);
insert into addressee (email, _to) values (224, 63);
insert into addressee (email, _to) values (225, 30);
insert into addressee (email, _to) values (226, 185);
insert into addressee (email, _to) values (225, 188);
insert into addressee (email, _to) values (227, 2);
insert into addressee (email, _to) values (224, 106);
insert into addressee (email, _to) values (228, 98);
insert into addressee (email, _to) values (229, 124);
insert into addressee (email, _to) values (229, 191);
insert into addressee (email, _to) values (230, 173);
insert into addressee (email, _to) values (231, 186);
insert into addressee (email, _to) values (232, 142);
insert into addressee (email, _to) values (233, 80);
insert into addressee (email, _to) values (234, 5);
insert into addressee (email, _to) values (233, 115);
insert into addressee (email, _to) values (235, 40);
insert into addressee (email, _to) values (235, 68);
insert into addressee (email, _to) values (233, 119);
insert into addressee (email, _to) values (236, 137);
insert into addressee (email, _to) values (236, 18);
insert into addressee (email, _to) values (232, 82);
insert into addressee (email, _to) values (237, 156);
insert into addressee (email, _to) values (238, 191);
insert into addressee (email, _to) values (238, 175);
insert into addressee (email, _to) values (237, 125);
insert into addressee (email, _to) values (239, 121);
insert into addressee (email, _to) values (239, 16);
insert into addressee (email, _to) values (239, 49);
insert into addressee (email, _to) values (230, 55);
insert into addressee (email, _to) values (240, 172);
insert into addressee (email, _to) values (241, 76);
insert into addressee (email, _to) values (242, 133);
insert into addressee (email, _to) values (243, 159);
insert into addressee (email, _to) values (243, 104);
insert into addressee (email, _to) values (243, 86);
insert into addressee (email, _to) values (241, 164);
insert into addressee (email, _to) values (244, 11);
insert into addressee (email, _to) values (245, 157);
insert into addressee (email, _to) values (244, 161);
insert into addressee (email, _to) values (246, 19);
insert into addressee (email, _to) values (246, 76);
insert into addressee (email, _to) values (244, 44);
insert into addressee (email, _to) values (247, 19);
insert into addressee (email, _to) values (247, 188);
insert into addressee (email, _to) values (240, 128);
insert into addressee (email, _to) values (248, 44);
insert into addressee (email, _to) values (249, 144);
insert into addressee (email, _to) values (250, 30);
insert into addressee (email, _to) values (249, 22);
insert into addressee (email, _to) values (251, 96);
insert into addressee (email, _to) values (248, 186);
insert into addressee (email, _to) values (252, 198);
insert into addressee (email, _to) values (253, 196);
insert into addressee (email, _to) values (253, 81);
insert into addressee (email, _to) values (248, 73);
insert into addressee (email, _to) values (254, 150);
insert into addressee (email, _to) values (255, 93);
insert into addressee (email, _to) values (255, 149);
insert into addressee (email, _to) values (255, 150);
insert into addressee (email, _to) values (230, 27);
insert into addressee (email, _to) values (256, 19);
insert into addressee (email, _to) values (257, 7);
insert into addressee (email, _to) values (258, 2);
insert into addressee (email, _to) values (259, 132);
insert into addressee (email, _to) values (259, 186);
insert into addressee (email, _to) values (259, 5);
insert into addressee (email, _to) values (257, 122);
insert into addressee (email, _to) values (260, 43);
insert into addressee (email, _to) values (261, 194);
insert into addressee (email, _to) values (261, 148);
insert into addressee (email, _to) values (260, 149);
insert into addressee (email, _to) values (262, 168);
insert into addressee (email, _to) values (262, 84);
insert into addressee (email, _to) values (260, 110);
insert into addressee (email, _to) values (263, 66);
insert into addressee (email, _to) values (257, 28);
insert into addressee (email, _to) values (264, 161);
insert into addressee (email, _to) values (265, 156);
insert into addressee (email, _to) values (265, 162);
insert into addressee (email, _to) values (265, 14);
insert into addressee (email, _to) values (264, 157);
insert into addressee (email, _to) values (266, 119);
insert into addressee (email, _to) values (264, 159);
insert into addressee (email, _to) values (267, 47);
insert into addressee (email, _to) values (256, 89);
insert into addressee (email, _to) values (268, 73);
insert into addressee (email, _to) values (269, 159);
insert into addressee (email, _to) values (270, 87);
insert into addressee (email, _to) values (270, 197);
insert into addressee (email, _to) values (269, 101);
insert into addressee (email, _to) values (271, 54);
insert into addressee (email, _to) values (271, 75);
insert into addressee (email, _to) values (271, 193);
insert into addressee (email, _to) values (268, 167);
insert into addressee (email, _to) values (272, 146);
insert into addressee (email, _to) values (273, 162);
insert into addressee (email, _to) values (273, 190);
insert into addressee (email, _to) values (274, 16);
insert into addressee (email, _to) values (275, 118);
insert into addressee (email, _to) values (276, 87);
insert into addressee (email, _to) values (277, 120);
insert into addressee (email, _to) values (277, 97);
insert into addressee (email, _to) values (276, 75);
insert into addressee (email, _to) values (278, 85);
insert into addressee (email, _to) values (276, 36);
insert into addressee (email, _to) values (279, 163);
insert into addressee (email, _to) values (274, 57);
insert into addressee (email, _to) values (280, 176);
insert into addressee (email, _to) values (281, 97);
insert into addressee (email, _to) values (282, 125);
insert into addressee (email, _to) values (280, 152);
insert into addressee (email, _to) values (283, 51);
insert into addressee (email, _to) values (284, 67);
insert into addressee (email, _to) values (284, 145);
insert into addressee (email, _to) values (280, 105);
insert into addressee (email, _to) values (285, 163);
insert into addressee (email, _to) values (286, 156);
insert into addressee (email, _to) values (286, 68);
insert into addressee (email, _to) values (286, 49);
insert into addressee (email, _to) values (287, 117);
insert into addressee (email, _to) values (288, 69);
insert into addressee (email, _to) values (288, 121);
insert into addressee (email, _to) values (288, 23);
insert into addressee (email, _to) values (289, 89);
insert into addressee (email, _to) values (289, 85);
insert into addressee (email, _to) values (289, 15);
insert into addressee (email, _to) values (290, 161);
insert into addressee (email, _to) values (291, 15);
insert into addressee (email, _to) values (291, 79);
insert into addressee (email, _to) values (292, 5);
insert into addressee (email, _to) values (293, 57);
insert into addressee (email, _to) values (293, 129);
insert into addressee (email, _to) values (294, 150);
insert into addressee (email, _to) values (295, 150);
insert into addressee (email, _to) values (295, 140);
insert into addressee (email, _to) values (296, 30);
insert into addressee (email, _to) values (297, 27);
insert into addressee (email, _to) values (297, 192);
insert into addressee (email, _to) values (298, 42);
insert into addressee (email, _to) values (298, 111);
insert into addressee (email, _to) values (299, 21);
insert into addressee (email, _to) values (299, 19);
insert into addressee (email, _to) values (300, 71);
insert into addressee (email, _to) values (301, 155);
insert into addressee (email, _to) values (301, 152);
insert into addressee (email, _to) values (302, 40);
insert into addressee (email, _to) values (302, 96);
insert into addressee (email, _to) values (302, 44);
insert into addressee (email, _to) values (303, 164);
insert into addressee (email, _to) values (303, 76);
insert into addressee (email, _to) values (303, 195);
insert into addressee (email, _to) values (304, 44);
insert into addressee (email, _to) values (304, 75);
insert into addressee (email, _to) values (305, 8);
insert into addressee (email, _to) values (305, 90);
insert into addressee (email, _to) values (305, 157);
insert into addressee (email, _to) values (306, 186);
insert into addressee (email, _to) values (306, 166);
insert into addressee (email, _to) values (306, 176);
insert into addressee (email, _to) values (307, 20);
insert into addressee (email, _to) values (307, 3);
insert into addressee (email, _to) values (308, 171);
insert into addressee (email, _to) values (308, 138);
insert into addressee (email, _to) values (309, 10);
insert into addressee (email, _to) values (310, 109);
insert into addressee (email, _to) values (310, 91);
insert into addressee (email, _to) values (310, 128);
insert into addressee (email, _to) values (311, 174);
insert into addressee (email, _to) values (312, 75);
insert into addressee (email, _to) values (312, 8);
insert into addressee (email, _to) values (313, 56);
insert into addressee (email, _to) values (313, 96);
insert into addressee (email, _to) values (314, 183);
insert into addressee (email, _to) values (314, 76);
insert into addressee (email, _to) values (314, 172);
insert into addressee (email, _to) values (315, 12);
insert into addressee (email, _to) values (315, 166);
insert into addressee (email, _to) values (315, 184);
insert into addressee (email, _to) values (316, 182);
insert into addressee (email, _to) values (316, 90);
insert into addressee (email, _to) values (317, 32);
insert into addressee (email, _to) values (317, 144);
insert into addressee (email, _to) values (317, 176);
insert into addressee (email, _to) values (318, 196);
insert into addressee (email, _to) values (318, 89);
insert into addressee (email, _to) values (319, 39);
insert into addressee (email, _to) values (320, 91);
insert into addressee (email, _to) values (321, 53);
insert into addressee (email, _to) values (321, 155);
insert into addressee (email, _to) values (321, 163);
insert into addressee (email, _to) values (322, 97);
insert into addressee (email, _to) values (323, 172);
insert into addressee (email, _to) values (323, 127);
insert into addressee (email, _to) values (324, 72);
insert into addressee (email, _to) values (325, 44);
insert into addressee (email, _to) values (326, 127);
insert into addressee (email, _to) values (326, 152);
insert into addressee (email, _to) values (327, 170);
insert into addressee (email, _to) values (327, 93);
insert into addressee (email, _to) values (328, 99);
insert into addressee (email, _to) values (328, 92);
insert into addressee (email, _to) values (329, 166);
insert into addressee (email, _to) values (330, 69);
insert into addressee (email, _to) values (330, 125);
insert into addressee (email, _to) values (331, 54);
insert into addressee (email, _to) values (332, 175);
insert into addressee (email, _to) values (332, 6);
insert into addressee (email, _to) values (333, 24);
insert into addressee (email, _to) values (334, 69);
insert into addressee (email, _to) values (334, 63);
insert into addressee (email, _to) values (335, 20);
insert into addressee (email, _to) values (335, 13);
insert into addressee (email, _to) values (335, 136);
insert into addressee (email, _to) values (336, 156);
insert into addressee (email, _to) values (337, 115);
insert into addressee (email, _to) values (338, 65);
insert into addressee (email, _to) values (338, 165);
insert into addressee (email, _to) values (339, 175);
insert into addressee (email, _to) values (340, 98);
insert into addressee (email, _to) values (340, 126);
insert into addressee (email, _to) values (341, 192);
insert into addressee (email, _to) values (341, 175);
insert into addressee (email, _to) values (341, 125);
insert into addressee (email, _to) values (342, 69);
insert into addressee (email, _to) values (343, 101);
insert into addressee (email, _to) values (343, 106);
insert into addressee (email, _to) values (344, 51);
insert into addressee (email, _to) values (345, 192);
insert into addressee (email, _to) values (346, 92);
insert into addressee (email, _to) values (347, 107);
insert into addressee (email, _to) values (348, 36);
insert into addressee (email, _to) values (349, 24);
insert into addressee (email, _to) values (349, 89);
insert into addressee (email, _to) values (350, 91);
insert into addressee (email, _to) values (350, 60);
insert into addressee (email, _to) values (351, 90);
insert into addressee (email, _to) values (351, 17);
insert into addressee (email, _to) values (351, 57);
insert into addressee (email, _to) values (352, 3);
insert into addressee (email, _to) values (352, 93);
insert into addressee (email, _to) values (352, 159);
insert into addressee (email, _to) values (353, 54);
insert into addressee (email, _to) values (353, 165);
insert into addressee (email, _to) values (354, 73);
insert into addressee (email, _to) values (354, 126);
insert into addressee (email, _to) values (355, 148);
insert into addressee (email, _to) values (355, 194);
insert into addressee (email, _to) values (356, 153);
insert into addressee (email, _to) values (356, 173);
insert into addressee (email, _to) values (357, 194);
insert into addressee (email, _to) values (358, 157);
insert into addressee (email, _to) values (359, 27);
insert into addressee (email, _to) values (360, 152);
insert into addressee (email, _to) values (361, 161);
insert into addressee (email, _to) values (361, 55);
insert into addressee (email, _to) values (362, 184);
insert into addressee (email, _to) values (363, 23);
insert into addressee (email, _to) values (363, 130);
insert into addressee (email, _to) values (363, 73);
insert into addressee (email, _to) values (364, 99);
insert into addressee (email, _to) values (365, 143);
insert into addressee (email, _to) values (365, 68);
insert into addressee (email, _to) values (366, 189);
insert into addressee (email, _to) values (366, 35);
insert into addressee (email, _to) values (366, 101);
insert into addressee (email, _to) values (367, 121);
insert into addressee (email, _to) values (367, 76);
insert into addressee (email, _to) values (368, 18);
insert into addressee (email, _to) values (368, 10);
insert into addressee (email, _to) values (369, 141);
insert into addressee (email, _to) values (369, 80);
insert into addressee (email, _to) values (369, 59);
insert into addressee (email, _to) values (370, 35);
insert into addressee (email, _to) values (371, 128);
insert into addressee (email, _to) values (372, 104);
insert into addressee (email, _to) values (372, 49);
insert into addressee (email, _to) values (372, 16);
insert into addressee (email, _to) values (373, 77);
insert into addressee (email, _to) values (373, 150);
insert into addressee (email, _to) values (374, 8);
insert into addressee (email, _to) values (374, 182);
insert into addressee (email, _to) values (375, 145);
insert into addressee (email, _to) values (376, 96);
insert into addressee (email, _to) values (377, 124);
insert into addressee (email, _to) values (377, 184);
insert into addressee (email, _to) values (377, 107);
insert into addressee (email, _to) values (378, 117);
insert into addressee (email, _to) values (379, 152);
insert into addressee (email, _to) values (379, 125);
insert into addressee (email, _to) values (380, 68);
insert into addressee (email, _to) values (381, 165);
insert into addressee (email, _to) values (381, 120);
insert into addressee (email, _to) values (382, 198);
insert into addressee (email, _to) values (382, 77);
insert into addressee (email, _to) values (383, 58);
insert into addressee (email, _to) values (383, 149);
insert into addressee (email, _to) values (383, 132);
insert into addressee (email, _to) values (384, 25);
insert into addressee (email, _to) values (384, 4);
insert into addressee (email, _to) values (385, 97);
insert into addressee (email, _to) values (386, 156);
insert into addressee (email, _to) values (386, 11);
insert into addressee (email, _to) values (386, 31);
insert into addressee (email, _to) values (387, 45);
insert into addressee (email, _to) values (387, 64);
insert into addressee (email, _to) values (387, 160);
insert into addressee (email, _to) values (388, 67);
insert into addressee (email, _to) values (389, 43);
insert into addressee (email, _to) values (389, 110);
insert into addressee (email, _to) values (389, 121);
insert into addressee (email, _to) values (390, 143);
insert into addressee (email, _to) values (390, 67);
insert into addressee (email, _to) values (390, 72);
insert into addressee (email, _to) values (391, 153);
insert into addressee (email, _to) values (391, 8);
insert into addressee (email, _to) values (392, 184);
insert into addressee (email, _to) values (392, 176);
insert into addressee (email, _to) values (393, 196);
insert into addressee (email, _to) values (393, 109);
insert into addressee (email, _to) values (393, 46);
insert into addressee (email, _to) values (394, 51);
insert into addressee (email, _to) values (394, 51);
insert into addressee (email, _to) values (394, 189);
insert into addressee (email, _to) values (395, 141);
insert into addressee (email, _to) values (395, 128);
insert into addressee (email, _to) values (395, 119);
insert into addressee (email, _to) values (396, 100);
insert into addressee (email, _to) values (396, 190);
insert into addressee (email, _to) values (396, 62);
insert into addressee (email, _to) values (397, 2);
insert into addressee (email, _to) values (397, 76);
insert into addressee (email, _to) values (397, 8);
insert into addressee (email, _to) values (398, 91);
insert into addressee (email, _to) values (399, 17);
insert into addressee (email, _to) values (399, 189);
insert into addressee (email, _to) values (399, 123);
insert into addressee (email, _to) values (400, 9);
insert into addressee (email, _to) values (400, 156);
insert into addressee (email, _to) values (401, 131);
insert into addressee (email, _to) values (402, 75);
insert into addressee (email, _to) values (402, 66);
insert into addressee (email, _to) values (403, 1);
insert into addressee (email, _to) values (404, 97);
insert into addressee (email, _to) values (404, 90);
insert into addressee (email, _to) values (405, 37);
insert into addressee (email, _to) values (406, 146);
insert into addressee (email, _to) values (407, 183);
insert into addressee (email, _to) values (407, 54);
insert into addressee (email, _to) values (407, 180);
insert into addressee (email, _to) values (408, 61);
insert into addressee (email, _to) values (408, 179);
insert into addressee (email, _to) values (408, 96);
insert into addressee (email, _to) values (409, 41);
insert into addressee (email, _to) values (409, 123);
insert into addressee (email, _to) values (410, 200);
insert into addressee (email, _to) values (410, 137);
insert into addressee (email, _to) values (411, 40);
insert into addressee (email, _to) values (412, 88);
insert into addressee (email, _to) values (412, 122);
insert into addressee (email, _to) values (412, 47);
insert into addressee (email, _to) values (413, 198);
insert into addressee (email, _to) values (413, 103);
insert into addressee (email, _to) values (413, 183);
insert into addressee (email, _to) values (414, 10);
insert into addressee (email, _to) values (415, 66);
insert into addressee (email, _to) values (415, 26);
insert into addressee (email, _to) values (416, 118);
insert into addressee (email, _to) values (416, 105);
insert into addressee (email, _to) values (416, 77);
insert into addressee (email, _to) values (417, 93);
insert into addressee (email, _to) values (418, 23);
insert into addressee (email, _to) values (419, 146);
insert into addressee (email, _to) values (419, 16);
insert into addressee (email, _to) values (420, 167);
insert into addressee (email, _to) values (421, 81);
insert into addressee (email, _to) values (422, 19);
insert into addressee (email, _to) values (422, 143);
insert into addressee (email, _to) values (422, 181);
insert into addressee (email, _to) values (423, 53);
insert into addressee (email, _to) values (423, 114);
insert into addressee (email, _to) values (423, 199);
insert into addressee (email, _to) values (424, 177);
insert into addressee (email, _to) values (425, 29);
insert into addressee (email, _to) values (425, 195);
insert into addressee (email, _to) values (425, 133);
insert into addressee (email, _to) values (426, 110);
insert into addressee (email, _to) values (426, 78);
insert into addressee (email, _to) values (427, 147);
insert into addressee (email, _to) values (427, 9);
insert into addressee (email, _to) values (427, 164);
insert into addressee (email, _to) values (428, 144);
insert into addressee (email, _to) values (428, 81);
insert into addressee (email, _to) values (428, 133);
insert into addressee (email, _to) values (429, 112);
insert into addressee (email, _to) values (429, 15);
insert into addressee (email, _to) values (429, 190);
insert into addressee (email, _to) values (430, 176);
insert into addressee (email, _to) values (430, 158);
insert into addressee (email, _to) values (430, 168);
insert into addressee (email, _to) values (431, 184);
insert into addressee (email, _to) values (431, 143);
insert into addressee (email, _to) values (431, 11);
insert into addressee (email, _to) values (432, 105);
insert into addressee (email, _to) values (432, 129);
insert into addressee (email, _to) values (432, 182);
insert into addressee (email, _to) values (433, 198);
insert into addressee (email, _to) values (433, 5);
insert into addressee (email, _to) values (434, 172);
insert into addressee (email, _to) values (434, 150);
insert into addressee (email, _to) values (434, 101);
insert into addressee (email, _to) values (435, 74);
insert into addressee (email, _to) values (436, 193);
insert into addressee (email, _to) values (436, 145);
insert into addressee (email, _to) values (436, 102);
insert into addressee (email, _to) values (437, 51);
insert into addressee (email, _to) values (438, 57);
insert into addressee (email, _to) values (439, 117);
insert into addressee (email, _to) values (439, 112);
insert into addressee (email, _to) values (439, 99);
insert into addressee (email, _to) values (440, 134);
insert into addressee (email, _to) values (441, 61);
insert into addressee (email, _to) values (441, 172);
insert into addressee (email, _to) values (442, 167);
insert into addressee (email, _to) values (443, 16);
insert into addressee (email, _to) values (444, 76);
insert into addressee (email, _to) values (445, 50);
insert into addressee (email, _to) values (445, 129);
insert into addressee (email, _to) values (446, 26);
insert into addressee (email, _to) values (446, 13);
insert into addressee (email, _to) values (446, 32);
insert into addressee (email, _to) values (447, 167);
insert into addressee (email, _to) values (448, 88);
insert into addressee (email, _to) values (449, 7);
insert into addressee (email, _to) values (449, 62);
insert into addressee (email, _to) values (450, 55);
insert into addressee (email, _to) values (451, 177);
insert into addressee (email, _to) values (452, 78);
insert into addressee (email, _to) values (452, 19);
insert into addressee (email, _to) values (453, 44);
insert into addressee (email, _to) values (453, 30);
insert into addressee (email, _to) values (453, 119);
insert into addressee (email, _to) values (454, 89);
insert into addressee (email, _to) values (454, 116);
insert into addressee (email, _to) values (454, 43);
insert into addressee (email, _to) values (455, 54);
insert into addressee (email, _to) values (455, 113);
insert into addressee (email, _to) values (455, 164);
insert into addressee (email, _to) values (456, 74);
insert into addressee (email, _to) values (457, 13);
insert into addressee (email, _to) values (457, 105);
insert into addressee (email, _to) values (457, 120);
insert into addressee (email, _to) values (458, 193);
insert into addressee (email, _to) values (458, 92);
insert into addressee (email, _to) values (458, 71);
insert into addressee (email, _to) values (459, 163);
insert into addressee (email, _to) values (459, 46);
insert into addressee (email, _to) values (459, 105);
insert into addressee (email, _to) values (460, 90);
insert into addressee (email, _to) values (460, 2);
insert into addressee (email, _to) values (460, 89);
insert into addressee (email, _to) values (461, 98);
insert into addressee (email, _to) values (462, 185);
insert into addressee (email, _to) values (462, 149);
insert into addressee (email, _to) values (463, 49);
insert into addressee (email, _to) values (463, 132);
insert into addressee (email, _to) values (463, 195);
insert into addressee (email, _to) values (464, 145);
insert into addressee (email, _to) values (465, 11);
insert into addressee (email, _to) values (466, 121);
insert into addressee (email, _to) values (466, 178);
insert into addressee (email, _to) values (466, 143);
insert into addressee (email, _to) values (467, 76);
insert into addressee (email, _to) values (467, 57);
insert into addressee (email, _to) values (468, 94);
insert into addressee (email, _to) values (468, 113);
insert into addressee (email, _to) values (468, 180);
insert into addressee (email, _to) values (469, 65);
insert into addressee (email, _to) values (469, 124);
insert into addressee (email, _to) values (470, 56);
insert into addressee (email, _to) values (470, 114);
insert into addressee (email, _to) values (471, 8);
insert into addressee (email, _to) values (471, 154);
insert into addressee (email, _to) values (472, 142);
insert into addressee (email, _to) values (473, 4);
insert into addressee (email, _to) values (474, 12);
insert into addressee (email, _to) values (474, 85);
insert into addressee (email, _to) values (474, 177);
insert into addressee (email, _to) values (475, 42);
insert into addressee (email, _to) values (476, 68);
insert into addressee (email, _to) values (476, 10);
insert into addressee (email, _to) values (477, 20);
insert into addressee (email, _to) values (477, 46);
insert into addressee (email, _to) values (478, 170);
insert into addressee (email, _to) values (478, 187);
insert into addressee (email, _to) values (479, 83);
insert into addressee (email, _to) values (480, 142);
insert into addressee (email, _to) values (480, 170);
insert into addressee (email, _to) values (480, 108);
insert into addressee (email, _to) values (481, 68);
insert into addressee (email, _to) values (481, 183);
insert into addressee (email, _to) values (482, 5);
insert into addressee (email, _to) values (482, 100);
insert into addressee (email, _to) values (482, 163);
insert into addressee (email, _to) values (483, 154);
insert into addressee (email, _to) values (484, 44);
insert into addressee (email, _to) values (484, 6);
insert into addressee (email, _to) values (484, 13);
insert into addressee (email, _to) values (485, 112);
insert into addressee (email, _to) values (486, 103);
insert into addressee (email, _to) values (486, 6);
insert into addressee (email, _to) values (486, 133);
insert into addressee (email, _to) values (487, 63);
insert into addressee (email, _to) values (487, 189);
insert into addressee (email, _to) values (488, 99);
insert into addressee (email, _to) values (488, 164);
insert into addressee (email, _to) values (489, 193);
insert into addressee (email, _to) values (489, 142);
insert into addressee (email, _to) values (489, 103);
insert into addressee (email, _to) values (490, 197);
insert into addressee (email, _to) values (491, 103);
insert into addressee (email, _to) values (491, 75);
insert into addressee (email, _to) values (491, 7);
insert into addressee (email, _to) values (492, 97);
insert into addressee (email, _to) values (493, 57);
insert into addressee (email, _to) values (494, 9);
insert into addressee (email, _to) values (494, 103);
insert into addressee (email, _to) values (494, 174);
insert into addressee (email, _to) values (495, 142);
insert into addressee (email, _to) values (495, 185);
insert into addressee (email, _to) values (495, 105);
insert into addressee (email, _to) values (496, 182);
insert into addressee (email, _to) values (496, 70);
insert into addressee (email, _to) values (497, 148);
insert into addressee (email, _to) values (497, 142);
insert into addressee (email, _to) values (498, 34);
insert into addressee (email, _to) values (499, 80);
insert into addressee (email, _to) values (500, 94);
insert into addressee (email, _to) values (500, 89);


------------------------------------------------------------


-- 3 a) Format Address
-- Given an address id, return its string representation in the format “username@domain”
create function FormatAddress(identifier int)
    RETURNS varchar(40) AS $$
select CONCAT(username,'@',domain) from address where adid=identifier;
$$ LANGUAGE sql ;

-- 3 b)Format User: Given an euser id, return the name of the user
-- depending on which type the euser belongs to
-- if euser is a person, it returns both names and nickname in brackets
-- else if euser is an organization it returns firm name and nickname in brackets
-- else it returns nickname without brackets

CREATE FUNCTION FormatUser( identifier int ) RETURNS varchar(40) AS $$
DECLARE qty int ; results varchar(40);
BEGIN SELECT euid INTO qty FROM person h WHERE  h.euid=identifier ;
if qty is not null then  select CONCAT(per.first_name,' ',per.surname,'(',per.nickname,')')
                         into results  from person per where per.euid=identifier ;
return results;
end if;
if qty is null then SELECT euid INTO qty FROM organization o WHERE  o.euid=identifier;
end if;
 if qty is not null then  select CONCAT(o.firm,' ','(',nickname,')')into results
                          from organization o where o.euid=identifier;

else  select nickname into results from euser eu where eu.euid=identifier;
end if;
RETURN results;
END $$ LANGUAGE plpgsql ;


-- 3c) a table function which computes an address book for a specific user.
    -- The function takes a euser id as input parameter and
    -- return the computed address book as a table with usernames and email addresses as columns
    create or replace function AddressBook(euserid int)
 RETURNS TABLE(username varchar ,addresse text )LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (select ads.username,CONCAT(ads.username,'@',ads.domain)
                             from use u,address ads where u.euser=euserid and u.address=ads.adid

	    union
	select ads5.username,CONCAT(ads5.username,'@',ads5.domain)
                             from address ads4,email em3, use u3, address ads5, addressee a1
	   where u3.euser= euserid and u3.address=ads4.adid and
	   em3._from=ads4.adid  and a1.email= em3.id and ads5.adid=a1._to
	union
		select ads3.username,CONCAT(ads3.username,'@',ads3.domain)
                             from use u2,address ads2,addressee addse1,email em1,address ads3
		where u2.euser=euserid and u2.address=ads2.adid and ads2.adid=addse1._to
		and addse1.email= em1.id and em1._from=ads3.adid
    );
END $func$ ;


-- 4: Email Attachment Search
-- exercise 4: Parameter are nickname, startdate , EndDate and keyword
-- this function will be called by TableToSearchIn function
-- As you use you have to call SearchKeyWord function with
-- Nickname startdate (like 2012-01-10), EndDate and Keyword
--Example: select * from EmailAttachment('fool875','2009-05-17','2012-01-10','or')
create or replace function NicknameInInterval(Nickn varchar, StartDate timestamp,EndDate timestamp)
 RETURNS TABLE(id int,
			   nickname varchar,
			   filename varchar,
			   subject varchar ,
			   content varchar,
			   addresse text )LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (select  em.id,eus.nickname,em.filename,em.subject,em.content, CONCAT(ads.username,'@',ads.domain) as addresse
	from email em, address ads, euser eus, use u where
		em.date>=StartDate and em.date<=EndDate
		and em._from=ads.adid and ads.adid=u.address
	and u.euser=eus.euid and eus.nickname ilike Nickn

	 union

	  select em2.id,eus2.nickname,em2.filename,em2.subject,em2.content,CONCAT(add2.username,'@',add2.domain) as addresse
	   from euser eus2, use u2,address add2,addressee adsse,email em2   where eus2.nickname ilike Nickn and eus2.euid=u2.euser
	           and add2.adid=u2.address and
	           adsse._to=add2.adid and adsse.email=em2.id and em2.date>=StartDate and em2.date<=EndDate );

END $func$ ;

-- This function will be called by SearchKeyWord
create or replace function TableToSearchIn(Nickn varchar, StartDate timestamp,
										   EndDate timestamp )
 RETURNS TABLE(id int,
			   nickname varchar,
			   addresse text,
			   Efilename varchar,
			   Afilename varchar,
			   Esubject varchar ,
			   Asubject varchar,
			   content varchar )LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (-- emails and picture
    select nick.id,nick.nickname,nick.addresse,nick.filename,pic.filename ,
                   nick.subject,pic.subject,nick.content
                       from picture pic right join NicknameInInterval(Nickn,StartDate,EndDate) nick
 on nick.id=pic.contained_in
 union
-- join with video and email
select nickInt.id,nickInt.nickname,nickInt.addresse,nickInt.filename,vid.filename,
                   nickInt.subject,vid.subject,nickInt.content
                       from video vid right join NicknameInInterval(Nickn,StartDate,EndDate) nickInt
 on nickInt.id=vid.contained_in
 union
-- join with text
select nickIn.id,nickIn.nickname,nickIn.addresse,nickIn.filename,tex.filename,
                   nickIn.subject,tex.subject,nickIn.content
                       from text tex right join  NicknameInInterval(Nickn,StartDate,EndDate) nickIn
 on nickIn.id=tex.contained_in
 union
-- join with attachment
select nickI.id,nickI.nickname,nickI.addresse,nickI.filename,att.filename,
                   nickI.subject,att.subject,nickI.content
                       from attachment att right join NicknameInInterval(Nickn,StartDate,EndDate) nickI
 on nickI.id=att.contained_in) ;
END $func$ ;

-- this is the overall function which calls all other functions
-- then it returns the tuples, which contain the sepecified keyword

create or replace function EmailAttachment(Nickn varchar, StartDate timestamp,
										 EndDate timestamp,KeyWord varchar)
 RETURNS TABLE(id int,
			   addresse text,
			   efilename varchar,
			   esubject varchar)LANGUAGE plpgsql AS
$func$
BEGIN
RETURN QUERY -- my addresse--
    (select res.id,res.addresse,res.efilename,res.esubject from TableToSearchIn(Nickn,StartDate,EndDate)res  where
       res.esubject ilike concat('%', KeyWord, '%') or
		res.content ilike concat('%', KeyWord, '%') or
		res.efilename ilike concat('%', KeyWord, '%') or
		res.afilename ilike concat('%', KeyWord, '%') or
		res.asubject ilike concat('%', KeyWord, '%')
    );
END $func$ ;





----------------------------------- WORKSHEET 2 -------------------------------------------


CREATE TABLE traders (
	t_id		integer PRIMARY KEY NOT NULL,
	t_name		varchar(25),
	t_balance 	integer NOT NULL
);

CREATE TABLE market (
	m_time		integer	NOT NULL,
	m_stock		varchar(20) PRIMARY KEY NOT NULL,
	m_stocktype	char(10),
	m_price		integer	
);

CREATE TABLE depots (
	d_trader 	integer NOT NULL REFERENCES traders(t_id), 
	d_stock 	varchar(20) NOT NULL REFERENCES market(m_stock),
	d_amount	integer NOT NULL,
    PRIMARY KEY (d_trader, d_stock)
);

CREATE TABLE orders (
	o_id		INTEGER PRIMARY KEY NOT NULL GENERATED BY DEFAULT AS IDENTITY,
	o_type		char(4) NOT NULL CHECK (o_type IN ('PUT', 'CALL')),
	o_trader	integer NOT NULL REFERENCES traders(t_id),
	o_amount	integer NOT NULL,
	o_stock		varchar(20) NOT NULL REFERENCES market(m_stock),
	o_limit		integer NOT NULL 
);


-- Sellers
insert into traders (t_id, t_name, t_balance) values (0, 'TRADER0',1000);
insert into traders (t_id, t_name, t_balance) values (1, 'TRADER1',2000);
insert into traders (t_id, t_name, t_balance) values (2, 'TRADER2',3000);
insert into traders (t_id, t_name, t_balance) values (3, 'TRADER3',4000);
insert into traders (t_id, t_name, t_balance) values (4, 'TRADER4',5000);

-- Buyers
insert into traders (t_id, t_name, t_balance) values (5, 'TRADER5',1000000);
insert into traders (t_id, t_name, t_balance) values (6, 'TRADER6',2000000);
insert into traders (t_id, t_name, t_balance) values (7, 'TRADER7',3000000);
insert into traders (t_id, t_name, t_balance) values (8, 'TRADER8',4000000);
insert into traders (t_id, t_name, t_balance) values (9, 'TRADER9',5000000);

-------------------------------------------------------------------------------
-- P1 (Oracle): Exactly one limit (36) yields highest trading volume (700)
-- New price: 36
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Oracle', 'BLUE CHIP', 34);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Oracle', 300);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Oracle', 200);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Oracle', 100);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Oracle', 120);
insert into depots (d_trader, d_stock, d_amount) values (4, 'Oracle', 100);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 250, 'Oracle', 33);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 150, 'Oracle', 33);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 80,  'Oracle', 34);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 120, 'Oracle', 34);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 4, 100, 'Oracle', 36);
insert into depots (d_trader, d_stock, d_amount) values (5, 'Oracle', 5);
insert into depots (d_trader, d_stock, d_amount) values (6, 'Oracle', 10);
insert into depots (d_trader, d_stock, d_amount) values (7, 'Oracle', 15);
insert into depots (d_trader, d_stock, d_amount) values (8, 'Oracle', 20);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 100, 'Oracle', 36);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 200, 'Oracle', 36);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 90,  'Oracle', 37);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 8, 110, 'Oracle', 37);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 9, 200, 'Oracle', 39);

-------------------------------------------------------------------------------
-- P2 (IBM): Several limits (628, 629) yield the highest trading volume (500) and there is a call backlog (100)
-- New price: 629
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'IBM', 'PENNY', 631);
insert into depots (d_trader, d_stock, d_amount) values (0, 'IBM', 210);
insert into depots (d_trader, d_stock, d_amount) values (1, 'IBM', 310);
insert into depots (d_trader, d_stock, d_amount) values (2, 'IBM', 310);
insert into depots (d_trader, d_stock, d_amount) values (3, 'IBM', 310);
insert into depots (d_trader, d_stock, d_amount) values (4, 'IBM', 400);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 100, 'IBM', 625);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 100, 'IBM', 625);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 180, 'IBM', 628);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 120, 'IBM', 628);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 4, 200, 'IBM', 632);
insert into depots (d_trader, d_stock, d_amount) values (5, 'IBM', 5);
insert into depots (d_trader, d_stock, d_amount) values (6, 'IBM', 10);
insert into depots (d_trader, d_stock, d_amount) values (7, 'IBM', 15);
insert into depots (d_trader, d_stock, d_amount) values (8, 'IBM', 20);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 50,  'IBM', 629);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 150, 'IBM', 629);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 230, 'IBM', 630);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 8, 170, 'IBM', 630);

-------------------------------------------------------------------------------
-- P3 (HP): Several limits (15, 16) yield the highest trading volume (500) and there is a put backlog (100)
-- New price: 15
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'HP', 'PENNY', 13);
insert into depots (d_trader, d_stock, d_amount) values (0, 'HP', 105);
insert into depots (d_trader, d_stock, d_amount) values (1, 'HP', 110);
insert into depots (d_trader, d_stock, d_amount) values (2, 'HP', 115);
insert into depots (d_trader, d_stock, d_amount) values (3, 'HP', 300);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 100, 'HP', 13);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 100, 'HP', 13);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 100, 'HP', 15);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 300, 'HP', 15);
insert into depots (d_trader, d_stock, d_amount) values (5, 'HP', 5);
insert into depots (d_trader, d_stock, d_amount) values (6, 'HP', 10);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 120, 'HP', 16);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 80,  'HP', 16);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 140, 'HP', 17);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 8, 160, 'HP', 17);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 9, 150, 'HP', 12);

-------------------------------------------------------------------------------
-- P4 (Google, Microsoft, Amazon): Several limits yield highest trading volume and there are both call and put backlogs
-- Google: Market price above - New price: 872
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Google', 'VALUE', 873);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Google', 55);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Google', 60);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Google', 20);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Google', 100);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 50, 'Google', 869);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 50, 'Google', 869);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 20, 'Google', 871);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 80, 'Google', 871);
insert into depots (d_trader, d_stock, d_amount) values (5, 'Google', 5);
insert into depots (d_trader, d_stock, d_amount) values (6, 'Google', 10);
insert into depots (d_trader, d_stock, d_amount) values (7, 'Google', 15);
insert into depots (d_trader, d_stock, d_amount) values (8, 'Google', 25);
insert into depots (d_trader, d_stock, d_amount) values (9, 'Google', 150);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 10, 'Google', 870);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 90, 'Google', 870);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 40, 'Google', 872);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 8, 60, 'Google', 872);

-- Microsoft: Market price in-between - New price: 34
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Microsoft', 'INCOME', 34);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Microsoft', 50);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Microsoft', 70);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Microsoft', 50);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Microsoft', 70);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 50, 'Microsoft', 32);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 50, 'Microsoft', 32);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 50, 'Microsoft', 34);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 50, 'Microsoft', 34);
insert into depots (d_trader, d_stock, d_amount) values (5, 'Microsoft', 10);
insert into depots (d_trader, d_stock, d_amount) values (6, 'Microsoft', 20);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 80,  'Microsoft', 33);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 20,  'Microsoft', 33);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 100, 'Microsoft', 35);

-- Amazon: Market price below - New price: 262
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Amazon', 'VALUE', 261);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Amazon', 170);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Amazon', 50);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Amazon', 150);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Amazon', 70);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 150, 'Amazon', 262);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 50,  'Amazon', 262);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 150, 'Amazon', 265);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 50,  'Amazon', 265);
insert into depots (d_trader, d_stock, d_amount) values (5, 'Amazon', 20);
insert into depots (d_trader, d_stock, d_amount) values (6, 'Amazon', 30);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 100, 'Amazon', 264);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 100, 'Amazon', 264);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 200, 'Amazon', 267);

-------------------------------------------------------------------------------
-- P5 (Apple, Facebook, Cisco): Several limits yield highest trading volume without backlogs
-- Apple: Market price above - New price: 443
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Apple', 'BLUE CHIP', 445);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Apple', 100);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Apple', 110);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Apple', 160);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Apple', 200);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 100, 'Apple', 441);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 100, 'Apple', 441);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 140, 'Apple', 442);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 160, 'Apple', 442);
insert into depots (d_trader, d_stock, d_amount) values (5, 'Apple', 50);
insert into depots (d_trader, d_stock, d_amount) values (6, 'Apple', 60);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 100, 'Apple', 443);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 100, 'Apple', 443);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 150, 'Apple', 445);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 8, 150, 'Apple', 445);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 9, 400, 'Apple', 440);

-- Facebook: Market price in-between - New price: 24
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Facebook', 'INCOME', 24);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Facebook', 130);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Facebook', 140);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Facebook', 300);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Facebook', 150);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 130, 'Facebook', 22);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 70,  'Facebook', 22);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 300, 'Facebook', 23);
insert into depots (d_trader, d_stock, d_amount) values (9, 'Facebook', 500);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 150, 'Facebook', 25);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 50,  'Facebook', 25);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 170, 'Facebook', 26);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 8, 130, 'Facebook', 26);

-- Cisco: Market price below - New price: 25
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Cisco', 'PENNY', 23);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Cisco', 100);
insert into depots (d_trader, d_stock, d_amount) values (1, 'Cisco', 100);
insert into depots (d_trader, d_stock, d_amount) values (2, 'Cisco', 160);
insert into depots (d_trader, d_stock, d_amount) values (3, 'Cisco', 140);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 100, 'Cisco', 22);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 1, 100, 'Cisco', 22);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 2, 160, 'Cisco', 25);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 3, 140, 'Cisco', 25);
insert into depots (d_trader, d_stock, d_amount) values (5, 'Cisco', 5);
insert into depots (d_trader, d_stock, d_amount) values (6, 'Cisco', 10);
insert into depots (d_trader, d_stock, d_amount) values (7, 'Cisco', 15);
insert into depots (d_trader, d_stock, d_amount) values (8, 'Cisco', 20);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 50,  'Cisco', 26);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 6, 150, 'Cisco', 26);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 7, 300, 'Cisco', 27);

-------------------------------------------------------------------------------
-- P6 (SAP, Intel, Yahoo!): There are both put and call orders but no trades are possible
-- SAP: Market price above - New price: 53
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'SAP', 'INCOME', 54);
insert into depots (d_trader, d_stock, d_amount) values (0, 'SAP', 210);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 200, 'SAP', 53);
insert into depots (d_trader, d_stock, d_amount) values (5, 'SAP', 5);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 100, 'SAP', 52);

-- Intel: Market price in-between - New price: 23
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Intel', 'BLUE CHIP', 23);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Intel', 200);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 200, 'Intel', 25);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 200, 'Intel', 22);

-- Yahoo!: Market price below - New price: 25
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Yahoo!', 'PENNY', 26);
insert into depots (d_trader, d_stock, d_amount) values (0, 'Yahoo!', 200);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 200, 'Yahoo!', 25);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 5, 200, 'Yahoo!', 23);

-------------------------------------------------------------------------------
-- P7 (AMD, NVIDIA): There are only call or put orders
-- AMD: Only call orders - New price: 171
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'AMD', 'INCOME', 171);
insert into depots (d_trader, d_stock, d_amount) values (0, 'AMD', 50);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 0, 100, 'AMD', 172);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('CALL', 1, 100, 'AMD', 173);

-- NVIDIA: Only put orders - New price: 14
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'NVIDIA', 'PENNY', 14);
insert into depots (d_trader, d_stock, d_amount) values (0, 'NVIDIA', 210);
insert into orders (o_type, o_trader, o_amount, o_stock, o_limit) values ('PUT', 0, 200, 'NVIDIA', 15);

-- Stock with no orders
insert into market (m_time, m_stock, m_stocktype, m_price) values (0, 'Dell', 'PENNY', 35);


-------------------------------------------- Solution --------------------------------------------

---------------------------------
---- ACCOUNT VIEW
---------------------
create view account_v as select o.o_trader as trader,
                    o.o_stock as stock, (select distinct d.d_amount from depots d where
        concat(d.d_trader,d.d_stock)= concat(o.o_trader,o.o_stock)) as AmountInDepots,
                    o.o_amount as call_amount,
                    o.o_limit as call_limit,
                    null as put_amount,
                    null as put_limit,m.m_price,
                    m.m_price*o.o_amount as marketvalue
                    from orders o,market m where o.o_type='CALL' and o.o_stock=m.m_stock

        union
                    select o.o_trader as trader,
                           o.o_stock as stock,(select distinct d.d_amount from depots d where
                                 concat(d.d_trader,d.d_stock)= concat(o.o_trader,o.o_stock)) as AmountInDepots,
                          null as call_amount,
                          null as call_limit,
                          o.o_amount as put_amount,
                          o.o_limit as put_limit,m.m_price,
                          m.m_price*o.o_amount as marketvalue
                         from orders o, market m where o.o_type='PUT' and o.o_stock=m.m_stock;


-------------------------------------------------
-- Function which helps to create orders view
--------------------------------------------

CREATE or replace FUNCTION ordervfinal()
RETURNS TABLE (stock varchar,callVolume int,
			   callBacklog int,limits int,putBacklog int,putVolume int)
    LANGUAGE plpgsql
AS
$$
DECLARE
--TABLE_RECORD RECORD;
alimit int;
	stockname varchar;
	callvolumei int:=0 ;
	callBacklogi int:=0;
	putBacklogi int :=0;
	putvolumei int :=0;
BEGIN
create table templateorder(stock varchar,callVolume int,
                           callBacklog int,limits int,putBacklog int,putVolume int);
FOR stockname in select distinct o_stock from orders
                                                  LOOP
    FOR alimit IN SELECT distinct(tr.o_limit) FROM orders tr where tr.o_stock ilike stockname order by tr.o_limit asc
                     LOOP
select sum(tcall.o_amount) into callvolumei from orders tcall where tcall.o_type='CALL'
                                                                and tcall.o_limit>=alimit and o_stock ilike stockname;
if callvolumei is null then callvolumei=0; end if;
select sum(tput.o_amount) into putvolumei from orders tput where tput.o_type='PUT'
                                                             and tput.o_limit<=alimit and o_stock ilike stockname;
if putvolumei is null then putvolumei=0; end if;
if callvolumei-putvolumei>=0 then callBacklogi=callvolumei-putvolumei;putBacklogi=0;
		elseif putvolumei-callvolumei>=0then putBacklogi=putvolumei-callvolumei; callBacklogi=0;
end if;
insert into templateorder(stock, callVolume,
                          callBacklog ,limits ,putBacklog,putVolume)
values(stockname,callvolumei,callBacklogi,alimit,putBacklogi,putvolumei);
END LOOP;
END LOOP;

RETURN QUERY select * from templateorder; drop table templateorder;
END
$$;
-----------------------------------------
    --- ORDERS VIEW
-----------------------------------------
create view order_v as(select * from ordervfinal());

-------------------------------------------





/*CREATE or replace FUNCTION order_v(stockname varchar)
RETURNS TABLE (stock varchar,callVolume int,
			   callBacklog int,limit_ int,putBacklog int,putVolume int)
    LANGUAGE plpgsql
AS
$$
DECLARE
TABLE_RECORD RECORD;
    alimit int;
	callvolumei int ;
	callBacklogi int:=0;
	putBacklogi int :=0;
	putvolumei int;
BEGIN
create table templateTable(stock varchar,callVolume int,
                           callBacklog int,limit_ int,putBacklog int,putVolume int);

FOR alimit IN SELECT distinct(tr.o_limit) FROM orders tr where tr.o_stock ilike stockname
        LOOP
select sum(tcall.o_amount) into callvolumei from orders tcall where tcall.o_type='CALL'
                                                                and tcall.o_limit>=alimit;
select sum(tput.o_amount) into putvolumei from orders tput where tput.o_type='PUT'
                                                             and tput.o_limit<=alimit;
if callvolumei-putvolumei>=0 then callBacklogi=callvolumei-putvolumei;putBacklogi=0;
		elseif putvolumei-callvolumei>=0then putBacklogi=putvolumei-callvolumei; callBacklogi=0;
end if;
insert into templateTable(stock, callVolume,
                          callBacklog ,limit_ ,putBacklog,putVolume)
values(stockname,callvolumei,callBacklogi,alimit,putBacklogi,putvolumei);
END LOOP;

RETURN QUERY select * from templateTable; drop table templateTable;
END
$$;

 */




--Exerise 2
--Insert Trigger--

------------------------------------------
CREATE OR REPLACE FUNCTION fn_account_v_insert() RETURNS trigger AS $trigger_bound$
DECLARE
depot_val INT;
acc_bal INT;
pending_order INT;
order_count INT;
BEGIN

    -- Check that Seller has enough stock amount in his depot
SELECT d_amount into depot_val from depots
where d_trader = NEW.trader and d_stock = NEW.stock;

IF NEW.put_amount > depot_val or (NEW.put_amount > 0 and depot_val IS NULL)  THEN
        RAISE EXCEPTION SQLSTATE '70001';
END IF;

	-- Check that Buyer has adequate account balance
SELECT t_balance into acc_bal from traders where t_id = NEW.trader;
SELECT sum(o_amount * o_limit) into pending_order from orders
where o_trader = NEW.trader and o_type = 'CALL';
IF pending_order is NULL THEN
    pending_order := 0;
END IF;
IF (NEW.call_limit * NEW.call_amount) > (acc_bal - pending_order) THEN
        RAISE SQLSTATE '70002';
END IF;

    -- Check if there is any order for the same stock
SELECT count(*) into order_count from orders
where o_trader = NEW.trader and o_stock = NEW.stock;

IF order_count > 0 THEN
        RAISE SQLSTATE '70003';
END IF;

    -- Check if it is not an empty order
    IF (NEW.call_amount < 0 or NEW.call_limit < 0
	or NEW.put_amount < 0 or NEW.put_limit < 0)
	or
    ((NEW.call_amount = 0 or NEW.call_amount IS NULL) and
    (NEW.call_limit = 0 or NEW.call_limit IS NULL) and
    (NEW.put_amount = 0 or NEW.put_amount IS NULL) and
    (NEW.put_limit = 0 or NEW.put_limit IS NULL))
	THEN
        RAISE SQLSTATE '70004';
END IF;

	-- Check if it is both call and put in one order
	IF NEW.put_amount + NEW.put_limit > 0 and
	NEW.call_amount + New.call_limit > 0 THEN
		RAISE SQLSTATE '70005';
END IF;

	-- Check if amountindepots, m_price, marketvalue are null or omited
	IF NEW.amountindepots IS NOT NULL OR
	NEW.m_price IS NOT NULL or NEW.marketvalue IS NOT NULL THEN
		RAISE SQLSTATE '70006';
END IF;

	--Insert Order
 	IF NEW.put_amount > 0 THEN
 		INSERT INTO orders (o_type, o_trader, o_amount, o_stock, o_limit)
 		VALUES('PUT', NEW."trader", NEW."put_amount", NEW."stock", NEW."put_limit");
 	ELSEIF NEW.call_amount > 0 THEN
 		INSERT INTO orders (o_type, o_trader, o_amount, o_stock, o_limit)
 		VALUES('CALL', NEW."trader", NEW."call_amount", NEW."stock", NEW."call_limit");
END IF;
RETURN NEW;
END;


$trigger_bound$
LANGUAGE plpgsql;

CREATE TRIGGER account_v_insert
    INSTEAD OF INSERT ON account_v FOR EACH ROW
    EXECUTE PROCEDURE fn_account_v_insert();

------------------------------------------

-- Delete Trigger --
------------------------------------------
CREATE OR REPLACE FUNCTION fn_account_v_delete() RETURNS trigger AS $trigger_bound$
DECLARE
order_type char(4);
del_amount int;
del_limit int;
BEGIN
	-- Check if the order is put or call
	IF OLD.put_amount > 0 and OLD.put_limit > 0 THEN
		order_type := 'PUT';
		del_amount := OLD.put_amount;
		del_limit := OLD.put_limit;
	ELSEIF OLD.call_amount > 0 and OLD.call_limit > 0 THEN
		order_type := 'CALL';
		del_amount := OLD.call_amount;
		del_limit := OLD.call_limit;
END IF;

	-- Delete the Row
DELETE FROM orders where o_type = order_type and o_trader = OLD.trader
                     and o_amount = del_amount and o_stock = OLD.stock and o_limit = del_limit;

RETURN NEW;
END;


$trigger_bound$
LANGUAGE plpgsql;

CREATE TRIGGER account_v_delete
    INSTEAD OF DELETE ON account_v FOR EACH ROW
    EXECUTE PROCEDURE fn_account_v_delete();

---------------------------------------------
--------------------------------------------------------------------------------------
-----EXERCISE 3 : PRICE CALCULATION
---- use this procedure like this: call calculate_prices('foo'), where foo is stockname
-----------------------------------------------------------------------------

create or replace function calculate_prices()
RETURNS TABLE (Stock varchar, Price int)
language plpgsql
as $$
declare
-- variable declaration
goodlimit int;
currentmarketprice int;
countslimit int;
lowestlimit int;
stockname varchar;
highestlimit int;
timer int;
begin
For stockname in select m_stock from market
                                         LOOP
drop table if exists templateTable;
drop table if exists records;
select	m_price into currentmarketprice from market where m_stock ilike stockname;
select m_time into timer from market where m_stock ilike stockname;
-- In this table we keep the orders of the current stock
create  table templateTable as select * from orders_v ordv where ordv.stock ilike stockname;
-- limits of highest trading volume: it creates the records for that
create table records as select limits,callbacklog,putbacklog,callvolume,putvolume from templateTable tmp where
            tmp.callvolume+tmp.putvolume >= all (select tmp2.callvolume+tmp2.putvolume from templateTable tmp2);

-- Here we implement P1 rule
select count(*) into countslimit from records;
if countslimit = 1 and exists (select * from records where putvolume >0 and callvolume >0)then
           select limits into goodlimit from records limit 1;
           update market set m_price=goodlimit,m_time=timer+1 where m_stock ilike stockname;
           drop table records;
           drop table templateTable;
-- Here we  implement p4 and p5 rules
elseif	countslimit >1 and (not exists (select * from records where callbacklog >0 or putbacklog >0) or
							(exists (select * from records where callbacklog >0) and exists (select * from records where putbacklog >0)))
							and exists (select * from records where putvolume >0 and callvolume >0)then

          select MIN(limits) into lowestlimit from records;
          select  Max(limits) into highestlimit from records;
          if currentmarketprice <= lowestlimit then
                         update market set m_price=lowestlimit,m_time=timer+1 where m_stock ilike stockname;
                         drop table records;
                         drop table templateTable;
          elseif currentmarketprice>=highestlimit then
                            update market set m_price=highestlimit,m_time= timer+1 where m_stock ilike stockname;
                            drop table records;
                            drop table templateTable;
          else
                  update market set m_time=timer+1 where m_stock ilike stockname;
                  drop table records;
                  drop table templateTable;
                  end if;
	--Here We implement p2 rules
 elseif countslimit>1 and exists (select * from records where callbacklog>0) and not exists (select * from records where putbacklog>0)
                               and exists (select * from records where putvolume >0 and callvolume >0)then
                    select MAX(limits) into highestlimit from records;
                    update market set m_price=highestlimit,m_time=timer+1 where m_stock ilike stockname;
                    drop table records;
                    drop table templateTable;
--Here We implement  p3 rule
elseif	countslimit>1 and exists (select * from records where putbacklog>0) and not exists (select * from records where callbacklog>0)
                           and exists (select * from records where putvolume >0 and callvolume >0)then
                       select MIN(limits) into lowestlimit from records;

                      update  market set m_price=lowestlimit,m_time= timer+1 where m_stock ilike stockname;
                      drop table records;drop table templateTable;-- end if;


--else p7: and p6 the part contains other parts (like the case of SAP)
elseif countslimit = 1 and not exists (select * from records where putvolume >0 and callvolume >0) then
               if  exists (select * from templateTable where callvolume >0 ) and exists (select * from templateTable where putvolume >0 )then
                         select MIN(limits) into lowestlimit from records;
                         select  Max(limits) into highestlimit from records;
                  if currentmarketprice>=highestlimit then
                       update market set m_price=highestlimit,m_time= timer+1 where m_stock ilike stockname;
                       drop table records;
                       drop table templateTable;
                  elseif currentmarketprice<=lowestlimit then
                        update market set m_price=lowestlimit,m_time= timer+1 where m_stock ilike stockname;
                        drop table records;
                        drop table templateTable;
                  else
                      update market set m_time= timer+1 where m_stock ilike stockname;
                      drop table records;
                    drop table templateTable; end if;
              --else p7
              else
                   update market set m_time= timer where m_stock ilike stockname;
                   drop table records;
                   drop table templateTable; end if;


--p6 normal parts like the case of Yahoo!
elseif countslimit >1 and not exists (select * from records where putvolume >0 and callvolume >0) then

                 select MIN(limits) into lowestlimit from records;
                 select  Max(limits) into highestlimit from records;
                 if currentmarketprice>=highestlimit then
                          update market set m_price=highestlimit,m_time= timer+1 where m_stock ilike stockname;
                          drop table records;
                          drop table templateTable;
                 elseif currentmarketprice<= lowestlimit then
                         update market set m_price=lowestlimit, m_time=timer+1 where m_stock ilike stockname;
                         drop table records;
                         drop table templateTable;
                else update market set m_time=timer+1 where m_stock ilike stockname;
                     drop table records;
                     drop table templateTable; end if;

--- unreachable part
else
       update market set m_time= timer where m_stock ilike stockname;
       drop table records;drop table templateTable;
       end if;
END LOOP;
RETURN QUERY select m_stock,m_price  from market order by m_stock collate "C";

end; $$;

---------------------------------------------------------------------------------
------------------------------EXERCISE 4-----------------------------------------
---------------------------------------------------------------------------------
create or replace function trade()
returns table (T_name varchar,T_Balance int)
    LANGUAGE plpgsql
AS
$$
DECLARE
TABLE_RECORD RECORD;
	backlogrecord RECORD;
	precords RECORD;
	callsrecords RECORD;
	putsrecords RECORD;
    stockname varchar;
	moneygot int:=0;
	moneygave int:=0;
	price int:=0;
	putremains int:=0;
	callremains int:=0;
	currentbalance int:=0;
	putvolume int:=0 ;
	callvolume int:=0 ;
BEGIN

FOR stockname in select m_stock from market
    LOOP

select m_price into price from market where m_stock ilike stockname;
-- to hold traders who can buy or sell on current market price
create table currentstockbuysell as select * from orders where o_stock ilike stockname and
                                                        ((o_type='PUT' and o_limit<=price)or
													   (o_type='CALL' and o_limit>=price))order by o_id ASC;
select sum(o_amount) into callvolume from currentstockbuysell where o_type='CALL';
select sum(o_amount) into putvolume from currentstockbuysell where o_type='PUT';
-- all puts can be sold and all calls can bought
if putvolume=callvolume then

     for TABLE_RECORD in select * from currentstockbuysell
         LOOP


    if TABLE_RECORD.o_type='CALL' then
          select tr1.t_balance into currentbalance from traders tr1 where tr1.t_id=TABLE_RECORD.o_trader;
          moneygave= price * TABLE_RECORD.o_amount;
         update traders tr set t_balance=currentbalance-moneygave where tr.t_id=TABLE_RECORD.o_trader;
          --UPDATE OF ORDERS BOOK
          delete from orders ord where ord.o_id=TABLE_RECORD.o_id;
    elseif TABLE_RECORD.o_type='PUT' then
          select tr1.t_balance into currentbalance from traders tr1 where tr1.t_id=TABLE_RECORD.o_trader;
          moneygot=price * TABLE_RECORD.o_amount;
          update traders tr set t_balance=currentbalance+moneygot  where tr.t_id=TABLE_RECORD.o_trader ;
          --UPDATE OF ORDERS BOOK
         delete from orders ord where ord.o_id=TABLE_RECORD.o_id;
    end if;
    END LOOP;
-- if call> put then all puts are served.
elseif	putvolume<callvolume	then

       create table onlyputs as select * from currentstockbuysell curs where curs.o_type='PUT';
       create table onlycalls as select * from currentstockbuysell curb where curb.o_type='CALL' order by o_id ASC;
       for precords in select * from onlyputs
               LOOP
               select tr.t_balance into currentbalance from traders tr where tr.t_id=precords.o_trader;
               moneygot=price * precords.o_amount;
               update traders set t_balance=currentbalance+moneygot  where t_id=precords.o_trader;
               --UPDATE OF ORDERS BOOK.
               delete from orders ors where  ors.o_id=precords.o_id;
               end loop;drop table if exists onlyputs;
       for backlogrecord in select * from onlycalls order by o_id asc
             loop
		   if backlogrecord.o_amount<=putvolume then
		         putremains=putvolume-backlogrecord.o_amount;
                 putvolume=putremains;
                 select tr.t_balance into currentbalance from traders tr where tr.t_id=backlogrecord.o_trader;
                 moneygave=price * backlogrecord.o_amount;
                update traders set t_balance=currentbalance-moneygave  where t_id=backlogrecord.o_trader;
                --UPDATE OF ORDERS BOOK
                delete from orders ord where ord.o_id=backlogrecord.o_id;

           elseif backlogrecord.o_amount> putvolume and putvolume >0 then
                 update orders ord set o_amount=backlogrecord.o_amount-putvolume where ord.o_id=backlogrecord.o_id;
                 select tr.t_balance into currentbalance from traders tr where tr.t_id=backlogrecord.o_trader;
                 moneygave=price * putvolume;
                 update traders set t_balance=currentbalance- moneygave where t_id=backlogrecord.o_trader;
                 putvolume=0;
           end if;
           end loop;
           drop table if exists onlycalls;
-- if put>call then all calls are served
elseif putvolume>callvolume then

               create table onlyputs2 as select * from currentstockbuysell curs where curs.o_type='PUT' order by o_id ASC;
               create table onlycalls2 as select * from currentstockbuysell curb where curb.o_type='CALL' ;
               for callsrecords in select * from onlycalls2
                          LOOP
                     select tr.t_balance into currentbalance from traders tr where tr.t_id=callsrecords.o_trader;
                     moneygave=price * callsrecords.o_amount;
                     update traders set t_balance=currentbalance-moneygave where t_id=callsrecords.o_trader;
                     --UPDATE OF ORDERS BOOK.
                     delete from orders ors where  ors.o_id=callsrecords.o_id;
                     end loop;drop table if exists onlycalls2;
               for putsrecords in select * from onlyputs2 order by o_id asc
                   loop
		        if putsrecords.o_amount<=callvolume then
		           callremains=callvolume-putsrecords.o_amount;
                   callvolume=callremains;
                   select tr.t_balance into currentbalance from traders tr where tr.t_id=putsrecords.o_trader;
                   moneygot=price * putsrecords.o_amount;
                   update traders set t_balance=currentbalance+moneygot where t_id=putsrecords.o_trader;
                   --UPDATE OF ORDERS BOOK
                   delete from orders ord where ord.o_id=putsrecords.o_id;

                elseif putsrecords.o_amount> callvolume and callvolume >0 then
                   update orders ord set o_amount=putsrecords.o_amount-callvolume where ord.o_id=putsrecords.o_id;
                   select tr.t_balance into currentbalance from traders tr where tr.t_id=putsrecords.o_trader;
                   moneygot=price * callvolume;
                   update traders set t_balance=currentbalance+moneygot  where t_id=putsrecords.o_trader;
                   callvolume=0;
                end if;
                end loop;
                drop table if exists onlyputs2;
end if;
drop table if exists currentstockbuysell;
END LOOP;

RETURN QUERY select ta.t_name,ta.t_balance from traders ta order by ta.t_name ASC;
END
$$;
